package com.buraq24.driver

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.ContentResolver
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.media.AudioAttributes
import android.net.Uri
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.buraq24.driver.ui.home.HomeActivity
import com.buraq24.driver.ui.signup.SignupActivity
import com.buraq24.driver.utils.LocaleHelper
import com.buraq24.driver.utils.getPhoneLang
import com.buraq24.utilities.LocaleManager
import com.buraq24.utilities.SharedPrefs
import com.buraq24.utilities.constants.ACCESS_TOKEN
import com.buraq24.utilities.constants.LANGUAGE_CODE
import com.buraq24.utilities.constants.PROFILE
import com.buraq24.utilities.constants.STEP
import com.buraq24.utilities.webservices.models.AppDetail
import io.fabric.sdk.android.services.settings.IconRequest.build


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val mNotificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val mChannel = NotificationChannel("1", getString(R.string.request_channel_title), NotificationManager.IMPORTANCE_HIGH)
            mChannel.setShowBadge(true)
            mChannel.lightColor = Color.GRAY
            mChannel.enableLights(true)
            mChannel.description = getString(R.string.request_channel_description)
            val attributes = AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                    .build()
            val sound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + applicationContext.packageName + "/" + R.raw.custom_tone)
            mChannel.setSound(sound, attributes)
            mChannel.enableVibration(true)
            // Set the Notification Channel for the Notification Manager.
            mNotificationManager.createNotificationChannel(mChannel)
        }
        Handler().postDelayed({
            val proifle = SharedPrefs.with(this).getObject(PROFILE, AppDetail::class.java)
            if (proifle != null) {

                ACCESS_TOKEN = proifle.access_token ?: ""
                val step = SharedPrefs.with(this).getInt(STEP, 0)
                if (step != 0) {
                    startActivity(Intent(this, SignupActivity::class.java).putExtra("step", step))
                } else {
                    val intentHome = Intent(this, HomeActivity::class.java)
                    if (intent.hasExtra("type")) {
                        intentHome.putExtra("order_id", intent.getStringExtra("order_id"))
                                .putExtra("type", intent.getStringExtra("type"))
                    }
                    startActivity(intentHome)
                }
            } else {
                startActivity(Intent(this, SignupActivity::class.java))
            }
            finishAffinity()
        }, 2000)
    }

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(LocaleManager.setLocale(newBase))
    }
}
