package com.buraq24.driver.ui.home.emergencyContacts

import android.content.Intent
import android.net.Uri
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.buraq24.driver.R
import com.buraq24.driver.webservices.models.EContact
import kotlinx.android.synthetic.main.item_e_contact.view.*

class  EmergencyContactsAdapter(private val response: List<EContact>?) : RecyclerView.Adapter<EmergencyContactsAdapter.ViewHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
            ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_e_contact,parent,
                    false))

    override fun getItemCount(): Int = response?.size?:0

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(response?.get(position))

    }

    inner class ViewHolder(itemView: View?): RecyclerView.ViewHolder(itemView) {
        private var contactData: EContact?=null
        init {
            itemView?.setOnClickListener {
                val phone = contactData?.phoneNumber.toString()
                val intent = Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null))
                it.context?.startActivity(intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP))
            }
        }
        fun bind(data: EContact?) {
            contactData = data
            itemView.tvContactName.text = data?.name
            itemView.tvContactNumber.text = data?.phoneNumber.toString()
            //Glide.with(itemView.context).load(data?.image_url).into(itemView.ivContactImage)
        }
    }

}