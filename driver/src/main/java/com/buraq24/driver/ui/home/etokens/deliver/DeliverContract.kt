package com.buraq24.driver.ui.home.etokens.deliver

import com.buraq24.driver.webservices.models.order.Order
import com.buraq24.utilities.basearc.BasePresenter
import com.buraq24.utilities.basearc.BaseView

class DeliverContract {

    interface View : BaseView {
        fun onApiSuccess(order: Order?)
    }

    interface Presenter : BasePresenter<View> {
        fun deliverOrderApi(map: HashMap<String, String?>)
    }
}