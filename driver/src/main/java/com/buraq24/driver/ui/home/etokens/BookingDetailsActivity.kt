package com.buraq24.driver.ui.home.etokens

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.buraq24.driver.R
import com.buraq24.driver.ui.home.etokens.deliver.DeliverFragment
import com.buraq24.driver.utils.getRatingSymbol
import com.buraq24.driver.webservices.models.order.Order
import com.buraq24.utilities.LocaleManager
import com.buraq24.utilities.constants.ORDER
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_booking_details2.*

class BookingDetailsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_booking_details2)
        setData()
    }

    @SuppressLint("SetTextI18n")
    private fun setData() {
        val order: Order? = Gson().fromJson(intent.getStringExtra(ORDER), Order::class.java)
        tvBrand.text = order?.brand?.brand_name
        tvProduct.text = "${order?.brand?.name} × ${order?.payment?.productQuantity}"
        tvAddress.text = order?.dropoff_address
        val customer = order?.user
        tvName.text = customer?.name
        ivReaction.setImageResource(getRatingSymbol(customer?.rating_avg?.toInt() ?: 0))
        tvRating.text = "· ${customer?.rating_count?.toString()}"
        Glide.with(this)
                .setDefaultRequestOptions(RequestOptions().circleCrop().placeholder(R.drawable.ic_user_black))
                .load(customer?.profile_pic_url)
                .into(ivProfilePic)

        val fragment = DeliverFragment()
        val bundle = Bundle()
        bundle.putString(ORDER, intent.getStringExtra(ORDER))
        fragment.arguments = bundle
        supportFragmentManager.beginTransaction().add(R.id.container, fragment, DeliverFragment::javaClass.name).commit()
    }

    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(LocaleManager.setLocale(newBase))
    }

}
