package com.buraq24.driver.ui.home.bookings

import com.buraq24.driver.webservices.models.RoadItem
import com.buraq24.driver.webservices.models.order.Order
import com.buraq24.utilities.basearc.BasePresenter
import com.buraq24.utilities.basearc.BaseView
import com.buraq24.utilities.webservices.models.Service
import com.google.android.gms.maps.model.LatLng
import org.json.JSONObject

class BookingContract{

    interface View: BaseView{
        fun onApiSuccess(listBookings: ArrayList<Order?>)
        fun onOrderDetailsSuccess(response: Order?)
        fun onScheduleCancelApiSuccess()

    }

    interface Presenter: BasePresenter<View>{
        fun getHistoryList(take: Int, skip: Int, type:Int)
        fun getOrderDetails(orderId: Long)
        fun confirmScheduleRequest(lalLng: LatLng, orderId: Long,reason:String)

    }

}