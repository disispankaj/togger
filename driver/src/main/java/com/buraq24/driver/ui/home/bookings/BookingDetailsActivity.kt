package com.buraq24.driver.ui.home.bookings

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.buraq24.driver.R
import com.buraq24.driver.ui.home.rateCustomer.RateCustomerFragment
import com.buraq24.driver.utils.*
import com.buraq24.driver.webservices.models.homeapi.User
import com.buraq24.driver.webservices.models.order.Order
import com.buraq24.showSnack
import com.buraq24.utilities.*
import com.buraq24.utilities.constants.PREF_CURRENT_LAT
import com.buraq24.utilities.constants.PREF_CURRENT_LNG
import com.google.android.gms.maps.model.LatLng
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_booking_details.*

class BookingDetailsActivity : AppCompatActivity(), BookingContract.View, RateCustomerFragment.OnRateDone {

    override fun onScheduleCancelApiSuccess() {
        Toast.makeText(this, getString(R.string.booking_cancelled), Toast.LENGTH_SHORT).show()
        setResult(Activity.RESULT_OK)
        finish()
    }

    override fun onRated() {
        onResume()
    }

    override fun onApiSuccess(listBookings: ArrayList<Order?>) {

    }

    override fun onOrderDetailsSuccess(response: Order?) {
        order = response
        if (response?.order_status != OrderStatus.CUSTOMER_CANCEL &&
                response?.order_status != OrderStatus.DRIVER_CANCELLED &&
                response?.order_status != OrderStatus.DRIVER_SCHEDULED_CANCELLED) {
            response?.brand?.name = response?.brand?.product_name
            if (response?.future == "1" && response.order_status == OrderStatus.SCHEDULED) {
                llBottomButton.visibility = View.VISIBLE
            }
        }
        setUserData(response?.user)
        setPaymentData(response)
        viewFlipperExtraDetails.displayedChild = 1

    }

    override fun showLoader(isLoading: Boolean) {
        dialogIndeterminate?.show(isLoading)
    }

    override fun apiFailure() {
        viewFlipperExtraDetails.displayedChild = 2
    }

    override fun handleApiError(code: Int?, error: String?) {
        tvCancelStart?.showSnack(error.toString())
    }

    private val presenter: BookingsPresenter = BookingsPresenter()
    private var dialogIndeterminate: DialogIndeterminate? = null
    private var order: Order? = null

    private var orderId: Long? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_booking_details)
        presenter.attachView(this)
        dialogIndeterminate = DialogIndeterminate(this)
        toolbarBookingDetails.setNavigationOnClickListener {
            onBackPressed()
        }
        val orderData = intent?.getParcelableExtra<Order>("data")
        orderId = orderData?.order_id?.toLong()
        setBookingData(orderData)
        presenter.getOrderDetails(orderId ?: 0L)
        tvCancelStart.setOnClickListener {
            if (CheckNetworkConnection.isOnline(this)) {
                val lat = SharedPrefs.with(this).getFloat(PREF_CURRENT_LAT,
                        0.0f)
                val long = SharedPrefs.with(this).getFloat(PREF_CURRENT_LNG, 0.0f)
                val latlng = LatLng(lat.toDouble(), long.toDouble())
                showCancellationDialog(latlng)
            }
        }

        order = orderData

        setListeners()
    }

    private fun setListeners() {
        ivCall.setOnClickListener {
            val phone = order?.user?.phone_number.toString()
            val intent = Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null))
            startActivity(intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP))
        }
    }

    private fun showCancellationDialog(latlng: LatLng) {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(true)
        dialog.setCanceledOnTouchOutside(true)
        dialog.setContentView(R.layout.dialog_cancel_reason)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        val etMessage = dialog.findViewById(R.id.etMessage) as EditText
        val tvSubmit = dialog.findViewById(R.id.tvSubmit) as TextView
        tvSubmit.setOnClickListener {
            if (etMessage.text.toString().trim().isNotEmpty()) {
                val reason = etMessage.text.toString().trim()
                if (CheckNetworkConnection.isOnline(this)) {
                    if (CheckNetworkConnection.isOnline(this)) {
                        presenter.confirmScheduleRequest(latlng, orderId ?: 0, reason)
                    }
                    dialog.dismiss()
                } else {
                    CheckNetworkConnection.showNetworkError(tvCancelStart)
                }
            } else {
                Toast.makeText(this, getString(R.string.cancellation_reason_validation_text), Toast.LENGTH_SHORT).show()
            }
        }

        dialog.show()
        dialog.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
    }


    private fun setPaymentData(order: Order?) {
        val services = resources.getStringArray(R.array.services_list)
        if (order?.ratingByDriver?.ratings != null) {
            ivRating?.visibility = View.VISIBLE
            ivRating.setImageResource(getRatingSymbol(order.ratingByDriver?.ratings))
            if (order.ratingByDriver?.comments.isNullOrEmpty()) {
                tvComments.visibility = View.GONE
            } else {
                tvComments.visibility = View.VISIBLE
                tvComments.text = order.ratingByDriver?.comments
            }
        } else {
            ivRating?.visibility = View.INVISIBLE
//            rlProfile.visibility = View.GONE
            if (order?.order_status == OrderStatus.SERVICE_COMPLETE) {
                val fragment = RateCustomerFragment()
                val bundle = Bundle()
                bundle.putString("order", Gson().toJson(order))
                fragment.arguments = bundle
                supportFragmentManager?.beginTransaction()?.setCustomAnimations(R.anim.slide_in_top,
                        R.anim.slide_in_bottom, R.anim.slide_out_bottom,
                        R.anim.slide_out_top)?.add(android.R.id.content,
                        fragment, RateCustomerFragment::class.java.name)?.addToBackStack(null)
                        ?.commitAllowingStateLoss()
            }

        }
        tvServiceName.text = when {
            order?.category_id == CategoryId.GAS -> {
                tvQuantity.text = "${order.brand?.name} × ${order.payment?.productQuantity}"
                getString(R.string.gas)
            }
            order?.category_id == CategoryId.MINERAL_WATER -> {
                tvQuantity.text = "${order.brand?.name} × ${order.payment?.productQuantity}"
                order.brand?.brand_name
            }
            order?.category_id == CategoryId.WATER_TANKER -> {
                tvQuantity.text = "${order.brand?.name} × ${order.payment?.productQuantity}"
                getString(R.string.water_tanker)
            }
            order?.category_id == CategoryId.FREIGHT -> {
                tvQuantity.text = "${order.brand?.name}"
                order.brand?.brand_name
            }
            else -> order?.brand?.brand_name
        }
        tvPrice.text = "${getFormattedPrice(order?.payment?.initialCharge)} ${getString(R.string.currency)}" // base fair
        tvTex.text = "${getFormattedPrice(0.0)} ${getString(R.string.currency)}"
        tvOthers.text = "${getFormattedPrice(0.0)} ${getString(R.string.currency)}"
        tvBuraqPercentage.text = "${getFormattedPrice(order?.payment?.adminCharge)} ${getString(R.string.currency)}"
        tvTotal.text = getFormattedPrice(order?.payment?.finalCharge) + " " + getString(R.string.currency)
    }

    private fun setUserData(user: User?) {
        var requestOptions = RequestOptions()
        requestOptions = requestOptions.transforms(CircleCrop()).placeholder(R.drawable.ic_user_black)
        Glide.with(this).applyDefaultRequestOptions(requestOptions).load(user?.profile_pic_url).into(imageView)
        tvName.text = user?.name
    }

    private fun setBookingData(orderData: Order?) {
        var requestOptions = RequestOptions()
        requestOptions = requestOptions.transforms(CenterCrop(), RoundedCorners(8))
        var lat = orderData?.pickup_latitude ?: 0.0
        var lng = orderData?.pickup_longitude ?: 0.0
        if (orderData?.dropoff_latitude?.toInt() != 0 && orderData?.dropoff_latitude?.toInt() != 0) {
            lat = orderData?.dropoff_latitude ?: 0.0
            lng = orderData?.dropoff_longitude ?: 0.0
        }

        Glide.with(this).applyDefaultRequestOptions(requestOptions).load(MapUtils.getStaticMapWithMarker(this, lat, lng)).into(ivMap)
        when (orderData?.order_status) {
            OrderStatus.SERVICE_COMPLETE,
            OrderStatus.CUSTOMER_CONFIRM_ETOKEN,
            OrderStatus.CUSTOMER_CANCEL -> {
                if (orderData.order_status == OrderStatus.CUSTOMER_CANCEL) {
                    tvCompleteAtHint.text = getString(R.string.cancelled_at)
                } else {
                    tvCompleteAtHint.text = getString(R.string.completed_at)
                }
                if (orderData.future == "1") {
                    tvStartedAtHint.visibility = View.GONE
                    tvStartedAt.visibility = View.GONE
                } else {
                    tvStartedAtHint.visibility = View.VISIBLE
                    tvStartedAt.visibility = View.VISIBLE
                }
                tvCompleteAtHint.visibility = View.VISIBLE
                tvCompleteAt.visibility = View.VISIBLE
                tvDateTime.text = DateUtils.getFormattedTimeForBooking(orderData.order_timings
                        ?: "")
                tvStartedAt.text = DateUtils.getFormattedTimeForBooking(orderData.created_at ?: "")
                tvCompleteAt.text = DateUtils.getFormattedTimeForBooking(orderData.updated_at ?: "")
            }

            OrderStatus.REACHED,OrderStatus.CONFIRMED, OrderStatus.DRIVER_SCHEDULE_SERVICE-> {
                tvStartedAt.visibility = View.GONE
                tvStartedAtHint.visibility = View.GONE
                tvCompleteAtHint.visibility = View.GONE
                tvCompleteAt.visibility = View.GONE
            }

            OrderStatus.DRIVER_CANCELLED,
            OrderStatus.DRIVER_SCHEDULED_CANCELLED -> {
                tvCompleteAtHint.text = getString(R.string.cancelled_at)
                tvStartedAtHint.visibility = View.GONE
                tvStartedAt.visibility = View.GONE
                tvCompleteAtHint.visibility = View.VISIBLE
                tvCompleteAt.visibility = View.VISIBLE
                tvDateTime.text = DateUtils.getFormattedTimeForBooking(orderData.order_timings
                        ?: "")
                tvCompleteAt.text = DateUtils.getFormattedTimeForBooking(orderData.updated_at ?: "")
            }
            else -> tvDateTime.text = DateUtils.getFormattedTimeForBooking(orderData?.order_timings
                    ?: "")
        }

        val paymentType = getString(getPaymentStringId(orderData?.payment?.paymentType
                ?: "0"))

        if (orderData?.payment?.paymentType == PaymentType.E_TOKEN) {
            tvPaymentTypeAmount.text = "$paymentType · ${orderData.payment?.productQuantity}"
        } else {
            tvPaymentTypeAmount.text = "$paymentType · ${getFormattedPrice(orderData?.payment?.finalCharge)} ${getString(R.string.currency)}"
        }
        tvBookingId.text = "ID:${orderData?.order_token}"
        tvBookingStatus.text =
                when (orderData?.order_status) {
                    OrderStatus.SERVICE_COMPLETE,
                    OrderStatus.CUSTOMER_CONFIRM_ETOKEN -> getString(R.string.completed)

                    OrderStatus.CONFIRMED -> getString(R.string.confirmed)

                    OrderStatus.REACHED -> {
                       getString(R.string.reached)
                    }

                    OrderStatus.CUSTOMER_CONFIRMATION_PENDING_ETOKEN -> getString(R.string.approval_pending)

                    OrderStatus.C_TIMEOUT_ETOKEN -> getString(R.string.timeout)

                    OrderStatus.DRIVER_ARROVED,
                    OrderStatus.DRIVER_START,
                    OrderStatus.DRIVER_SCHEDULE_SERVICE,
                    OrderStatus.SCHEDULED -> getString(R.string.schedule)

                    else -> getString(R.string.canceled)
                }
        tvDropOffLocation.text = if (orderData?.dropoff_address.isNullOrEmpty()) orderData?.pickup_address else orderData?.dropoff_address
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.detachView()
    }

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(LocaleManager.setLocale(newBase))
    }


}
