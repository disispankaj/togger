package com.buraq24.driver.ui.home.earnings
import com.buraq24.driver.webservices.models.earning.EarningResultModel
import com.buraq24.driver.webservices.models.order.Order
import com.buraq24.utilities.basearc.BasePresenter
import com.buraq24.utilities.basearc.BaseView

interface EarningsContract {
    interface View: BaseView {
        fun onApiSuccess(response: ArrayList<EarningResultModel?>)
    }

    interface Presenter: BasePresenter<View> {
        fun getEarnings(startDate: String, endDate: String)
    }
}