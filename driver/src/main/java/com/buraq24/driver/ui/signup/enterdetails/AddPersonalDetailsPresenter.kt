package com.buraq24.driver.ui.signup.enterdetails

import com.buraq24.driver.webservices.ApiResponse
import com.buraq24.driver.webservices.RestClient
import com.buraq24.driver.webservices.models.Organization
import com.buraq24.driver.webservices.models.RequestPersonalDetails
import com.buraq24.driver.webservices.models.ResponseCommon
import com.buraq24.getApiError
import com.buraq24.utilities.DateUtils
import com.buraq24.utilities.basearc.BasePresenterImpl
import com.buraq24.utilities.constants.SUCCESS_CODE
import okhttp3.MediaType
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File

class AddPersonalDetailsPresenter : BasePresenterImpl<AddPersonalDetailsContract.View>(), AddPersonalDetailsContract.Presenter {
    override fun getOrganization() {
        getView()?.showLoader(true)
        RestClient.get().searchOrg()
                .enqueue(object : Callback<ApiResponse<ArrayList<Organization>>> {
                    override fun onResponse(call: Call<ApiResponse<ArrayList<Organization>>>?, response: Response<ApiResponse<ArrayList<Organization>>>?) {
                        getView()?.showLoader(false)
                        if (response?.isSuccessful == true) {
                            if (response.body()?.statusCode == SUCCESS_CODE) {
                                getView()?.onOrgsSuccess(response.body()?.result?:ArrayList())
                            } else {
                                getView()?.handleApiError(response.body()?.statusCode, response.body()?.msg)
                            }
                        } else {
                            val errorModel = getApiError(response?.errorBody()?.string())
                            getView()?.handleApiError(errorModel.statusCode, errorModel.msg)
                        }
                    }

                    override fun onFailure(call: Call<ApiResponse<ArrayList<Organization>>>?, t: Throwable?) {
                        getView()?.showLoader(false)
                        getView()?.apiFailure()
                    }

                })
    }

    override fun addPersonalDetails(requestPersonalDetails: RequestPersonalDetails) {
        getView()?.showLoader(true)
        val call: Call<ApiResponse<ResponseCommon>>
        call = if(requestPersonalDetails.profile_pic != null){
            val map = HashMap<String, RequestBody>()
            map.put("name", RequestBody.create(MediaType.parse("text/plain"),
                    requestPersonalDetails.name?:"") )
            map.put("organisation_id", RequestBody.create(MediaType.parse("text/plain"),
                    requestPersonalDetails.organisation_id.toString()))
            val body = RequestBody.create(MediaType.parse("image/jpeg"), File(requestPersonalDetails.profile_pic))
            map.put("profile_pic\"; filename=\"pp1.png\" ", body )
            map.put("email", RequestBody.create(MediaType.parse("text/plain"),
                    requestPersonalDetails.email.toString()))
            map.put("date_of_birth", RequestBody.create(MediaType.parse("text/plain"),
                    requestPersonalDetails.date_of_birth.toString()))
            RestClient.get().addPersonalDetails(map)
        }
        else{
            RestClient.get().addPersonalDetails(requestPersonalDetails)
        }

               call.enqueue(object : Callback<ApiResponse<ResponseCommon>> {
            override fun onResponse(call: Call<ApiResponse<ResponseCommon>>?, response: Response<ApiResponse<ResponseCommon>>?) {
                getView()?.showLoader(false)
                if (response?.isSuccessful == true) {
                    if (response.body()?.statusCode == SUCCESS_CODE) {
                        getView()?.onApiSuccess(response.body()?.result)
                    } else {
                        getView()?.handleApiError(response.body()?.statusCode, response.body()?.msg)
                    }
                } else {
                    val errorModel = getApiError(response?.errorBody()?.string())
                    getView()?.handleApiError(errorModel.statusCode, errorModel.msg)
                }
            }

            override fun onFailure(call: Call<ApiResponse<ResponseCommon>>?, t: Throwable?) {
                getView()?.showLoader(false)
                getView()?.apiFailure()
            }

        })
    }

}