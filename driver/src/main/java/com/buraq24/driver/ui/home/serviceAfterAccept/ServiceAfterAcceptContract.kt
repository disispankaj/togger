package com.buraq24.driver.ui.home.serviceAfterAccept

import com.buraq24.utilities.basearc.BasePresenter
import com.buraq24.utilities.basearc.BaseView
import com.google.android.gms.maps.model.LatLng

class ServiceAfterAcceptContract{

    interface View: BaseView{
        fun onStartApiSuccess()
        fun onRejectApiSuccess()
    }

    interface Presenter: BasePresenter<View>{
        fun startRequest(lalLng: LatLng, orderId: Long,distance : String)
        fun rejectRequest(lalLng: LatLng, orderId: Long, reason: String)
    }
}