package com.buraq24.driver.ui.home.bookings

import com.buraq24.driver.webservices.ApiResponse
import com.buraq24.driver.webservices.RestClient
import com.buraq24.driver.webservices.models.order.Order
import com.buraq24.driver.webservices.models.order.OrderCommon
import com.buraq24.getApiError
import com.buraq24.utilities.basearc.BasePresenterImpl
import com.google.android.gms.maps.model.LatLng
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class BookingsPresenter: BasePresenterImpl<BookingContract.View>(), BookingContract.Presenter {
    override fun getHistoryList(take: Int, skip: Int, type: Int) {
        getView()?.showLoader(true)
        RestClient.get().getBookingsHistory(skip = skip,take = take,type = type)
                .enqueue(object : Callback<ApiResponse<ArrayList<Order?>>> {

            override fun onResponse(call: Call<ApiResponse<ArrayList<Order?>>>?, response: Response<ApiResponse<ArrayList<Order?>>>?) {
                getView()?.showLoader(false)
                if (response?.isSuccessful == true){
                    getView()?.onApiSuccess(response.body()?.result?:ArrayList())

                }else{
                    val errorModel = getApiError(response?.errorBody()?.string())
                    getView()?.handleApiError(errorModel.statusCode, errorModel.msg)
                }
            }

            override fun onFailure(call: Call<ApiResponse<ArrayList<Order?>>>?, t: Throwable?) {
                getView()?.showLoader(false)
                getView()?.apiFailure()
            }
        })
    }

    override fun getOrderDetails(orderId: Long) {
        getView()?.showLoader(true)
        RestClient.get().getOrderDetails(orderId).enqueue(object : Callback<OrderCommon> {

            override fun onResponse(call: Call<OrderCommon>?, response: Response<OrderCommon>?) {
                getView()?.showLoader(false)
                if (response?.isSuccessful == true) {
                    if (response.body()?.statusCode == 200) {
                        getView()?.onOrderDetailsSuccess(response.body()?.order)
                    } else {
                        getView()?.handleApiError(response.body()?.statusCode, response.body()?.msg)
                    }
                } else {
                    val errorModel = getApiError(response?.errorBody()?.string())
                    getView()?.handleApiError(errorModel.statusCode, errorModel.msg)
                }
            }

            override fun onFailure(call: Call<OrderCommon>?, t: Throwable?) {
                getView()?.showLoader(false)
                getView()?.apiFailure()
            }
        })
    }

  /*  override fun confirmScheduleRequest(lalLng: LatLng, orderId: Long, approve:Int) {
        getView()?.showLoader(true)
        RestClient.get().confirmUnConfirmService(lalLng.latitude,lalLng.longitude,orderId,approve).
                enqueue(object : Callback<Any> {

                    override fun onResponse(call: Call<Any>?, response: Response<Any>?) {
                        getView()?.showLoader(false)
                        if (response?.isSuccessful == true) {
                            getView()?.onScheduleCancelApiSuccess()
                        }else {
                            val errorModel = getApiError(response?.errorBody()?.string())
                            getView()?.handleApiError(errorModel.statusCode, errorModel.msg)
                        }
                    }

                    override fun onFailure(call: Call<Any>?, t: Throwable?) {
                        getView()?.showLoader(false)
                        getView()?.apiFailure()
                    }
                })

    }*/

    override fun confirmScheduleRequest(lalLng: LatLng, orderId: Long, reason: String) {
        getView()?.showLoader(true)
        RestClient.get().cancelService(lalLng.latitude,lalLng.longitude,orderId,reason).
                enqueue(object : Callback<Any> {

                    override fun onResponse(call: Call<Any>?, response: Response<Any>?) {
                        getView()?.showLoader(false)
                        if (response?.isSuccessful == true) {
                            getView()?.onScheduleCancelApiSuccess()
                        }else {
                            val errorModel = getApiError(response?.errorBody()?.string())
                            getView()?.handleApiError(response?.code(), errorModel.msg)
                        }
                    }

                    override fun onFailure(call: Call<Any>?, t: Throwable?) {
                        getView()?.showLoader(false)
                        getView()?.apiFailure()
                    }
                })
    }
}