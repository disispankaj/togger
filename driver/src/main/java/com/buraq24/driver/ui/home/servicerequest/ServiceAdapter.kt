package com.buraq24.driver.ui.home.servicerequest

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.buraq24.driver.R
import com.buraq24.driver.webservices.models.order.Order

class ServiceAdapter (private val listRequests: ArrayList<Order>) : RecyclerView.Adapter<ServiceViewHolder>() {
    override fun getItemCount(): Int {
        return listRequests.size
    }

    override fun onBindViewHolder(holder: ServiceViewHolder, position: Int) {
        holder.onBind(listRequests[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ServiceViewHolder {
        return ServiceViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.layout_all_request, parent, false))
    }

    fun addItems(listRequest :  ArrayList<Order>){
        val oldLength =listRequest.size
        this.listRequests.addAll(listRequest)
        notifyItemRangeInserted(oldLength,listRequest.size)
    }

}