package com.buraq24.driver.ui.signup.verifytop

import com.buraq24.driver.webservices.ApiResponse
import com.buraq24.driver.webservices.RestClient
import com.buraq24.driver.webservices.models.SendOtp
import com.buraq24.getApiError
import com.buraq24.utilities.basearc.BasePresenterImpl
import com.buraq24.utilities.constants.SUCCESS_CODE
import com.buraq24.utilities.webservices.models.LoginModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class VerifyOtpPresenter : BasePresenterImpl<VerifyOtpContract.View>(), VerifyOtpContract.Presenter {

    override fun verifyOtp(map: Map<String, String>) {
        getView()?.showLoader(true)
        RestClient.get().verifyOtp(map).enqueue(object : Callback<ApiResponse<LoginModel>> {

            override fun onResponse(call: Call<ApiResponse<LoginModel>>?, response: Response<ApiResponse<LoginModel>>?) {
                getView()?.showLoader(false)
                if (response?.isSuccessful == true) {
                    if (response.body()?.statusCode == 200) {
                        getView()?.onApiSuccess(response.body()?.result)
                    } else {
                        getView()?.handleApiError(response.body()?.statusCode, response.body()?.msg)
                    }
                } else {
                    val errorModel = getApiError(response?.errorBody()?.string())
                    getView()?.handleApiError(errorModel.statusCode, errorModel.msg)
                }
            }

            override fun onFailure(call: Call<ApiResponse<LoginModel>>?, t: Throwable?) {
                getView()?.showLoader(false)
                getView()?.apiFailure()
            }
        })
    }


    override fun sendOtpApiCall(map: Map<String, String>) {
        getView()?.showLoader(true)
        RestClient.get().sendOtp(map).enqueue(object : Callback<ApiResponse<SendOtp>>{

            override fun onResponse(call: Call<ApiResponse<SendOtp>>?, response: Response<ApiResponse<SendOtp>>?) {
                getView()?.showLoader(false)
                if (response?.isSuccessful == true){
                    if (response.body()?.statusCode == SUCCESS_CODE) {
                        getView()?.onOtpApiSuccess(response.body()?.result)
                    }else{
                        getView()?.handleApiError(response.body()?.statusCode, response.body()?.msg)
                    }
                }else{
                    val errorModel = getApiError(response?.errorBody()?.string())
                    getView()?.handleApiError(errorModel.statusCode, errorModel.msg)
                }
            }

            override fun onFailure(call: Call<ApiResponse<SendOtp>>?, t: Throwable?) {
                getView()?.showLoader(false)
                getView()?.apiFailure()
            }
        })
    }

}