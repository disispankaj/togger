package com.buraq24.driver.ui.home.rateCustomer

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.annotation.DrawableRes
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions

import com.buraq24.driver.webservices.models.order.Order
import com.buraq24.driver.R
import com.buraq24.driver.utils.signOut
import com.buraq24.showSWWerror
import com.buraq24.showSnack
import com.buraq24.utilities.*
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_rate_customer.*


class RateCustomerFragment : Fragment(), RateCustomerContract.View, View.OnClickListener {

    private val presenter = RateCustomerPresenter()
    private lateinit var orderDetail: Order
    private var rating = 5.0
    private var selectedTextView: TextView? = null
    private var onRateDone: OnRateDone? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_rate_customer, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.attachView(this)
        orderDetail = Gson().fromJson(arguments?.getString("order"), Order::class.java)
        setRatingData()
        setRateListeners()
    }

    private fun setRatingData() {
        tvNameRate.text = orderDetail.user?.name
        activity?.sendBroadcast(Intent("tokenRequest"))
        //selectedTextView = tvRate5
        //setSelectedViewRating(R.drawable.ic_5)
        Glide.with(imageViewRate.context).setDefaultRequestOptions(RequestOptions().circleCrop().placeholder(R.drawable.ic_user_black)).load(orderDetail.user?.profile_pic_url).into(imageViewRate)
    }

    private fun setSelectedViewRating(@DrawableRes drawable: Int) {
        selectedTextView?.setCompoundDrawablesWithIntrinsicBounds(0, drawable, 0, 0)
        selectedTextView?.setTextColor(ContextCompat.getColor(tvNameRate.context, R.color.colorRate))

    }

    private fun setRateListeners() {
        tvRate1.setOnClickListener(this)
        tvRate2.setOnClickListener(this)
        tvRate3.setOnClickListener(this)
        tvRate4.setOnClickListener(this)
        tvRate5.setOnClickListener(this)
        tvSubmitRate.setOnClickListener {
            val comments = etComments.text.toString().trim()
            if (selectedTextView != null) {
                if (CheckNetworkConnection.isOnline(activity)) {
                    presenter.rateOrder(rating, comments, orderDetail.order_id?.toLong() ?: 0)
                }
            } else {
                Toast.makeText(activity, getString(R.string.select_rating), Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun showLoader(isLoading: Boolean) {

    }

    override fun apiFailure() {
        view?.showSWWerror()
    }

    override fun handleApiError(code: Int?, error: String?) {
        if (code == StatusCode.UNAUTHORIZED) {
            signOut(activity)
            return
        }
        view?.showSnack(error.toString())
    }

    override fun onClick(v: View?) {
        setRatingDefault()
        when (v?.id) {
            R.id.tvRate1 -> {
                rating = 1.00
                selectedTextView = tvRate1
                setSelectedViewRating(R.drawable.ic_1)
            }
            R.id.tvRate2 -> {
                rating = 2.00
                selectedTextView = tvRate2
                setSelectedViewRating(R.drawable.ic_2)

            }
            R.id.tvRate3 -> {
                rating = 3.00
                selectedTextView = tvRate3
                setSelectedViewRating(R.drawable.ic_3)

            }
            R.id.tvRate4 -> {
                rating = 4.00
                selectedTextView = tvRate4
                setSelectedViewRating(R.drawable.ic_4)

            }
            R.id.tvRate5 -> {
                rating = 5.00
                selectedTextView = tvRate5
                setSelectedViewRating(R.drawable.ic_5)

            }
        }
    }

    private fun setRatingDefault() {
        tvRate1.setTextColor(ContextCompat.getColor(tvRate1.context, R.color.white))
        tvRate2.setTextColor(ContextCompat.getColor(tvRate2.context, R.color.white))
        tvRate3.setTextColor(ContextCompat.getColor(tvRate3.context, R.color.white))
        tvRate4.setTextColor(ContextCompat.getColor(tvRate4.context, R.color.white))
        tvRate5.setTextColor(ContextCompat.getColor(tvRate5.context, R.color.white))
        tvRate1.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_1off, 0, 0)
        tvRate2.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_2off, 0, 0)
        tvRate3.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_3off, 0, 0)
        tvRate4.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_4off, 0, 0)
        tvRate5.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_5off, 0, 0)
    }

    override fun onApiSuccess() {
        if (orderDetail.payment?.paymentType == PaymentType.E_TOKEN) {
            activity?.finish()
        } else {
            onRateDone?.onRated()
            activity?.supportFragmentManager?.beginTransaction()?.remove(this)?.commitAllowingStateLoss()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        presenter.detachView()
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        onRateDone = context as? OnRateDone
    }

    interface OnRateDone {
        fun onRated()
    }


}
