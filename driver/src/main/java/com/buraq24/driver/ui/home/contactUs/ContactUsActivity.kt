package com.buraq24.driver.ui.home.contactUs

import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.buraq24.driver.R
import kotlinx.android.synthetic.main.activity_contact_us.*
import android.widget.Toast
import android.content.Intent
import android.net.Uri
import com.buraq24.driver.utils.signOut
import com.buraq24.showSWWerror
import com.buraq24.showSnack
import com.buraq24.utilities.*

class ContactUsActivity : AppCompatActivity(), ContactUsContract.View{

    private var presenter = ContactUsPresenter()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contact_us)
        presenter.attachView(this)

        toolbar.setNavigationOnClickListener {
            onBackPressed()
        }

        tvSubmit.setOnClickListener {
            val msg = etMsg.text.toString().trim()
            if(!msg.isEmpty()){
                presenter.sendMsg(msg)
            }
            else{
                tvSubmit?.showSnack(getString(R.string.enter_msg))
            }
        }

        tvReachViaMail.setOnClickListener {
            val intent = Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", "support@buraq24.com", null))
            try {
                startActivity(Intent.createChooser(intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP), getString(R.string.send_mail_to)))
            } catch (ex: android.content.ActivityNotFoundException) {
                Toast.makeText(this@ContactUsActivity, getString(R.string.no_email_clients), Toast.LENGTH_SHORT).show()
            }
        }

        tvReachViaPhone.setOnClickListener {
            val phone = "+968-24453336"
            val intent = Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null))
            startActivity(intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP))
        }
    }

    override fun onApiSuccess() {
        finish()
    }

    override fun showLoader(isLoading: Boolean) {

    }

    override fun apiFailure() {
        tvSubmit?.showSWWerror()
    }

    override fun handleApiError(code: Int?, error: String?) {
        if (code == StatusCode.UNAUTHORIZED) {
            signOut(this)
            return
        }
        tvSubmit?.showSnack(error.toString())
    }


    override fun onDestroy() {
        super.onDestroy()
        presenter.detachView()
    }

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(LocaleManager.setLocale(newBase))
    }
}