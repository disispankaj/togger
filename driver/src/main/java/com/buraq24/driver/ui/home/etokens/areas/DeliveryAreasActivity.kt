package com.buraq24.driver.ui.home.etokens.areas

import android.content.Context
import android.content.Intent
import android.location.Location
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.buraq24.driver.R
import com.buraq24.driver.utils.signOut
import com.buraq24.driver.webservices.models.customers.Area
import com.buraq24.utilities.*
import com.buraq24.driver.utils.location.LocationProvider
import com.buraq24.showSWWerror
import com.buraq24.showSnack
import com.google.android.gms.tasks.OnSuccessListener
import kotlinx.android.synthetic.main.activity_delivery_areas.*
import java.util.*

class DeliveryAreasActivity : AppCompatActivity(), DeliveryAreasContract.View, OnSuccessListener<Location> {

    private val presenter = DeliveryAreasPresenter()

    private var areasList: ArrayList<Area>? = ArrayList()

    private var locationProvider: LocationProvider? = null

    private var dialogIndeterminate: DialogIndeterminate? = null

    var location: Location? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_delivery_areas)
        presenter.attachView(this)
        dialogIndeterminate = DialogIndeterminate(this)
        locationProvider = LocationProvider.CurrentLocationBuilder(this).build()
        locationProvider?.getLastKnownLocation(this)
        setListeners()

    }

    private fun setListeners() {
        toolbar.setNavigationOnClickListener { onBackPressed() }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        locationProvider?.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        locationProvider?.onActivityResult(requestCode, resultCode, data)
    }

    override fun onSuccess(location: Location?) {
        this.location = location
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = DeliveryAreaAdapter(areasList)
        if (CheckNetworkConnection.isOnline(this)) {
            val map = HashMap<String, String>()
            map["day"] = Calendar.getInstance().get(Calendar.DAY_OF_WEEK).toString()
            map["latitude"] = location?.latitude.toString()
            map["longitude"] = location?.longitude.toString()
            map["skip"] = "0"
            map["take"] = "1000"
            presenter.areaListingApi(map)
        } else {
            CheckNetworkConnection.showNetworkError(rootView)
        }
    }


    override fun onApiSuccess(areas: List<Area>?) {
        areasList?.clear()
        areas?.let { areasList?.addAll(it) }
        if (areasList?.isEmpty() == true) {
            tvNoData.visibility = View.VISIBLE
        } else {
            tvNoData.visibility = View.GONE
        }
        recyclerView?.adapter?.notifyDataSetChanged()
    }

    override fun showLoader(isLoading: Boolean) {
        dialogIndeterminate?.show(isLoading)
    }

    override fun apiFailure() {
        rootView?.showSWWerror()
    }

    override fun handleApiError(code: Int?, error: String?) {
        if (code == StatusCode.UNAUTHORIZED) {
            signOut(this)
            return
        }
        rootView?.showSnack(error.toString())
    }

    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(LocaleManager.setLocale(newBase))
    }

}
