package com.buraq24.driver.ui.home.etokens.areas

import com.buraq24.driver.webservices.models.customers.Area
import com.buraq24.utilities.basearc.BasePresenter
import com.buraq24.utilities.basearc.BaseView

class DeliveryAreasContract {

    interface View : BaseView {
        fun onApiSuccess(areas: List<Area>?)
    }

    interface Presenter : BasePresenter<View> {
        fun areaListingApi(map: HashMap<String, String>)
    }
}