package com.buraq24.driver.ui.home.support

import com.buraq24.driver.webservices.models.order.Order
import com.buraq24.utilities.basearc.BasePresenter
import com.buraq24.utilities.basearc.BaseView
import com.buraq24.utilities.webservices.models.Service
import org.json.JSONObject

class SupportContract{

    interface View: BaseView{
        fun onSupportListApiSuccess(response: List<Service>?)
    }

    interface Presenter: BasePresenter<View>{
        fun getSupportList()
    }

}