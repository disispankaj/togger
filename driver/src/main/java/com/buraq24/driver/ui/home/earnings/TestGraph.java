package com.buraq24.driver.ui.home.earnings;/*
package com.buraq24.driver.ui.home.earnings;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.IMarker;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;

import java.io.IOException;
import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VendorSalesReportFragment extends Fragment implements View.OnClickListener, OnChartValueSelectedListener {

    private RecyclerView recyclerView;
    private TextView tvTotalSales, tvPeakHours, tvMonth, tvYear;
    private TextView tvNoData;
    private BarChart barChart;
    private View viewExtra;
    private BarDataSet set;
    private String type, selectionType;
    private int year, month;
    private ProgressBar progressBar;
    private final Description description = null;
    private List<BarEntry> entries;
    private ArrayList<String> daysList, weeksList, monthsList, yearList, graphList;
    private String startDate, endDate, yearChoose;
    private List<Result> responseData;
    private LinearLayout llOrderReport;
    private int firstCount = 0;

    public static VendorSalesReportFragment newInstance(String type) {
        Bundle args = new Bundle();
        args.putString("type", type);
        VendorSalesReportFragment fragment = new VendorSalesReportFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_vendor_sales_report, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view);
        setMonthYear();
        setListeners();
        switch (type) {
            case Constants.CASE_DAILY:
                tvMonth.setVisibility(View.VISIBLE);
                tvYear.setVisibility(View.VISIBLE);
                viewExtra.setVisibility(View.VISIBLE);
                setDayData(month, year);
                hitApiGetReportsDMW(daysList.get(daysList.size() - 1), type);
                break;
            case Constants.CASE_WEEKLY:
                tvMonth.setVisibility(View.VISIBLE);
                tvYear.setVisibility(View.VISIBLE);
                viewExtra.setVisibility(View.VISIBLE);
                setWeekData(month, year);
                hitApiGetReportsDMW(weeksList.get(weeksList.size() - 1), type);
                break;
            case Constants.CASE_MONTHLY:
                tvYear.setVisibility(View.VISIBLE);
                viewExtra.setVisibility(View.VISIBLE);
                setMonthRecyclerView(year);
                hitApiGetReportsDMW(monthsList.get(monthsList.size() - 1), type);
                recyclerView.scrollToPosition(monthsList.size() - 1);
                break;
            case Constants.CASE_YEARLY:
                setYearRecyclerView();
                hitApiGetReportsDMW(yearList.get(yearList.size() - 1), type);
                recyclerView.scrollToPosition(yearList.size() - 1);
                break;
        }
    }

    private void setListeners() {
        tvMonth.setOnClickListener(this);
        tvYear.setOnClickListener(this);
        llOrderReport.setOnClickListener(this);
    }

    private void setMonthYear() {
        Calendar calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        tvYear.setText("" + year);
        month = calendar.get(Calendar.MONTH);
        calendar.set(Calendar.MONTH, month);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMM");
        tvMonth.setText(simpleDateFormat.format(calendar.getTime()));
    }

    private void setDayData(int month, int year) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMM");
        String monthName = simpleDateFormat.format(calendar.getTime());
        //get number of days in month
        int days = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        String start = "1 " + monthName + " " + year;
        String end = days + " " + monthName + " " + year;
        if (Calendar.getInstance().get(Calendar.MONTH) == month && year == Calendar.getInstance().get(Calendar.YEAR)) {
            end = calendar.get(Calendar.DAY_OF_MONTH) + " " + monthName + " " + year;
        }
        setDayRecyclerView(start, end);
    }

    private void setWeekData(int month, int year) {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("d MMM yyyy", Locale.getDefault());
        dateFormat.setTimeZone(TimeZone.getDefault());
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMM");
        String monthName = simpleDateFormat.format(calendar.getTime());
        int day = (Calendar.SUNDAY - calendar.get(Calendar.DAY_OF_WEEK));
        if (day < 0) {
            calendar.add(Calendar.DATE, 7 + (day));
        } else {
            calendar.add(Calendar.DATE, day);
        }
        int days = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        String start = dateFormat.format(calendar.getTime());
        String end = days + " " + monthName + " " + year;
        if (Calendar.getInstance().get(Calendar.MONTH) == month) {
            end = calendar.get(Calendar.DAY_OF_MONTH) + " " + monthName + " " + year;
        }
        setWeekRecyclerView(start, end);
    }

    private void init(View view) {
        type = getArguments().getString("type");
        getActivity().registerReceiver(broadcastReceiver, new IntentFilter("vendorSalesReportFragment"));
        tvNoData = view.findViewById(R.id.tvNoData);
        tvMonth = view.findViewById(R.id.tvMonth);
        viewExtra = view.findViewById(R.id.viewExtra);
        tvYear = view.findViewById(R.id.tvYear);
        tvTotalSales = view.findViewById(R.id.tvTotalSales);
        tvPeakHours = view.findViewById(R.id.tvPeakHours);
        TextView tvPeakHoursTitle = view.findViewById(R.id.tvPeakHoursTitle);
        graphList = new ArrayList<>();
        responseData = new ArrayList<>();
        progressBar = view.findViewById(R.id.pbHeaderProgress);
        recyclerView = view.findViewById(R.id.recyclerView);
        llOrderReport = view.findViewById(R.id.llOrderReport);
        barChart = view.findViewById(R.id.barChart);
        if (type.equals(Constants.CASE_MONTHLY) || type.equals(Constants.CASE_WEEKLY))
            tvPeakHoursTitle.setText(getString(R.string.peakDay));
        else if (type.equals(Constants.CASE_YEARLY)) {
            tvPeakHoursTitle.setText(R.string.peak_month);
        }
    }

    private void setDayRecyclerView(String start, String end) {
        daysList = new ArrayList<>();
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("d MMM yyyy");
        dateFormat.setTimeZone(TimeZone.getDefault());

        try {
            Date startDate = dateFormat.parse(start);
            Date endDate = dateFormat.parse(end);
            calendar.setTime(startDate);
            SimpleDateFormat calendarFormat = new SimpleDateFormat("d MMM");

            while (calendar.getTime().getTime() <= endDate.getTime()) {
                daysList.add(String.valueOf(calendarFormat.format(calendar.getTime())));
                calendar.add(Calendar.DATE, 1);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        recyclerView.setAdapter(new DatesAdapter(getActivity(), daysList, daysList.size() - 1, this, type));
        recyclerView.smoothScrollToPosition(daysList.size() - 1);
    }

    private void setWeekRecyclerView(String start, String end) {
        weeksList = new ArrayList<>();
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("d MMM yyyy", Locale.getDefault());
        dateFormat.setTimeZone(TimeZone.getDefault());

        try {
            Date startDate = dateFormat.parse(start);
            Date endDate = dateFormat.parse(end);
            calendar.setTime(startDate);
            SimpleDateFormat calendarFormat = new SimpleDateFormat("d MMM", Locale.getDefault());

            while (calendar.getTime().getTime() <= endDate.getTime()) {
                String date = String.valueOf(calendarFormat.format(calendar.getTime())) + "-";
                calendar.add(Calendar.DATE, 6);
                date = date + String.valueOf(calendarFormat.format(calendar.getTime()));
                weeksList.add(date);
                calendar.add(Calendar.DATE, 1);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        recyclerView.setAdapter(new DatesAdapter(getActivity(), weeksList, weeksList.size() - 1, this, type));
        recyclerView.smoothScrollToPosition(weeksList.size() - 1);
    }

    private void setMonthRecyclerView(int year) {
        monthsList = new ArrayList<>();
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM");
        dateFormat.setTimeZone(TimeZone.getDefault());
        int currentMonth = Integer.parseInt(dateFormat.format(calendar.getTime()));
        String[] months = new DateFormatSymbols().getShortMonths();
        if (Calendar.getInstance().get(Calendar.YEAR) == year)
            monthsList.addAll(Arrays.asList(months).subList(0, currentMonth));
        else
            monthsList.addAll(Arrays.asList(months).subList(0, 12));
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        recyclerView.setAdapter(new DatesAdapter(getActivity(), monthsList, monthsList.size() - 1, this, type));
        recyclerView.smoothScrollToPosition(monthsList.size() - 1);
    }

    private void setYearRecyclerView() {
        yearList = new ArrayList<>();
        Calendar calendarYear = Calendar.getInstance();
        SimpleDateFormat dateFormatYear = new SimpleDateFormat("yyyy");
        dateFormatYear.setTimeZone(TimeZone.getDefault());
        int startingYear = 2017;
        int currentYear = Integer.parseInt(dateFormatYear.format(calendarYear.getTime()));
        ArrayList<String> list = new ArrayList<>();
        for (int i = startingYear; i <= currentYear; i++) {
            list.add("" + i);
        }
        yearList.addAll(list);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        recyclerView.setAdapter(new DatesAdapter(getActivity(), yearList, yearList.size() - 1, this, type));
        recyclerView.smoothScrollToPosition(yearList.size() - 1);
    }

    private void hitApiGetReportsDMW(String duration, final String type) {
        initialiseEndDates(duration, type);
        if (GeneralFunctions.isConnectedToNetwork(getActivity(), true)) {
            progressBar.setVisibility(View.VISIBLE);
            barChart.setVisibility(View.GONE);
            tvNoData.setVisibility(View.GONE);
            RestClient.get().getVendorReportsDMW(Prefs.with(getActivity()).getString(Constants.ACCESS_TOKEN, ""),
                    getHashMap()).enqueue(new Callback<CommonPojo>() {
                @Override
                public void onResponse(Call<CommonPojo> call, Response<CommonPojo> response) {
                    if (getActivity() != null) {
                        progressBar.setVisibility(View.GONE);
                        if (response.isSuccessful()) {
                            checkDataAndCreateGraph(response);
                        } else {
                            try {
                                GeneralFunctions.checkError(getActivity(), response.errorBody().string(), response.code());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call<CommonPojo> call, Throwable t) {
                    if (getActivity() != null) {
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(getActivity(), getString(R.string.something_went_wrong), Toast.LENGTH_LONG).show();
                    }
                }
            });
        }
    }

    private void checkDataAndCreateGraph(Response<CommonPojo> response) {
        if (response.body().data.total == 0) {
            tvNoData.setVisibility(View.VISIBLE);
            barChart.setVisibility(View.GONE);
            tvTotalSales.setText(getActivity().getString(R.string.value0));
            tvPeakHours.setText(getActivity().getString(R.string.symbol));
        } else {
            barChart.setVisibility(View.VISIBLE);
            tvNoData.setVisibility(View.GONE);
            responseData = response.body().data.results;
            tvTotalSales.setText(GeneralFunctions.formatPrice(response.body().data.total));
            switch (type) {
                case Constants.CASE_DAILY:
                    tvPeakHours.setText(response.body().data.peekHour.hour);
                    break;
                case Constants.CASE_WEEKLY:
                case Constants.CASE_MONTHLY:
                    tvPeakHours.setText(GeneralFunctions.getFormattedDate("MM/dd/yyyy", "d MMM",
                            response.body().data.peekDate, TimeZone.getTimeZone("UTC"), TimeZone.getDefault()));
                    break;
                case Constants.CASE_YEARLY:
                    tvPeakHours.setText(response.body().data.peekMonth);
                    break;

            }
            createGraphData();
        }
    }

    private HashMap<String, String> getHashMap() {
        HashMap<String, String> hashMap = new HashMap<>();
        if (type.equals(Constants.CASE_YEARLY)) {
            hashMap.put("year", yearChoose);
        } else {
            hashMap.put(WebConstants.PARAM_START_DATE, startDate);
            hashMap.put(WebConstants.PARAM_END_DATE, endDate);
        }
        hashMap.put(WebConstants.PARAM_TIME_ZONE, TimeZone.getDefault().getID());
        return hashMap;
    }

    private void initialiseEndDates(String duration, String type) {
        //String year = String.valueOf(Calendar.getInstance().get(Calendar.YEAR));
        switch (type) {
            case Constants.CASE_DAILY:
                startDate = duration + " " + year;
                endDate = duration + " " + year;
                break;
            case Constants.CASE_WEEKLY:
                String[] parts = duration.split("-");
                startDate = parts[0] + " " + year;
                endDate = parts[1] + " " + year;
                break;
            case Constants.CASE_MONTHLY:
                Date date = null;
                try {
                    date = new SimpleDateFormat("MMM").parse(duration);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                Calendar cal = Calendar.getInstance();
                cal.setTime(date);
                int monthNum = cal.get(Calendar.MONTH);
                Calendar calendar = new GregorianCalendar(year, monthNum, 1);
                int daysInMonth = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
                startDate = "01 " + duration + " " + year;
                endDate = daysInMonth + " " + duration + " " + year;
                break;
            case Constants.CASE_YEARLY:
                yearChoose = duration;
                break;
        }
    }

    private void createGraphData() {
        graphList.clear();
        switch (type) {
            case Constants.CASE_MONTHLY:
            case Constants.CASE_WEEKLY:
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy");
                dateFormat.setTimeZone(TimeZone.getDefault());
                try {
                    Date startDate1 = dateFormat.parse(startDate);
                    Date endDate1 = dateFormat.parse(endDate);
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(startDate1);
                    SimpleDateFormat calendarFormat = new SimpleDateFormat("MM/dd/yyyy");

                    while (calendar.getTime().getTime() <= endDate1.getTime()) {
                        graphList.add(String.valueOf(calendarFormat.format(calendar.getTime())));
                        calendar.add(Calendar.DATE, 1);
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                break;
            case Constants.CASE_YEARLY:
                Calendar calendarYear = Calendar.getInstance();
                SimpleDateFormat dateFormatYear = new SimpleDateFormat("yyyy");
                dateFormatYear.setTimeZone(TimeZone.getDefault());
                int currentYear = Integer.parseInt(dateFormatYear.format(calendarYear.getTime()));
                for (int i = 10; i > 0; i--) {
                    graphList.add(String.valueOf(currentYear - i));
                }
                graphList.add(String.valueOf(currentYear));
                break;
        }
        setUpBarGraph();
    }


    private void setUpBarGraph() {
        entries = new ArrayList<>();
        switch (type) {
            default:
                for (int i = 0; i < responseData.size(); i++) {
                    if (responseData.get(i).count > 0 && firstCount == 0) {
                        firstCount = i;
                    }
                    entries.add(new BarEntry(i, (int) responseData.get(i).count));
                }

                break;
        }
        barChart.invalidate();
        setYChartAxis();
        setXChartAxis();
        setBarData();
        setBarDecor();
    }

    private void setBarDecor() {
        barChart.getLegend().setEnabled(false);
        barChart.setDescription(description);
        barChart.animateY(2000);
        barChart.animateX(2000);
        barChart.setDoubleTapToZoomEnabled(false);
        barChart.setScaleEnabled(false);
        barChart.setVisibleXRangeMaximum(10);
        if (firstCount == 0) {
            barChart.moveViewToX(firstCount);
        } else {
            barChart.moveViewToX(firstCount - 1);
        }
        barChart.setOnChartValueSelectedListener(VendorSalesReportFragment.this);
    }

    private void setBarData() {
        set = new BarDataSet(entries, "");
        set.setColor(ContextCompat.getColor(getActivity(), R.color.colorAccent));
        BarData data = new BarData(set);
        data.setValueTextColor(ContextCompat.getColor(getActivity(), R.color.colorAccent));
        data.setDrawValues(false);
        data.setBarWidth(.40f);
        barChart.setData(data);
    }

    private void setXChartAxis() {
        switch (type) {
            case Constants.CASE_DAILY:
                final String[] quarters = new String[]{"0-2", "2-4", "4-6", "6-8", "8-10", "10-12", "12-14"
                        , "14-16", "16-18", "18-20", "20-22", "22-24"};
                final IAxisValueFormatter formatter = new IAxisValueFormatter() {
                    @Override
                    public String getFormattedValue(float value, AxisBase axis) {
                        return quarters[(int) value];
                    }
                };

                barChart.getAxisRight().setEnabled(false);
                XAxis xAxis = barChart.getXAxis();
                xAxis.setDrawGridLines(false);
                xAxis.setValueFormatter(formatter);
                xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
                xAxis.setAvoidFirstLastClipping(false);
                xAxis.setTextColor(ActivityCompat.getColor(getActivity(), R.color.colorTextHint));
                break;

            case Constants.CASE_WEEKLY:
            case Constants.CASE_MONTHLY:
                final String[] quarters1 = new String[graphList.size()];
                for (int i = 0; i < graphList.size(); i++) {
                    quarters1[i] = GeneralFunctions.getFormattedDate("MM/dd/yyyy", "d MMM", graphList.get(i),
                            TimeZone.getTimeZone("UTC"), TimeZone.getDefault());
                }
                final IndexAxisValueFormatter formatter1 = new IndexAxisValueFormatter() {
                    @Override
                    public String getFormattedValue(float value, AxisBase axis) {
                        if (value <= axis.getAxisMaximum())
                            return quarters1[(int) value];
                        else
                            return "";
                    }
                };
                barChart.getAxisRight().setEnabled(false);
                XAxis xAxis1 = barChart.getXAxis();
                xAxis1.setDrawGridLines(false);
                xAxis1.setValueFormatter(formatter1);
                xAxis1.setPosition(XAxis.XAxisPosition.BOTTOM);
                xAxis1.setAvoidFirstLastClipping(false);
                xAxis1.setTextColor(ActivityCompat.getColor(getActivity(), R.color.colorTextHint));
                break;
            case Constants.CASE_YEARLY:
                final String[] months = new String[]{"Jan", "Feb", "Mar", "Apr", "May", "Jun",
                        "July", "Aug", "Sept", "Nov", "Dec"};
                final IAxisValueFormatter formatter2 = new IAxisValueFormatter() {
                    @Override
                    public String getFormattedValue(float value, AxisBase axis) {
                        return months[(int) value];
                    }
                };
                barChart.getAxisRight().setEnabled(false);
                final XAxis xAxis2 = barChart.getXAxis();
                xAxis2.setDrawGridLines(false);
                xAxis2.setValueFormatter(formatter2);
                xAxis2.setPosition(XAxis.XAxisPosition.BOTTOM);
                xAxis2.setAvoidFirstLastClipping(false);
                xAxis2.setTextColor(ActivityCompat.getColor(getActivity(), R.color.colorTextHint));
                break;
        }
    }

    private void setYChartAxis() {
        YAxis yAxis = barChart.getAxisLeft();
        yAxis.setDrawLabels(true);
        yAxis.setDrawAxisLine(false);
        yAxis.setDrawGridLines(true);
        yAxis.setAxisMinimum(0);
        yAxis.setTextColor(ActivityCompat.getColor(getActivity(), R.color.colorTextHint));
    }

    @Override
    public void onValueSelected(Entry e, Highlight h) {
        IMarker marker = new MyMarkerView(getActivity(), R.layout.bar_chart_popup);
        barChart.setMarker(marker);
        marker.refreshContent(e, h);
    }

    @Override
    public void onNothingSelected() {
        barChart.invalidate();
        set.setColor(ContextCompat.getColor(getActivity(), R.color.colorAccent));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvMonth:
                selectionType = Constants.CASE_MONTHLY;
                openSelectionDialogFragment();
                break;

            case R.id.tvYear:
                selectionType = Constants.CASE_YEARLY;
                openSelectionDialogFragment();
                break;

            case R.id.llOrderReport:
                VendorSalesTrendReportDialogFragment vendorSalesTrendReportDialogFragment = new VendorSalesTrendReportDialogFragment();
                vendorSalesTrendReportDialogFragment.show(getActivity().getSupportFragmentManager(),
                        "vendorSalesTrendReportDialogFragment");
                getActivity().getSupportFragmentManager().popBackStack();
                break;
        }
    }

    private void openSelectionDialogFragment() {
        SelectionDialogFragment selectionDialogFragment = SelectionDialogFragment.newInstance(year, selectionType);
        selectionDialogFragment.setTargetFragment(this, 123);
        selectionDialogFragment.show(getFragmentManager(), null);
    }

    private final BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (type.equals(intent.getStringExtra("from")))
                hitApiGetReportsDMW(intent.getStringExtra("duration"), intent.getStringExtra("from"));
        }
    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 123:
                if (selectionType.equals(Constants.CASE_MONTHLY)) {
                    tvMonth.setText(data.getStringExtra(Constants.DATA));
                    month = data.getIntExtra(Constants.POSITION, 0);
                } else if (selectionType.equals(Constants.CASE_YEARLY)) {
                    tvYear.setText(data.getStringExtra(Constants.DATA));
                    year = Integer.parseInt(data.getStringExtra(Constants.DATA));
                }
                switch (type) {
                    case Constants.CASE_DAILY:
                        setDayData(month, year);
                        hitApiGetReportsDMW(daysList.get(daysList.size() - 1), type);
                        break;
                    case Constants.CASE_WEEKLY:
                        setWeekData(month, year);
                        hitApiGetReportsDMW(weeksList.get(weeksList.size() - 1), type);
                        break;
                    case Constants.CASE_MONTHLY:
                        setMonthRecyclerView(year);
                        hitApiGetReportsDMW(monthsList.get(monthsList.size() - 1), type);
                        break;
                }
                break;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getActivity().unregisterReceiver(broadcastReceiver);
    }
}

*/
