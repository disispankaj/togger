package com.buraq24.driver.ui.home.profileEdit

//import kotlinx.android.synthetic.main.activity_edit_profile.rv_color
import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.os.Environment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.AdapterView
import android.widget.Spinner
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.buraq24.driver.ColorListArrayAdapter
import com.buraq24.driver.ImageUtils
import com.buraq24.driver.R
import com.buraq24.driver.ui.home.stripe.ConnectStripeActivity
import com.buraq24.driver.utils.PermissionUtils
import com.buraq24.driver.utils.signOut
import com.buraq24.driver.webservices.ApiResponse
import com.buraq24.driver.webservices.models.RequestPersonalDetails
import com.buraq24.driver.webservices.models.ResponseCommon
import com.buraq24.setRoundProfileUrl
import com.buraq24.showSWWerror
import com.buraq24.showSnack
import com.buraq24.utilities.*
import com.buraq24.utilities.constants.PROFILE
import com.buraq24.utilities.webservices.BASE_IMAGE_URL
import com.buraq24.utilities.webservices.BASE_URL_LOAD_IMAGE
import com.buraq24.utilities.webservices.models.AppDetail
import com.buraq24.utilities.webservices.models.LoginModel
import kotlinx.android.synthetic.main.activity_edit_profile.*
import okhttp3.MediaType
import okhttp3.RequestBody
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import permissions.dispatcher.*
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.util.*


@RuntimePermissions
class EditProfileActivity : AppCompatActivity(), ProfileContract.View, DateUtils.OnDateSelectedListener,
        ColorListAdapter.Colorcallback {

    override fun onSuccessConnectStripe() {

    }

    override fun dateTimeSelected(dateCal: Calendar) {
        date = DateUtils.getFormattedDateForUTC(dateCal)
        textView14.text = DateUtils.getFormattedTime(dateCal)
    }

    override fun timeSelected(dateCal: Calendar) {
    }

    companion object {
        const val REQUEST_CODE_UPDATED_PROFILE = 1001
    }

    private var pathBackImage = ""
    private var pathFrontImage = ""
    private var pathImage = ""
    private var imageType = 0
    private val presenter = ProfilePresenter()
    private var date: String = ""
    private var colorname = ""
    private var category_id = ""
    private var dialogIndeterminate: DialogIndeterminate? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_profile)
        presenter.attachView(this)
        dialogIndeterminate = DialogIndeterminate(this)
        toolbarProfile.setNavigationOnClickListener {
            onBackPressed()
        }
        setData()
        setListener()


        val colorNames = arrayOf("Red", "Black", "Silver", "White", "Blue", "Brown")
        val spinner = findViewById<Spinner>(R.id.colorSpinner)
        if (spinner != null) {

//            val arrayAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, colorNames)

//            spnColors=(Spinner)findViewById(R.id.spnColor);
//            spnColors.setAdapter(new SpinnerAdapter(this));

            val arrayAdapter = ColorListArrayAdapter(this)
            spinner.adapter = arrayAdapter

            spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

                override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                    colorNames[position]
                    // Toast.makeText(this@MainActivity, getString(R.string.selected_item) + " " + personNames[position], Toast.LENGTH_SHORT).show()
                }

                override fun onNothingSelected(parent: AdapterView<*>) {
                    // Code to perform some action when nothing is selected
                }
            }
        }

    }

    private fun setListener() {
        imageView2.setOnClickListener {
            imageType = 0
            getStorageWithPermissionCheck()
        }
        imageView4.setOnClickListener {
            imageType = 1
            getStorageWithPermissionCheck()
        }
        imageView5.setOnClickListener {
            imageType = 2
            getStorageWithPermissionCheck()
        }
        textView14.setOnClickListener {
            DateUtils.openDateDialog(it.context, this, false,
                    Calendar.getInstance())
        }

        tvSubmit.setOnClickListener {
            val name = etName.text.toString().trim()
            val mulkiyanNumber = editText3.text.toString().trim()
            val date = date

            if (checkValidations(name, mulkiyanNumber)) {
                if (pathImage.isNotEmpty()) {
                    val body = RequestBody.create(MediaType.parse("image/jpeg"),
                            File(pathImage))
                    presenter.updateProfile(body)
                } else {
                    presenter.updateProfileData(RequestPersonalDetails(name = name, pic1 = pathBackImage,
                            vehicle_name = mulkiyanNumber,
                            pic0 = pathFrontImage))
                }
            }
        }

        tvConnectStripe.setOnClickListener {
            ConnectStripeActivity.start(this)
        }
    }

    private fun vehicleValidate(): Boolean {
        return when {

            edCarModel.text.toString().trim().isEmpty() -> {
                tvSubmit.showSnack(getString(R.string.model_name_missing))
                false
            }

            colorname.isEmpty() -> {
                tvSubmit.showSnack(getString(R.string.color_missing))
                false
            }
            else -> true
        }
    }


    private fun setData() {
        val profileData = SharedPrefs.get().getObject(PROFILE, AppDetail::class.java)

        if (profileData.category_id == 7) {
            group.visibility = View.VISIBLE

            val layoutManager = LinearLayoutManager(this)

            layoutManager.orientation = RecyclerView.HORIZONTAL
            //         rv_color.layoutManager = layoutManager

            val colorAdapter = ColorListAdapter()
            colorAdapter.settingcallback(this)
            //         rv_color.adapter = colorAdapter


            if (profileData?.vehicle_color?.isNotEmpty()!!) {
                profileData.vehicle_color.let {
                    colorAdapter.notifyAdapter(it!!)
                }

            }


            edCarModel.setText(profileData.vehicle_name)

        } else {
            ////////////////////
            group.visibility = View.VISIBLE
        }


        setMulkiyanImage(profileData?.mulkiya_front, profileData?.mulkiya_back)
        setProfilePic(profileData?.profile_pic_url)

        etName.setText(profileData?.user?.name)
        editText3.setText(profileData?.vehicle_name)

        date = profileData?.mulkiya_validity ?: ""
        textView14.text = DateUtils.getFormattedDateForUTCLocale(date)
        getImageInBack(profileData?.mulkiya_front, profileData?.mulkiya_back)
    }

    private fun getImageInBack(mulkiya_front: String?, mulkiya_back: String?) {
        doAsync {
            val theBitmap = Glide.with(this@EditProfileActivity).applyDefaultRequestOptions(RequestOptions().dontTransform()).asBitmap().load(BASE_IMAGE_URL + mulkiya_front).into(400, 600). // Width and height
                    get()
            val bytes = ByteArrayOutputStream()
            theBitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
            val file = File(Environment.getExternalStorageDirectory().path + File.separator + "temporary_file.jpg")
            try {
                val fo = FileOutputStream(file)
                fo.write(bytes.toByteArray())
                fo.flush()
                fo.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }

            uiThread {
                pathFrontImage = file.path
            }

            val theBitmap2 = Glide.with(this@EditProfileActivity).applyDefaultRequestOptions(RequestOptions().dontTransform()).asBitmap().load(BASE_IMAGE_URL + mulkiya_back).into(400, 600). // Width and height
                    get()
            val bytes2 = ByteArrayOutputStream()
            theBitmap2.compress(Bitmap.CompressFormat.JPEG, 100, bytes2)
            val file2 = File(Environment.getExternalStorageDirectory().path + File.separator + "temporary_file2.jpg")
            try {
                val fo = FileOutputStream(file2)
                fo.write(bytes2.toByteArray())
                fo.flush()
                fo.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }

            uiThread {
                pathBackImage = file2.path
            }

        }

    }

    private fun setMulkiyanImage(mulkiya_front: String?, mulkiya_back: String?) {
//        var requestOptions1 = RequestOptions()
//        requestOptions1 = requestOptions1.placeholder(R.drawable.ic_add_bg)
        Glide.with(this).load("${BASE_URL_LOAD_IMAGE + mulkiya_front}").into(imageView4)

        Glide.with(this).load("${BASE_URL_LOAD_IMAGE + mulkiya_back}").into(imageView5)
    }

    private fun setProfilePic(profile_pic_url: String?) {
//        var requestOptions = RequestOptions()
//        requestOptions = requestOptions.transforms(CircleCrop()).placeholder(R.drawable.ic_bg_menu)
//        Glide.with(this).applyDefaultRequestOptions(requestOptions).
//                load(profile_pic_url).into(imageView2)
        imageView2.setRoundProfileUrl(profile_pic_url)
    }


    @NeedsPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    fun getStorage() {
        ImageUtils.displayImagePicker(this)

    }

    @OnShowRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    fun showLocationRationale(request: PermissionRequest) {
        PermissionUtils.showRationalDialog(imageView2.context, R.string.permission_required_to_select_image, request)
    }

    @OnNeverAskAgain(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    fun onNeverAskAgainRationale() {
        PermissionUtils.showAppSettingsDialog(imageView2.context,
                R.string.permission_required_to_select_image)
    }

    @SuppressLint("NeedOnRequestPermissionsResult")
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        onRequestPermissionsResult(requestCode, grantResults)
    }

    override fun onPicApiSuccess(response: ResponseCommon?) {
        val name = etName.text.toString().trim()
        val mulkiyanNumber = editText3.text.toString().trim()
//        val date = textView14.text.toString().trim()
        presenter.updateProfileData(RequestPersonalDetails(name = name, pic1 = pathBackImage,
                vehicle_name = mulkiyanNumber,
                pic0 = pathFrontImage))
    }


    override fun onApiSuccess(response: ResponseCommon?) {

        // SharedPrefs.with(this).save(PROFILE, response)

        onBackPressed()
    }

    override fun showLoader(isLoading: Boolean) {
        dialogIndeterminate?.show(isLoading)
    }

    override fun apiFailure() {
        view?.showSWWerror()
    }

    override fun handleApiError(code: Int?, error: String?) {
        if (code == StatusCode.UNAUTHORIZED) {
            signOut(this)
            return
        }
        view?.showSnack(error.toString())
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                ImageUtils.REQ_CODE_CAMERA_PICTURE -> {
                    val file = ImageUtils.getFile(this)
                    setImageByType(file)
                }

                ImageUtils.REQ_CODE_GALLERY_PICTURE -> {
                    data?.let {
                        setImageByType(ImageUtils.getImagePathFromGallery(this, data.data))
                    }

                }
            }
        }
    }

    private fun checkValidations(name: String, license: String): Boolean {
        return when {
            name.isEmpty() -> {
                tvSubmit.showSnack(getString(R.string.name_empty_validation_message))
                false
            }
            license.isEmpty() -> {
                tvSubmit.showSnack(getString(R.string.liecense_missing))
                false
            }
            else -> true
        }
    }

    private fun setImageByType(file: File) {
        var requestOptions = RequestOptions()
        requestOptions = requestOptions.transforms(CircleCrop()).placeholder(R.drawable.ic_user_black)
        var requestOptions1 = RequestOptions()
        requestOptions1 = requestOptions1.transforms(RoundedCorners(8))
        if (imageType == 0) {
            Glide.with(this).applyDefaultRequestOptions(requestOptions).load(file).into(imageView2)
            pathImage = file.absolutePath
        } else if (imageType == 1) {
            Glide.with(imageView4.context).applyDefaultRequestOptions(RequestOptions().dontTransform()).load(file).into(imageView4)
            pathFrontImage = file.absolutePath
        } else if (imageType == 2) {
            Glide.with(imageView5.context).applyDefaultRequestOptions(RequestOptions().dontTransform()).load(file).into(imageView5)
            pathBackImage = file.absolutePath
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.detachView()
    }

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(LocaleManager.setLocale(newBase))
    }

    override fun onBackPressed() {
        setResult(Activity.RESULT_OK)
        super.onBackPressed()
    }

    override fun getColor(color: String) {
        colorname = color
    }
}