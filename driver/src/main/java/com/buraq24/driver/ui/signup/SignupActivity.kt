package com.buraq24.driver.ui.signup

import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.buraq24.driver.R
import com.buraq24.driver.ui.signup.addBankDetails.AddBankDetailsFragment
import com.buraq24.driver.ui.signup.addMulkiyanDetails.AddMulkiyanDetailsFragment
import com.buraq24.driver.ui.signup.enterdetails.AddPersonalDetailsFragment
import com.buraq24.driver.ui.signup.login.LoginFragment
import com.buraq24.driver.ui.signup.verifytop.OtpFragment
import com.buraq24.driver.utils.LocaleHelper
import com.buraq24.driver.utils.getPhoneLang
import com.buraq24.utilities.LocaleManager
import com.buraq24.utilities.SharedPrefs
import com.buraq24.utilities.constants.LANGUAGE_CODE

class SignupActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signup)
        supportFragmentManager.beginTransaction().add(R.id.container, LoginFragment(),LoginFragment::class.java.simpleName).commit()
    }

    override fun onBackPressed() {
         if(supportFragmentManager?.findFragmentByTag(LoginFragment::class.java.simpleName)?.isVisible == true||
                (supportFragmentManager?.findFragmentByTag(AddMulkiyanDetailsFragment::class.java.simpleName)!= null &&
                        supportFragmentManager?.findFragmentByTag(AddBankDetailsFragment::class.java.simpleName) != null) ||
                (supportFragmentManager?.findFragmentByTag(AddBankDetailsFragment::class.java.simpleName)!= null &&
                        supportFragmentManager?.findFragmentByTag(AddPersonalDetailsFragment::class.java.simpleName)!= null) ||
                (supportFragmentManager?.findFragmentByTag(AddPersonalDetailsFragment::class.java.simpleName) == null &&
                        supportFragmentManager?.findFragmentByTag(AddBankDetailsFragment::class.java.simpleName) == null &&
                        supportFragmentManager?.findFragmentByTag(AddMulkiyanDetailsFragment::class.java.simpleName) == null &&
                        supportFragmentManager?.findFragmentByTag(OtpFragment::class.java.simpleName)!= null)){
            super.onBackPressed()
        }
        else{
            supportFragmentManager?.popBackStack("backstack",android.support.v4.app.FragmentManager.POP_BACK_STACK_INCLUSIVE)
        }
    }

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(LocaleManager.setLocale(newBase))
    }
}
