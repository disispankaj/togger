package com.buraq24.driver.ui.home.serviceAfterAccept

import com.buraq24.driver.webservices.RestClient
import com.buraq24.getApiError
import com.buraq24.utilities.basearc.BasePresenterImpl
import com.google.android.gms.maps.model.LatLng
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ServiceAfterAcceptPresenter : BasePresenterImpl<ServiceAfterAcceptContract.View>(), ServiceAfterAcceptContract.Presenter {
    override fun startRequest(lalLng: LatLng, orderId: Long, distance: String) {
        getView()?.showLoader(true)
        RestClient.get().startService(lalLng.latitude, lalLng.longitude, distance.toDouble(), orderId).enqueue(object : Callback<Any> {

            override fun onResponse(call: Call<Any>?, response: Response<Any>?) {
                getView()?.showLoader(false)
                if (response?.isSuccessful == true) {
                    getView()?.onStartApiSuccess()
                } else {
                    val errorModel = getApiError(response?.errorBody()?.string())
                    getView()?.handleApiError(errorModel.statusCode, errorModel.msg)
                }
            }

            override fun onFailure(call: Call<Any>?, t: Throwable?) {
                getView()?.showLoader(false)
                getView()?.apiFailure()
            }
        })

    }

    override fun rejectRequest(lalLng: LatLng, orderId: Long, reason: String) {
        getView()?.showLoader(true)
        RestClient.get().cancelService(lalLng.latitude, lalLng.longitude, orderId, reason).enqueue(object : Callback<Any> {

            override fun onResponse(call: Call<Any>?, response: Response<Any>?) {
                getView()?.showLoader(false)
                if (response?.isSuccessful == true) {
                    getView()?.onRejectApiSuccess()
                } else {
                    //val errorModel = getApiError(response?.errorBody()?.string())
                    getView()?.handleApiError(response?.code(), response?.message())
                }
            }

            override fun onFailure(call: Call<Any>?, t: Throwable?) {
                getView()?.showLoader(false)
                getView()?.apiFailure()
            }
        })
    }

}