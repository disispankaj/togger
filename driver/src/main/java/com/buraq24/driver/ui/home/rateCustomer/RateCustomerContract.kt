package com.buraq24.driver.ui.home.rateCustomer

import com.buraq24.utilities.basearc.BasePresenter
import com.buraq24.utilities.basearc.BaseView
import com.google.android.gms.maps.model.LatLng

class RateCustomerContract{

    interface View: BaseView{
        fun onApiSuccess()
    }

    interface Presenter: BasePresenter<View>{
        fun rateOrder(rating: Double,comments: String, orderId: Long)
    }
}