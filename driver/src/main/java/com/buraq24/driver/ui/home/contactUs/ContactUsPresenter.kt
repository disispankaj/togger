package com.buraq24.driver.ui.home.contactUs

import com.buraq24.driver.webservices.RestClient
import com.buraq24.getApiError
import com.buraq24.utilities.basearc.BasePresenterImpl
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ContactUsPresenter : BasePresenterImpl<ContactUsContract.View>(), ContactUsContract.Presenter {
    override fun sendMsg(msg: String) {
        getView()?.showLoader(true)
        RestClient.get().sendMessage(msg).
                enqueue(object : Callback<Any> {
                    override fun onResponse(call: Call<Any>?, response: Response<Any>?) {
                        getView()?.showLoader(false)
                        if (response?.isSuccessful == true) {
                            getView()?.onApiSuccess()
                        }else {
                            val errorModel = getApiError(response?.errorBody()?.string())
                            getView()?.handleApiError(errorModel.statusCode, errorModel.msg)
                        }
                    }

                    override fun onFailure(call: Call<Any>?, t: Throwable?) {
                        getView()?.showLoader(false)
                        getView()?.apiFailure()
                    }
                })
    }


}