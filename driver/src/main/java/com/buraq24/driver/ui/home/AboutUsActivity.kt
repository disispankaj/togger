package com.buraq24.driver.ui.home

import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.buraq24.driver.BuildConfig
import com.buraq24.driver.R
import com.buraq24.utilities.LocaleManager
import kotlinx.android.synthetic.main.activity_about_us.*

class AboutUsActivity: AppCompatActivity(){
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_about_us)
        toolbarAbout.setNavigationOnClickListener {
            finish()
        }

//        tvVersion.text = getString(R.string.version_app)+" - ${BuildConfig.VERSION_NAME}"
    }

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(LocaleManager.setLocale(newBase))
    }
}