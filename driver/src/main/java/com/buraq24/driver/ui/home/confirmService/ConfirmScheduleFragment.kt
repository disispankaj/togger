package com.buraq24.driver.ui.home.confirmService


import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.buraq24.driver.R
import com.buraq24.driver.utils.getFinalCharge
import com.buraq24.driver.utils.getRatingSymbol
import com.buraq24.driver.utils.signOut
import com.buraq24.driver.webservices.models.order.Order
import com.buraq24.showSWWerror
import com.buraq24.showSnack
import com.buraq24.utilities.*
import com.buraq24.utilities.constants.PREF_CURRENT_LAT
import com.buraq24.utilities.constants.PREF_CURRENT_LNG
import com.google.android.gms.maps.model.LatLng
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_accept_reject.*
import org.json.JSONArray
import org.json.JSONObject


class ConfirmScheduleFragment : Fragment(), ConfirmScheduleContract.View {
    override fun onConfirmCancelApiSuccess() {
        activity?.supportFragmentManager?.beginTransaction()?.remove(this)
                ?.commitAllowingStateLoss()
    }

    private lateinit var onConfirmRequest: OnConfirmRequest
    override fun polyLine(jsonRootObject: JSONObject) {
        val routeArray = jsonRootObject.getJSONArray("routes")
        if((routeArray?.length()?:0)>0) {
            val routes = routeArray.getJSONObject(0)
            if ((routes.get("legs") as JSONArray).get(0) != null) {
                //SharedPrefs.with(activity).save(PREF_DISTANCE, estimatedDistance.div(1000).toInt())
                //orderDetail.order_distance =estimatedDistance.div(1000).toInt().toString()
                val distance = (((routes.get("legs") as JSONArray).get(0) as JSONObject).get("distance") as JSONObject).get("text") as String
                val time = (((routes.get("legs") as JSONArray).get(0) as JSONObject).get("duration") as JSONObject).get("text") as String
                tvDistanceStartRequest?.text = distance
                tvTimeStartRequest?.text = time
            }
        }

    }

    override fun onConfirmAcceptApiSuccess() {
        if(orderDetail.future == "1"){

            orderDetail.order_status = OrderStatus.DRIVER_APPROVED
            //result?.brand?.name = orderDetail.brand?.brand_name
            onConfirmRequest.confirmAccepted(orderDetail)

            activity?.supportFragmentManager?.beginTransaction()?.remove(this)
                    ?.commitAllowingStateLoss()
        }

    }

    private var dialogIndeterminate: DialogIndeterminate? = null
    private val presenter = ConfirmSchedulePresenter()
    private lateinit var orderDetail: Order

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_accept_reject, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.attachView(this)
        dialogIndeterminate = DialogIndeterminate(view.context)
        orderDetail = Gson().fromJson(arguments?.getString("order"), Order::class.java)
        setData()
        val lat= SharedPrefs.with(activity).getFloat(PREF_CURRENT_LAT,
                0.0f)
        val long =SharedPrefs.with(activity).
                getFloat(PREF_CURRENT_LNG,0.0f)
        val destLatLong = LatLng(orderDetail.pickup_latitude ?: 0.0, orderDetail.pickup_longitude
                ?: 0.0)
        presenter.drawPolyLine(lat.toDouble(),long.toDouble(),destLatLong.latitude,destLatLong.longitude)
        progressBar.visibility = View.VISIBLE
        setListeners()
    }

    private fun setData() {
        val services= resources.getStringArray(R.array.services_list)
        tvProductName.text = if (orderDetail.category_id == CategoryId.GAS) {
            tvQuantity.text = "${orderDetail.brand?.name} x ${orderDetail.payment?.productQuantity}"
            services[1]
        } else if (orderDetail.category_id == CategoryId.MINERAL_WATER){
            tvQuantity.text = "${orderDetail.brand?.name} x ${orderDetail.payment?.productQuantity}"
            orderDetail.brand?.brand_name?:services[2]
        } else if(orderDetail.category_id == CategoryId.WATER_TANKER) {
            tvQuantity.text = "${orderDetail.brand?.name} x ${orderDetail.payment?.productQuantity}"
            services[3]
        }else if(orderDetail.category_id == CategoryId.FREIGHT || orderDetail.category_id == CategoryId.FREIGHT_2) {
            tvQuantity.text = orderDetail.brand?.name
            orderDetail.brand?.brand_name?:services[4]
        }
        else{
            tvQuantity.text = "${orderDetail.brand?.name} x ${orderDetail.payment?.productQuantity}"
            orderDetail.brand?.brand_name?:services[0]
        }
        if(orderDetail.future == "1"){
            rlSchedule.visibility = View.VISIBLE
            tvScheduleDate.text = DateUtils.getFormattedTimeForBooking(orderDetail.order_timings?:"")
        }
        else{
            rlSchedule.visibility = View.GONE
        }

        tvLocation.text = orderDetail.pickup_address
        tvName.text = orderDetail.user?.name
        ivRating.setImageResource(getRatingSymbol(orderDetail.user?.rating_avg?.toInt()?:0))
        textView.text = orderDetail.user?.rating_count?.toString()?:"0"
        tvTotal.text =getFinalCharge(orderDetail, 0)+" "+getString(R.string.currency)

    }


    private fun setListeners() {
        val lat= SharedPrefs.with(activity).getFloat(PREF_CURRENT_LAT,
               0.0f)
        val long =SharedPrefs.with(activity).
                getFloat(PREF_CURRENT_LNG,0.0f)
        val latlng= LatLng(lat.toDouble(),long.toDouble())
        tvAccept.setOnClickListener {
            if (CheckNetworkConnection.isOnline(activity)) {
                presenter.confirmScheduleRequest(latlng,orderDetail.order_id?.toLong()?:0,1)
            }
        }
        tvCancel.setOnClickListener {
            if (CheckNetworkConnection.isOnline(activity)) {
                presenter.confirmScheduleRequest(latlng,orderDetail.order_id?.toLong()?:0,0)
            }
        }
    }

    override fun showLoader(isLoading: Boolean) {
        dialogIndeterminate?.show(isLoading)
    }

    override fun apiFailure() {
        view?.showSWWerror()
    }

    override fun handleApiError(code: Int?, error: String?) {
        if (code == StatusCode.UNAUTHORIZED) {
            signOut(activity)
            return
        }
        view?.showSnack(error.toString())
        activity?.supportFragmentManager?.popBackStackImmediate()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        presenter.detachView()
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        onConfirmRequest = context as OnConfirmRequest
    }
    interface OnConfirmRequest{
        fun confirmAccepted(order: Order?)
    }
}
