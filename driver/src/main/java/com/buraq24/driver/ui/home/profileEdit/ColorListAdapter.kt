package com.buraq24.driver.ui.home.profileEdit

import android.graphics.Color
import android.graphics.PorterDuff
import android.graphics.PorterDuffColorFilter
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.GradientDrawable
import android.graphics.drawable.ShapeDrawable
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.buraq24.driver.R
import kotlinx.android.synthetic.main.item_car_color.view.*


class ColorListAdapter : RecyclerView.Adapter<ColorListAdapter.ViewHolder>() {


    private val mColors = listOf("#FFFFFF", "#000000", "#B3B3B3", "#BB0A30", "#B6B1A9", "#999999")

    private var mPosition: Int = -1

    private var mcallback: Colorcallback? = null

    fun settingcallback(mcallback: Colorcallback) {
        this.mcallback = mcallback
    }

    fun notifyAdapter(color: String) {
        mPosition = mColors.indexOf(color)


        if (mPosition != -1) {
            mcallback?.getColor(mColors[mPosition])
            notifyDataSetChanged()
        }

    }


    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ViewHolder {

        return ViewHolder(LayoutInflater.from(viewGroup.context).inflate(R.layout.item_car_color, viewGroup, false))
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, i: Int) {

        val pos = viewHolder.adapterPosition

        viewHolder.onBind(pos)

        viewHolder.checkStatus(mPosition)


    }

    override fun getItemCount(): Int {
        return mColors.size
    }

    interface Colorcallback {

        fun getColor(color: String)
    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        init {

            itemView.setOnClickListener {

                mPosition = adapterPosition

                mcallback?.getColor(mColors[mPosition])

                notifyDataSetChanged()


            }
        }

        fun onBind(position: Int) {

            when (val background = itemView.background) {
                is ShapeDrawable -> {
                    // cast to 'ShapeDrawable'
                    background.paint.color = Color.parseColor(mColors[position])
                }
                is GradientDrawable -> {
                    // cast to 'GradientDrawable'
                    background.setColor(Color.parseColor(mColors[position]))
                }
                is ColorDrawable -> {
                    // alpha value may need to be set again after this call
                    background.color = Color.parseColor(mColors[position])
                }
            }
        }

        fun checkStatus(position: Int) {
            if (position == adapterPosition) {
                itemView.iv_selection.visibility = View.VISIBLE
                val greyFilter = PorterDuffColorFilter(Color.LTGRAY, PorterDuff.Mode.MULTIPLY)
                itemView.background.colorFilter = greyFilter
            } else {
                itemView.iv_selection.visibility = View.INVISIBLE
                val greyFilter = PorterDuffColorFilter(Color.WHITE, PorterDuff.Mode.MULTIPLY)
                itemView.background.colorFilter = greyFilter
            }

        }


    }

}
