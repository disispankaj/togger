package com.buraq24.driver.ui.home.confirmService

import com.buraq24.utilities.basearc.BasePresenter
import com.buraq24.utilities.basearc.BaseView
import com.google.android.gms.maps.model.LatLng
import org.json.JSONObject

class ConfirmScheduleContract{

    interface View: BaseView{
        fun onConfirmAcceptApiSuccess()
        fun onConfirmCancelApiSuccess()
        fun polyLine(jsonRootObject: JSONObject)
    }

    interface Presenter: BasePresenter<View>{
        fun confirmScheduleRequest(lalLng: LatLng, orderId: Long,approve: Int)
        fun drawPolyLine(sourceLat: Double, sourceLong: Double, destLat: Double, destLong: Double)
    }
}