package com.buraq24.driver.ui.signup.verifytop


import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import com.buraq24.driver.R
import com.buraq24.driver.ui.home.HomeActivity
import com.buraq24.driver.ui.signup.enterdetails.AddPersonalDetailsFragment
import com.buraq24.driver.webservices.models.SendOtp
import com.buraq24.utilities.*
import com.buraq24.utilities.constants.*
import com.buraq24.utilities.webservices.models.LoginModel
import com.google.firebase.iid.FirebaseInstanceId
import kotlinx.android.synthetic.main.activity_otp.*
import java.util.*
import java.util.regex.Pattern
import android.view.inputmethod.InputMethodManager
import com.buraq24.Utils
import com.buraq24.driver.ui.signup.SignupActivity
import com.buraq24.getFormatFromDateUtc
import com.buraq24.showSWWerror
import com.buraq24.showSnack
import io.fabric.sdk.android.services.common.CommonUtils.hideKeyboard

class OtpFragment : Fragment(), VerifyOtpContract.View {

    override fun onOtpApiSuccess(response: SendOtp?) {
        startTimer()
        ACCESS_TOKEN = response?.access_token ?: ""
        SharedPrefs.with(activity).save(ACCESS_TOKEN_KEY, ACCESS_TOKEN)
        fabNext.showSnack(getString(R.string.otp_resent))
    }

    private val presenter = VerifyOtpPresenter()
    //    private var smsVerifyCatcher: SmsVerifyCatcher? = null
    private lateinit var dialogIndeterminate: DialogIndeterminate


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        activity?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)
        return inflater.inflate(R.layout.activity_otp, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        pinView.isFocusable = true
        pinView.isFocusableInTouchMode = true
        pinView.requestFocus()
//        openKeyboardManually(context,view)
        presenter.attachView(this)
        startTimer()
        dialogIndeterminate = DialogIndeterminate(activity)
        pinView.setAnimationEnable(true)
        tvPhone.text = String.format(Locale.US, "%s\n%s", getString(R.string.enter_otp_msg), arguments?.getString("phone_number"))
        setListeners()
    }

    private fun openKeyboardManually(context: Context?, view: View) {
        val inputMethodManager = activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager
        inputMethodManager?.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)
    }

    private fun setListeners() {
        fabNext.setOnClickListener {
            if (pinView.text.toString().length < 4) {
                it.showSnack(getString(R.string.otp_validation_message))
            } else {
                hideKeyboard(context, view)
                val map = HashMap<String, String>()
                map["otp"] = pinView.text.toString()
                presenter.verifyOtp(map)
            }
        }
        tvEditNumber.setOnClickListener {
            fragmentManager?.popBackStackImmediate()
        }
        ivBack.setOnClickListener {
            fragmentManager?.popBackStackImmediate()
        }
        tvResend.setOnClickListener {
            val lat = SharedPrefs.with(activity).getFloat(Constants.LATITUDE, 0.0f)
            val lng = SharedPrefs.with(activity).getFloat(Constants.LONGITUDE, 0.0f)
            val map = HashMap<String, String>()
            map["phone_code"] = arguments?.getString("phone_code") ?: ""
            map["phone_number"] = arguments?.getString("number") ?: ""
            map["timezone"] = TimeZone.getDefault().id
            map["latitude"] = lat.toString()
            map["longitude"] = lng.toString()
            FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener {
                map["fcm_id"] = it?.token ?: ""
                presenter.sendOtpApiCall(map)
            }
        }
    }

    private fun startTimer() {
        pinView.setText("")
        tvResend.text = ""
        tvResend.isEnabled = false
        countDownTimer.start()
    }

    private val countDownTimer = object : CountDownTimer(120000, 1000) {

        override fun onTick(millisUntilFinished: Long) {
            tvResend?.text = getFormatFromDateUtc(Date(millisUntilFinished), "mm:ss")
        }

        override fun onFinish() {
            tvResend?.text = getString(R.string.resend_otp)
            tvResend?.isEnabled = true
        }
    }

    private fun parseCode(message: String): String {
        val p = Pattern.compile("\\b\\d{4}\\b")
        val m = p.matcher(message)
        var code = ""
        while (m.find()) {
            code = m.group(0)
        }
        return code
    }

    override fun onApiSuccess(response: LoginModel?) {
        if (response?.AppDetail?.approved == "0" && response.AppDetail?.vehicle_name?.isNotEmpty() == true) {
            val dialog = AlertDialogUtil.getInstance().createOkCancelDialog(activity,
                    R.string.alert,
                    R.string.approval_pending_msg,
                    R.string.okay,
                    0,
                    true,
                    object : AlertDialogUtil.OnOkCancelDialogListener {
                        override fun onOkButtonClicked() {
                            SharedPrefs.with(context).removeAllSharedPrefsChangeListeners()
                            SharedPrefs.with(context).removeAll()
                            (context as? Activity)?.finishAffinity()
                            context?.startActivity(Intent(context, SignupActivity::class.java))
                        }

                        override fun onCancelButtonClicked() {
                            // Do nothing
                        }
                    })
            dialog.show()
            return
        }
        ACCESS_TOKEN = response?.AppDetail?.access_token ?: ""
        SharedPrefs.with(activity).save(PROFILE, response?.AppDetail)
        SharedPrefs.with(activity).save(SERVICES, response)
        SharedPrefs.with(activity).save(ACCESS_TOKEN_KEY, ACCESS_TOKEN)
        if (response?.AppDetail?.user?.name?.isEmpty() == true) {
            SharedPrefs.with(context).save(STEP, 1)
            activity?.supportFragmentManager?.let { it1 ->
                Utils.replaceFragment(it1, AddPersonalDetailsFragment(),
                        R.id.container, AddPersonalDetailsFragment::class.java.simpleName)
            }
        } else {
            SharedPrefs.with(context).save(STEP, 0)
            activity?.finish()
            startActivity(Intent(activity, HomeActivity::class.java))
        }
    }

    override fun showLoader(isLoading: Boolean) {
        dialogIndeterminate.show(isLoading)
    }

    override fun apiFailure() {
        fabNext.showSWWerror()
    }

    override fun handleApiError(code: Int?, error: String?) {
        fabNext.showSnack(error.toString())
    }

//    override fun onStart() {
//        super.onStart()
//        smsVerifyCatcher?.onStart()
//    }

//    override fun onStop() {
//        super.onStop()
//        smsVerifyCatcher?.onStop()
//    }

    override fun onDestroyView() {
        super.onDestroyView()
        hideKeyboard(context, view)
        presenter.detachView()
    }

//    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
//        smsVerifyCatcher?.onRequestPermissionsResult(requestCode, permissions, grantResults)
//    }
}
