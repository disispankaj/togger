package com.buraq24.driver.ui.home.etokens.deliver

import com.buraq24.driver.webservices.ApiResponse
import com.buraq24.driver.webservices.RestClient
import com.buraq24.driver.webservices.models.order.Order
import com.buraq24.getApiError
import com.buraq24.utilities.basearc.BasePresenterImpl
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DeliverPresenter : BasePresenterImpl<DeliverContract.View>(), DeliverContract.Presenter {

    override fun deliverOrderApi(map: HashMap<String, String?>) {
        getView()?.showLoader(true)
        RestClient.get().endWaterService(map).enqueue(object : Callback<ApiResponse<Order>> {
            override fun onResponse(call: Call<ApiResponse<Order>>?, response: Response<ApiResponse<Order>>?) {
                getView()?.showLoader(false)
                if (response?.isSuccessful == true) {
                    getView()?.onApiSuccess(response.body()?.result)
                } else {
                    val errorModel = getApiError(response?.errorBody()?.string())
                    getView()?.handleApiError(errorModel.statusCode, errorModel.msg)
                }
            }

            override fun onFailure(call: Call<ApiResponse<Order>>?, t: Throwable?) {
                getView()?.showLoader(false)
                getView()?.apiFailure()
            }
        })
    }

}