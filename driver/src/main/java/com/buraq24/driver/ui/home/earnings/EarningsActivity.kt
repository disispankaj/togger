package com.buraq24.driver.ui.home.earnings

import android.content.Context
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import com.buraq24.driver.R
import com.buraq24.driver.utils.signOut
import com.buraq24.driver.webservices.models.StartEndDateModel
import com.buraq24.driver.webservices.models.earning.EarningResultModel
import com.buraq24.utilities.DateUtils.getFormattedTimeForHistory2
import com.buraq24.utilities.Day
import com.buraq24.utilities.DialogIndeterminate
import com.buraq24.utilities.LocaleManager
import com.buraq24.utilities.StatusCode
import com.buraq24.utilities.constants.ORDER_COMPLETED
import com.github.mikephil.charting.components.AxisBase
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter
import com.github.mikephil.charting.highlight.Highlight
import com.github.mikephil.charting.listener.OnChartValueSelectedListener
import kotlinx.android.synthetic.main.activity_earnings.*
import java.text.DecimalFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.LinkedHashMap

class EarningsActivity : AppCompatActivity(), OnChartValueSelectedListener, EarningsContract.View, WeekAdapter.OnclickWeek {

    private var dataSet: BarDataSet? = null
    private var weeksList: ArrayList<String>? = null

    private var startEndDateList: ArrayList<StartEndDateModel>? = null
    private var graphList: ArrayList<String>? = null
    private var startDateForApi: String? = null
    private var endDateForApi: String? = null
    private var yearChoose: String? = null
    private var entries: ArrayList<BarEntry>? = null
    private var year = 2018
    private var month = 1
    private val earningsPresenter = EarningsPresenter()
    private lateinit var earningList: ArrayList<EarningResultModel?>
    private lateinit var earningAdapter: EarningAdapter
    private var dialogIndeterminate: DialogIndeterminate? = null
    var xAxisLabel: ArrayList<String> = ArrayList<String>()
    lateinit var dayMap: LinkedHashMap<String, ArrayList<EarningResultModel?>>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_earnings)
        setXAxis()
        earningsPresenter.attachView(this)
        dialogIndeterminate = DialogIndeterminate(this)
        setListener()
        setMonthYear()
        setWeekData(month, year)
        initAdapter()
        earningsPresenter.getEarnings(startEndDateList?.get(0)?.startDate + " 00:00:00",
                startEndDateList?.get(0)?.endDate + " 00:00:00")

    }

    private fun setListener() {
        toolbarProfile.setNavigationOnClickListener {
            onBackPressed()
        }
    }

    private fun setMonthYear() {
        val calendar = Calendar.getInstance()
        year = calendar.get(Calendar.YEAR)
        month = calendar.get(Calendar.MONTH)
    }

    private fun setWeekData(month: Int, year: Int) {
        val calendar = Calendar.getInstance()
        val dateFormat = SimpleDateFormat("d MMM yyyy", Locale.getDefault())
        dateFormat.timeZone = TimeZone.getDefault()
        calendar.set(Calendar.YEAR, year)
        calendar.set(Calendar.MONTH, month)
        calendar.set(Calendar.DAY_OF_MONTH, 1)
        val simpleDateFormat = SimpleDateFormat("MMM")
        val monthName = simpleDateFormat.format(calendar.time)
        val day = Calendar.SUNDAY - calendar.get(Calendar.DAY_OF_WEEK)
        if (day < 0) {
            calendar.add(Calendar.DATE, 7 + day)
        } else {
            calendar.add(Calendar.DATE, day)
        }
        val days = calendar.getActualMaximum(Calendar.DAY_OF_MONTH)
        val start = dateFormat.format(calendar.time)
        var end = days.toString() + " " + monthName + " " + year
//        if (Calendar.getInstance().get(Calendar.MONTH) == month) {
//            end = calendar.get(Calendar.DAY_OF_MONTH).toString() + " " + monthName + " " + year
//        }
        setWeekRecyclerView(start, end)
    }

    fun setXAxis() {
        xAxisLabel.add(Day.SUN)
        xAxisLabel.add(Day.MON)
        xAxisLabel.add(Day.TUE)
        xAxisLabel.add(Day.WED)
        xAxisLabel.add(Day.THU)
        xAxisLabel.add(Day.FRI)
        xAxisLabel.add(Day.SAT)
    }

    fun initAdapter() {
        earningList = ArrayList<EarningResultModel?>()
        earningAdapter = EarningAdapter(earningList)
        rvDataList.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        rvDataList.adapter = earningAdapter
        rvDataList.isNestedScrollingEnabled = false
    }

    private fun setWeekRecyclerView(start: String, end: String) {
        weeksList = ArrayList()
        startEndDateList = ArrayList<StartEndDateModel>()
        val calendar = Calendar.getInstance()
        val dateFormat = SimpleDateFormat("d MMM yyyy", Locale.getDefault())
        dateFormat.timeZone = TimeZone.getDefault()

        val dateFormotApi = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())

        try {
            val startDate = dateFormat.parse(start)
            val endDate = dateFormat.parse(end)

            startDateForApi = dateFormotApi.format(startDate).toString()

            calendar.time = startDate
            val calendarFormat = SimpleDateFormat("d MMM", Locale.getDefault())
            val apiFormatDate = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())

            while (calendar.time.time <= endDate.time) {
                var model = StartEndDateModel()
                var date = calendarFormat.format(calendar.time).toString() + "-"

                model.startDate = apiFormatDate.format(calendar.time).toString()  // for startdate of weeek

                calendar.add(Calendar.DATE, 6)

                model.endDate = apiFormatDate.format(calendar.time).toString()  // for enddate week

                model.isSelected = (startEndDateList?.size ?: 0) <= 0
                startEndDateList?.add(model)

                endDateForApi = dateFormotApi.format(dateFormat.parse(dateFormat.format(calendar.time))).toString()
                date = date + calendarFormat.format(calendar.time).toString()
                weeksList?.add(date)
                calendar.add(Calendar.DATE, 1)

            }
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        rvWeekList.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        rvWeekList.adapter = WeekAdapter(weeksList, startEndDateList, this)
        rvWeekList.smoothScrollToPosition((weeksList?.size ?: 1) - 1)
    }


    private fun setUpBarGraph() {
        entries = ArrayList<BarEntry>()

        for (i in xAxisLabel.indices) {
            when (xAxisLabel.get(i)) {
                Day.SUN -> {
                    if (!((dayMap[Day.SUN]?.size ?: 0) > 0))
                        entries?.add(BarEntry(i.toFloat(), (0).toFloat()))
                    else {
                        var dayEarning = 0.0F
                        for (model in (dayMap[Day.SUN] ?: ArrayList<EarningResultModel>())) {
                            dayEarning = dayEarning + (model?.final_charge)!!.toFloat()
                        }
                        entries?.add(BarEntry(i.toFloat(), dayEarning))
                    }
                }
                Day.MON -> {
                    if (!((dayMap[Day.MON]?.size ?: 0) > 0))
                        entries?.add(BarEntry(i.toFloat(), (0).toFloat()))
                    else {
                        var dayEarning = 0.0F
                        for (model in (dayMap[Day.MON] ?: ArrayList<EarningResultModel>())) {
                            dayEarning = dayEarning + (model?.final_charge)!!.toFloat()
                        }
                        entries?.add(BarEntry(i.toFloat(), dayEarning))
                    }
                }
                Day.TUE -> {
                    if (!((dayMap[Day.TUE]?.size ?: 0) > 0))
                        entries?.add(BarEntry(i.toFloat(), (0).toFloat()))
                    else {
                        var dayEarning = 0.0F
                        for (model in (dayMap[Day.TUE] ?: ArrayList<EarningResultModel>())) {
                            dayEarning = dayEarning + (model?.final_charge)!!.toFloat()
                        }
                        entries?.add(BarEntry(i.toFloat(), dayEarning))
                    }
                }
                Day.WED -> {
                    if (!((dayMap[Day.WED]?.size ?: 0) > 0))
                        entries?.add(BarEntry(i.toFloat(), (0).toFloat()))
                    else {
                        var dayEarning = 0.0F
                        for (model in (dayMap[Day.WED] ?: ArrayList<EarningResultModel>())) {
                            dayEarning = dayEarning + (model?.final_charge)!!.toFloat()
                        }
                        entries?.add(BarEntry(i.toFloat(), dayEarning))
                    }

                }
                Day.THU -> {
                    if (!((dayMap[Day.THU]?.size ?: 0) > 0))
                        entries?.add(BarEntry(i.toFloat(), (0).toFloat()))
                    else {
                        var dayEarning = 0.0F
                        for (model in (dayMap[Day.THU] ?: ArrayList<EarningResultModel>())) {
                            dayEarning = dayEarning + (model?.final_charge)!!.toFloat()
                        }
                        entries?.add(BarEntry(i.toFloat(), dayEarning))
                    }

                }
                Day.FRI -> {
                    if (!((dayMap[Day.FRI]?.size ?: 0) > 0))
                        entries?.add(BarEntry(i.toFloat(), (0).toFloat()))
                    else {
                        var dayEarning = 0.0F
                        for (model in (dayMap[Day.FRI] ?: ArrayList<EarningResultModel>())) {
                            dayEarning = dayEarning + (model?.final_charge)!!.toFloat()
                        }
                        entries?.add(BarEntry(i.toFloat(), dayEarning))
                    }

                }
                Day.SAT -> {
                    if (!((dayMap[Day.SAT]?.size ?: 0) > 0))
                        entries?.add(BarEntry(i.toFloat(), (0).toFloat()))
                    else {
                        var dayEarning = 0.0F
                        for (model in (dayMap[Day.SAT] ?: ArrayList<EarningResultModel>())) {
                            dayEarning = dayEarning + (model?.final_charge)!!.toFloat()
                        }
                        entries?.add(BarEntry(i.toFloat(), dayEarning))
                    }
                }
            }
        }

        barChart.invalidate()
        setYChartAxis()
        setXChartAxis()
        setBarData()
        setBarDecor()
    }

    private fun setYChartAxis() {
        val yAxis = barChart.axisLeft
        yAxis.setDrawLabels(false)
        yAxis.setDrawAxisLine(false)
        yAxis.setDrawGridLines(false)
        yAxis.axisMinimum = 0f
        yAxis.textColor = ActivityCompat.getColor(this, R.color.white)
    }

    private fun setXChartAxis() {
        val quarters1 = arrayOfNulls<String>(xAxisLabel.size)
        for (i in xAxisLabel.indices) {
            quarters1[i] = xAxisLabel.get(i)
        }
        val formatter1 = object : IndexAxisValueFormatter() {
            override fun getFormattedValue(value: Float, axis: AxisBase): String? {
                return xAxisLabel.get(value.toInt())
            }
        }
        barChart.axisRight.isEnabled = false
        val xAxis1 = barChart.xAxis
        xAxis1.setDrawGridLines(false)
        xAxis1.valueFormatter = formatter1
        xAxis1.position = XAxis.XAxisPosition.BOTTOM
        xAxis1.setAvoidFirstLastClipping(false)
        xAxis1.textColor = ActivityCompat.getColor(this, R.color.white)
    }

    private fun setBarData() {
        dataSet = BarDataSet(entries, "")
        dataSet?.color = ContextCompat.getColor(this, R.color.white)
        val data = BarData(dataSet)
        data.setValueTextColor(ContextCompat.getColor(this, R.color.white))
        data.setDrawValues(true)
        data.barWidth = .40f
        barChart.data = data
    }


    private fun setBarDecor() {
        barChart.legend.isEnabled = false
        barChart.description = null
        barChart.animateY(2000)
        barChart.animateX(2000)
        barChart.isDoubleTapToZoomEnabled = false
        barChart.setScaleEnabled(false)
        barChart.setDrawValueAboveBar(true)
        barChart.setVisibleXRangeMaximum(10f)
//         if (firstCount == 0) {
//             barChart.moveViewToX(firstCount.toFloat())
//         } else {
//             barChart.moveViewToX((firstCount - 1).toFloat())
//         }
        barChart.setOnChartValueSelectedListener(this)
    }

    override fun onDestroy() {
        earningsPresenter.detachView()
        super.onDestroy()
    }

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(LocaleManager.setLocale(newBase))
    }

    override fun onApiSuccess(response: ArrayList<EarningResultModel?>) {
        earningList.clear()
        graphList = ArrayList<String>()
        graphList?.clear()
        earningList.addAll(response)
        earningAdapter.notifyDataSetChanged()
        setData(response)
        dayEarningCategory(response)
        setUpBarGraph()
    }

    override fun showLoader(isLoading: Boolean) {
        dialogIndeterminate?.show(isLoading)
    }

    override fun apiFailure() {

    }

    override fun handleApiError(code: Int?, error: String?) {
        if (code == StatusCode.UNAUTHORIZED) {
            signOut(this)
            return
        }
    }

    override fun onNothingSelected() {

    }

    override fun onValueSelected(e: Entry?, h: Highlight?) {

    }

    fun setData(response: ArrayList<EarningResultModel?>) {
        var weeklyEarning = 0.0F
        for (model in response) {
            if ((model?.Order?.order_status ?: "").equals(ORDER_COMPLETED)) {
                weeklyEarning = weeklyEarning + (model?.final_charge)!!.toFloat()
            }
            graphList?.add(model?.created_at ?: "")
        }
        tvBookingStatus.text = "${DecimalFormat("0.00").format(weeklyEarning)} ${getString(R.string.currency)}"
    }

    fun dayEarningCategory(response: ArrayList<EarningResultModel?>) {

        dayMap = LinkedHashMap<String, ArrayList<EarningResultModel?>>()
        var dayListDummy = ArrayList<EarningResultModel?>()

        for (key in 0..(xAxisLabel.size - 1)) {
            dayMap.put(xAxisLabel.get(key), dayListDummy)
        }

        for (i in 0..(response.size - 1)) {

            when (getFormattedTimeForHistory2(response.get(i)?.created_at ?: "")) {
                Day.SUN,"dom." -> {
                    var dayList = ArrayList<EarningResultModel?>()
                    dayList.clear()
                    for (j in 0..(response.size - 1)) {
                        if (getFormattedTimeForHistory2(response.get(j)?.created_at
                                        ?: "") == Day.SUN) {

                            if ((response.get(j)?.Order?.order_status
                                            ?: "").equals(ORDER_COMPLETED)) {
                                dayList.add(response.get(j))
                            }
                        }
                    }
                    dayMap.put(Day.SUN, dayList)
                }
                Day.MON,"lun." -> {
                    var dayList = ArrayList<EarningResultModel?>()
                    dayList.clear()
                    for (j in 0..(response.size - 1)) {
                        if (getFormattedTimeForHistory2(response.get(j)?.created_at
                                        ?: "") == Day.MON) {
                            if ((response.get(j)?.Order?.order_status
                                            ?: "").equals(ORDER_COMPLETED)) {
                                dayList.add(response.get(j))
                            }
                        }
                    }
                    dayMap.put(Day.MON, dayList)
                }
                Day.TUE,"mar." -> {
                    var dayList = ArrayList<EarningResultModel?>()
                    dayList.clear()
                    for (j in 0..(response.size - 1)) {
                        if (getFormattedTimeForHistory2(response.get(j)?.created_at
                                        ?: "") == Day.TUE) {
                            if ((response.get(j)?.Order?.order_status
                                            ?: "").equals(ORDER_COMPLETED)) {
                                dayList.add(response.get(j))
                            }
                        }
                    }
                    dayMap.put(Day.TUE, dayList)
                }
                Day.WED,"mié." -> {
                    var dayList = ArrayList<EarningResultModel?>()
                    dayList.clear()
                    for (j in 0..(response.size - 1)) {
                        if (getFormattedTimeForHistory2(response.get(j)?.created_at
                                        ?: "") == Day.WED) {
                            if ((response.get(j)?.Order?.order_status
                                            ?: "").equals(ORDER_COMPLETED)) {
                                dayList.add(response.get(j))
                            }
                        }
                    }
                    dayMap.put(Day.WED, dayList)
                }
                Day.THU,"jue." -> {
                    var dayList = ArrayList<EarningResultModel?>()
                    dayList.clear()
                    for (j in 0..(response.size - 1)) {
                        if (getFormattedTimeForHistory2(response.get(j)?.created_at
                                        ?: "") == Day.THU) {
                            if ((response.get(j)?.Order?.order_status
                                            ?: "").equals(ORDER_COMPLETED)) {
                                dayList.add(response.get(j))
                            }
                        }
                    }
                    dayMap.put(Day.THU, dayList)
                }
                Day.FRI,"vie." -> {
                    var dayList = ArrayList<EarningResultModel?>()
                    dayList.clear()
                    for (j in 0..(response.size - 1)) {
                        if (getFormattedTimeForHistory2(response.get(j)?.created_at
                                        ?: "") == Day.FRI) {
                            if ((response.get(j)?.Order?.order_status
                                            ?: "").equals(ORDER_COMPLETED)) {
                                dayList.add(response.get(j))
                            }
                        }
                    }
                    dayMap.put(Day.FRI, dayList)
                }
                Day.SAT,"sáb." -> {
                    var dayList = ArrayList<EarningResultModel?>()
                    dayList.clear()
                    for (j in 0..(response.size - 1)) {
                        if (getFormattedTimeForHistory2(response.get(j)?.created_at
                                        ?: "") == Day.SAT) {
                            if ((response.get(j)?.Order?.order_status
                                            ?: "").equals(ORDER_COMPLETED)) {
                                dayList.add(response.get(j))
                            }
                        }
                    }
                    dayMap.put(Day.SAT, dayList)
                }
            }

        }
    }

    override fun onClcikWeekListener(position: Int) {
        earningsPresenter.getEarnings(startEndDateList?.get(position)?.startDate + " 00:00:00",
                startEndDateList?.get(position)?.endDate + " 00:00:00")
    }

}