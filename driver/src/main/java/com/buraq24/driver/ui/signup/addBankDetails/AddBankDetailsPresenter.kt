package com.buraq24.driver.ui.signup.addBankDetails

import com.buraq24.driver.webservices.ApiResponse
import com.buraq24.driver.webservices.RestClient
import com.buraq24.driver.webservices.models.BankDetailsResult
import com.buraq24.driver.webservices.models.RequestPersonalDetails
import com.buraq24.driver.webservices.models.homeapi.Product
import com.buraq24.getApiError
import com.buraq24.utilities.basearc.BasePresenterImpl
import com.buraq24.utilities.constants.SUCCESS_CODE
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AddBankDetailsPresenter : BasePresenterImpl<AddBankDetailsContract.View>(), AddBankDetailsContract.Presenter {

    override fun addBankDetails(requestPersonalDetails: RequestPersonalDetails) {
        getView()?.showLoader(true)
        RestClient.get().addBankDetails2(requestPersonalDetails)
                .enqueue(object : Callback<ApiResponse<BankDetailsResult>> {
            override fun onResponse(call: Call<ApiResponse<BankDetailsResult>>?, response: Response<ApiResponse<BankDetailsResult>>?) {
                getView()?.showLoader(false)
                if (response?.isSuccessful == true) {
                    if (response.body()?.statusCode == SUCCESS_CODE) {
                        getView()?.onApiSuccess(response.body()?.result)
                    } else {
                        getView()?.handleApiError(response.body()?.statusCode, response.body()?.msg)
                    }
                } else {
                    val errorModel = getApiError(response?.errorBody()?.string())
                    getView()?.handleApiError(errorModel.statusCode, errorModel.msg)
                }
            }

            override fun onFailure(call: Call<ApiResponse<BankDetailsResult>>?, t: Throwable?) {
                getView()?.showLoader(false)
                getView()?.apiFailure()
            }

        })
    }

    override fun getProducts(id: String) {
        getView()?.showLoader(true)
        RestClient.get().getProductsByBrand(id.toLong())
                .enqueue(object : Callback<ApiResponse<ArrayList<Product>>> {
                    override fun onResponse(call: Call<ApiResponse<ArrayList<Product>>>?, response: Response<ApiResponse<ArrayList<Product>>>?) {
                        getView()?.showLoader(false)
                        if (response?.isSuccessful == true) {
                            if (response.body()?.statusCode == SUCCESS_CODE) {
                                getView()?.onProductsSuccess(response.body()?.result?:ArrayList())
                            } else {
                                getView()?.handleApiError(response.body()?.statusCode, response.body()?.msg)
                            }
                        } else {
                            val errorModel = getApiError(response?.errorBody()?.string())
                            getView()?.handleApiError(errorModel.statusCode, errorModel.msg)
                        }
                    }

                    override fun onFailure(call: Call<ApiResponse<ArrayList<Product>>>?, t: Throwable?) {
                        getView()?.showLoader(false)
                        getView()?.apiFailure()
                    }

                })
    }

}