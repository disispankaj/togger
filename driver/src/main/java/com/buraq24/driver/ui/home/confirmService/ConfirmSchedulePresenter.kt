package com.buraq24.driver.ui.home.confirmService

import com.buraq24.driver.webservices.API
import com.buraq24.driver.webservices.ApiResponse
import com.buraq24.driver.webservices.RestClient
import com.buraq24.driver.webservices.models.order.Order
import com.buraq24.getApiError
import com.buraq24.utilities.basearc.BasePresenterImpl
import com.google.android.gms.maps.model.LatLng
import okhttp3.ResponseBody
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException
import java.util.*

class ConfirmSchedulePresenter : BasePresenterImpl<ConfirmScheduleContract.View>(), ConfirmScheduleContract.Presenter {
    override fun confirmScheduleRequest(lalLng: LatLng, orderId: Long, approve:Int) {
        getView()?.showLoader(true)
        RestClient.get().confirmUnConfirmService(lalLng.latitude,lalLng.longitude,orderId,approve).
                enqueue(object : Callback<ApiResponse<Order>> {

                    override fun onResponse(call: Call<ApiResponse<Order>>?, response: Response<ApiResponse<Order>>?) {
                        getView()?.showLoader(false)
                        if (response?.isSuccessful == true) {
                            if(approve ==1){
                            getView()?.onConfirmAcceptApiSuccess()
                            }
                            else{
                                getView()?.onConfirmCancelApiSuccess()
                            }
                        }else {
                            val errorModel = getApiError(response?.errorBody()?.string())
                            getView()?.handleApiError(errorModel.statusCode, errorModel.msg)
                        }
                    }

                    override fun onFailure(call: Call<ApiResponse<Order>>?, t: Throwable?) {
                        getView()?.showLoader(false)
                        getView()?.apiFailure()
                    }
                })

    }


    override fun drawPolyLine(sourceLat: Double, sourceLong: Double, destLat: Double, destLong: Double) {
        val BASE_URL_for_map = "https://maps.googleapis.com/maps/api/"
        val retrofit = Retrofit.Builder().baseUrl(BASE_URL_for_map).addConverterFactory(GsonConverterFactory.create()).build()
        val api = retrofit.create(API::class.java)
        val service = api.getPolYLine(sourceLat.toString() + "," + sourceLong,
                destLat.toString() + "," + destLong, Locale.getDefault().language)
        service.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val responsePolyline = response.body()?.string()
                        val jsonRootObject = JSONObject(responsePolyline)
                        getView()?.polyLine(jsonRootObject)
                    } catch (e: IOException) {
                        e.printStackTrace()

                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                } else {
                    val errorModel = getApiError(response.errorBody()?.string())
                    getView()?.handleApiError(errorModel.statusCode, errorModel.msg)
                }
            }

            override fun onFailure(call: Call<ResponseBody>?, t: Throwable?) {
                getView()?.showLoader(false)
                getView()?.apiFailure()
            }
        })
    }

}