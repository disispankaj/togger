package com.buraq24.driver.ui.signup.addMulkiyanDetails


import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.Activity.RESULT_OK
import android.app.FragmentManager
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.Spinner
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.buraq24.driver.ColorListArrayAdapter
import com.buraq24.driver.ImageUtils
import com.buraq24.driver.R
import com.buraq24.driver.ui.MultiSelectionSpinner
import com.buraq24.driver.ui.home.HomeActivity
import com.buraq24.driver.ui.home.profileEdit.ColorListAdapter
import com.buraq24.driver.ui.signup.SignupActivity
import com.buraq24.driver.ui.signup.addBankDetails.AddBankDetailsFragment
import com.buraq24.driver.utils.EmojiExcludeFilter
import com.buraq24.driver.utils.PermissionUtils
import com.buraq24.driver.webservices.models.BankDetailsResult
import com.buraq24.driver.webservices.models.RequestPersonalDetails
import com.buraq24.driver.webservices.models.ResponseCommon
import com.buraq24.driver.webservices.models.homeapi.Product
import com.buraq24.showSWWerror
import com.buraq24.showSnack
import com.buraq24.utilities.*
import com.buraq24.utilities.constants.*
import com.yalantis.ucrop.UCrop
import kotlinx.android.synthetic.main.fragment_add_mulkiya_details.*
import permissions.dispatcher.*
import java.util.*


@RuntimePermissions
class AddMulkiyanDetailsFragment : Fragment(), AddMulkiyanDetailsContract.View,
        MultiSelectionSpinner.OnMultipleItemsSelectedListener, DateUtils.OnDateSelectedListener, ColorListAdapter.Colorcallback {

    override fun dateTimeSelected(dateCal: Calendar) {
        date = DateUtils.getFormattedDateForUTC(dateCal)
        textView14.text = DateUtils.getFormattedTime(dateCal)
    }

    override fun timeSelected(dateCal: Calendar) {
    }

    override fun selectedStrings(strings: MutableList<String>?) {
        brandProductIds = ""
        strings?.forEach {
            brandProductIds = if (brandProductIds.isNotEmpty()) {
                "$brandProductIds,${listProducts.find { pro -> pro.name == it }?.category_brand_product_id}"
            } else {
                listProducts.find { pro -> pro.name == it }?.category_brand_product_id.toString()
            }
        }
        if (strings?.isEmpty() == true) {
            brandProductIds = ""
        }
    }

    override fun selectedIndices(indices: MutableList<Int>?) {

    }

    private val presenter = AddMulkiyanDetailsPresenter()

    private var listProducts = ArrayList<Product>()
    private var date: String = ""
    private var brandProductIds = ""
    private lateinit var dialogIndeterminate: DialogIndeterminate
    private var pathBackImage = ""
    private var pathFrontImage = ""
    private var colorname = ""
    private var isFrontPic: Boolean = true
    private var categoryId = ""
    private var bankName = ""
    private var accountNumber = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val rootView = inflater.inflate(R.layout.fragment_add_mulkiya_details, container, false)

    //    val colorNames = arrayOf("Red", "Black", "Silver", "White", "Blue", "Brown")
        val spinner = rootView.findViewById<Spinner>(R.id.colorSpinner)
        if (spinner != null) {

//            val arrayAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, colorNames)

//            spnColors=(Spinner)findViewById(R.id.spnColor);
//            spnColors.setAdapter(new SpinnerAdapter(this));

            val arrayAdapter = context?.let { ColorListArrayAdapter(it) }
            spinner.adapter = arrayAdapter

            spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

                override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {

                    // Toast.makeText(this@MainActivity, getString(R.string.selected_item) + " " + personNames[position], Toast.LENGTH_SHORT).show()
                }

                override fun onNothingSelected(parent: AdapterView<*>) {
                    // Code to perform some action when nothing is selected
                }
            }
        }

        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.attachView(this)
        date = DateUtils.getFormattedDateForUTC(Calendar.getInstance())
        textView14.text = DateUtils.getFormattedTime(Calendar.getInstance())
        editText3.filters = arrayOf(EmojiExcludeFilter())
        val products = SharedPrefs.with(view.context).getObject(PRODUCTS, BankDetailsResult::class.java)
        listProducts.addAll(products?.products ?: ArrayList())
        if (listProducts.isNotEmpty())
            brandProductIds = listProducts.get(0).category_brand_product_id?.toString() ?: ""
        dialogIndeterminate = DialogIndeterminate(activity)


        if (arguments != null && arguments?.containsKey("categoryId")!!) {
            categoryId = arguments?.getString("categoryId")!!
            if (categoryId.equals("7")) {


                group.visibility = View.GONE

                val layoutManager = LinearLayoutManager(activity)

                layoutManager.orientation = RecyclerView.HORIZONTAL
           //     rv_color.layoutManager = layoutManager

                val colorAdapter = ColorListAdapter()
                colorAdapter.settingcallback(this)
            //    rv_color.adapter = colorAdapter
            } else
                group.visibility = View.GONE
        }

        setListeners()
        setSpinner()
    }

    private fun setSpinner() {
        val listProductsName = ArrayList<String>()
        listProducts.forEach {
            listProductsName.add(it.name ?: "")
        }
        if (listProductsName.isNotEmpty()) {
            spinner2.setItems(listProductsName)
            spinner2.setListener(this)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        presenter.detachView()
    }

    private fun setListeners() {
        fabNext.setOnClickListener {
            val license = editText3.text.toString().trim()
            if ((categoryId == "7")  && checkValidations(license)) {
//                presenter.addMulkiyanDetails(RequestPersonalDetails(mulkiya_number = license,
//                        mulkiya_validity = date,
//                        vehicle_name = edCarModel.text.toString().trim(),
//                        vehicle_color = colorname,
//                        pic0 = pathFrontImage,
//                        pic1 = pathBackImage,
//                        bank_name = bankName,
//                        bank_account_number = accountNumber
//                ))

                presenter.addMulkiyanDetails(RequestPersonalDetails(mulkiya_number = license,
                        pic0 = pathFrontImage,
                        pic1 = pathBackImage
                ))
            }
        }

        textView14.setOnClickListener {
            DateUtils.openDateDialog(it.context, this, false,
                    Calendar.getInstance())
        }

        ivProfile.setOnClickListener {
            if (activity?.supportFragmentManager?.findFragmentByTag(AddBankDetailsFragment::class.java.simpleName) != null) {
                activity?.supportFragmentManager?.popBackStack()
            } else {
                activity?.supportFragmentManager?.popBackStack("backstack", FragmentManager.POP_BACK_STACK_INCLUSIVE)
            }
        }

        imageView4.setOnClickListener {
            isFrontPic = true
            getStorageWithPermissionCheck()
        }

        imageView5.setOnClickListener {
            isFrontPic = false
            getStorageWithPermissionCheck()
        }


    }

    private fun checkValidations(license: String): Boolean {
        return when {

            license.isEmpty() -> {
                fabNext.showSnack(getString(R.string.liecense_missing))
                false
            }

            pathFrontImage.isEmpty() -> {
                fabNext.showSnack(getString(R.string.select_front_image))
                false
            }
            pathBackImage.isEmpty() -> {
                fabNext.showSnack(getString(R.string.select_back_image))
                false
            }

//            bankName.isEmpty() ->{
//                fabNext.showSnack(getString(R.string.enter_your_bank_name))
//                false
//            }
//            accountNumber.isEmpty() ->{
//                fabNext.showSnack(getString(R.string.enter_your_bank_account_no))
//                false
//            }
            else -> true
        }
    }

    private fun vehicleValidate(): Boolean {
        return when {

            edCarModel.text.toString().trim().isEmpty() -> {
                fabNext.showSnack(getString(R.string.model_name_missing))
                false
            }

            colorname.isEmpty() -> {
                fabNext.showSnack(getString(R.string.color_missing))
                false
            }
            else -> true
        }
    }

    override fun onApiSuccess(response: ResponseCommon?) {
        if (response?.AppDetail?.approved == "0" && response.AppDetail.mulkiya_number?.isNotEmpty() == true) {
            val dialog = AlertDialogUtil.getInstance().createOkCancelDialog(activity,
                    R.string.alert,
                    R.string.approval_pending_msg,
                    R.string.okay,
                    0,
                    false,
                    object : AlertDialogUtil.OnOkCancelDialogListener {
                        override fun onOkButtonClicked() {
                            // Do nothing
                            SharedPrefs.with(context).removeAllSharedPrefsChangeListeners()
                            SharedPrefs.with(context).removeAll()
                            (context as? Activity)?.finishAffinity()
                            context?.startActivity(Intent(context, SignupActivity::class.java))
                        }

                        override fun onCancelButtonClicked() {
                            // Do nothing
                        }
                    })
            dialog.show()
            return
        }
        ACCESS_TOKEN = response?.AppDetail?.access_token ?: ""
        SharedPrefs.with(context).save(ACCESS_TOKEN_KEY, ACCESS_TOKEN)
        SharedPrefs.with(activity).save(PROFILE, response?.AppDetail)
        SharedPrefs.with(activity).save(STEP, 0)
        activity?.finishAffinity()
        startActivity(Intent(activity, HomeActivity::class.java))
    }

    override fun showLoader(isLoading: Boolean) {
        dialogIndeterminate.show(isLoading)
    }

    override fun apiFailure() {
        fabNext.showSWWerror()
    }

    override fun handleApiError(code: Int?, error: String?) {
        fabNext.showSnack(error.toString())
    }


    @NeedsPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    fun getStorage() {
        ImageUtils.displayImagePicker(this)

    }

    @OnShowRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    fun showLocationRationale(request: PermissionRequest) {
        PermissionUtils.showRationalDialog(imageView5.context, R.string.permission_required_to_select_image, request)
    }

    @OnNeverAskAgain(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    fun onNeverAskAgainRationale() {
        PermissionUtils.showAppSettingsDialog(imageView5.context,
                R.string.permission_required_to_select_image)
    }


    @SuppressLint("NeedOnRequestPermissionsResult")
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        onRequestPermissionsResult(requestCode, grantResults)
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK) {
            when (requestCode) {
                ImageUtils.REQ_CODE_CAMERA_PICTURE -> {
                    val file = ImageUtils.getFile(activity)


                    openCropActivity(Uri.fromFile(file),Uri.fromFile(file))

                }

                ImageUtils.REQ_CODE_GALLERY_PICTURE -> {
                    data?.let {
                        val file = ImageUtils.getImagePathFromGallery(activity, data.data)
                        openCropActivity(Uri.fromFile(file),Uri.fromFile(file))

                       /* if (isFrontPic) {
                            Glide.with(imageView4.context).setDefaultRequestOptions(RequestOptions()).load(file).into(imageView4)
                            pathFrontImage = file.absolutePath
                        } else {
                            Glide.with(imageView5.context).setDefaultRequestOptions(RequestOptions()).load(file).into(imageView5)
                            pathBackImage = file.absolutePath
                        }*/
                    }

                }

                UCrop.REQUEST_CROP->
                {
                    val uri = data?.let { UCrop.getOutput(it) }
                    if (isFrontPic) {
                        Glide.with(imageView4.context).setDefaultRequestOptions(RequestOptions()).load(uri).into(imageView4)
                        pathFrontImage = uri?.path?:""
                    } else {
                        Glide.with(imageView5.context).setDefaultRequestOptions(RequestOptions()).load(uri).into(imageView5)
                        pathBackImage = uri?.path?:""
                    }
                    //showImage(uri)
                }



            }
        }
    }

    private fun openCropActivity(sourceUri : Uri?, destinationUri:Uri?)
    {
        activity?.let {act ->
            sourceUri?.let {
                destinationUri?.let {
                    UCrop.of(sourceUri, destinationUri)
                            //.withMaxResultSize(maxWidth, maxHeight)
                            .withAspectRatio(5f, 5f)
                            .start(imageView4.context,this)
                }

            }

        }
    }

//    protected override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
//
//        // handle result of CropImageActivity
//        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
//            val result = CropImage.getActivityResult(data)
//            if (resultCode == Activity.RESULT_OK) {
//                (findViewById(R.id.quick_start_cropped_image) as ImageView).setImageURI(result.uri)
////                Toast.makeText(
////                        this, "Cropping successful, Sample: " + result.sampleSize, Toast.LENGTH_LONG)
////                        .show()
//            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
////                Toast.makeText(this, "Cropping failed: " + result.error, Toast.LENGTH_LONG).show()
//            }
//        }
//    }


    override fun getColor(color: String) {
        colorname = color
    }

//    private fun openCamera() {
//        val pictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
//        val file = getImageFile() // 1
//        val uri: Uri
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
//        // 2
//            uri = FileProvider.getUriForFile(activity, BuildConfig.APPLICATION_ID.concat(".provider"), file)
//        else
//            uri = Uri.fromFile(file) // 3
//        pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri) // 4
//        startActivityForResult(pictureIntent, CAMERA_ACTION_PICK_REQUEST_CODE)
//    }
//
//    currentPhotoPath
//    private File getImageFile()
//    {
//        String imageFileName = "JPEG_"+System.currentTimeMillis()+"_";
//        File storageDir = new File(
//                Environment.getExternalStoragePublicDirectory(
//                        Environment.DIRECTORY_DCIM
//                ), "Camera"
//        );
//        File file = File . createTempFile (
//                imageFileName, ".jpg", storageDir
//        );
//        currentPhotoPath = "file:" + file.getAbsolutePath();
//        return file;
//    }
}

