package com.buraq24.driver.ui.home

import android.Manifest
import android.animation.ValueAnimator
import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.app.NotificationManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.PixelFormat
import android.graphics.drawable.ColorDrawable
import android.media.MediaPlayer
import android.net.Uri
import android.os.*
import android.provider.Settings
import android.speech.tts.TextToSpeech
import android.speech.tts.UtteranceProgressListener
import android.support.design.widget.FloatingActionButton
import android.support.design.widget.NavigationView
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v4.view.GravityCompat
import android.support.v4.view.ViewPager
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.AlertDialog
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.OrientationHelper
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.style.RelativeSizeSpan
import android.util.Log
import android.view.*
import android.view.animation.LinearInterpolator
import android.widget.Toast
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.buraq24.driver.BuildConfig
import com.buraq24.driver.MainActivity
import com.buraq24.driver.R
import com.buraq24.driver.ui.ChatModule.ChatUserListActivity
import com.buraq24.driver.ui.home.bookings.BookingsActivity
import com.buraq24.driver.ui.home.confirmService.ConfirmScheduleFragment
import com.buraq24.driver.ui.home.contactUs.ContactUsActivity
import com.buraq24.driver.ui.home.earnings.EarningsActivity
import com.buraq24.driver.ui.home.emergencyContacts.EContactsActivity
import com.buraq24.driver.ui.home.etokens.areas.DeliveryAreasActivity
import com.buraq24.driver.ui.home.profileEdit.EditProfileActivity
import com.buraq24.driver.ui.home.profileEdit.EditProfileActivity.Companion.REQUEST_CODE_UPDATED_PROFILE
import com.buraq24.driver.ui.home.servicerequest.ServiceAllRequestFragment
import com.buraq24.driver.ui.home.servicerequest.ServiceRequestFragment
import com.buraq24.driver.ui.home.settings.SettingsActivity
import com.buraq24.driver.ui.signup.SignupActivity
import com.buraq24.driver.utils.PermissionUtils
import com.buraq24.driver.utils.getRatingSymbol
import com.buraq24.driver.utils.location.LocationProvider
import com.buraq24.driver.utils.location.LocationUpdatesHelper
import com.buraq24.driver.utils.location.LocationUpdatesService
import com.buraq24.driver.webservices.PolylineModel
import com.buraq24.driver.webservices.models.RoadItem
import com.buraq24.driver.webservices.models.Versioning
import com.buraq24.driver.webservices.models.order.Order
import com.buraq24.getAccessToken
import com.buraq24.showForcedUpdateDialogDriver
import com.buraq24.showSnack
import com.buraq24.utilities.*
import com.buraq24.utilities.constants.*
import com.buraq24.utilities.webservices.models.AppDetail
import com.buraq24.utilities.webservices.models.LoginModel
import com.buraq24.utilities.webservices.models.Service
import com.chaos.view.PinView
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.GoogleMap.OnCameraMoveStartedListener.REASON_GESTURE
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.google.android.gms.tasks.OnSuccessListener
import com.google.firebase.iid.FirebaseInstanceId
import com.google.gson.Gson
import io.fabric.sdk.android.services.common.CommonUtils
import io.socket.client.Ack
import io.socket.emitter.Emitter
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.activity_otp.*
import kotlinx.android.synthetic.main.fragment_after_accept.*
import kotlinx.android.synthetic.main.nav_header_main2.view.*
import org.json.JSONArray
import org.json.JSONObject
import permissions.dispatcher.*
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

@RuntimePermissions
class HomeActivity : LocationUpdatesHelper(),
        NavigationView.OnNavigationItemSelectedListener, OnMapReadyCallback,
        HomeContract.View, ServiceAllRequestFragment.OnRequestStatusChange, ConfirmScheduleFragment.OnConfirmRequest, ServiceRequestFragment.OnAcceptRequest {

    override fun onOrderTimeoutOrError() {
        stopRingTone()
    }

    override fun refreshOrders(order: Order?) {
        stopRingTone()
        removeAdapterItemAtIndex(order?.order_id ?: 0L)
    }

    override fun swipeStartStop(isStop: Boolean) {
        pagerRequests?.setOnTouchListener { _, _ ->
            isStop
        }
    }

    override fun requestConfirmed(orderDetail: Order?) {
        isServiceGoing = true
        sourceLatLong = currentLatLng

        when (orderDetail?.brand?.category_id) {
            CategoryId.GAS, CategoryId.WATER_TANKER, CategoryId.MINERAL_WATER -> {
                destLatLong = LatLng(orderDetail.dropoff_latitude
                        ?: 0.0, orderDetail.dropoff_longitude ?: 0.0)
                destMarker?.remove()
                destMarker = mMap.addMarker(MarkerOptions()
                        .anchor(0.5f, 0.5f)
                        .position(destLatLong ?: LatLng(0.0, 0.0)))
                destMarker?.setIcon((BitmapDescriptorFactory.fromResource(R.drawable.ic_drop_location_mrkr)))
            }
            else -> {
                destLatLong = LatLng(orderDetail?.pickup_latitude
                        ?: 0.0, orderDetail?.pickup_longitude ?: 0.0)
                destMarker?.remove()
                destMarker = mMap.addMarker(MarkerOptions()
                        .anchor(0.5f, 0.5f)
                        .position(destLatLong ?: LatLng(0.0, 0.0)))
                destMarker?.setIcon((BitmapDescriptorFactory.fromResource(R.drawable.ic_drop_location_other)))
            }
        }

        if (!hashMapMarkers.containsKey(orderDetail?.order_id)) {
            hashMapMarkers[orderDetail?.order_id] = destMarker
        }
        drawPolyLine()
        tts?.stop()
        tts?.shutdown()
        stopRingTone()
    }

    override fun changeTurnSuccess(order: Order?) {
        homePresenter.onGoingOrderApi()
        // pagerAdapter?.getFragments()?.forEachIndexed { index, fragment ->
        //     (fragment as ServiceAllRequestFragment).showFragmentAsPerStatus(listRequests[index])
        // }
    }

    override fun confirmAccepted(order: Order?) {
        openServiceRequest(Gson().toJson(order), "")
    }


    companion object {
        private val CODE_DRAW_OVER_OTHER_APP_PERMISSION: Int = 600
        private const val UPDATE_INTERVAL = 10000L  /* 5 secs */
        private const val FASTEST_INTERVAL: Long = 10000L /*5 sec */
        const val ICON_WIDTH: Int = 60
        const val ICON_HEIGHT: Int = 90
        const val REQUEST_CHECK_SETTINGS = 2
        var isServiceGoing = false
        val listRequests: ArrayList<Order> = ArrayList()
    }

    private var carMarker: Marker? = null
    private var destMarker: Marker? = null
    private var list = ArrayList<LatLng>()
    private var listRequestOrderIds = ArrayList<Long>()
    private var sourceLatLong: LatLng? = null
    private var destLatLong: LatLng? = null
    private lateinit var dialogIndeterminate: DialogIndeterminate
    private var currentLatLng: LatLng? = null
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var mLocationRequest: LocationRequest
    private lateinit var locationCallBack: LocationCallback
    private var homePresenter = HomePresenter()
    private lateinit var mMap: GoogleMap
    private lateinit var mapFragment: SupportMapFragment
    private var profile: AppDetail? = null
    private var line: Polyline? = null
    private var isFirstTime = true
    private var isCurrentLocation = true
    private var estimatedTime: String = ""
    private var estimatedDistance: String = ""
    private var tts: TextToSpeech? = null
    private var dialog: Dialog? = null
    private var mBackPressed: Long = 0
    private var loginData: LoginModel? = null
    private var brandName = ""
    private var pagerAdapter: com.buraq24.driver.utils.GeneralFragmentPagerAdapter? = null
    //private var adapterService: ServiceAdapter? = null
    private val hashMapMarkers: HashMap<Long?, Marker?> = HashMap()
    private var currentZoomLevel = 15f
    private var lastKnownLocation: android.location.Location? = null
    private var currentBearing = 0f
    private var dialogOverlayPermission: AlertDialog? = null
    private var ringTonePlayer: MediaPlayer? = null
    lateinit var dialogCode : Dialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        registerReceiver(receiverToken, IntentFilter("tokenRequest"))
        homePresenter.attachView(this)
        ivSupport.isEnabled = false
        drawer_layout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
        AppSocket.get().init(getAccessToken(this))
        loginData = SharedPrefs.with(this).getObject(SERVICES, LoginModel::class.java)
        profile = SharedPrefs.with(this).getObject(PROFILE, AppDetail::class.java)
        dialogIndeterminate = DialogIndeterminate(this)
        if (profile?.category_id != CategoryId.MINERAL_WATER) {
            val menu = nav_view.menu
            menu.findItem(R.id.nav_etokens).isVisible = false
//            menu.findItem(R.id.nav_support).isVisible = false
        }
        pagerAdapter = com.buraq24.driver.utils.GeneralFragmentPagerAdapter(supportFragmentManager)
        LocationProvider.CurrentLocationBuilder(this).build().getLastKnownLocation(OnSuccessListener {
            lastKnownLocation = it
//            currentLatLng = LatLng(it.latitude, it.longitude)
        })

        if (profile?.otp_verified.equals("0")){
            showCancellationDialog()
        }
    }

    private val receiverToken = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            homePresenter.onGoingOrderApi()
        }
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        val dataCategory = loginData?.services?.find { it.category_id == profile?.category_id }

        val dataSubCat = dataCategory?.brands?.find { it.category_brand_id == profile?.category_brand_id }

        brandName = dataSubCat?.name ?: ""
        initPager()
        checkNotifications()
        initLocationService()
        createOverlayPermissionDialog()
    }

    private fun initPager() {
        pagerRequests?.adapter = pagerAdapter
        pagerRequests?.pageMargin = 8
        pagerRequests.offscreenPageLimit = 10
        registerReceiver(receiverNotifications, IntentFilter("notifications"))
        pagerRequests.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {

            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

            }

            override fun onPageSelected(position: Int) {
                if (position >= 0 && position < listRequests.size) {
                    val order = listRequests[position]
                    requestStart(order)
                }

            }

        })
    }

    private fun createOverlayPermissionDialog() {
        dialogOverlayPermission = AlertDialogUtil.getInstance().createOkCancelDialog(this,
                R.string.premission_required,
                R.string.draw_overlay_permission_required,
                R.string.yes,
                0,
                false,
                object : AlertDialogUtil.OnOkCancelDialogListener {
                    override fun onCancelButtonClicked() {

                    }

                    override fun onOkButtonClicked() {
                        //If the draw over permission is not available open the settings screen
                        //to grant the permission.
                        val intent = Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                                Uri.parse("package:$packageName"))
                        startActivityForResult(intent, CODE_DRAW_OVER_OTHER_APP_PERMISSION)
                    }

                })
    }

    private fun checkStatus() {
        if (profile?.online_status == null) {
            homePresenter.driverStatusChange("1", true)
        }
    }

    override fun onRated(orderId: Long?) {
        orderId?.let { removeAdapterItemAtIndex(orderId) }
    }


    private fun setListeners() {

        nav_view.setNavigationItemSelectedListener(this)
        ivProfile.setOnClickListener {
            drawer_layout?.closeDrawer(Gravity.END)
            drawer_layout?.openDrawer(Gravity.START)
        }


        ivSupport.setOnClickListener {
            drawer_layout?.closeDrawer(Gravity.START)
            drawer_layout?.openDrawer(Gravity.END)
        }

        fabCurrentLocation.setOnClickListener {
            //            if (profile?.online_status == "1" && !isServiceGoing) {
            if (profile?.online_status == "1") {
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                        == PackageManager.PERMISSION_GRANTED && currentLatLng != null) {
                    isCurrentLocation = true
                    updateCameraWithBearing()
                } else {
                    getLocationWithPermissionCheck()
                }
            }
//            } else if (isServiceGoing) {
//                line?.let {
//                    //                    animateCameraToPolyline(it)
//                    updateCameraWithBearing()
//                }
//            }
        }

        ivOnlineOffline.setOnClickListener {
            if (!isServiceGoing) {
                homePresenter.driverStatusChange(if (profile?.online_status == "1") "0" else "1", true)
            }
        }

        tvMap.setOnClickListener {
            mMap.mapType = GoogleMap.MAP_TYPE_NORMAL
            line?.color = (ContextCompat.getColor(this, R.color.text_dark))
            tvMap.isChecked = true
            tvSatellite.isChecked = false
        }
        tvSatellite.setOnClickListener {
            mMap.mapType = GoogleMap.MAP_TYPE_HYBRID
            line?.color = (ContextCompat.getColor(this, R.color.white))
            tvSatellite.isChecked = true
            tvMap.isChecked = false
        }
    }

    private fun initLocationService() {
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        locationCallBackPeriodic()
        mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    private fun checkNotifications() {
        if (intent.hasExtra("type")) {
            if (intent.getStringExtra("type") == Events.ORDER_EVENT
                    || intent?.getStringExtra("type") == OrderStatus.DRIVER_SCHEDULE_SERVICE
                    || intent?.getStringExtra("type") == OrderStatus.DRIVER_ARROVED
                    || intent?.getStringExtra("type") == OrderStatus.DRIVER_START) {
                val orderId = intent?.getStringExtra("order_id")
                orderId?.let {
                    homePresenter.getOrderDetails(orderId.toLong())
                }
            }
        }
    }

    private fun listSearchinOrders(listOrders: List<Order>?) {
        listOrders?.forEach {
            it.order_id?.let { orderId ->
                homePresenter.getOrderDetails(orderId.toLong())
            }
        }
    }

    private fun setSpanOnSupportHeader() {
        try {
            val spannableBuilder = SpannableStringBuilder(tvSupportHeader.text)
            spannableBuilder.setSpan(RelativeSizeSpan(1.5f), 0,
                    (getString(R.string.support)).length, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            tvSupportHeader.text = spannableBuilder
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    private fun setSupportServices() {
        homePresenter.getSupportList()
    }

    private fun animateCameraToPolyline(polyline: Polyline) {
        val builder = LatLngBounds.Builder()
        for (item in polyline.points) {
            builder.include(item)
        }
//        mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), resources.getDimensionPixelOffset(android.R.dimen.notification_large_icon_height)))
    }

    private fun onOffSocketForEvents() {
        if (profile?.online_status == "1") {
            AppSocket.get().on("OrderEvent", orderEventListener)
        } else {
            AppSocket.get().off("OrderEvent", orderEventListener)
        }
    }

    private val orderEventListener = Emitter.Listener { args ->
        runOnUiThread {
            if (JSONObject(args[0].toString()).has("message")) {
                matchTypeAndDoAccordingly(JSONObject(args[0].toString()).getString("type"),
                        JSONObject(args[0].toString()).get("order").toString(), JSONObject(args[0].toString()).get("message").toString())
            } else {
                matchTypeAndDoAccordingly(JSONObject(args[0].toString()).getString("type"),
                        JSONObject(args[0].toString()).get("order").toString(), "")
            }
        }
    }

    private fun matchTypeAndDoAccordingly(type: String?, orderJsonString: String, message: String) {
        when (type) {

            OrderEventType.SERVICE_REQUEST -> {
                openServiceRequest(orderJsonString, message)
            }

            OrderEventType.DRIVER_START -> {
                val order = Gson().fromJson(orderJsonString, Order::class.java)
                order.order_id?.let { homePresenter.getOrderDetails(order.order_id?.toLong() ?: 0) }
            }

            OrderEventType.SERVICE_CANCEL -> {
                try {
                    ServiceAllRequestFragment.dialog?.dismiss()
                    ServiceAllRequestFragment.dialogDropoffConfirmation?.dismiss()
                    removeAdapterItem(orderJsonString)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            OrderEventType.SERVICE_COMPLETE -> {
                removeAdapterItem(orderJsonString)
            }

            OrderEventType.CUSTOMER_CANCEL, OrderEventType.SERVICE_OTHER_ACCEPT, OrderEventType.SERVICE_REJECT,
            OrderEventType.SERVICE_TIMEOUT -> {
                ServiceAllRequestFragment.dialog?.dismiss()
                ServiceAllRequestFragment.dialogDropoffConfirmation?.dismiss()
                removeAdapterItem(orderJsonString)
                if (OrderEventType.CUSTOMER_CANCEL == type)
                    Toast.makeText(this, getString(R.string.your_ride_canelled), Toast.LENGTH_SHORT).show()
            }

            OrderEventType.DRIVER_TIME_OUT -> {
                removeAdapterItem(orderJsonString)

            }

            OrderEventType.SYSTEM_CANCELLED -> {
                removeAdapterItem(orderJsonString)
            }

            OrderEventType.DRIVER_SCHEDULE_SERVICE -> {
                val order = Gson().fromJson(orderJsonString, Order::class.java)
                order.order_id?.let { homePresenter.getOrderDetails(order.order_id?.toLong() ?: 0) }
            }

            OrderEventType.CUSTOMER_RATED_SERVICE -> {
                isFirstTime = true
                sendIdleDataToSocket(currentLatLng ?: LatLng(0.0, 0.0))
            }
        }
    }

    private fun removeAdapterItem(orderJsonString: String) {
        val order = Gson().fromJson(orderJsonString, Order::class.java)
        val item = listRequests.find {
            it.order_id == order?.order_id
        }

        if (item != null) {
            val index = listRequests.indexOf(item)
            listRequests.remove(item)
            if (hashMapMarkers.containsKey(order?.order_id)) {
                hashMapMarkers[order?.order_id]?.remove()
            }
            //adapterService?.notifyItemRemoved(index)
            pagerAdapter?.removeFragmentAll()
            pagerAdapter?.notifyDataSetChanged()
            if (listRequests.isEmpty()) {
                pagerRequests.visibility = View.GONE
                resetAllToDefault()
            } else {
                val requests = ArrayList<Order>()
                requests.addAll(listRequests)
                listRequests.clear()
                addRequestList(requests, false)
            }
        }
        if (listRequests.isEmpty()) {
            pagerRequests.visibility = View.GONE
            resetAllToDefault()
        }
        for (fragment in supportFragmentManager.fragments) {
            if (fragment is ServiceRequestFragment && fragment.tag == order?.order_id.toString())
                supportFragmentManager.beginTransaction().remove(fragment).commitAllowingStateLoss()
        }
        tts?.stop()
        tts?.shutdown()
        stopRingTone()
    }

    override fun requestStart(order: Order?) {
        isServiceGoing = true
        sourceLatLong = currentLatLng
        destLatLong = LatLng(order?.dropoff_latitude ?: 0.0, order?.dropoff_longitude ?: 0.0)
        destMarker?.remove()
        destMarker = mMap.addMarker(MarkerOptions().anchor(0.5f, 0.5f).position(destLatLong
                ?: LatLng(0.0, 0.0)))
        destMarker?.setIcon((BitmapDescriptorFactory.fromResource(R.drawable.ic_drop_location_mrkr)))
        if (!hashMapMarkers.containsKey(order?.order_id)) {
            hashMapMarkers[order?.order_id] = destMarker
        }
        drawPolyLine()
        //showMarker()
        tts?.stop()
        tts?.shutdown()
        stopRingTone()
    }

    private fun removeAdapterItemAtIndex(orderId: Long) {
        val item = listRequests.find {
            it.order_id == orderId
        }
        if (item != null) {
            val index = listRequests.indexOf(item)
            listRequests.removeAt(index)
            if (hashMapMarkers.containsKey(orderId)) {
                hashMapMarkers[orderId]?.remove()
            }
            pagerAdapter?.removeFragmentAll()

            if (listRequests.isEmpty()) {
                pagerAdapter?.notifyDataSetChanged()
                recyclerRequest.visibility = View.GONE
                resetAllToDefault()
            } else {
                val requests = ArrayList<Order>()
                requests.addAll(listRequests)
                listRequests.clear()
                addRequestList(requests, false)
            }

        }
        if (listRequests.isEmpty()) {
            pagerAdapter?.notifyDataSetChanged()
            recyclerRequest.visibility = View.GONE
            resetAllToDefault()
        }
        tts?.stop()
        tts?.shutdown()
        stopRingTone()
    }


    private fun openServiceRequest(orderJson: String, message: String) {
        val order = Gson().fromJson(orderJson, Order::class.java)
        if (order?.order_status == OrderStatus.DRIVER_ARROVED) {
            val latlng =
                    if (order.category_id == 4 || order.category_id == 5) {
                        LatLng(order.pickup_latitude
                                ?: 0.0, order.pickup_longitude
                                ?: 0.0)
                    } else {
                        LatLng(order.dropoff_latitude
                                ?: 0.0, order.dropoff_longitude
                                ?: 0.0)
                    }


            val listOrders = ArrayList<Order>()
            listOrders.add(order)
            addRequestList(listOrders, false)
            homePresenter.drawPolyLine(sourceLatLong?.latitude ?: 0.0, sourceLatLong?.longitude
                    ?: 0.0, latlng.latitude, latlng.longitude)

        } else {
            val item = listRequestOrderIds.find { it == order?.order_id }
            if (item == null) {
                if (order?.order_status == OrderStatus.DRIVER_SCHEDULE_SERVICE ||
                        order?.order_status == OrderStatus.DRIVER_START) {


                    listRequestOrderIds.add(order.order_id ?: 0L)
                    val fragment = ConfirmScheduleFragment()
                    val bundle = Bundle()
                    bundle.putString("order", orderJson)
                    fragment.arguments = bundle
                    supportFragmentManager?.beginTransaction()?.add(android.R.id.content,
                            fragment, order.order_id?.toString())?.commitAllowingStateLoss()


                } else if (order.order_status == null ||
                        order.order_status == OrderStatus.SEARCHING) {
                    listRequestOrderIds.add(order.order_id ?: 0L)
                    val fragment = ServiceRequestFragment()
                    val bundle = Bundle()
                    if (!message.isEmpty()) {
                        startTextToSpeech(message, order)
                    }
                    bundle.putString("order", orderJson)
                    fragment.arguments = bundle
                    //if(supportFragmentManager?.isStateSaved == true) return
                    supportFragmentManager?.beginTransaction()?.add(android.R.id.content,
                            fragment, order?.order_id?.toString())?.commitAllowingStateLoss()
                }

            }
        }
    }

    private fun addRequestList(listOrders: ArrayList<Order>, isOngoing: Boolean) {
        if (isOngoing) {
            if (listOrders.isEmpty()) {
                listRequests.clear()
                pagerAdapter?.removeFragmentAll()
                pagerAdapter?.notifyDataSetChanged()
                recyclerRequest.visibility = View.GONE
                resetAllToDefault()
            } else {
                val listRequestMissing = listRequests.filter { item -> listOrders.find { it.order_id == item.order_id } == null }
                listRequestMissing.forEach { request ->
                    removeAdapterItemAtIndex(request.order_id ?: 0L)
                }
            }
        }
        listRequests.clear()
        pagerAdapter?.removeFragmentAll()
        pagerAdapter?.notifyDataSetChanged()
        listOrders.forEach { order ->
            if (order.order_status != null && order.order_status != OrderStatus.SEARCHING &&
                    order.order_status != OrderStatus.DRIVER_SCHEDULE_SERVICE
            ) {
                if (listRequests.find { it.order_id == order.order_id } == null) {
                    if (order.order_status == OrderStatus.DRIVER_ARROVED) {
                        val latlng =
                                if (order.category_id == 4 || order.category_id == 5) {
                                    LatLng(order.pickup_latitude ?: 0.0,
                                            order.pickup_longitude ?: 0.0)
                                } else {
                                    LatLng(order.dropoff_latitude ?: 0.0, order.dropoff_longitude
                                            ?: 0.0)
                                }
                        listRequests.add(order)
                        pagerRequests.visibility = View.VISIBLE
                        val fragment = ServiceAllRequestFragment()
                        fragment.setRequestChangeListener(this)
                        pagerAdapter?.addFragment(fragment, "", order.order_id ?: 0, order)
                        pagerAdapter?.notifyDataSetChanged()

                    } else {
                        listRequests.add(order)
                        pagerRequests.visibility = View.VISIBLE
                        val fragment = ServiceAllRequestFragment()
                        fragment.setRequestChangeListener(this)
                        pagerAdapter?.addFragment(fragment, "", order.order_id ?: 0, order)
                        pagerAdapter?.notifyDataSetChanged()
                    }
                }
            } else if (order.future == "1") {
                openServiceRequest(Gson().toJson(order), "")
            } else if (order.order_status == null || order.order_status == OrderStatus.SEARCHING) {
                val timing = DateUtils.matchTimeExecuted(order.created_at ?: "")
                if (timing < 40) {
                    order.timeProgress = timing
                    openServiceRequest(Gson().toJson(order), "")
                }
            }
        }

        //Handler().postDelayed({
        if (listRequests.size > 0) {
            val item = listRequests.find { it.my_turn == "1" }
            val index = if (item == null) 0 else listRequests.indexOf(item)
            pagerRequests?.currentItem = if (index < 0) 0 else index
        }
        // },200)
        pagerAdapter?.notifyDataSetChanged()
    }

    private fun addRequest(order: Order?) {
        order?.let { orderItem ->
            if (orderItem.order_status != null && orderItem.order_status != OrderStatus.SEARCHING) {
                if (listRequests.find { orderItem.order_id == it.order_id } == null) {
                    listRequests.add(orderItem)
                    pagerRequests.visibility = View.VISIBLE
                    val fragment = ServiceAllRequestFragment()
                    fragment.setRequestChangeListener(this)
                    pagerAdapter?.addFragment(fragment, "", orderItem.order_id ?: 0, orderItem)
                    pagerAdapter?.notifyDataSetChanged()
                }
            }
        }
    }

    private fun setHeaderView() {
        profile = SharedPrefs.with(this).getObject(PROFILE, AppDetail::class.java)
        val services = resources.getStringArray(R.array.services_list)
        val viewHeader = nav_view.getHeaderView(0)
        val phone = profile?.user?.phone_code + "-" + profile?.user?.phone_number
        viewHeader?.tvName?.text = profile?.user?.name
        viewHeader?.tvNumber?.text = phone
        if (profile?.ratings_avg != null && profile?.ratings_avg != 0) {
            viewHeader?.textView?.visibility = View.VISIBLE
            viewHeader?.ivRating?.visibility = View.VISIBLE
            viewHeader?.ivRating?.setImageResource(getRatingSymbol(profile?.ratings_avg ?: 0))
            viewHeader?.textView?.text = profile?.rating_count?.toString() ?: "0"
        } else {
            viewHeader?.textView?.visibility = View.GONE
            viewHeader?.ivRating?.visibility = View.GONE
        }

        Glide.with(applicationContext).setDefaultRequestOptions(RequestOptions()
                .circleCrop().placeholder(R.drawable.ic_user_black)).load(profile?.profile_pic_url)
                .into(viewHeader.imageView)
        viewHeader.setOnClickListener {
            openEditProfileWithPermissionCheck()
        }

        viewHeader?.tvType?.text = loginData?.services?.find { it.category_id == profile?.category_id }?.name + " - $brandName"
        /*        when {
                    profile?.category_id == 1 -> services[1]
                    profile?.category_id == 2 -> services[2] + " - $brandName"
                    profile?.category_id == 3 -> services[3]
                    profile?.category_id == 4 -> services[4] + " - $brandName"
                    else -> services[0] + " -$brandName "
                }*/
    }

    @NeedsPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    fun openEditProfile() {
        startActivityForResult(Intent(this, EditProfileActivity::class.java)
                .addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), REQUEST_CODE_UPDATED_PROFILE)
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else if (drawer_layout.isDrawerOpen(GravityCompat.END)) {
            drawer_layout.closeDrawer(GravityCompat.END)
        } else {
            if (supportFragmentManager.backStackEntryCount == 1) {
                if (mBackPressed + 3000 > System.currentTimeMillis()) {
                    finish()
                } else {
                    Toast.makeText(this, getString(R.string.press_back), Toast.LENGTH_SHORT).show()
                    mBackPressed = System.currentTimeMillis()
                }
            } else {
                if (supportFragmentManager?.fragments?.get(0) is SupportMapFragment) {
                    if (mBackPressed + 3000 > System.currentTimeMillis()) {
                        super.onBackPressed()
                    } else {
                        Toast.makeText(this, getString(R.string.press_back), Toast.LENGTH_SHORT).show()
                        mBackPressed = System.currentTimeMillis()
                    }
                }
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main2, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_settings -> return true
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.nav_sign_out -> {
                val builder = AlertDialog.Builder(this)
                builder.setMessage(getString(R.string.logout_msg))
                builder.setPositiveButton(getString(R.string.yes)) { dialog, _ ->
                    dialog?.dismiss()
                    if (!isServiceGoing) {
                        homePresenter.logout()
                    } else {
                        Toast.makeText(this@HomeActivity, getString(R.string.on_way_signout), Toast.LENGTH_SHORT).show()
                    }
                }
                builder.setNegativeButton(getString(R.string.no)) { dialog, _ -> dialog?.dismiss() }
                builder.show()
            }
            R.id.nav_referral -> {
                startActivity(Intent(this, ReferralActivity::class.java)
                        .addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP))
            }

            R.id.nav_chat -> {
                var intent = Intent(this, ChatUserListActivity::class.java)
                startActivity(intent)
            }

            R.id.nav_contact_us -> {
                startActivity(Intent(this, ContactUsActivity::class.java)
                        .addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP))
            }

//            R.id.nav_support -> {
//                startActivity(Intent(this, SupportActivity::class.java)
//                        .addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP))
//            }

            R.id.nav_emergency_contact -> {
                startActivity(Intent(this, EContactsActivity::class.java)
                        .addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP))
            }
            R.id.nav_bookings -> {
                startActivity(Intent(this, BookingsActivity::class.java)
                        .addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP))
            }
            R.id.nav_settings -> {
                startActivity(Intent(this, SettingsActivity::class.java)
                        .addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP))
            }
            R.id.nav_payments -> {
                startActivity(Intent(this, EarningsActivity::class.java)
                        .addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP))
            }

            R.id.nav_etokens -> {
                startActivity(Intent(this, DeliveryAreasActivity::class.java))
            }
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    private fun signOut() {
        listRequests.clear()
        AppSocket.get().off()
        AppSocket.get().disconnect()
        SharedPrefs.with(this@HomeActivity).removeAllSharedPrefsChangeListeners()
        SharedPrefs.with(this@HomeActivity).removeAll()
        finishAffinity()
        startActivity(Intent(this@HomeActivity, SignupActivity::class.java))
        val intent = Intent(this, LocationUpdatesService::class.java)
        intent.putExtra("logged_out", true)
        startService(intent)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        mMap.uiSettings?.isTiltGesturesEnabled = false
        mMap.uiSettings?.isCompassEnabled = false
        mMap.uiSettings?.isMyLocationButtonEnabled = false
        mMap.clear()

        setHeaderView()
        checkStatus()
        setSupportServices()
        setListeners()
        setSpanOnSupportHeader()


        if (profile?.online_status == "1") {
            ivOnlineOffline.setImageResource(R.drawable.ic_switch_on)
            getLocationWithPermissionCheck()
        } else {
            ivOnlineOffline.setImageResource(R.drawable.ic_switch_off)
        }


        homePresenter.onGoingOrderApi()

        mMap.setOnCameraIdleListener {
            if (MapUtils.getDistanceBetweenTwoPoints(LatLng(mMap.cameraPosition?.target?.latitude
                            ?: 0.0,
                            mMap.cameraPosition?.target?.longitude
                                    ?: 0.0), LatLng(currentLatLng?.latitude
                            ?: 0.0, currentLatLng?.longitude ?: 0.0)) < 1)
                if (isCurrentLocation) {
                    fabCurrentLocation.setImageResource(R.drawable.ic_my_location_blue_24dp)
                } else {
                    fabCurrentLocation.setImageResource(R.drawable.ic_my_location_black_24dp)
                }
            carMarker?.let {
                if (currentZoomLevel != mMap.cameraPosition?.zoom) {
                    currentZoomLevel = mMap.cameraPosition?.zoom ?: 15f
                    if (currentZoomLevel == 18f) {
                        setMarkerIcon()
                    } else
                        scaleDownMarker((currentZoomLevel / 18f))
                }
            }


        }

        mMap.setOnCameraMoveStartedListener {
            if (it == REASON_GESTURE) {
                isCurrentLocation = false
                fabCurrentLocation?.setImageResource(R.drawable.ic_my_location_black_24dp)
            }
        }

        mMap.setOnCameraMoveListener {

        }
    }

    private fun scaleDownMarker(scale: Float) {
        val mapIcon = getIconBitmap()
        if ((ICON_WIDTH * scale).toInt() > 0 && (ICON_HEIGHT * scale).toInt() > 0) {
            val newBitMap = Bitmap.createScaledBitmap(
                    mapIcon,
                    (ICON_WIDTH * scale).toInt(),
                    (ICON_HEIGHT * scale).toInt(),
                    false)
            carMarker?.isVisible = true
            carMarker?.setIcon(BitmapDescriptorFactory.fromBitmap(newBitMap))
        }
    }

    private fun getIconBitmap(): Bitmap {
        return when (profile?.category_id) {
            1 -> BitmapFactory.decodeResource(resources, R.drawable.ic_mini_truck_m_gas)
            2 -> BitmapFactory.decodeResource(resources, R.drawable.ic_mini_truck_water_m)
            3 -> BitmapFactory.decodeResource(resources, R.drawable.ic_truck_water_tank_m)
            4 -> BitmapFactory.decodeResource(resources, R.drawable.ic_truck_m_freights)
            7 -> BitmapFactory.decodeResource(resources, R.drawable.ic_pin)
            else -> BitmapFactory.decodeResource(resources, R.drawable.ic_mini_truck_m_tow)
        }
    }

    @SuppressLint("MissingPermission")
    private fun startLocationUpdates() {
        mLocationRequest = LocationRequest()
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        mLocationRequest.interval = UPDATE_INTERVAL
        mLocationRequest.fastestInterval = FASTEST_INTERVAL
        mLocationRequest.smallestDisplacement = 30f

        val builder = LocationSettingsRequest.Builder()
        builder.addLocationRequest(mLocationRequest)
        builder.setAlwaysShow(true)
        val locationSettingsRequest = builder.build()
        val settingsClient = LocationServices.getSettingsClient(this)
        settingsClient.checkLocationSettings(locationSettingsRequest)
        val result =
                LocationServices.getSettingsClient(this).checkLocationSettings(builder.build())
        result.addOnCompleteListener {
            try {
                it.getResult(ApiException::class.java)
                getLastLocation()
            } catch (e: ApiException) {
                when (e.statusCode) {
                    LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> {
                        try {
                            val resolvable = e as ResolvableApiException
                            resolvable.startResolutionForResult(this, REQUEST_CHECK_SETTINGS)
                        } catch (e: ClassCastException) {
                            e.printStackTrace()
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }
                }
            }
        }
    }

    private fun locationCallBackPeriodic() {
        locationCallBack = object : LocationCallback() {

            override fun onLocationAvailability(p0: LocationAvailability?) {
                super.onLocationAvailability(p0)
                Log.e("location_available", p0?.isLocationAvailable.toString())
            }
        }
    }

    override fun onLocationReceived(location: android.location.Location?) {
        lastKnownLocation = location
        if (isServiceGoing) {
            homePresenter.getRoadPoints(location?.latitude ?: 0.0, location?.longitude ?: 0.0)
        } else {
            onLocationProvided(location?.latitude, location?.longitude)
        }
        Logger.e("location_service", "updated")
    }


    private fun sendDataToSocket(latLng: LatLng, polylineModel: PolylineModel) {
        try {
            val dataJson1 = JSONObject()
            dataJson1.put("type", "DCurrentOrders")
            dataJson1.put("access_token", ACCESS_TOKEN)
            dataJson1.put("latitude", latLng.latitude)
            dataJson1.put("longitude", latLng.longitude)
            dataJson1.put("polyline", JSONObject(Gson().toJson(polylineModel)))
            if (currentBearing == Float.NaN) {
                dataJson1.put("bearing", 0.0f)
            } else {
                dataJson1.put("bearing", currentBearing)
            }
            AppSocket.get()?.emit("CommonEvent", dataJson1, Ack {

            })
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun sendIdleDataToSocket(latLng: LatLng) {
        try {
            val dataJson = JSONObject()
            dataJson.put("type", "UpdateData")
            dataJson.put("access_token", SharedPrefs.with(this).getString(ACCESS_TOKEN_KEY, ""))
            dataJson.put("latitude", latLng.latitude)
            dataJson.put("longitude", latLng.longitude)
            if (carMarker != null && currentBearing != Float.NaN) {
                dataJson.put("bearing", currentBearing)
            } else {
                dataJson.put("bearing", 90.0f)
            }
            dataJson.put("language_id", SharedPrefs.get().getInt(PREFS_LANGUAGE_ID, 1).toString())
            dataJson.put("timezone", TimeZone.getDefault().id)
            FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener { it ->
                it?.token?.let { it ->
                    dataJson.put("fcm_id", it)
                    AppSocket.get()?.emit("CommonEvent", dataJson, Ack { args ->
                        if (JSONObject(args[0].toString()).getInt("statusCode") == StatusCode.UNAUTHORIZED) {
                            signOut()
                            return@Ack
                        }
                        if (JSONObject(args[0].toString()).has("Versioning")) {
                            val versioningJson = JSONObject(args[0].toString()).get("Versioning").toString()
                            val version = Gson().fromJson<Versioning>(versioningJson, Versioning::class.java)
                            runOnUiThread {
                                version?.let { versioning ->
                                    if (dialog?.isShowing == true) {
                                        dialog?.dismiss()
                                    }
                                    dialog = showForcedUpdateDialogDriver(this@HomeActivity, versioning.ANDROID.driver.force.toString(),
                                            versioning.ANDROID.driver.normal.toString(), BuildConfig.VERSION_NAME)
                                }

                            }
                        }
                        val resultJson = JSONObject(args[0].toString()).get("AppDetail").toString()
                        val response = Gson().fromJson<AppDetail>(resultJson, AppDetail::class.java)

                        if (response.online_status != profile?.online_status) {
                            response.online_status = profile?.online_status
                            homePresenter.driverStatusChange(profile?.online_status
                                    ?: "1", false)
                        }
                        SharedPrefs.get().save(PROFILE, response)
                        runOnUiThread {
                            if (isFirstTime) {
                                setHeaderView()
                                isFirstTime = false
                            }
                            if (response?.approved == "0") {
                                Toast.makeText(this, "You are blocked by the admin. Please contact admin.", Toast.LENGTH_LONG).show()
                                signOut()
                            }
                        }
                    })
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    private fun setMarkerIcon() {
        if (profile?.category_id == 1) {
            carMarker?.setIcon((BitmapDescriptorFactory.fromResource(R.drawable.ic_mini_truck_m_gas)))
        } else if (profile?.category_id == 3) {
            carMarker?.setIcon((BitmapDescriptorFactory.fromResource(R.drawable.ic_truck_water_tank_m)))
        } else if (profile?.category_id == 2) {
            carMarker?.setIcon((BitmapDescriptorFactory.fromResource(R.drawable.ic_mini_truck_water_m)))
        } else if (profile?.category_id == 4) {
            carMarker?.setIcon((BitmapDescriptorFactory.fromResource(R.drawable.ic_mini_truck_water_m)))
        } else {
            carMarker?.setIcon((BitmapDescriptorFactory.fromResource(R.drawable.ic_pin)))
        }
    }

    @SuppressLint("MissingPermission")
    private fun getLastLocation() {
        fusedLocationClient.requestLocationUpdates(mLocationRequest, locationCallBack,
                Looper.myLooper())
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_CHECK_SETTINGS) {
            getLastLocation()
        } else if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_CODE_UPDATED_PROFILE) {
            isFirstTime = true
            sendIdleDataToSocket(currentLatLng ?: LatLng(0.0, 0.0))
        } else if (requestCode == CODE_DRAW_OVER_OTHER_APP_PERMISSION) {
//            checkAndShowOverlayPermissionDialog()
//            finishAffinity()
            val intent = Intent(this, MainActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
            System.exit(0)
        }
    }

    @NeedsPermission(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION)
    fun getLocation() {
//        startLocationUpdates()
        requestLocationUpdates()
        checkAndShowOverlayPermissionDialog()
    }

    private fun checkAndShowOverlayPermissionDialog() {
        if (!canDrawOverlays(this)) {
            dialogOverlayPermission?.show()
        } else {
            dialogOverlayPermission?.dismiss()
        }
    }

    @OnShowRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    fun showStorageRationale(request: PermissionRequest) {
        PermissionUtils.showRationalDialog(this, R.string.storage_permission_required, request)
    }

    @OnNeverAskAgain(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    fun onNeverAskAgainStorageRationale() {
        PermissionUtils.showAppSettingsDialog(this, R.string.storage_permission_required)
    }

    @OnShowRationale(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION)
    fun showLocationRationale(request: PermissionRequest) {
        //com.app.bart.utils.PermissionUtils.showRationalDialog(activity, R.string.permission_location_deny, request)
    }

    @OnNeverAskAgain(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION)
    fun onNeverAskAgainRationale() {
        //com.app.bart.utils.PermissionUtils.showAppSettingsDialog(activity,
        //      R.string.permission_location_deny)
    }

    @SuppressLint("NeedOnRequestPermissionsResult")
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        onRequestPermissionsResult(requestCode, permissions, grantResults)
    }


    override fun snappedPoints(response: List<RoadItem>?) {
        if ((response?.size ?: 0) > 0) {
            onLocationProvided(response?.get(0)?.location?.latitude, response?.get(0)?.location?.longitude)
        }
    }

    private fun onLocationProvided(lat: Double?, lng: Double?) {
        val prevLatLng = LatLng(currentLatLng?.latitude ?: 0.0, currentLatLng?.longitude ?: 0.0)
        currentLatLng = LatLng(lat ?: 0.0, lng ?: 0.0)
        currentBearing = MapUtils.bearingBetweenLocations(prevLatLng, currentLatLng
                ?: LatLng(0.0, 0.0))
        Logger.e("bearing", currentBearing)
        SharedPrefs.with(this@HomeActivity).save(PREF_CURRENT_LAT,
                currentLatLng?.latitude?.toFloat() ?: 0.0f)
        SharedPrefs.with(this@HomeActivity).save(PREF_CURRENT_LNG, currentLatLng?.longitude?.toFloat()
                ?: 0.0f)

        if (isServiceGoing) {
            sourceLatLong = currentLatLng
//            val points = line?.points
//            if (line != null) {
//                val isLocationOnPath = PolyUtil.isLocationOnPath(LatLng(lat ?: 0.0, lng
//                        ?: 0.0), points, true, 10.0)
//                if (isLocationOnPath) {
//                    var prevDistance = MapUtils.getDistanceBetweenTwoPoints(LatLng(lat ?: 0.0, lng
//                            ?: 0.0), points?.get(0) ?: LatLng(0.0, 0.0))
//                    for (i in 0..(points?.size?.minus(1) ?: 0)) {
//                        val distance = MapUtils.getDistanceBetweenTwoPoints(LatLng(lat ?: 0.0, lng
//                                ?: 0.0), points?.get(i) ?: LatLng(0.0, 0.0))
//                        if (distance <= prevDistance) {
//                            prevDistance = distance
//                        } else {
//                            line?.points = points?.subList(i - 1, points.size - 1)
//                            break
//                        }
//                    }
//                } else {
//                    drawPolyLine()
//                    Logger.e("directionapi", "called")
//                }
//            } else {
            drawPolyLine()
//                Logger.e("directionapi", "called")
//            }
            animateMarker()
            if (isCurrentLocation) {
                updateCameraWithBearing()
            }
        } else {
            if (carMarker == null) {
                carMarker = mMap.addMarker(MarkerOptions().position(currentLatLng
                        ?: LatLng(0.0, 0.0)).anchor(.5f, .5f).flat(true).rotation(currentBearing))
                setMarkerIcon()
            } else {
//                carMarker?.rotation = currentBearing
                carMarker?.let {
                    //                        animateMarker(it, currentLatLng ?: LatLng(0.0, 0.0), false)
//                        rotateMarker(it, currentBearing, startRotation)
                    animateMarker()
                    if (isCurrentLocation) {
                        updateCameraWithBearing()
                    }
                }
            }
            if (isFirstTime) {
//                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng
//                            ?: LatLng(0.0, 0.0), 15f))
                updateCameraWithBearing()
            }
            sendIdleDataToSocket(currentLatLng ?: LatLng(0.0, 0.0))
            line?.remove()
        }
    }

    private fun updateCameraWithBearing() {
        val camPos = CameraPosition
                .builder(mMap.cameraPosition) // current Camera
                .bearing(currentBearing)
                .zoom(15f)
                .target(currentLatLng ?: LatLng(0.0, 0.0))
                .build()
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(camPos))
    }

    override fun polyLine(jsonRootObject: JSONObject) {
        val routeArray = jsonRootObject.getJSONArray("routes")
        if (routeArray.length() == 0) {
            return
        }
        val routes = routeArray.getJSONObject(0)
        line?.remove()
        val overviewPolyLines = routes.getJSONObject("overview_polyline")
        val encodedString = overviewPolyLines.getString("points")
        val legsJsonObject = (((routes.get("legs") as JSONArray).get(0) as JSONObject))
        estimatedDistance = (legsJsonObject.get("distance") as JSONObject).get("text") as String
        estimatedTime = (legsJsonObject.get("duration") as JSONObject).get("text") as String
        sendBroadcast(Intent("updateETA").putExtra("time", estimatedTime).putExtra("distance", estimatedDistance))
        line?.remove()
        list.clear()
        list.addAll(decodePoly(encodedString))

        /*    if (list.size >= 2) {
                currentLatLng = list[1]
            }*/

        if (carMarker == null) {
            carMarker = mMap.addMarker(MarkerOptions().position(currentLatLng
                    ?: LatLng(0.0, 0.0)).anchor(.5f, .5f).rotation(currentBearing).flat(true))
            setMarkerIcon()
        }
        line = mMap.addPolyline(PolylineOptions()
                .addAll(list)
                .width(8f)
                .color(if (mMap.mapType == GoogleMap.MAP_TYPE_NORMAL) ContextCompat.getColor(this, R.color.text_dark) else ContextCompat.getColor(this, R.color.text_dark))
                .geodesic(true))
        if (list.size >= 2) {
            val polylineModel = PolylineModel(
                    encodedString,
                    estimatedDistance,
                    (legsJsonObject.get("distance") as JSONObject).getInt("value"),
                    estimatedTime,
                    (legsJsonObject.get("duration") as JSONObject).getInt("value"))

            updateMarkerOfDriver(list[0], polylineModel)
        }

        mMap.uiSettings.isZoomGesturesEnabled = true
    }

    private fun decodePoly(encoded: String): ArrayList<LatLng> {

        val poly = java.util.ArrayList<LatLng>()
        var index = 0
        val len = encoded.length
        var lat = 0
        var lng = 0
        while (index < len) {
            var b: Int
            var shift = 0
            var result = 0
            do {
                b = encoded[index++].toInt() - 63
                result = result or (b and 0x1f shl shift)
                shift += 5
            } while (b >= 0x20)
            val dlat = if (result and 1 != 0) (result shr 1).inv() else result shr 1
            lat += dlat
            shift = 0
            result = 0
            do {
                b = encoded[index++].toInt() - 63
                result = result or (b and 0x1f shl shift)
                shift += 5
            } while (b >= 0x20)
            val dlng = if (result and 1 != 0) (result shr 1).inv() else result shr 1
            lng += dlng
            val p = LatLng(lat.toDouble() / 1E5,
                    lng.toDouble() / 1E5)
            poly.add(p)
        }
        return poly
    }


    private fun showMarker() {
        val builder = LatLngBounds.Builder()
        sourceLatLong?.let { builder.include(currentLatLng) }
        destLatLong?.let { builder.include(destLatLong) }
        val bounds = builder.build()
//        mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds,
//                resources.getDimensionPixelOffset(R.dimen.map_padding_camera)))
        updateCameraWithBearing()
        drawPolyLine()
    }

    private fun showCurrentMarker(b: Boolean) {
        if (!isServiceGoing) {
            destMarker?.remove()
            line?.remove()
        }
        if (carMarker == null) {
            carMarker = mMap.addMarker(MarkerOptions().anchor(0.5f, 0.5f).position(currentLatLng
                    ?: LatLng(0.0, 0.0)).flat(true).rotation(currentBearing))
            setMarkerIcon()
        }/* else {
            if (MapUtils.getDistanceBetweenTwoPoints(carMarker?.position
                            ?: LatLng(0.0, 0.0), currentLatLng ?: LatLng(0.0, 0.0)) > 100) {
                carMarker?.rotation = currentBearing
            }

        }*/
        updateCameraWithBearing()
    }

    private fun moveCameraToUserLocation(animate: Boolean) {
        carMarker?.let {
            val builder = CameraPosition.Builder()
            builder.zoom(15f)
            builder.target(currentLatLng ?: LatLng(0.0, 0.0))
            val cameraUpdate = CameraUpdateFactory.newCameraPosition(builder.build())
            if (animate) {
                mMap.animateCamera(cameraUpdate)
            } else {
                mMap.moveCamera(cameraUpdate)
            }
        }
    }

    private fun drawPolyLine() {
        sourceLatLong?.latitude.let {
            sourceLatLong?.longitude.let { it1 ->
                destLatLong?.latitude.let { it2 ->
                    destLatLong?.longitude.let { it3 ->
                        homePresenter.drawPolyLine(sourceLat = it ?: 0.0, sourceLong = it1
                                ?: 0.0,
                                destLat = it2 ?: 0.0, destLong = it3 ?: 0.0)
                    }
                }
            }
        }
    }


    private fun updateMarkerOfDriver(latLng: LatLng?, polylineModel: PolylineModel) {
        if (list.size < 2) {
            return
        }
        val startLatLng = carMarker?.position
        val rotation = currentBearing

        carMarker?.let {
            //            animateMarker()
//            if (isCurrentLocation) {
//                updateCameraWithBearing()
//            }
//            rotateMarker(it, rotation, startRotation)
            sendDataToSocket(latLng ?: LatLng(0.0, 0.0), polylineModel)
//            animateMarker(it, latLng ?: LatLng(0.0, 0.0), false)
//            animateMarker(it, currentLatLng ?: LatLng(0.0, 0.0), false)
//            animateMarker()

        }
    }


    private val receiverNotifications = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            when (intent?.getStringExtra("type")) {
                OrderStatus.DRIVER_ARROVED,
                OrderStatus.DRIVER_START,
                OrderStatus.SEARCHING,
                Events.ORDER_EVENT -> {
                    homePresenter.getOrderDetails(intent.getStringExtra("order_id").toLong())
                }

                OrderStatus.CUSTOMER_CANCEL -> {
                    homePresenter.onGoingOrderApi()
                }
            }
        }
    }


    private fun resetAllToDefault() {
        stopRingTone()
        tts?.stop()
        tts?.shutdown()
        isServiceGoing = false
        showCurrentMarker(true)
        line?.remove()
        mMap.setPadding(0, 0, 0, 0)

    }

    override fun onDestroy() {
        tts?.stop()
        tts?.shutdown()
        stopRingTone()
        isServiceGoing = false
        unregisterReceiver(receiverNotifications)
        unregisterReceiver(receiverToken)
        homePresenter.detachView()
        listRequests.clear()
        AppSocket.get().off("OrderEvent", orderEventListener)
        super.onDestroy()
    }

    override fun onStop() {
        tts?.stop()
        tts?.shutdown()
        stopRingTone()
        super.onStop()
    }

    override fun onResume() {
        super.onResume()
        val nMgr = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        nMgr.cancelAll()
        if (listRequests.size == 0) {
            currentLatLng?.let {
                sendIdleDataToSocket(it)
            }
        }
        if (profile?.online_status == "1") {
            homePresenter.driverStatusChange("1", false)
        }
    }


    private var startRotation = 0.0f

    private fun rotateMarker(marker: Marker?, toRotation: Float, st: Float) {
        rotateHandler?.removeCallbacks(rotateRunnable)
        rotateHandler = Handler()
        val start = SystemClock.uptimeMillis()
        val duration = 2000L
        val stRotation = marker?.rotation ?: 0f
        val interpolator = LinearInterpolator()
        val deltaRotation = Math.abs(toRotation - stRotation) % 360
        val rotation = (if (deltaRotation > 180) 360 - deltaRotation else deltaRotation) * if (toRotation - stRotation >= 0 && toRotation - stRotation <= 180 || toRotation - stRotation <= -180 && toRotation - stRotation >= -360)
            1
        else
            -1
//        rotateRunnable = object : Runnable {
//            override fun run() {
//                val elapsed = SystemClock.uptimeMillis() - start
//                val t = interpolator.getInterpolation(elapsed.toFloat() / duration)
//                val rot: Float = t * toRotation + (1 - t) * (stRotation ?: 0f)
//                marker?.setRotation((stRotation + t * rotation) % 360);
////                marker?.rotation = if (-rot > 180) rot / 2 else rot
////                startRotation = if (-rot > 180) rot / 2 else rot
//                if (t < 1.0) {
//                    // Post again 16ms later.
//                    rotateHandler?.postDelayed(this, 16)
//                }
//            }
//        }
//
//        rotateHandler?.post(rotateRunnable)

        var deltaRotation1 = Math.abs(toRotation - stRotation) % 360
        var rotation1 = 0f
        if (deltaRotation1 > 180) {
            360 - deltaRotation
        }
        val va = ValueAnimator.ofFloat(stRotation, rotation)
        va.duration = 2000L
        va.addUpdateListener {
            carMarker?.rotation = it.animatedValue as Float
        }
        va.start()
    }

    var valueAnimatorMove: ValueAnimator? = null
    private fun animateMarker() {
        if (carMarker != null) {
            val startPosition = carMarker?.position
            val endPosition = LatLng(currentLatLng?.latitude ?: 0.0, currentLatLng?.longitude
                    ?: 0.0)
            val startRotation = carMarker?.rotation
            val latLngInterpolator = LatLngInterpolator.LinearFixed()
            valueAnimatorMove?.cancel()
            valueAnimatorMove = ValueAnimator.ofFloat(0f, 1f)
            valueAnimatorMove?.duration = 10000 // duration 1 second
            valueAnimatorMove?.interpolator = LinearInterpolator()
            valueAnimatorMove?.addUpdateListener { animation ->
                try {
                    val v = animation.animatedFraction
                    val newPosition = latLngInterpolator.interpolate(v, startPosition
                            ?: LatLng(0.0, 0.0), endPosition)
                    carMarker?.position = newPosition
                    // carMarker?.setRotation(computeRotation(v, startRotation?:0f, currentBearing))
                } catch (ex: Exception) {
                    ex.printStackTrace()
                }
            }
            valueAnimatorMove?.start()
            rotateMarker()
        }
    }

    var valueAnimatorRotate: ValueAnimator? = null
    private fun rotateMarker() {
        if (carMarker != null) {
            val startPosition = carMarker?.position
            val endPosition = LatLng(currentLatLng?.latitude ?: 0.0, currentLatLng?.longitude
                    ?: 0.0)

            val latLngInterpolator = LatLngInterpolator.LinearFixed()
            valueAnimatorRotate?.cancel()
            val startRotation = carMarker?.rotation
            valueAnimatorRotate = ValueAnimator.ofFloat(0f, 1f)
//            valueAnimatorRotate?.startDelay = 200
            valueAnimatorRotate?.duration = 5000 // duration 2 second
            valueAnimatorRotate?.interpolator = LinearInterpolator()
            valueAnimatorRotate?.addUpdateListener { animation ->
                try {
                    val v = animation.animatedFraction
                    val newPosition = latLngInterpolator.interpolate(v, startPosition
                            ?: LatLng(0.0, 0.0), endPosition)
                    //                        carMarker?.setPosition(newPosition)
                    carMarker?.rotation = computeRotation(v, startRotation
                            ?: 0f, currentBearing)
                } catch (ex: Exception) {
                    ex.printStackTrace()
                }
            }
            valueAnimatorRotate?.start()

        }
    }

    private fun computeRotation(fraction: Float, start: Float, end: Float): Float {
        val normalizeEnd = end - start // rotate start to 0
        val normalizedEndAbs = (normalizeEnd + 360) % 360

        val direction = (if (normalizedEndAbs > 180) -1 else 1).toFloat() // -1 = anticlockwise, 1 = clockwise
        val rotation: Float
        rotation = if (direction > 0) normalizedEndAbs else normalizedEndAbs - 360
        val result = fraction * rotation + start
        return (result + 360) % 360
    }

    private interface LatLngInterpolator {
        fun interpolate(fraction: Float, a: LatLng, b: LatLng): LatLng
        class LinearFixed : LatLngInterpolator {
            override fun interpolate(fraction: Float, a: LatLng, b: LatLng): LatLng {
                val lat = (b.latitude - a.latitude) * fraction + a.latitude
                var lngDelta = b.longitude - a.longitude
                // Take the shortest path across the 180th meridian.
                if (Math.abs(lngDelta) > 180) {
                    lngDelta -= Math.signum(lngDelta) * 360
                }
                val lng = lngDelta * fraction + a.longitude
                return LatLng(lat, lng)
            }
        }
    }


    private var animateRunnable: Runnable? = null
    private var animateHandler: Handler? = null
    private var rotateRunnable: Runnable? = null
    private var rotateHandler: Handler? = null

    private fun animateMarker(marker: Marker, toPosition: LatLng, hideMarker: Boolean) {
        animateHandler?.removeCallbacks(animateRunnable)
        animateHandler = Handler()
        val start = SystemClock.uptimeMillis()
        val proj = mMap.projection
        val startPoint = proj?.toScreenLocation(marker.position)
        val startLatLng = proj?.fromScreenLocation(startPoint)
        val duration: Long = 3500
        val interpolator = LinearInterpolator()
        animateRunnable = object : Runnable {
            override fun run() {
                val elapsed = SystemClock.uptimeMillis() - start
                val t = interpolator.getInterpolation(elapsed.toFloat() / duration)
                val lng = t * toPosition.longitude + (1 - t) * (startLatLng?.longitude
                        ?: 0.0)
                val lat = t * toPosition.latitude + (1 - t) * (startLatLng?.latitude ?: 0.0)
                marker.position = LatLng(lat, lng)
                if (t < 1.0) {
                    // Post again 16ms later.
                    animateHandler?.postDelayed(this, 16)
                } else {
                    marker.isVisible = !hideMarker
                }
            }
        }
        animateHandler?.post(animateRunnable)
    }

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(LocaleManager.setLocale(newBase))
    }


    override fun logoutSuccess() {
        signOut()
    }

    override fun onOrderDetailsSuccess(response: Order?) {
        response?.let {
            if (response.order_status == null || response.order_status == OrderStatus.SEARCHING) {
                val timing = DateUtils.matchTimeExecuted(response.created_at ?: "")
                if (timing < 40) {
                    response.timeProgress = timing
                    openServiceRequest(Gson().toJson(response), "")
                }
            } else {
                openServiceRequest(Gson().toJson(response), "")
            }
        }
    }


    override fun requestRejected(orderId: Long?) {
        tts?.stop()
        tts?.shutdown()
        stopRingTone()
        orderId?.let {
            removeAdapterItemAtIndex(orderId)
        }
    }


    override fun onSupportListApiSuccess(response: List<Service>?) {
        rvSupportServices.layoutManager = GridLayoutManager(this, 2,
                OrientationHelper.VERTICAL,
                false)
        rvSupportServices.adapter = SupportServicesAdapter(response)
    }

    override fun onServiceEnd(orderId: Long?) {
        orderId?.let { removeAdapterItemAtIndex(orderId) }
    }

    override fun requestAccepted(order: Order?) {
        order?.let {
            listRequestOrderIds.find { order.order_id == it }?.let {
                listRequestOrderIds.remove(it)
            }
            homePresenter.onGoingOrderApi()
            /*if (order.future == "1") {
                homePresenter.onGoingOrderApi()
            } else {
                if (order.order_status != null && order.order_status != OrderStatus.SEARCHING) {
                    if (listRequests.find { it.order_id == order.order_id } == null) {
                        addRequest(order)
//                        mMap.setPadding(50, 150, 50, 550)
                        isServiceGoing = true
                        sourceLatLong = currentLatLng
                        if (order.category_id == 4 || order.category_id == 5) {
                            destLatLong = LatLng(order.pickup_latitude
                                    ?: 0.0, order.pickup_longitude
                                    ?: 0.0)
                        } else {
                            destLatLong = LatLng(order.dropoff_latitude
                                    ?: 0.0, order.dropoff_longitude
                                    ?: 0.0)
                        }

                        if (!hashMapMarkers.containsKey(order.order_id)) {
                            destMarker = mMap.addMarker(MarkerOptions().anchor(0.5f, 0.5f).position(destLatLong
                                    ?: LatLng(0.0, 0.0)))
                            if (order.category_id == 4 || order.category_id == 5) {
                                destMarker?.setIcon((BitmapDescriptorFactory.fromResource(R.drawable.ic_drop_location_other)))
                            } else {
                                destMarker?.setIcon((BitmapDescriptorFactory.fromResource(R.drawable.ic_drop_location_mrkr)))
                            }
                            hashMapMarkers[order.order_id] = destMarker
                        }
                        showMarker()
                    }
                }
            }*/
            tts?.stop()
            tts?.shutdown()
            stopRingTone()
        }
    }

    override fun requestRejected() {
        tts?.stop()
        tts?.shutdown()
        stopRingTone()
    }

    override fun onOrdersApiSuccess(response: ArrayList<Order>?) {

        response?.let { addRequestList(it, true) }
//            isServiceGoing = true
//            isFirstTime = false
        //  } else {

        // }
    }


    private fun startTextToSpeech(textToSpeech: String, order: Order) {
        tts?.stop()
        tts?.shutdown()
        stopRingTone()
        tts = TextToSpeech(this, TextToSpeech.OnInitListener {
            if (it == TextToSpeech.SUCCESS) {
                val result = tts?.setLanguage(Locale("en"))
                //if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                //"TTS", "This Language is not supported")
                //val installIntent = Intent()
                //installIntent.action = TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA
                //startActivity(installIntent)
                // } else {
                tts?.setOnUtteranceProgressListener(object : UtteranceProgressListener() {
                    override fun onDone(p0: String?) {
                        startRingTone()
                    }

                    override fun onError(p0: String?) {

                    }

                    override fun onStart(p0: String?) {

                    }

                })
                speak(textToSpeech + " delivery location" + order.dropoff_address)
                //}
            }
        })

    }

    private fun speak(text: String) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            tts?.speak(text, TextToSpeech.QUEUE_FLUSH, null, "UniqueID")
        } else {
            val map = HashMap<String, String>()
            map[TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID] = "UniqueID"
            tts?.speak(text, TextToSpeech.QUEUE_FLUSH, map)
        }
    }

    private fun startRingTone() {
        ringTonePlayer?.stop()
        ringTonePlayer = MediaPlayer.create(this, R.raw.custom_tone)
        ringTonePlayer?.isLooping = true
        ringTonePlayer?.start()
    }

    private fun stopRingTone() {
        ringTonePlayer?.stop()
    }

    override fun onApiSuccess(status: String?) {
        profile?.online_status = status
        SharedPrefs.with(this).save(PROFILE, profile)
        onOffSocketForEvents()
        if (profile?.online_status == "1") {
            ivOnlineOffline.setImageResource(R.drawable.ic_switch_on)
            getLocationWithPermissionCheck()
        } else {
            stopLocationUpdtes()
            fusedLocationClient.removeLocationUpdates(locationCallBack)
            ivOnlineOffline.setImageResource(R.drawable.ic_switch_off)
        }
    }

    override fun showLoader(isLoading: Boolean) {
        dialogIndeterminate.show(isLoading)
    }

    override fun apiFailure() {
        ivOnlineOffline?.showSnack(getString(R.string.sww_error))
    }

    override fun handleApiError(code: Int?, errorBody: String?) {
        ivOnlineOffline?.showSnack(errorBody.toString())
        if (code == StatusCode.UNAUTHORIZED) {
            signOut()
        }
    }

    private fun canDrawOverlays(context: Context): Boolean {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) return true
        else if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.O_MR1) {
            return Settings.canDrawOverlays(context)
        } else {
            if (Settings.canDrawOverlays(context)) return true
            else if (Build.MANUFACTURER == "Xiaomi") {
                return Settings.canDrawOverlays(context)
            }
            try {
                val mgr = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
                if (mgr == null) return false //getSystemService might return null
                val viewToAdd = View(context)
                val params = WindowManager.LayoutParams(0, 0, if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O)
                    WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY
                else
                    WindowManager.LayoutParams.TYPE_SYSTEM_ALERT,
                        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE or WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE, PixelFormat.TRANSPARENT)
                viewToAdd.layoutParams = params
                mgr.addView(viewToAdd, params)
                mgr.removeView(viewToAdd)
                return true
            } catch (e: Exception) {
                e.printStackTrace()
            }
            return false
        }
    }


    private fun showCancellationDialog() {
        dialogCode = Dialog(this)
        dialogCode.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialogCode.setCancelable(true)
        dialogCode.setCanceledOnTouchOutside(false)
        dialogCode.setContentView(R.layout.dialog_code_layout)
        dialogCode.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        val pinViewCode = dialogCode.findViewById(R.id.pinViewCode) as PinView
        val fabNextCode = dialogCode.findViewById(R.id.fabNextCode) as FloatingActionButton
        pinViewCode.setAnimationEnable(true)
        fabNextCode.setOnClickListener {

            if (pinViewCode.text.toString().length == 4) {
                CommonUtils.hideKeyboard(this, pinViewCode)
                if (CheckNetworkConnection.isOnline(this)) {
                    if (CheckNetworkConnection.isOnline(this)) {
                        val map = java.util.HashMap<String, String>()
                        map["otp_code"] = pinViewCode.text.toString()
                        homePresenter.verifyCode(map)
                    }
                } else {
                    CheckNetworkConnection.showNetworkError(tvCancelStart)
                }
            } else {
                Toast.makeText(this, getString(R.string.enter_enter_otp), Toast.LENGTH_SHORT).show()
            }
        }

        dialogCode.show()
        dialogCode.window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
    }

    override fun onSuccessCode(response: LoginModel?) {
        dialogCode.dismiss()
    }
}
