package com.buraq24.driver.ui.home.etokens.customers

import android.content.Intent
import android.net.Uri
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.buraq24.driver.R
import com.buraq24.driver.utils.getRatingSymbol
import com.buraq24.driver.webservices.models.customers.Customer
import com.buraq24.utilities.OrderStatus
import kotlinx.android.synthetic.main.item_area_customer.view.*

class AreaCustomersAdapter(private val customersList: ArrayList<Customer>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_area_customer, parent, false))
    }

    override fun getItemCount(): Int {
        return customersList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ViewHolder).onBind(customersList[position])
    }

    inner class ViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {

        init {
            itemView?.btnStartDelivery?.setOnClickListener {
                (it.context as AreaCustomersActivity).initiateOrderApiCall(customersList[adapterPosition]
                        .organisation_coupon_user_id)
            }
            itemView?.ivCall?.setOnClickListener {
                val phone = customersList[adapterPosition].phone_number.toString()
                val intent = Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null))
                itemView.context?.startActivity(intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP))
            }
        }

        fun onBind(customer: Customer?) = with(itemView) {
            tvCustomerName?.text = customer?.name
            tvAddress?.text = customer?.address
            tvProduct?.text = "${customer?.brand_name} · ${customer?.product_name} x ${customer?.bottle_quantity}"
            tvRating?.text = customer?.ratings_avg
            ivReaction.setImageResource(getRatingSymbol(customer?.ratings_avg?.toInt()))
            btnStartDelivery.isEnabled = false
            btnStartDelivery.setBackgroundResource(R.drawable.shape_customer_status_bg)
            btnStartDelivery.setTextColor(ContextCompat.getColor(context, R.color.text_dark))
            btnStartDelivery.text = when (customer?.todayOrderStatus) {
                "NoOrder" -> {
                    btnStartDelivery.isEnabled = true
                    btnStartDelivery.setBackgroundResource(R.drawable.back_status_accept)
                    btnStartDelivery.setTextColor(ContextCompat.getColor(context, R.color.white))
                    context?.getString(R.string.start_delivery)
                }
                OrderStatus.SERVICE_COMPLETE, OrderStatus.CUSTOMER_CONFIRM_ETOKEN -> {
                    context?.getString(R.string.completed)
                }
                OrderStatus.SCHEDULED, OrderStatus.DRIVER_PENDING, OrderStatus.DRIVER_APPROVED -> {
                    context?.getString(R.string.Scheduled)
                }
                OrderStatus.CUSTOMER_CONFIRMATION_PENDING_ETOKEN, OrderStatus.ONGOING -> {
                    context?.getString(R.string.ongoing)
                }
                OrderStatus.CONFIRMED -> {
                    context?.getString(R.string.confirmed)
                }
                OrderStatus.REACHED -> {
                    context.getString(R.string.reached)
                }
                OrderStatus.C_TIMEOUT_ETOKEN -> {
                    context?.getString(R.string.timeout)
                }
                else -> {
                    context?.getString(R.string.cancelled)
                }
            }
        }


    }

}