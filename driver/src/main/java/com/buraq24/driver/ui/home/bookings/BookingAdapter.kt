package com.buraq24.driver.ui.home.bookings

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.request.RequestOptions
import com.buraq24.driver.R
import com.buraq24.driver.utils.getFormattedPrice
import com.buraq24.driver.utils.getPaymentStringId
import com.buraq24.driver.webservices.models.order.Order
import com.buraq24.utilities.DateUtils
import com.buraq24.utilities.MapUtils
import com.buraq24.utilities.OrderStatus
import com.buraq24.utilities.PaymentType
import jp.wasabeef.glide.transformations.RoundedCornersTransformation
import kotlinx.android.synthetic.main.item_booking.view.*


class BookingAdapter(private val listBookings: ArrayList<Order?>, private val onClickBookingDetails: OnClickBookingDetails) : RecyclerView.Adapter<BookingAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
            ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_booking, parent,
                    false))

    override fun getItemCount(): Int = listBookings.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(listBookings[position])
    }

    inner class ViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {
        private var orderData: Order? = null

        init {
            itemView?.setOnClickListener {
                onClickBookingDetails.onOpenBookingDetails(orderData)
            }
        }

        fun bind(orderData: Order?) {
            this.orderData = orderData
            with(itemView) {
                var requestOptions = RequestOptions()
                requestOptions = requestOptions.transforms(CenterCrop(), RoundedCornersTransformation(8,
                        0, RoundedCornersTransformation.CornerType.TOP))
                var lat = orderData?.pickup_latitude ?: 0.0
                var lng = orderData?.pickup_longitude ?: 0.0
                if (orderData?.dropoff_latitude?.toInt() != 0 && orderData?.dropoff_latitude?.toInt() != 0) {
                    lat = orderData?.dropoff_latitude ?: 0.0
                    lng = orderData?.dropoff_longitude ?: 0.0
                }

                Glide.with(context).applyDefaultRequestOptions(requestOptions).load(MapUtils.getStaticMapWithMarker(context, lat, lng)).into(ivMap)
                tvDateTime.text = DateUtils.getFormattedTimeForBooking(orderData?.order_timings
                        ?: "")
                val paymentType = context.getString(getPaymentStringId(orderData?.payment?.paymentType
                        ?: "0"))
                if (orderData?.payment?.paymentType == PaymentType.E_TOKEN) {
                    tvPaymentTypeAmount.text = "$paymentType · ${orderData.payment?.productQuantity}"
                } else {
                    tvPaymentTypeAmount.text = "$paymentType · ${getFormattedPrice(orderData?.payment?.finalCharge)} ${context.getString(R.string.currency)}"
                }
                tvBookingId.text = "Id:${orderData?.order_token}"
                tvBookingStatus.text =
                        when (orderData?.order_status) {
                            OrderStatus.SERVICE_COMPLETE,
                            OrderStatus.CUSTOMER_CONFIRM_ETOKEN -> context.getString(R.string.completed)

                            OrderStatus.CONFIRMED -> context.getString(R.string.confirmed)

                            OrderStatus.REACHED -> {
                                context.getString(R.string.reached)
                            }

                            OrderStatus.CUSTOMER_CONFIRMATION_PENDING_ETOKEN -> context?.getString(R.string.approval_pending)

                            OrderStatus.C_TIMEOUT_ETOKEN -> context?.getString(R.string.timeout)

                            OrderStatus.DRIVER_ARROVED,
                            OrderStatus.DRIVER_START,
                            OrderStatus.DRIVER_SCHEDULE_SERVICE,
                            OrderStatus.SCHEDULED -> context.getString(R.string.schedule)

                            else -> context.getString(R.string.canceled)
                        }
            }


        }
    }

    fun addBookings(listBookings: ArrayList<Order?>) {
        this.listBookings.addAll(listBookings)
        notifyDataSetChanged()
    }

    interface OnClickBookingDetails {
        fun onOpenBookingDetails(orderData: Order?)
    }
}