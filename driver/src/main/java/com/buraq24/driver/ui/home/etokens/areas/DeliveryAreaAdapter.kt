package com.buraq24.driver.ui.home.etokens.areas

import android.app.Activity
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.buraq24.driver.R
import com.buraq24.driver.ui.home.etokens.customers.AreaCustomersActivity
import com.buraq24.driver.webservices.models.customers.Area
import com.buraq24.utilities.Constants
import kotlinx.android.synthetic.main.item_delivery_areas.view.*

class DeliveryAreaAdapter(private val areasList: ArrayList<Area>?) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_delivery_areas, parent, false))
    }

    override fun getItemCount(): Int {
        return areasList?.size ?: 0
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ViewHolder).onBind(areasList?.get(position))
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        init {
            itemView.setOnClickListener {
                val intent = Intent(itemView.context, AreaCustomersActivity::class.java)
                intent.putExtra("area", areasList?.get(adapterPosition)?.label)
                intent.putExtra("organisation_area_id", areasList?.get(adapterPosition)?.organisation_area_id)
                intent.putExtra(Constants.LATITUDE, (itemView.context as DeliveryAreasActivity).location?.latitude)
                intent.putExtra(Constants.LONGITUDE, (itemView.context as DeliveryAreasActivity).location?.longitude)
                itemView.context.startActivity(intent)
                (itemView.context as? Activity)?.finish()
            }
        }

        fun onBind(area: Area?) = with(itemView) {
            tvAreaName?.text = area?.label
        }
    }

}