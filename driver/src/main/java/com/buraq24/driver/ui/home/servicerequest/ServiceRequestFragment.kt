package com.buraq24.driver.ui.home.servicerequest


import android.content.Context
import android.os.Bundle
import android.os.CountDownTimer
import android.os.Handler
import android.support.annotation.StringRes
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.OrientationHelper
import android.view.ContextThemeWrapper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.buraq24.driver.R
import com.buraq24.driver.utils.getFinalCharge
import com.buraq24.driver.utils.getRatingSymbol
import com.buraq24.driver.utils.getServiceName
import com.buraq24.driver.webservices.models.order.Order
import com.buraq24.showSWWerror
import com.buraq24.showSnack
import com.buraq24.utilities.*
import com.buraq24.utilities.constants.PREF_CURRENT_LAT
import com.buraq24.utilities.constants.PREF_CURRENT_LNG
import com.buraq24.utilities.constants.PREF_DISTANCE
import com.buraq24.utilities.constants.PROFILE
import com.buraq24.utilities.webservices.models.AppDetail
import com.google.android.gms.maps.model.LatLng
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_service_request.*
import org.json.JSONArray
import org.json.JSONObject


class ServiceRequestFragment : Fragment(), ServiceRequestContract.View {


    override fun handleOrderError(order: Order?) {
        when (order?.order_status) {
            OrderStatus.CUSTOMER_CANCEL -> {
                showToast(R.string.order_already_cancelled_by_customer)
                onAcceptRequest.refreshOrders(order)
            }
            OrderStatus.DRIVER_CANCELLED -> {
                showToast(R.string.order_already_cancelled_by_driver)
                onAcceptRequest.refreshOrders(order)
            }
            OrderStatus.SERVICE_REJECT -> {
                showToast(R.string.order_already_rejected_by_driver)
                onAcceptRequest.refreshOrders(order)
            }
            OrderStatus.SERVICE_TIMEOUT, OrderStatus.DRIVER_SCHEDULED_TIMEOUT -> {
                showToast(R.string.request_timed_out)
                onAcceptRequest.refreshOrders(order)
            }
            else -> {
                val profileData = SharedPrefs.get().getObject(PROFILE, AppDetail::class.java)
                if (order?.driver_user_detail_id != profileData.user_detail_id) {
                    showToast(R.string.request_accepted_by_other_driver)
                } else {
                    showToast(R.string.invalid_order)
                }
                onAcceptRequest.refreshOrders(order)
            }
        }
        if (order != null) {
            activity?.supportFragmentManager?.beginTransaction()?.remove(this@ServiceRequestFragment)
                    ?.commitAllowingStateLoss()
        }
    }

    override fun onChangeTurnSuccess() {

    }

    override fun onConfirmAcceptApiSuccess2(order: Order?) {
        onAcceptRequest.requestAccepted(order)
    }

    override fun onRateApiSuccess() {

    }

    override fun onStartApiSuccess() {

    }

    override fun onEndApiSuccess() {

    }

    override fun onReachApiSuccess() {

    }

    override fun polyLine(jsonRootObject: JSONObject) {
        val routeArray = jsonRootObject.getJSONArray("routes")
        if ((routeArray?.length() ?: 0) > 0) {
            val routes = routeArray.getJSONObject(0)
            if ((routes.get("legs") as JSONArray).get(0) != null) {
                val estimatedDistance = (((routes.get("legs") as JSONArray).get(0) as JSONObject).get("distance")
                        as JSONObject).get("value") as Int
                SharedPrefs.with(activity).save(PREF_DISTANCE, estimatedDistance.div(1000).toInt())
                orderDetail.order_distance = estimatedDistance.div(1000).toInt().toString()
                val distance = (((routes.get("legs") as JSONArray).get(0) as JSONObject).get("distance") as JSONObject).get("text") as String
                val time = (((routes.get("legs") as JSONArray).get(0) as JSONObject).get("duration") as JSONObject).get("text") as String
                tvDistanceStartRequest?.text = distance
                tvTimeStartRequest?.text = time
            }
        }

    }

    override fun onAcceptApiSuccess(result: Order?) {
        if (orderDetail.future == "1") {
            onAcceptRequest.requestAccepted(result)
            activity?.supportFragmentManager?.beginTransaction()?.remove(this)
                    ?.commitAllowingStateLoss()
        } else {
            val profileData = SharedPrefs.get().getObject(PROFILE, AppDetail::class.java)
            Logger.e("order_driver_id", result?.driver?.user_detail_id.toString())
            Logger.e("my_driver_id", result?.driver?.user_detail_id.toString())
            if (result?.driver_user_detail_id == profileData.user_detail_id) {
                result?.order_status = OrderStatus.CONFIRMED
                onAcceptRequest.requestAccepted(result)
                activity?.supportFragmentManager?.beginTransaction()?.remove(this@ServiceRequestFragment)
                        ?.commitAllowingStateLoss()
                timer?.cancel()
            } else {
                showToast(R.string.request_accepted_by_other_driver)
                onAcceptRequest.refreshOrders(result)
            }
        }
    }

    private fun showToast(@StringRes msg: Int?) {
        Toast.makeText(activity, msg ?: 0, Toast.LENGTH_LONG).show()
    }

    override fun onRejectApiSuccess() {
        activity?.supportFragmentManager?.beginTransaction()?.remove(this@ServiceRequestFragment)
                ?.commitAllowingStateLoss()
        onAcceptRequest.requestRejected()
        timer?.cancel()

    }

    private var timeLimit = 45000L
    private var timer: CountDownTimer? = null
    private val presenter = ServiceRequestPresenter()
    private lateinit var orderDetail: Order
    private lateinit var onAcceptRequest: OnAcceptRequest
    private var dialogIndeterminate: DialogIndeterminate? = null
    private var handler = Handler()


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val contextThemeWrapper = ContextThemeWrapper(activity, R.style.ThemeRequest_Dark)
        val profile = SharedPrefs.with(context).getObject(PROFILE, AppDetail::class.java)
        val localInflater = inflater.cloneInContext(contextThemeWrapper)
        return localInflater.inflate(R.layout.fragment_service_request, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.attachView(this)
        tvAccept.isEnabled = true
        tvCancel.isEnabled = true
        orderDetail = Gson().fromJson(arguments?.getString("order"), Order::class.java)
        dialogIndeterminate = DialogIndeterminate(view.context)
        setData()
        val lat = SharedPrefs.with(activity).getFloat(PREF_CURRENT_LAT,
                0.0f)
        val long = SharedPrefs.with(activity).getFloat(PREF_CURRENT_LNG, 0.0f)
        val destLatLong =
                when (orderDetail.brand?.category_id) {
                    CategoryId.GAS, CategoryId.WATER_TANKER, CategoryId.MINERAL_WATER -> {
                        LatLng(orderDetail.dropoff_latitude
                                ?: 0.0, orderDetail.dropoff_longitude
                                ?: 0.0)
                    }
                    else -> {
                        LatLng(orderDetail.pickup_latitude
                                ?: 0.0, orderDetail.pickup_longitude
                                ?: 0.0)
                    }
                }

        presenter.drawPolyLine(lat.toDouble(), long.toDouble(), destLatLong.latitude, destLatLong.longitude)
        timeLimit -= (orderDetail.timeProgress ?: 0) * 1000
        progressBar.max = timeLimit.toInt()
        timer = object : CountDownTimer(timeLimit, 16) {
            override fun onFinish() {
                activity?.supportFragmentManager?.beginTransaction()?.remove(this@ServiceRequestFragment)
                        ?.commitAllowingStateLoss()
                onAcceptRequest.onOrderTimeoutOrError()
            }

            override fun onTick(millisUntilFinished: Long) {
                progressBar?.progress = timeLimit.toInt() - millisUntilFinished.toInt()
            }
        }
        tvTotal.text = getFinalCharge(orderDetail, 0) + " " + getString(R.string.currency)
        progressBar.visibility = View.VISIBLE
        startTimer()
        setListeners()
    }

    private fun setData() {
        val services = resources.getStringArray(R.array.services_list)
        tvProductName.text = getServiceName(orderDetail, services)
        textViewPickLocation?.visibility = View.INVISIBLE

        if(orderDetail.brand?.category_id==7)
        {
            textView18.text=getString(R.string.drop_off_location)
        }


        if (orderDetail.brand?.category_id == 1 || orderDetail.brand?.category_id == 2 || orderDetail.brand?.category_id == 3) {
            tvLocation2?.visibility = View.GONE
            rvImage?.visibility = View.GONE
            tvAdditionalInfo?.visibility = View.GONE
        } else {
            textViewPickLocation.visibility = View.VISIBLE
            tvLocation2?.visibility = View.VISIBLE
            tvLocation2?.text = orderDetail.pickup_address
            if ((orderDetail.order_images_url?.size ?: 0) > 0) {
                rvImage?.visibility = View.VISIBLE
                rvImage?.layoutManager = LinearLayoutManager(context, OrientationHelper.HORIZONTAL, false)
                rvImage?.adapter = ImageAdapter(orderDetail.order_images_url ?: ArrayList())
            }
            if (!orderDetail.details.isNullOrEmpty()) {
                tvAdditionalInfo.visibility = View.VISIBLE
                tvAdditionalInfo.text = orderDetail.details
            }

        }
        if (orderDetail.future == "1") {
            rlSchedule.visibility = View.VISIBLE
            tvScheduleDate.text = DateUtils.getFormattedTimeForBooking(orderDetail.order_timings
                    ?: "")
        } else {
            rlSchedule.visibility = View.GONE
        }
        tvQuantity.text = when {
            orderDetail.brand?.category_id == 1 -> orderDetail.brand?.name + " x " + orderDetail.payment?.productQuantity
            orderDetail.brand?.category_id == 3 -> orderDetail.brand?.name + " x " + orderDetail.payment?.productQuantity
            orderDetail.brand?.category_id == 4 -> orderDetail.brand?.name
            else -> orderDetail.brand?.name + " x " + orderDetail.payment?.productQuantity
        }
        tvLocation.text = orderDetail.dropoff_address
        tvName.text = orderDetail.user?.name
        if (orderDetail.user?.rating_avg?.isNotEmpty() == true) {
            ivRating.visibility = View.VISIBLE
            textView.visibility = View.VISIBLE
            ivRating.setImageResource(getRatingSymbol(orderDetail.user?.rating_avg?.toInt() ?: 0))
            textView.text = orderDetail.user?.rating_count?.toString() ?: "0"
        } else {
            ivRating.visibility = View.GONE
            textView.visibility = View.GONE
        }
        Glide.with(imageView.context).setDefaultRequestOptions(RequestOptions()
                .circleCrop().placeholder(R.drawable.ic_user_black)).load(orderDetail.user?.profile_pic_url)
                .into(imageView)
    }

    private val buttonsDisableRunnable = Runnable {
        tvAccept.isEnabled = true
        tvCancel.isEnabled = true
    }


    private fun setListeners() {
        val lat = SharedPrefs.with(activity).getFloat(PREF_CURRENT_LAT,
                0.0f)
        val long = SharedPrefs.with(activity).getFloat(PREF_CURRENT_LNG, 0.0f)
        val latlng = LatLng(lat.toDouble(), long.toDouble())

        tvAccept.setOnClickListener {
            val profile = SharedPrefs.with(tvAccept.context).getObject(PROFILE, AppDetail::class.java)

//            tvAccept.isEnabled = false
//            tvCancel.isEnabled = false
//            handler.postDelayed(buttonsDisableRunnable, 1500)
            if (profile.online_status == "1") {
                if (CheckNetworkConnection.isOnline(activity)) {
                    timer?.cancel()
                    if (orderDetail.order_status == null || orderDetail.order_status == OrderStatus.SEARCHING) {
                        presenter.acceptRequest(latlng, orderDetail.order_id?.toLong() ?: 0)
                    } else {
                        presenter.confirmScheduleRequest(latlng, orderDetail.order_id?.toLong()
                                ?: 0, 1)
                    }
                } else {
                    CheckNetworkConnection.showNetworkError(tvAccept)
                }
            } else {
                view?.showSnack(getString(R.string.go_online_to_accept_request))
            }
        }
        tvCancel.setOnClickListener {
//            tvAccept.isEnabled = false
//            tvCancel.isEnabled = false
//            handler.postDelayed(buttonsDisableRunnable, 1500)

            if (CheckNetworkConnection.isOnline(activity)) {
                if (orderDetail.order_status == null || orderDetail.order_status == OrderStatus.SEARCHING) {
                    presenter.rejectRequest(latlng, orderDetail.order_id ?: 0)
                } else {
                    presenter.confirmScheduleRequest(latlng, orderDetail.order_id?.toLong()
                            ?: 0, 0)
                }
            } else {
                CheckNetworkConnection.showNetworkError(tvCancel)
            }

        }
    }

    private fun startTimer() {
        timer?.start()
    }


    override fun showLoader(isLoading: Boolean) {
        dialogIndeterminate?.show(isLoading)
    }

    override fun apiFailure() {
        view?.showSWWerror()
    }

    override fun handleApiError(code: Int?, error: String?) {
        view?.showSnack(error.toString())
        timer?.cancel()
        activity?.supportFragmentManager?.popBackStackImmediate()
        onAcceptRequest.onOrderTimeoutOrError()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        handler.removeCallbacks(buttonsDisableRunnable)
        dialogIndeterminate?.show(false)
        presenter.detachView()
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        onAcceptRequest = context as OnAcceptRequest
    }

    interface OnAcceptRequest {
        fun requestAccepted(order: Order?)
        fun requestRejected()
        fun refreshOrders(order: Order?)
        fun onOrderTimeoutOrError()
    }

}
