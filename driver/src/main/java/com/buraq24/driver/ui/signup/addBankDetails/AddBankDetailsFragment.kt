package com.buraq24.driver.ui.signup.addBankDetails


import android.app.FragmentManager
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.bumptech.glide.Glide
import com.buraq24.Utils
import com.buraq24.driver.R
import com.buraq24.driver.ui.MultiSelectionSpinner
import com.buraq24.driver.ui.signup.addMulkiyanDetails.AddMulkiyanDetailsFragment
import com.buraq24.driver.ui.signup.enterdetails.AddPersonalDetailsFragment
import com.buraq24.driver.utils.EmojiExcludeFilter
import com.buraq24.driver.webservices.models.BankDetailsResult
import com.buraq24.driver.webservices.models.RequestPersonalDetails
import com.buraq24.driver.webservices.models.homeapi.Product
import com.buraq24.showSWWerror
import com.buraq24.showSWWerrorWithError
import com.buraq24.showSnack
import com.buraq24.utilities.*
import com.buraq24.utilities.constants.*
import com.buraq24.utilities.webservices.models.Brand
import com.buraq24.utilities.webservices.models.LoginModel
import com.buraq24.utilities.webservices.models.Service
import kotlinx.android.synthetic.main.fragment_add_bank_details.*


class AddBankDetailsFragment : Fragment(), AddBankDetailsContract.View,
        MultiSelectionSpinner.OnMultipleItemsSelectedListener {
    private var listProducts = java.util.ArrayList<Product>()

    override fun onProductsSuccess(products: ArrayList<Product>?) {
        listProducts.clear()
        listProducts.addAll(products ?: java.util.ArrayList())
        if (listProducts.isNotEmpty() && serviceId=="7")
            brandProductIds = listProducts.get(0).category_brand_product_id?.toString() ?: ""
        setSpinnerForCategory()
    }


    private val presenter = AddBankDetailsPresenter()
    private var brandProductIds = ""
    private var listServices = ArrayList<Service>()
    private var listbrands = ArrayList<Brand>()
    private var serviceId = ""
    private var brandId = 0L

    private lateinit var dialogIndeterminate: DialogIndeterminate

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_add_bank_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.attachView(this)
        editText.filters = arrayOf(EmojiExcludeFilter())
        editText2.filters = arrayOf(EmojiExcludeFilter())
        val profile = SharedPrefs.with(view.context).getObject(PROFILE, LoginModel::class.java)
        listServices.clear()

        listServices.addAll(profile?.services ?: ArrayList())
        dialogIndeterminate = DialogIndeterminate(activity)
        setListeners()
        setSpinner()
    }

    private fun setSpinner() {
        val adapter = ArrayAdapter(activity,
                android.R.layout.simple_spinner_item, listServices)
        adapter.setDropDownViewResource(R.layout.item_capacity)
        spinner.adapter = adapter
    }

    private fun setSpinnerForCategory() {
        val listProductsName = ArrayList<String>()
        listProducts.forEach {
            listProductsName.add(it.name ?: "")
        }
        if (listProductsName.isNotEmpty()) {
            spinner3.setItems(listProductsName)
            spinner3.setListener(this)

        }
    }

    override fun selectedStrings(strings: MutableList<String>?) {
        brandProductIds = ""
        strings?.forEach {
            brandProductIds = if (brandProductIds.isNotEmpty()) {
                "$brandProductIds,${listProducts.find { pro -> pro.name == it }?.category_brand_product_id}"
            } else {
                listProducts.find { pro -> pro.name == it }?.category_brand_product_id.toString()
            }
        }
        if (strings?.isEmpty() == true) {
            brandProductIds = ""
        }
    }

    override fun selectedIndices(indices: MutableList<Int>?) {

    }

    override fun onDestroyView() {
        super.onDestroyView()
        presenter.detachView()
    }

    private fun setListeners() {
        fabNext.setOnClickListener {
            val brandIds = "[" +
                    brandProductIds + "]"
            // if(serviceId == "1" || serviceId == "2" || serviceId == "3" || serviceId=="4") {
            if (brandProductIds.isNotEmpty()) {
                presenter.addBankDetails(RequestPersonalDetails(category_id = serviceId,
                        category_brand_id = brandId, category_brand_product_ids = brandIds))
            } else {
                fabNext.showSWWerrorWithError(R.string.select_category)
            }
            /*  }
              else{
                  fabNext.showSWWerrorWithError(R.string.only_gas_water_allowed)
              }*/
        }

        spinner2.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                brandId = listbrands[position].category_brand_id?.toLong() ?: 0L
                presenter.getProducts(brandId.toString())
                Glide.with(spinner2.context).load(listbrands[position].image_url).into(imageOffer)
            }
        }

        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                serviceId = (listServices[position].category_id).toString()

                group.visibility = if (serviceId == "7") View.GONE else View.VISIBLE


                listbrands.clear()
                listbrands.addAll(listServices[position].brands)
                brandId = listbrands[0].category_brand_id?.toLong() ?: 0L
                setBrandsAdapter()
                if (listServices[position].default_brands != "0") {
                    spinner2.visibility = View.GONE
                    imageOffer.visibility = View.GONE
                    textView10.visibility = View.GONE
                    view1.visibility = View.GONE
                    presenter.getProducts(brandId.toString())
                } else {
                    spinner2.visibility = View.VISIBLE
                    imageOffer.visibility = View.VISIBLE
                    textView10.visibility = View.VISIBLE
                    view1.visibility = View.VISIBLE
                }
            }
        }


        ivProfile.setOnClickListener {
            if (activity?.supportFragmentManager?.findFragmentByTag(AddPersonalDetailsFragment::class.java.simpleName) != null) {
                activity?.supportFragmentManager?.popBackStack()
            } else {
                activity?.supportFragmentManager?.popBackStack("backstack", FragmentManager.POP_BACK_STACK_INCLUSIVE)
            }

        }
    }

    private fun setBrandsAdapter() {
        val adapter = ArrayAdapter(activity,
                android.R.layout.simple_spinner_item, listbrands)
        adapter.setDropDownViewResource(R.layout.item_capacity)
        spinner2.adapter = adapter
    }


    override fun onApiSuccess(response: BankDetailsResult?) {
        ACCESS_TOKEN = response?.AppDetail?.access_token ?: ""
        SharedPrefs.with(context).save(ACCESS_TOKEN_KEY, ACCESS_TOKEN)
        SharedPrefs.with(activity).save(PROFILE, response?.AppDetail)
        SharedPrefs.with(activity).save(PRODUCTS, response)
        SharedPrefs.with(activity).save(STEP, 3)

        val bundle = Bundle()
        bundle.putString("categoryId", serviceId)
        val addMulkiyanFrag = AddMulkiyanDetailsFragment()
        addMulkiyanFrag.arguments = bundle
        activity?.supportFragmentManager?.let { it1 ->
            Utils.addFragment(it1, addMulkiyanFrag,
                    R.id.container, AddMulkiyanDetailsFragment::class.java.simpleName)
        }
    }

    override fun showLoader(isLoading: Boolean) {
        dialogIndeterminate.show(isLoading)
    }

    override fun apiFailure() {
        fabNext.showSWWerror()
    }

    override fun handleApiError(code: Int?, error: String?) {
        fabNext.showSnack(error.toString())
    }

}
