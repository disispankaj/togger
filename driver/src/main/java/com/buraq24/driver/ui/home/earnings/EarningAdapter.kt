package com.buraq24.driver.ui.home.earnings

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.buraq24.driver.R
import com.buraq24.driver.webservices.models.earning.EarningResultModel
import com.buraq24.utilities.constants.ORDER_COMPLETED
import kotlinx.android.synthetic.main.earning_item_layout.view.*
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*

class EarningAdapter(val response: ArrayList<EarningResultModel?>) :
        RecyclerView.Adapter<EarningAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
            ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.earning_item_layout, parent, false))

    override fun getItemCount(): Int = response.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(response.get(position))
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(data: EarningResultModel?) {
            val dateFormat = SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.getDefault())
            val date = dateFormat.parse(data?.created_at)
            val dateFormat2 = SimpleDateFormat("MMM dd, hh:mm aa", Locale.getDefault())
            itemView.tvPriceEarn.text = DecimalFormat("0.00").format((data?.final_charge)!!.toFloat()) + "${" USD"}"
            itemView.tvQuantityEarn.text = "Quantity: " + data.brand?.name + " × ${data.product_quantity}"
            itemView.tvEarningDate.text = (dateFormat2.format(date)).toString()
            if ((data.Order?.order_status ?: "").equals(ORDER_COMPLETED))
                itemView.tvOrderStatus.text = "Order Status: Completed"
            else
                itemView.tvOrderStatus.text = "Order Status: Cancelled"
        }
    }
}