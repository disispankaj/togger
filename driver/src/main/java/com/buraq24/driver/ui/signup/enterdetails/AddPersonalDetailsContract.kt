package com.buraq24.driver.ui.signup.enterdetails

import com.buraq24.driver.webservices.models.Organization
import com.buraq24.driver.webservices.models.RequestPersonalDetails
import com.buraq24.driver.webservices.models.ResponseCommon
import com.buraq24.utilities.basearc.BasePresenter
import com.buraq24.utilities.basearc.BaseView
import com.buraq24.utilities.webservices.models.AppDetail

class AddPersonalDetailsContract{

    interface View: BaseView{
        fun onApiSuccess(response: ResponseCommon?)
        fun onOrgsSuccess(listOrg: ArrayList<Organization>)
    }

    interface Presenter: BasePresenter<View>{
        fun addPersonalDetails(requestPersonalDetails: RequestPersonalDetails)
        fun getOrganization()
    }

}