package com.buraq24.driver.ui.home.etokens.deliver


import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast

import com.buraq24.driver.R
import com.buraq24.driver.ui.home.invoice.InvoiceFragment
import com.buraq24.driver.utils.signOut
import com.buraq24.driver.webservices.ApiResponse
import com.buraq24.driver.webservices.models.order.Order
import com.buraq24.showSWWerror
import com.buraq24.showSnack
import com.buraq24.utilities.*
import com.buraq24.utilities.constants.ACCESS_TOKEN
import com.buraq24.utilities.constants.ORDER
import com.buraq24.utilities.constants.SUCCESS_CODE
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import io.socket.client.Ack
import io.socket.emitter.Emitter
import kotlinx.android.synthetic.main.fragment_deliver.*
import org.jetbrains.anko.runOnUiThread
import org.json.JSONObject
import java.util.*

class DeliverFragment : Fragment(), DeliverContract.View {

    private var order: Order? = null

    private val presenter = DeliverPresenter()

    private var dialogIndeterminate: DialogIndeterminate? = null

    private var checkIdleTimer = Timer()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_deliver, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.attachView(this)
        dialogIndeterminate = DialogIndeterminate(activity)
        order = Gson().fromJson(arguments?.getString(ORDER), Order::class.java)
        setListeners()
    }

    override fun onResume() {
        super.onResume()
        if (AppSocket.get().isConnected) {
            restartCheckIdleTimer(0)
        }
        AppSocket.get().on(Events.ORDER_EVENT, orderEventListener)
    }

    override fun onPause() {
        super.onPause()
        AppSocket.get().off(Events.ORDER_EVENT, orderEventListener)
        checkIdleTimer.cancel()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        presenter.detachView()
    }

    private fun setListeners() {
        tvDeliver.setOnClickListener {
            if (etQuantity?.text?.toString()?.trim()?.isEmpty() == true || etQuantity?.text?.toString()?.trim()?.toInt() == 0) {
                rootView?.showSnack(R.string.quantity_empty_error)
            } else {
                deliverOrderApiCall()
            }
        }
    }

    private val orderEventListener = Emitter.Listener { args ->
        restartCheckIdleTimer(20000)
        activity?.runOnUiThread {
            when (JSONObject(args[0].toString()).getString("type")) {

                OrderEventType.CUSTOMER_CONFIRM_ETOKEN -> {
                    val orderJson = JSONObject(args[0].toString()).getJSONArray("order").get(0).toString()
                    val orderModel = Gson().fromJson(orderJson, Order::class.java)
                    if (order?.order_id == orderModel?.order_id) {
                        order = orderModel
                        openInvoiceFragment()
                    }
                }

                OrderEventType.CUSTOMER_CONFIRMATION_PENDING_ETOKEN -> {
                    val orderJson = JSONObject(args[0].toString()).getJSONArray("order").get(0).toString()
                    val orderModel = Gson().fromJson(orderJson, Order::class.java)
                    if (order?.order_id == orderModel?.order_id) {
                        order = orderModel
                        tvDeliver.text = getString(R.string.approval_pending)
                        tvDeliver.isEnabled = false
                    }
                }
                OrderEventType.C_TIMEOUT_ETOKEN->{
                    activity?.sendBroadcast(Intent("tokenRequest"))
                    Toast.makeText(activity, R.string.order_timeout_error, Toast.LENGTH_LONG).show()
                    activity?.finish()
                }

                OrderEventType.CUSTOMER_CANCELLED_ETOKEN->{
                    activity?.sendBroadcast(Intent("tokenRequest"))
                    Toast.makeText(activity, R.string.order_cancelled_by_customer, Toast.LENGTH_LONG).show()
                    activity?.finish()
                }

            }
        }
    }

    private fun deliverOrderApiCall() {
        if (CheckNetworkConnection.isOnline(activity)) {
            val map = HashMap<String, String?>()
            map["order_id"] = order?.order_id.toString()
            map["latitude"] = order?.dropoff_latitude?.toString()
            map["longitude"] = order?.dropoff_longitude?.toString()
            map["product_quantity"] = etQuantity.text.toString().trim()
            presenter.deliverOrderApi(map)
        } else {
            CheckNetworkConnection.showNetworkError(rootView)
        }
    }

    override fun onApiSuccess(order: Order?) {
        this.order = order
        tvDeliver.text = getString(R.string.approval_pending)
        tvDeliver.isEnabled = false
    }

    private fun openInvoiceFragment() {
        val fragment = InvoiceFragment()
        val bundle = Bundle()
        bundle.putString("order", Gson().toJson(this@DeliverFragment.order))
        fragment.arguments = bundle
        fragmentManager?.beginTransaction()?.replace(R.id.container, fragment)?.commit()
    }

    override fun showLoader(isLoading: Boolean) {
        dialogIndeterminate?.show(isLoading)
    }

    override fun apiFailure() {
        rootView?.showSWWerror()
    }

    override fun handleApiError(code: Int?, error: String?) {
        if (code == StatusCode.UNAUTHORIZED) {
            signOut(activity)
        } else {
            rootView.showSnack(error ?: "")
        }
    }

    private fun restartCheckIdleTimer(checkIdleTimeInterval: Long) {
        checkIdleTimer.cancel()
        checkIdleTimer = Timer()
        checkIdleTimer.schedule(object : TimerTask() {
            override fun run() {
                if (CheckNetworkConnection.isOnline(activity)) {
                    checkOrderStatus()
                }
                restartCheckIdleTimer(20000L)
            }
        }, checkIdleTimeInterval)
    }

    private fun checkOrderStatus() {
        val request = JSONObject()
        request.put("type", OrderEventType.CUSTOMER_SINGLE_ORDER)
        request.put("access_token", ACCESS_TOKEN)
        request.put("order_token", order?.order_token)
        AppSocket.get().emit(Events.COMMON_EVENT, request, Ack { args ->
            val response = Gson().fromJson<ApiResponse<Order>>(args[0].toString(),
                    object : TypeToken<ApiResponse<Order>>() {}.type)
            if (response?.statusCode == SUCCESS_CODE) {
                val orderModel = response.result
                if (order?.order_id == orderModel?.order_id) {
                    context?.runOnUiThread {
                        when (orderModel?.order_status) {

                            OrderEventType.CUSTOMER_CONFIRM_ETOKEN -> {
                                activity?.sendBroadcast(Intent("tokenRequest"))
                                openInvoiceFragment()
                            }

                            OrderEventType.CUSTOMER_CONFIRMATION_PENDING_ETOKEN -> {
                                tvDeliver.text = getString(R.string.approval_pending)
                                tvDeliver.isEnabled = false
                            }

                            OrderEventType.C_TIMEOUT_ETOKEN->{
                                activity?.sendBroadcast(Intent("tokenRequest"))
                                Toast.makeText(activity, R.string.order_timeout_error, Toast.LENGTH_LONG).show()
                                activity?.finish()
                            }

                            OrderEventType.CUSTOMER_CANCELLED_ETOKEN->{
                                activity?.sendBroadcast(Intent("tokenRequest"))
                                Toast.makeText(activity, R.string.order_cancelled_by_customer, Toast.LENGTH_LONG).show()
                                activity?.finish()
                            }
                        }
                    }
                }
            } else if (response.statusCode == StatusCode.UNAUTHORIZED) {
//                AppUtils.logout(this)
            }
        })
    }

}
