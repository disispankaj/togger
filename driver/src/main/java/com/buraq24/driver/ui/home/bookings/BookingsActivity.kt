package com.buraq24.driver.ui.home.bookings

import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.buraq24.GeneralFragmentPagerAdapter
import com.buraq24.driver.R
import com.buraq24.driver.ui.home.bookings.BookingsListFragment.Companion.TYPE_PAST
import com.buraq24.driver.ui.home.bookings.BookingsListFragment.Companion.TYPE_UPCOMING
import com.buraq24.driver.utils.LocaleHelper
import com.buraq24.driver.utils.getPhoneLang
import com.buraq24.utilities.LocaleManager
import com.buraq24.utilities.SharedPrefs
import com.buraq24.utilities.constants.LANGUAGE_CODE
import kotlinx.android.synthetic.main.activity_bookings.*

class BookingsActivity : AppCompatActivity(), BookingsListFragment.OnBookingCancelled {
    override fun onBookingCancelled() {
        viewPagerBookings?.adapter?.notifyDataSetChanged()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bookings)
        setAdapter()
        toolbar.setNavigationOnClickListener {
            onBackPressed()
        }
    }


    private fun setAdapter() {
        val adapter = GeneralFragmentPagerAdapter(supportFragmentManager)
        val bundle = Bundle()
        val fragment1 = BookingsListFragment()
        bundle.putString("type", TYPE_PAST)
        fragment1.arguments = bundle
        fragment1.setInterface(this)
        adapter.addFragment(fragment1, getString(R.string.past))

        val bundle1 = Bundle()
        val fragment2 = BookingsListFragment()
        bundle1.putString("type", TYPE_UPCOMING)
        fragment2.arguments = bundle1
        fragment1.setInterface(this)
        adapter.addFragment(fragment2, getString(R.string.upcoming))

        viewPagerBookings.adapter = adapter
        tabLayout.setupWithViewPager(viewPagerBookings)

    }

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(LocaleManager.setLocale(newBase))
    }
}