package com.buraq24.driver.ui.home.serviceAfterStart

import com.buraq24.utilities.basearc.BasePresenter
import com.buraq24.utilities.basearc.BaseView
import com.google.android.gms.maps.model.LatLng

class ServiceAfterStartContract{

    interface View: BaseView{
        fun onApiSuccess()
    }

    interface Presenter: BasePresenter<View>{
        fun dropAtAddressRequest(lalLng: LatLng, orderId: Long)
    }
}