package com.buraq24.driver.ui.home.serviceAfterAccept


import android.app.Dialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions

import com.buraq24.driver.R
import com.buraq24.driver.utils.getFinalCharge
import com.buraq24.driver.ui.home.serviceAfterStart.ServiceAfterStartFragment
import com.buraq24.driver.utils.getRatingSymbol
import com.buraq24.driver.utils.signOut
import com.buraq24.driver.webservices.models.order.Order
import com.buraq24.showSWWerror
import com.buraq24.showSnack
import com.buraq24.utilities.*
import com.buraq24.utilities.constants.PREF_CURRENT_LAT
import com.buraq24.utilities.constants.PREF_CURRENT_LNG
import com.buraq24.utilities.constants.PREF_DISTANCE
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_after_accept.*


class ServiceAfterAcceptFragment : Fragment(), ServiceAfterAcceptContract.View {

    override fun onStartApiSuccess() {
        val fragment = ServiceAfterStartFragment()
        val bundle = Bundle()
        bundle.putString("order", arguments?.getString("order"))
        bundle.putString("time", time)
        bundle.putString("distance", distance)
        fragment.arguments = bundle
        activity?.supportFragmentManager?.beginTransaction()?.
                replace(R.id.container,fragment)
                ?.commitAllowingStateLoss()
    }

    override fun onRejectApiSuccess() {
        for (fragment in activity?.supportFragmentManager?.fragments?:ArrayList()) {
            if (!(fragment is SupportMapFragment))
                activity?.supportFragmentManager?.beginTransaction()?.remove(fragment)
                        ?.commitAllowingStateLoss()
        }
        onCancelRequest.onCanceled()
    }
    private var time =""
    private var distance =""
    private val presenter = ServiceAfterAcceptPresenter()
    private lateinit var onCancelRequest: OnCancelRequest

    private lateinit var orderDetail: Order


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_after_accept, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity?.registerReceiver(receiver, IntentFilter("updateETA"))
        presenter.attachView(this)
        orderDetail = Gson().fromJson(arguments?.getString("order"), Order::class.java)
        setData()
        setListeners()
    }

    private fun setData() {
        val services= resources.getStringArray(R.array.services_list)
        tvProductNameAccept.text = if (orderDetail.category_id == 1) {
            services[1] +" . " + orderDetail.brand?.name +" x "+orderDetail.payment?.productQuantity
        } else if(orderDetail.category_id == 3) {
            services[3]+"."+ orderDetail.product?.name +" x "+orderDetail.payment?.productQuantity
        }
        else {
            orderDetail.brand?.brand_name+"."+ orderDetail.brand?.name +" x "+orderDetail.payment?.productQuantity
        }
        tvNameAccept.text = orderDetail.user?.name
        if(orderDetail.user?.rating_avg?.isNotEmpty()== true) {
            ivRatingAccept.visibility = View.VISIBLE
            textViewAccept.visibility = View.VISIBLE
            ivRatingAccept.setImageResource(getRatingSymbol(orderDetail.user?.rating_avg?.toInt() ?: 0))
            textViewAccept.text = orderDetail.user?.rating_count?.toString() ?: "0"
        }
        else{
            ivRatingAccept.visibility = View.GONE
            textViewAccept.visibility = View.GONE
        }
        tvTotalAccept.text = getFinalCharge(orderDetail,SharedPrefs.get().getInt(PREF_DISTANCE,1))+" "+getString(R.string.currency)
        Glide.with(imageViewAccept.context).setDefaultRequestOptions(RequestOptions()
                .circleCrop().placeholder(R.drawable.ic_user_black)).load(orderDetail.user?.profile_pic_url)
                .into(imageViewAccept)
    }

    private fun setListeners() {
        val lat= SharedPrefs.with(activity).getFloat(PREF_CURRENT_LAT,
               0.0f)
        val long =SharedPrefs.with(activity).
                getFloat(PREF_CURRENT_LNG,0.0f)
        val latlng= LatLng(lat.toDouble(),long.toDouble())
        tvStart.setOnClickListener {
            if (CheckNetworkConnection.isOnline(activity)) {
                presenter.startRequest(latlng,orderDetail.order_id?.toLong()?:0,distance)
            }
        }
        tvCancelStart.setOnClickListener {
            showCancellationDialog(latlng)
        }

        imageViewCallAccept.setOnClickListener {
            val phone = orderDetail.user?.phone_number?.toString()?:""
            val intent = Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null))
            startActivity(intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP))
        }
    }

    private fun showCancellationDialog(latlng: LatLng) {
        val dialog = Dialog(activity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(true)
        dialog.setCanceledOnTouchOutside(true)
        dialog.setContentView(R.layout.dialog_cancel_reason)
        dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        val etMessage = dialog.findViewById(R.id.etMessage) as EditText
        val tvSubmit = dialog.findViewById(R.id.tvSubmit) as TextView
        tvSubmit.setOnClickListener {
            if (etMessage.text.toString().trim().isNotEmpty()) {
                val reason = etMessage.text.toString().trim()
                if (CheckNetworkConnection.isOnline(activity)) {
                    if (CheckNetworkConnection.isOnline(activity)) {
                        presenter.rejectRequest(latlng,orderDetail.order_id?.toLong()?:0,reason)
                    }
                    dialog.dismiss()
                } else {
                    CheckNetworkConnection.showNetworkError(tvCancelStart)
                }
            } else {
                Toast.makeText(activity, getString(R.string.cancellation_reason_validation_text), Toast.LENGTH_SHORT).show()
            }
        }

        dialog.show()
        dialog.window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
    }


    override fun showLoader(isLoading: Boolean) {

    }

    override fun apiFailure() {
        view?.showSWWerror()
    }

    override fun handleApiError(code: Int?, error: String?) {
        if (code == StatusCode.UNAUTHORIZED) {
            signOut(activity)
            return
        }
        view?.showSnack(error.toString())
    }

    override fun onDestroyView() {
        super.onDestroyView()
        activity?.unregisterReceiver(receiver)
        presenter.detachView()
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        onCancelRequest = context as OnCancelRequest
    }
    interface OnCancelRequest{
        fun onCanceled()
    }

    private val receiver = object : BroadcastReceiver(){
        override fun onReceive(context: Context?, intent: Intent?) {
            time = intent?.getStringExtra("time")?:""
            distance = intent?.getStringExtra("distance")?:""
            tvDistanceAccept?.text = distance
            tvTimeAccept?.text = time
        }
    }
}
