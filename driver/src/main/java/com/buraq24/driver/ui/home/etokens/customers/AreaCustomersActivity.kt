package com.buraq24.driver.ui.home.etokens.customers

import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.buraq24.driver.R
import com.buraq24.driver.utils.signOut
import com.buraq24.driver.webservices.models.customers.Customer
import com.buraq24.driver.webservices.models.customers.CustomersModel
import com.buraq24.driver.webservices.models.order.Order
import com.buraq24.showSWWerror
import com.buraq24.showSnack
import com.buraq24.utilities.*
import kotlinx.android.synthetic.main.activity_delivery_areas.*

class AreaCustomersActivity : AppCompatActivity(), AreaCustomersContract.View,
        SwipeRefreshLayout.OnRefreshListener {

    private val presenter = AreaCustomersPresenter()

    private val customersList = ArrayList<Customer>()

    private var dialogIndeterminate: DialogIndeterminate? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_delivery_areas)
        presenter.attachView(this)
        dialogIndeterminate = DialogIndeterminate(this)
        toolbar?.title = intent?.getStringExtra("area")
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = AreaCustomersAdapter(customersList)
        customersListApi()
        setListeners()
    }

    private fun setListeners() {
        swipeRefreshLayout?.setOnRefreshListener(this)
        toolbar?.setNavigationOnClickListener { onBackPressed() }
    }

    override fun onRefresh() {
        customersListApi()
    }

    private fun customersListApi() {
        if (!CheckNetworkConnection.isOnline(this)) {
            CheckNetworkConnection.showNetworkError(rootView)
            return
        }
        val map = HashMap<String, String>()
        map["organisation_area_id"] = intent.getIntExtra("organisation_area_id", -1).toString()
        map["latitude"] = intent.getDoubleExtra(Constants.LATITUDE, 0.0).toString()
        map["longitude"] = intent.getDoubleExtra(Constants.LATITUDE, 0.0).toString()
        map["skip"] = "0"
        map["take"] = "1000"
        presenter.areaListingApi(map)
    }

    fun initiateOrderApiCall(organisationCouponUserId: Int?) {
        if (CheckNetworkConnection.isOnline(this)) {
            val map = HashMap<String, String>()
            map["organisation_coupon_user_id"] = organisationCouponUserId.toString()
            map["latitude"] = intent.getDoubleExtra(Constants.LATITUDE, 0.0).toString()
            map["longitude"] = intent.getDoubleExtra(Constants.LATITUDE, 0.0).toString()
            dialogIndeterminate?.show()
            presenter.initiateOrderApi(map)
        } else {
            CheckNetworkConnection.showNetworkError(rootView)
        }
    }

    override fun onApiSuccess(customersModel: CustomersModel?) {
        customersList.clear()
        customersModel?.customers?.let { customersList.addAll(it) }
        if (customersList.isEmpty()) {
            tvNoData?.visibility = View.VISIBLE
        } else {
            tvNoData?.visibility = View.GONE
        }
        recyclerView?.adapter?.notifyDataSetChanged()
    }

    override fun onInitiateOrderApiSuccess(order: Order?) {
        dialogIndeterminate?.dismiss()
        sendBroadcast(Intent("tokenRequest"))
        finish()
//        customersListApi()

    }

    override fun showLoader(isLoading: Boolean) {
        swipeRefreshLayout?.isRefreshing = isLoading
    }

    override fun apiFailure() {
        dialogIndeterminate?.dismiss()
        rootView?.showSWWerror()
    }

    override fun handleApiError(code: Int?, error: String?) {
        dialogIndeterminate?.dismiss()
        if (code == StatusCode.UNAUTHORIZED) {
            signOut(this)
        } else {
            rootView.showSnack(error ?: "")
        }
    }

    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(LocaleManager.setLocale(newBase))
    }

}
