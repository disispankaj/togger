package com.buraq24.driver.ui.signup.addMulkiyanDetails

import com.buraq24.driver.webservices.ApiResponse
import com.buraq24.driver.webservices.RestClient
import com.buraq24.driver.webservices.models.RequestPersonalDetails
import com.buraq24.driver.webservices.models.ResponseCommon
import com.buraq24.getApiError
import com.buraq24.utilities.basearc.BasePresenterImpl
import com.buraq24.utilities.constants.SUCCESS_CODE
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File


class AddMulkiyanDetailsPresenter : BasePresenterImpl<AddMulkiyanDetailsContract.View>(), AddMulkiyanDetailsContract.Presenter {

    override fun addMulkiyanDetails(requestPersonalDetails: RequestPersonalDetails) {
        getView()?.showLoader(true)
        /* val brandIds = "[" +
                 requestPersonalDetails.category_brand_product_ids + "]"*/


        val file = File(requestPersonalDetails.pic0)


        val file1 = File(requestPersonalDetails.pic1)


        val number = RequestBody.create(MediaType.parse("text/plain"),
                requestPersonalDetails.mulkiya_number)
//        val dateExpiry = RequestBody.create(MediaType.parse("text/plain"),
//                requestPersonalDetails.mulkiya_validity)
//
//        val bank_name = RequestBody.create(MediaType.parse("text/plain"),
//                requestPersonalDetails.bank_name)
//        val bank_acc_number = RequestBody.create(MediaType.parse("text/plain"),
//                requestPersonalDetails.bank_account_number)
        //val brands = RequestBody.create(MediaType.parse("text/plain"),
        //         brandIds)
        val image1 = RequestBody.create(MediaType.parse("image/jpeg"), file)
        val image2 = RequestBody.create(MediaType.parse("image/jpeg"), file1)
        val map = HashMap<String, RequestBody>()
//        map.put("mulkiya_number", number)
//        map.put("mulkiya_validity", dateExpiry)
//        map.put("bank_name",bank_name)
//        map.put("bank_account_number",bank_acc_number)

//        val vehicleColor = RequestBody.create(MediaType.parse("text/plain"),
//                requestPersonalDetails.vehicle_color!!)
//        val vehicleName = RequestBody.create(MediaType.parse("text/plain"),
//                requestPersonalDetails.vehicle_name!!)

        map.put("vehicle_name", number)
//        map.put("vehicle_color", vehicleColor)


        // map.put("category_brand_product_ids", brands)
        val data1 = MultipartBody.Part.createFormData("mulkiya",
                file.name, image1)
        val data2 = MultipartBody.Part.createFormData("mulkiya",
                file1.name, image2)
        val images = ArrayList<MultipartBody.Part>()
        images.add(data1)
        images.add(data2)
        // map.put("mulkiya\"; filename=\"pp.png\" ", image1)[]
        // map.put("mulkiya\"; filename=\"pp1.png\" ", image2)
        RestClient.get().addMulkiyanDetails(map, images)
                .enqueue(object : Callback<ApiResponse<ResponseCommon>> {
                    override fun onResponse(call: Call<ApiResponse<ResponseCommon>>?, response: Response<ApiResponse<ResponseCommon>>?) {
                        getView()?.showLoader(false)
                        if (response?.isSuccessful == true) {
                            if (response.body()?.statusCode == SUCCESS_CODE) {
                                getView()?.onApiSuccess(response.body()?.result)
                            } else {
                                getView()?.handleApiError(response.body()?.statusCode, response.body()?.msg)
                            }
                        } else {
                            val errorModel = getApiError(response?.errorBody()?.string())
                            getView()?.handleApiError(errorModel.statusCode, errorModel.msg)
                        }
                    }

                    override fun onFailure(call: Call<ApiResponse<ResponseCommon>>?, t: Throwable?) {
                        getView()?.showLoader(false)
                        getView()?.apiFailure()
                    }

                })
    }

}