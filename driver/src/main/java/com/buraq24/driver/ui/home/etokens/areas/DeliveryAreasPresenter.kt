package com.buraq24.driver.ui.home.etokens.areas

import com.buraq24.driver.webservices.ApiResponse
import com.buraq24.driver.webservices.RestClient
import com.buraq24.driver.webservices.models.customers.Area
import com.buraq24.getApiError
import com.buraq24.utilities.basearc.BasePresenterImpl
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DeliveryAreasPresenter : BasePresenterImpl<DeliveryAreasContract.View>(), DeliveryAreasContract.Presenter {
    override fun areaListingApi(map: HashMap<String, String>) {
        getView()?.showLoader(true)
        RestClient.get().areasList(map).enqueue(object : Callback<ApiResponse<List<Area>>> {
            override fun onResponse(call: Call<ApiResponse<List<Area>>>?, response: Response<ApiResponse<List<Area>>>?) {
                getView()?.showLoader(false)
                if (response?.isSuccessful == true) {
                    getView()?.onApiSuccess(response.body()?.result)
                } else {
                    val errorModel = getApiError(response?.errorBody()?.string())
                    getView()?.handleApiError(errorModel.statusCode, errorModel.msg)
                }
            }

            override fun onFailure(call: Call<ApiResponse<List<Area>>>?, t: Throwable?) {
                getView()?.showLoader(false)
                getView()?.apiFailure()
            }
        })
    }
}