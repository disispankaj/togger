package com.buraq24.driver.ui.home.servicerequest

import com.buraq24.driver.webservices.models.order.Order
import com.buraq24.utilities.basearc.BasePresenter
import com.buraq24.utilities.basearc.BaseView
import com.google.android.gms.maps.model.LatLng
import org.json.JSONObject

class ServiceRequestContract {

    interface View : BaseView {
        fun onAcceptApiSuccess(order: Order?)
        fun onRejectApiSuccess()
        fun onStartApiSuccess()
        fun onReachApiSuccess()
        fun onEndApiSuccess()
        fun polyLine(jsonRootObject: JSONObject)
        fun onRateApiSuccess()
        fun onConfirmAcceptApiSuccess2(order: Order?)
        fun onChangeTurnSuccess()
        fun handleOrderError(order: Order?)
    }

    interface Presenter : BasePresenter<View> {
        fun startRequest(lalLng: LatLng, distance:Double,orderId: Long)
        fun reachRequest(lalLng: LatLng, orderId: Long)
        fun rejectRequest(lalLng: LatLng, orderId: Long, reason: String)
        fun acceptRequest(lalLng: LatLng, orderId: Long)
        fun rejectRequest(lalLng: LatLng, orderId: Long)
        fun drawPolyLine(sourceLat: Double, sourceLong: Double, destLat: Double, destLong: Double)
        fun dropAtAddressRequest(lalLng: LatLng, orderId: Long)
        fun rateOrder(rating: Double, comments: String, orderId: Long)
        fun confirmScheduleRequest(lalLng: LatLng, orderId: Long, approve: Int)
        fun startOtherRide(orderId: Long)

    }
}