package com.buraq24.driver.ui.home.profileEdit

import com.buraq24.driver.webservices.ApiResponse
import com.buraq24.driver.webservices.ProfileResponse
import com.buraq24.driver.webservices.RestClient
import com.buraq24.driver.webservices.models.RequestPersonalDetails
import com.buraq24.driver.webservices.models.ResponseCommon
import com.buraq24.getApiError
import com.buraq24.utilities.basearc.BasePresenterImpl
import com.buraq24.utilities.constants.SUCCESS_CODE
import com.buraq24.utilities.webservices.models.LoginModel
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File

class ProfilePresenter : BasePresenterImpl<ProfileContract.View>(), ProfileContract.Presenter {

    override fun updateProfile(pic: RequestBody) {
        getView()?.showLoader(true)
        RestClient.get().picUpdate(pic).enqueue(object : Callback<ApiResponse<ResponseCommon>> {
            override fun onResponse(call: Call<ApiResponse<ResponseCommon>>?,
                                    response: Response<ApiResponse<ResponseCommon>>?) {
                getView()?.showLoader(false)
                if (response?.isSuccessful == true) {
                    if (response.body()?.statusCode == 200) {
                        getView()?.onPicApiSuccess(response.body()?.result)
                    } else {
                        getView()?.handleApiError(response.body()?.statusCode, response.body()?.msg)
                    }
                } else {
                    val errorModel = getApiError(response?.errorBody()?.string())
                    getView()?.handleApiError(errorModel.statusCode, errorModel.msg)
                }
            }

            override fun onFailure(call: Call<ApiResponse<ResponseCommon>>?, t: Throwable?) {
                getView()?.showLoader(false)
                getView()?.apiFailure()
            }
        })
    }

    override fun updateProfileData(requestPersonalDetails: RequestPersonalDetails) {
        getView()?.showLoader(true)

//        val number = RequestBody.create(MediaType.parse("text/plain"),
//                requestPersonalDetails.mulkiya_number)
//        val dateExpiry = RequestBody.create(MediaType.parse("text/plain"),
//                requestPersonalDetails.mulkiya_validity)
        val name = RequestBody.create(MediaType.parse("text/plain"),
                requestPersonalDetails.name)
        val map = HashMap<String, RequestBody>()

//        map.put("mulkiya_number", number)
        map.put("name",name)
//        map.put("mulkiya_validity", dateExpiry)


//        val vehicleColor = RequestBody.create(MediaType.parse("text/plain"),
//                requestPersonalDetails.vehicle_color!!)
        val vehicleName = RequestBody.create(MediaType.parse("text/plain"),
                requestPersonalDetails.vehicle_name!!)

        map.put("vehicle_name", vehicleName)
//        map.put("vehicle_color", vehicleColor)



        val call: Call<ProfileResponse<ResponseCommon>>
        if (!requestPersonalDetails.pic0.isNullOrEmpty() && !requestPersonalDetails.pic1.isNullOrEmpty()) {
            val file = File(requestPersonalDetails.pic0)
            val file1 = File(requestPersonalDetails.pic1)
            val image1 = RequestBody.create(MediaType.parse("image/jpeg"), file)
            val image2 = RequestBody.create(MediaType.parse("image/jpeg"), file1)
            val data1 = MultipartBody.Part.createFormData("mulkiya",
                    file.name, image1)
            val data2 = MultipartBody.Part.createFormData("mulkiya",
                    file1.name, image2)
            val images = ArrayList<MultipartBody.Part>()
            images.add(data1)
            images.add(data2)
            call = RestClient.get().updateProfileWithImage(map, images)
        }
        else {
            call = RestClient.get().updateProfile(map)
        }
        call.enqueue(object : Callback<ProfileResponse<ResponseCommon>> {
            override fun onResponse(call: Call<ProfileResponse<ResponseCommon>>?, response: Response<ProfileResponse<ResponseCommon>>?) {
                getView()?.showLoader(false)
                if (response?.isSuccessful == true) {
                    if (response.body()?.statusCode == SUCCESS_CODE) {
                        getView()?.onApiSuccess(response.body()?.AppDetail)
                    } else {
                        getView()?.handleApiError(response.body()?.statusCode, response.body()?.msg)
                    }
                } else {
                    val errorModel = getApiError(response?.errorBody()?.string())
                    getView()?.handleApiError(errorModel.statusCode, errorModel.msg)
                }
            }

            override fun onFailure(call: Call<ProfileResponse<ResponseCommon>>?, t: Throwable?) {
                getView()?.showLoader(false)
                getView()?.apiFailure()
            }

        })
    }


    override fun connectStripe(map: HashMap<String, String>) {
        getView()?.showLoader(true)
        RestClient.get().connectStripe(map).enqueue(object : Callback<ApiResponse<LoginModel>> {
            override fun onResponse(call: Call<ApiResponse<LoginModel>>?,
                                    response: Response<ApiResponse<LoginModel>>?) {
                getView()?.showLoader(false)
                if (response?.isSuccessful == true) {
                    if (response.body()?.statusCode == 200) {
                        getView()?.onSuccessConnectStripe()
                    } else {
                        getView()?.handleApiError(response.body()?.statusCode, response.body()?.msg)
                    }
                } else {
                    val errorModel = getApiError(response?.errorBody()?.string())
                    getView()?.handleApiError(errorModel.statusCode, errorModel.msg)
                }
            }

            override fun onFailure(call: Call<ApiResponse<LoginModel>>?, t: Throwable?) {
                getView()?.showLoader(false)
                getView()?.apiFailure()
            }
        })
    }
}