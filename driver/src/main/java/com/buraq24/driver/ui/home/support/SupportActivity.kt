package com.buraq24.driver.ui.home.support

import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.OrientationHelper
import com.buraq24.driver.R
import com.buraq24.driver.ui.home.SupportServicesAdapter
import com.buraq24.utilities.webservices.models.Service
import kotlinx.android.synthetic.main.activity_support.*
import com.buraq24.driver.utils.GridSpacingItemDecoration
import com.buraq24.driver.utils.signOut
import com.buraq24.showSWWerror
import com.buraq24.showSnack
import com.buraq24.utilities.*


class SupportActivity: AppCompatActivity() , SupportContract.View{

    private var supportList = ArrayList<Service>()
    private val presenter = SupportPresenter()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_support)
        presenter.attachView(this)
        toolbar.setNavigationOnClickListener {
            onBackPressed()
        }
        setAdapter()
        presenter.getSupportList()
    }

    private fun setAdapter() {
        rvSupportList.layoutManager = GridLayoutManager(this, 2, OrientationHelper.VERTICAL, false)
        rvSupportList.adapter = SupportServicesAdapter(supportList)
        val spacingInPixels = resources.getDimensionPixelSize(R.dimen.fab_margin)
        rvSupportList.addItemDecoration(GridSpacingItemDecoration(2, spacingInPixels, true))

    }

    override fun onSupportListApiSuccess(response: List<Service>?) {
        if(response?.isNotEmpty() == true){
            flipperSupport.displayedChild =1
            supportList.addAll(response)
            rvSupportList.adapter.notifyDataSetChanged()
        }
        else{
            flipperSupport.displayedChild = 2
        }
    }

    override fun showLoader(isLoading: Boolean) {

    }

    override fun apiFailure() {
        rvSupportList?.showSWWerror()
    }

    override fun handleApiError(code: Int?, error: String?) {
        if (code == StatusCode.UNAUTHORIZED) {
            signOut(this)
            return
        }
        rvSupportList?.showSnack(error.toString())
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.detachView()
    }
    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(LocaleManager.setLocale(newBase))
    }
}