package com.buraq24.driver.ui.home.emergencyContacts

import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import com.buraq24.driver.R
import com.buraq24.driver.webservices.models.EContact
import com.buraq24.driver.utils.signOut
import com.buraq24.showSWWerror
import com.buraq24.showSnack
import com.buraq24.utilities.*
import kotlinx.android.synthetic.main.activity_contacts.*


class EContactsActivity: AppCompatActivity() , EContactsContract.View{

    private var supportList = ArrayList<EContact>()
    private val presenter = EContactPresenter()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contacts)
        presenter.attachView(this)
        toolbar.setNavigationOnClickListener {
            onBackPressed()
        }
        setAdapter()
        presenter.getContactsList()
    }

    private fun setAdapter() {
        rvEContactList.layoutManager = LinearLayoutManager(this)
        rvEContactList.adapter = EmergencyContactsAdapter(supportList)
    }

    override fun onApiSuccess(response: List<EContact>?) {
        if(response?.isNotEmpty() == true){
            flipperContacts.displayedChild =1
            supportList.addAll(response)
            rvEContactList.adapter.notifyDataSetChanged()
        }
        else{
            flipperContacts.displayedChild = 2
        }
    }

    override fun showLoader(isLoading: Boolean) {

    }

    override fun apiFailure() {
        rvEContactList?.showSWWerror()
    }

    override fun handleApiError(code: Int?, error: String?) {
        if (code == StatusCode.UNAUTHORIZED) {
            signOut(this)
            return
        }
        rvEContactList?.showSnack(error.toString())
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.detachView()
    }

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(LocaleManager.setLocale(newBase))
    }
}
