package com.buraq24.driver.ui.home.rateCustomer

import com.buraq24.driver.webservices.RestClient
import com.buraq24.getApiError
import com.buraq24.utilities.basearc.BasePresenterImpl
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RateCustomerPresenter : BasePresenterImpl<RateCustomerContract.View>(), RateCustomerContract.Presenter {
    override fun rateOrder(rating: Double, comments: String, orderId: Long) {
        getView()?.showLoader(true)
        RestClient.get().rateService(rating,orderId,comments).
                enqueue(object : Callback<Any> {
                    override fun onResponse(call: Call<Any>?, response: Response<Any>?) {
                        getView()?.showLoader(false)
                        if (response?.isSuccessful == true) {
                            getView()?.onApiSuccess()
                        }else {
                            val errorModel = getApiError(response?.errorBody()?.string())
                            getView()?.handleApiError(errorModel.statusCode, errorModel.msg)
                        }
                    }

                    override fun onFailure(call: Call<Any>?, t: Throwable?) {
                        getView()?.showLoader(false)
                        getView()?.apiFailure()
                    }
                })

    }

}