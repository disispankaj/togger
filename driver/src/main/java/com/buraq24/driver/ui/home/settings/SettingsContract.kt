package com.buraq24.driver.ui.home.settings

import com.buraq24.utilities.basearc.BasePresenter
import com.buraq24.utilities.basearc.BaseView

class SettingsContract {

    interface View: BaseView{
        fun onSettingsApiSuccess()
        fun logoutSuccess()
    }

    interface Presenter: BasePresenter<View>{
        fun logout()
        fun updateSettingsApiCall(languageId: String?, notificationFlag: String?)
    }
}