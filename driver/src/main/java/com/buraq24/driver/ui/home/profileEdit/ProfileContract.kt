package com.buraq24.driver.ui.home.profileEdit

import com.buraq24.driver.webservices.ApiResponse
import com.buraq24.driver.webservices.models.RequestPersonalDetails
import com.buraq24.driver.webservices.models.ResponseCommon
import com.buraq24.utilities.basearc.BasePresenter
import com.buraq24.utilities.basearc.BaseView
import com.buraq24.utilities.webservices.models.LoginModel
import okhttp3.RequestBody

class ProfileContract{

    interface View: BaseView{
        fun onApiSuccess(response: ResponseCommon?)
        fun onPicApiSuccess(response: ResponseCommon?)

        fun onSuccessConnectStripe()
    }

    interface Presenter: BasePresenter<View>{
        fun updateProfile(pic : RequestBody)
        fun updateProfileData(requestPersonalDetails: RequestPersonalDetails)

        fun connectStripe(map : HashMap<String,String>)
    }

}