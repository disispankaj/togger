package com.buraq24.driver.ui.home.invoice


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.buraq24.driver.R
import com.buraq24.driver.utils.getFinalCharge
import com.buraq24.driver.ui.home.rateCustomer.RateCustomerFragment
import com.buraq24.driver.utils.getFormattedPrice
import com.buraq24.driver.webservices.models.order.Order
import com.buraq24.utilities.CategoryId
import com.buraq24.utilities.SharedPrefs
import com.buraq24.utilities.constants.PREF_DISTANCE
import com.buraq24.utilities.constants.PROFILE
import com.buraq24.utilities.constants.SERVICES
import com.buraq24.utilities.webservices.models.AppDetail
import com.buraq24.utilities.webservices.models.LoginModel
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_invoice.*

class InvoiceFragment : Fragment() {

    private var order: Order? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_invoice, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setInvoiceData()
        tvNextInvoice.setOnClickListener {
            val fragment = RateCustomerFragment()
            val bundle = Bundle()
            bundle.putString("order", arguments?.getString("order"))
            fragment.arguments = bundle
            activity?.supportFragmentManager?.beginTransaction()?.replace(R.id.container, fragment, RateCustomerFragment::class.java.name)
                    ?.commitAllowingStateLoss()
        }
    }

    private fun setInvoiceData() {
        order = Gson().fromJson(arguments?.getString("order"), Order::class.java)
        val profile = SharedPrefs.with(activity).getObject(PROFILE, AppDetail::class.java)
        tvServiceNameInvoice.text = when (profile?.category_id) {
            CategoryId.GAS -> {
                tvQuantityInvoice.text = """${order?.brand?.name} x ${order?.payment?.productQuantity}"""
                getString(R.string.gas)
            }
            CategoryId.MINERAL_WATER -> {
                tvQuantityInvoice.text = """${order?.brand?.name} x ${order?.payment?.productQuantity}"""
                order?.brand?.brand_name
            }
            CategoryId.WATER_TANKER -> {
                tvQuantityInvoice.text = """${order?.brand?.name} x ${order?.payment?.productQuantity}"""
                getString(R.string.water_tanker)
            }
            CategoryId.FREIGHT,CategoryId.FREIGHT_2 -> {
                val loginData = SharedPrefs.with(activity).getObject(SERVICES, LoginModel::class.java)
                val dataCategory = loginData?.services?.find { it.category_id == profile.category_id }

                val dataSubCat = dataCategory?.brands?.find { it.category_brand_id == profile.category_brand_id }

                val brandName = dataSubCat?.name ?: ""
                tvQuantityInvoice.text = order?.brand?.name
                brandName
            }
            else -> {
                getString(R.string.default_category_name)
            }
        }
        tvTotalLabel.setText(R.string.etoken_used)
        tvTotalInvoice.text = order?.payment?.productQuantity.toString()
        tvPriceInvoice.visibility = View.GONE
        tvTexInvoice.visibility = View.GONE
        tvBuraqPercentage.visibility = View.GONE
        tvGstInvoice.visibility = View.GONE
        tvBaseFairLabel.visibility = View.GONE
        tvTaxLabel.visibility = View.GONE
        tvBuraqPercentageLabel.visibility = View.GONE
        tvOthersLabel.visibility = View.GONE
//        tvPriceInvoice.text = "${getFormattedPrice(order?.payment?.initialCharge)} ${getString(com.buraq24.driver.R.string.currency)}" // base fair
//        tvTexInvoice.text = "${getFormattedPrice(0.0)} ${getString(com.buraq24.driver.R.string.currency)}"
//        tvBuraqPercentage.text = "${getFormattedPrice(order?.payment?.adminCharge)} ${getString(com.buraq24.driver.R.string.currency)}"
//        tvGstInvoice.text = "${getFormattedPrice(0.0)} ${getString(com.buraq24.driver.R.string.currency)}"
//        tvTotalInvoice.text = "${getFinalCharge(order, 1)} ${getString(com.buraq24.driver.R.string.currency)}"
    }


}
