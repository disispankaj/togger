package com.buraq24.driver.ui.ChatModule.chatMessage

import com.buraq24.utilities.basearc.BasePresenter
import com.buraq24.utilities.basearc.BaseView
import com.buraq24.utilities.chatModel.ChatMessageListing

class ChatMessageContract {


    interface View : BaseView {
        fun chatMessagesApiSuccess(chatList: ArrayList<ChatMessageListing>?, messageOrder: String)
    }

    interface Presenter : BasePresenter<View> {
        fun getChatMessagesApiCall(authorization: String, messageId: String, receiverId: String, limit: Int, skip: Int, messageOrder: String)
    }
}