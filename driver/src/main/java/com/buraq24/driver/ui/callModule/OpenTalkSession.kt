package com.buraq24.driver.ui.callModule


data class OpenTalkSession (var data:Data)

data class Data (var session_id:String,
                            var token:String)