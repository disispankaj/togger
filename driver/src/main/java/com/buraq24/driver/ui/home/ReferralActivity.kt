package com.buraq24.driver.ui.home

import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.buraq24.driver.R
import kotlinx.android.synthetic.main.activity_referral.*
import android.content.Intent
import com.buraq24.driver.utils.LocaleHelper
import com.buraq24.driver.utils.getPhoneLang
import com.buraq24.utilities.LocaleManager
import com.buraq24.utilities.SharedPrefs
import com.buraq24.utilities.constants.LANGUAGE_CODE


class ReferralActivity : AppCompatActivity(){

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_referral)
        toolbar.setNavigationOnClickListener {
            onBackPressed()
        }

        tvShare.setOnClickListener {
            shareLink()
        }

    }

    private fun shareLink() {
        val share = Intent(android.content.Intent.ACTION_SEND)
        share.type = "text/plain"
        share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        share.putExtra(Intent.EXTRA_SUBJECT, "Share Buraq24-Driver")
        share.putExtra(Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=com.buraq24.driver&hl=en_US")
        startActivity(Intent.createChooser(share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP), "Share Buraq24-Driver!"))
    }

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(LocaleManager.setLocale(newBase))
    }
}