package com.buraq24.driver.ui.signup.login

import com.buraq24.driver.webservices.models.SendOtp
import com.buraq24.utilities.basearc.BasePresenter
import com.buraq24.utilities.basearc.BaseView

class LoginContract{

    interface View: BaseView{
        fun onApiSuccess(response: SendOtp?)
    }

    interface Presenter: BasePresenter<View>{
        fun sendOtpApiCall(map: Map<String, String>)
    }

}