package com.buraq24.driver.ui.signup.addBankDetails

import com.buraq24.driver.webservices.models.BankDetailsResult
import com.buraq24.driver.webservices.models.Organization
import com.buraq24.driver.webservices.models.RequestPersonalDetails
import com.buraq24.driver.webservices.models.homeapi.Product
import com.buraq24.utilities.basearc.BasePresenter
import com.buraq24.utilities.basearc.BaseView
import com.buraq24.utilities.webservices.models.AppDetail

class AddBankDetailsContract{

    interface View: BaseView{
        fun onApiSuccess(response: BankDetailsResult?)
        fun onProductsSuccess(products: ArrayList<Product>?)
    }

    interface Presenter: BasePresenter<View>{
        fun addBankDetails(requestPersonalDetails: RequestPersonalDetails)
        fun getProducts(brandId: String)
    }

}