package com.buraq24.driver.ui.home.servicerequest

import android.support.v7.widget.RecyclerView
import android.view.View
import com.buraq24.driver.webservices.models.order.Order

class ServiceViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView){
    init {
        itemView?.setOnClickListener {

        }
    }

    fun onBind(orderDetails: Order) {

    }
}