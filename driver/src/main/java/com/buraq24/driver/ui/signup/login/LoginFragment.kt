package com.buraq24.driver.ui.signup.login


import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.widget.ListPopupWindow
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.text.style.UnderlineSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.ArrayAdapter
import com.buraq24.*
import com.buraq24.driver.R
import com.buraq24.driver.ui.signup.verifytop.OtpFragment
import com.buraq24.driver.webservices.models.SendOtp
import com.buraq24.utilities.*
import com.buraq24.driver.utils.location.LocationProvider
import com.google.android.gms.tasks.OnSuccessListener
import com.google.firebase.iid.FirebaseInstanceId
import com.hbb20.CountryCodePicker
import kotlinx.android.synthetic.main.fragment_login.*
import java.util.*
import com.buraq24.driver.MainActivity
import com.buraq24.driver.WebViewActivity
import com.buraq24.driver.utils.getTermsAndCond
import com.buraq24.utilities.constants.*

class LoginFragment : Fragment(), View.OnClickListener, LoginContract.View {

    private lateinit var locationProvider: LocationProvider

    private var isPhoneNoValid = false

    private val presenter = LoginPresenter()

    private lateinit var dialogIndeterminate: DialogIndeterminate

    private var langPickerPopup: ListPopupWindow? = null


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        activity?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.attachView(this)
        dialogIndeterminate = DialogIndeterminate(activity)
        countryCodePicker.registerCarrierNumberEditText(etPhone)
        setListeners()

        locationProvider = LocationProvider.CurrentLocationBuilder(activity, this).build()
        locationProvider.getLastKnownLocation(OnSuccessListener {
            SharedPrefs.with(activity).save(Constants.LATITUDE, it?.latitude ?: 0.0)
            SharedPrefs.with(activity).save(Constants.LONGITUDE, it?.longitude ?: 0.0)
        })
        val lang = getLanguageId(LocaleManager.getLanguage(activity)) - 1
        tvChangeLanguage.text = resources.getStringArray(R.array.language_list)[lang]
        textView.text = getString(R.string.agree_to) + " " + getString(R.string.terms_and_condition)
        setSpan()
        initLanguagePicker(tvChangeLanguage)
    }

    private fun setSpan() {
        val span = SpannableString(textView.text)
        span.setSpan(ForegroundColorSpan(ContextCompat.getColor(textView.context, R.color.colorPrimary)),
                textView.text.indexOf(getString(R.string.terms_and_condition)), textView.text.length
                , Spannable.SPAN_INCLUSIVE_INCLUSIVE)
        span.setSpan(UnderlineSpan(), textView.text.indexOf(getString(R.string.terms_and_condition)),
                textView.text.length, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
        textView.text = span
    }

    private fun setListeners() {
        fabNext.setOnClickListener(this)
        tvChangeLanguage.setOnClickListener(this)
        textView.setOnClickListener(this)
        countryCodePicker.setCountryForPhoneCode(91)
        countryCodePicker.setNumberAutoFormattingEnabled(false)
        countryCodePicker.setPhoneNumberValidityChangeListener(phoneNoValidationChangeListener)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.fabNext -> {
                if (isPhoneNoValid) {
                    if (checkboxView.isChecked) {
                        locationProvider.getLastKnownLocation(OnSuccessListener { it ->
                            SharedPrefs.with(activity).save(Constants.LATITUDE, it?.latitude?.toFloat()
                                    ?: 0.0f)
                            SharedPrefs.with(activity).save(Constants.LONGITUDE, it?.longitude?.toFloat()
                                    ?: 0.0f)
                            val map = HashMap<String, String>()
                            map["phone_code"] = countryCodePicker.selectedCountryCodeWithPlus
                            map["phone_number"] = etPhone.text.toString().replace(Regex("[^0-9]"), "")
                            map["timezone"] = TimeZone.getDefault().id
                            map["latitude"] = it?.latitude?.toString() ?: "0.0"
                            map["longitude"] = it?.longitude?.toString() ?: "0.0"
                            map["device_type"] = "Android"
                            FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener { result ->
                                map["fcm_id"] = result?.token ?: ""
                                presenter.sendOtpApiCall(map)
                            }
                            FirebaseInstanceId.getInstance().instanceId.addOnFailureListener { ex ->
                                Logger.e("FirebaseInstanceId", ex.message ?: "")
                                fabNext?.showSWWerror()
                            }
                        })
                    } else {
                        fabNext?.showSnack(getString(R.string.agree_to_terms_conditions))
                    }
                } else {
                    fabNext?.showSnack(getString(R.string.phone_validation_message))
                }
            }

            R.id.tvChangeLanguage -> {
                openLangPicker()
            }
            R.id.textView -> {
                startActivity(Intent(activity, WebViewActivity::class.java).putExtra(WebViewActivity.EXTRA_URL,
                        getTermsAndCond(SharedPrefs.get().getInt(PREFS_LANGUAGE_ID, 1)))
                        .putExtra(WebViewActivity.EXTRA_TITLE, getString(R.string.terms_and_condition)))
            }
        }
    }

    private fun initLanguagePicker(anchorView: View) {
        langPickerPopup = ListPopupWindow(anchorView.context)
        langPickerPopup?.setAdapter(ArrayAdapter(context,
                R.layout.support_simple_spinner_dropdown_item, resources.getStringArray(R.array.language_list)))
        langPickerPopup?.isModal = true
        langPickerPopup?.anchorView = anchorView
        langPickerPopup?.width = ListPopupWindow.WRAP_CONTENT
        langPickerPopup?.height = ListPopupWindow.WRAP_CONTENT
        langPickerPopup?.setSelection(getLanguageId(LocaleManager.getLanguage(activity)) - 1)
        Logger.e("langId", getLanguageId(LocaleManager.getLanguage(activity)) - 1)

        langPickerPopup?.setOnItemClickListener { _, _, position, _ ->
            if (LocaleManager.getLanguage(activity) != getLanguage(position + 1)) {
                SharedPrefs.get().save(PREFS_LANGUAGE_ID, position + 1)
                SharedPrefs.get().save(LANGUAGE_CHANGED, true)
                setNewLocale(getLanguage(position + 1), true)
            }
            langPickerPopup?.dismiss()
        }
    }

    private fun openLangPicker() {
        langPickerPopup?.show()
    }

    private fun setNewLocale(language: String, restartProcess: Boolean): Boolean {
        LocaleManager.setNewLocale(activity, language)

        val intent = Intent(activity, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)

        if (restartProcess) {
            System.exit(0)
        }
        return true
    }

    private val phoneNoValidationChangeListener = CountryCodePicker.PhoneNumberValidityChangeListener {
        isPhoneNoValid = it

    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        locationProvider.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        locationProvider.onActivityResult(requestCode, resultCode, data)
    }

    override fun onApiSuccess(response: SendOtp?) {
        ACCESS_TOKEN = response?.access_token ?: ""
        SharedPrefs.with(activity).save(ACCESS_TOKEN_KEY, ACCESS_TOKEN)
        SharedPrefs.with(activity).remove(PROFILE)
        SharedPrefs.with(activity).remove(SERVICES)
        val fragment = OtpFragment()
        val bundle = Bundle()
        bundle.putString("phone_number", countryCodePicker.selectedCountryCodeWithPlus + "-" + etPhone.text.toString().replace(Regex("[^0-9]"), ""))
        bundle.putString("phone_code", countryCodePicker.selectedCountryCodeWithPlus)
        bundle.putString("number", etPhone.text.toString().replace(Regex("[^0-9]"), ""))
        bundle.putString("otp", response?.otp.toString())
        bundle.putString("access_token", response?.access_token)
        fragment.arguments = bundle
        activity?.supportFragmentManager?.let { it1 ->
            Utils.replaceFragment(it1, fragment, R.id.container, OtpFragment::class.java.simpleName)
        }
    }


    override fun showLoader(isLoading: Boolean) {
        dialogIndeterminate.show(isLoading)
    }

    override fun apiFailure() {
        fabNext.showSnack(getString(R.string.sww_error))
    }

    override fun handleApiError(code: Int?, errorBody: String?) {
        fabNext.showSnack(errorBody.toString())
    }

    override fun onDestroyView() {
        super.onDestroyView()
        presenter.detachView()
    }
}
