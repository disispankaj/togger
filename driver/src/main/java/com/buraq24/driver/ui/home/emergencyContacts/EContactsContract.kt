package com.buraq24.driver.ui.home.emergencyContacts

import com.buraq24.driver.webservices.models.EContact
import com.buraq24.driver.webservices.models.order.Order
import com.buraq24.utilities.basearc.BasePresenter
import com.buraq24.utilities.basearc.BaseView
import com.buraq24.utilities.webservices.models.Service
import org.json.JSONObject

class EContactsContract{

    interface View: BaseView{
        fun onApiSuccess(response: List<EContact>?)
    }

    interface Presenter: BasePresenter<View>{
        fun getContactsList()
    }

}