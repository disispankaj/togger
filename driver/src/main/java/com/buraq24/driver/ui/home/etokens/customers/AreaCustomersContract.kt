package com.buraq24.driver.ui.home.etokens.customers

import com.buraq24.driver.webservices.models.customers.Area
import com.buraq24.driver.webservices.models.customers.CustomersModel
import com.buraq24.driver.webservices.models.order.Order
import com.buraq24.utilities.basearc.BasePresenter
import com.buraq24.utilities.basearc.BaseView

class AreaCustomersContract {

    interface View : BaseView {
        fun onApiSuccess(customersModel: CustomersModel?)
        fun onInitiateOrderApiSuccess(order: Order?)
    }

    interface Presenter : BasePresenter<View> {
        fun areaListingApi(map: HashMap<String, String>)
        fun initiateOrderApi(map: HashMap<String, String>)
    }
}