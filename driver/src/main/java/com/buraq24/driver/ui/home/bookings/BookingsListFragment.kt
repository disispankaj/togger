package com.buraq24.driver.ui.home.bookings

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.buraq24.driver.R
import com.buraq24.driver.utils.signOut
import com.buraq24.driver.webservices.models.order.Order
import com.buraq24.utilities.StatusCode
import com.buraq24.showSnack
import kotlinx.android.synthetic.main.fragment_booking.*

class BookingsListFragment : Fragment(), BookingContract.View, BookingAdapter.OnClickBookingDetails {
    override fun onOpenBookingDetails(orderData: Order?) {
        val intent = Intent(context, BookingDetailsActivity::class.java)
        startActivityForResult(intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                .putExtra("data", orderData), 105)
    }

    override fun onOrderDetailsSuccess(response: Order?) {

    }

    override fun onScheduleCancelApiSuccess() {

    }


    private val presenter = BookingsPresenter()
    private val listBookings = ArrayList<Order?>()
    private var onBookingCancelled: OnBookingCancelled? = null

    companion object {
        const val TYPE_PAST = "PAST"
        const val TYPE_UPCOMING = "UPCOMING"
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_booking, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.attachView(this)
        setAdapter()
    }

    private fun setAdapter() {
        listBookings.clear()
        rvBookings.layoutManager = LinearLayoutManager(rvBookings.context)
        rvBookings.adapter = BookingAdapter(listBookings, this)
        when (arguments?.getString("type")) {
            TYPE_PAST -> {
                presenter.getHistoryList(100, 0, 1)//type=1
            }

            TYPE_UPCOMING -> {
                presenter.getHistoryList(100, 0, 2)//type=2
            }
        }
    }


    override fun onApiSuccess(listBookings: ArrayList<Order?>) {
        if (listBookings.size == 0 && rvBookings.adapter.itemCount == 0) {
            viewFlipperBooking.displayedChild = 2
        } else if (listBookings.size > 0) {
            viewFlipperBooking.displayedChild = 1
            this.listBookings.addAll(listBookings)
            rvBookings.adapter.notifyDataSetChanged()
        }
    }

    override fun showLoader(isLoading: Boolean) {
        if (isLoading)
            viewFlipperBooking.displayedChild = 0
    }

    override fun apiFailure() {
        //viewFlipperBooking.displayedChild =2
    }

    override fun handleApiError(code: Int?, error: String?) {
        view?.showSnack(error.toString())
        if (code == StatusCode.UNAUTHORIZED) {
            signOut(activity)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        onBookingCancelled?.onBookingCancelled()
    }

    fun setInterface(onBookingCancelled: OnBookingCancelled) {
        this.onBookingCancelled = onBookingCancelled
    }

    interface OnBookingCancelled {
        fun onBookingCancelled()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        presenter.detachView()
    }
}
