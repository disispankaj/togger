package com.buraq24.driver.ui.home.servicerequest

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.buraq24.driver.R
import com.buraq24.driver.webservices.models.order.OrderImageUrls
import kotlinx.android.synthetic.main.item_image.view.*

class ImageAdapter(private val response: List<OrderImageUrls?>?) : RecyclerView.Adapter<ImageAdapter.ViewHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
            ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_image,parent,false))

    override fun getItemCount(): Int = response?.size?:0

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(response?.get(position))

    }


    inner class ViewHolder(itemView: View?): RecyclerView.ViewHolder(itemView) {
        init {
            itemView?.setOnClickListener {

            }
        }
        fun bind(data: OrderImageUrls?) {
            itemView?.let {
                Glide.with(itemView.context).applyDefaultRequestOptions(RequestOptions().centerInside().
                        transform(RoundedCorners(8))).
                        load(data?.image_url).into(itemView.ivImageRequest)
            }
        }
    }

}