package com.buraq24.driver.ui.home.stripe

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.webkit.WebChromeClient
import android.webkit.WebView
import android.webkit.WebViewClient
import com.buraq24.driver.R
import com.buraq24.driver.ui.home.profileEdit.ProfileContract
import com.buraq24.driver.ui.home.profileEdit.ProfilePresenter
import com.buraq24.driver.utils.signOut
import com.buraq24.driver.webservices.ApiResponse
import com.buraq24.driver.webservices.models.ResponseCommon
import com.buraq24.showSWWerror
import com.buraq24.showSnack
import com.buraq24.utilities.CheckNetworkConnection
import com.buraq24.utilities.DialogIndeterminate
import com.buraq24.utilities.StatusCode
import com.buraq24.utilities.Stripe.STRIPE_CONNECT_URL
import com.buraq24.utilities.webservices.models.LoginModel
import kotlinx.android.synthetic.main.activity_connect_stripe.*
import kotlinx.android.synthetic.main.activity_edit_profile.*


class ConnectStripeActivity : AppCompatActivity(), ProfileContract.View {

    private val presenter = ProfilePresenter()
    private var dialogIndeterminate: DialogIndeterminate? = null

    companion object {
        fun start(mContext: Context) {
            mContext.startActivity(Intent(mContext, ConnectStripeActivity::class.java))
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_connect_stripe)
        initViews()
        initWebView()
        webViewStripe?.loadUrl(STRIPE_CONNECT_URL)
    }

    fun initViews() {
        presenter.attachView(this)
        dialogIndeterminate = DialogIndeterminate(this)
        toolbarStripe?.setNavigationOnClickListener {
            finish()
        }
    }

    fun initWebView() {
        webViewStripe?.settings?.javaScriptEnabled = true
        webViewStripe?.settings?.allowFileAccess = true
        webViewStripe?.settings?.allowContentAccess = true
        webViewStripe?.settings?.allowFileAccessFromFileURLs = true
        webViewStripe?.settings?.allowUniversalAccessFromFileURLs = true

        webViewStripe?.webChromeClient = object : WebChromeClient() {
            override fun onProgressChanged(view: WebView, newProgress: Int) {
                progressBarStripe.progress = newProgress
                super.onProgressChanged(view, newProgress)
            }
        }
        webViewStripe?.webViewClient = object : WebViewClient() {
            override fun onPageFinished(view: WebView, url: String) {
                progressBarStripe.visibility = View.GONE
                if (url.contains("code=")){
                    var urlCode = url
                    val parts = urlCode.split("code=").toTypedArray()
                    val part1 = parts[0]
                    val part2 = parts[1].split("&state").toTypedArray()
                    val part3 = part2[0]    //ac_GDbagXxwKpiwa2afixdriP3Kpa0Jdxq3&state=%7BSTATE_VALUE%7D
                    Log.d("code", "code=" + part3)

                    if (CheckNetworkConnection.isOnline(this@ConnectStripeActivity)) {
                        var map : HashMap<String,String> = HashMap<String,String>()
                        map.put("code",part3)
                        presenter.connectStripe(map)
                    }
                }

                super.onPageFinished(view, url)
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (webViewStripe != null) {
            rlContainerStripe.removeView(webViewStripe)
            webViewStripe.destroy()
        }
        presenter.detachView()
    }

    override fun showLoader(isLoading: Boolean) {
        dialogIndeterminate?.show(isLoading)
    }

    override fun onApiSuccess(response: ResponseCommon?) {

    }

    override fun onPicApiSuccess(response: ResponseCommon?) {

    }

    override fun apiFailure() {
        webViewStripe?.showSWWerror()
    }

    override fun handleApiError(code: Int?, error: String?) {
        if (code == StatusCode.UNAUTHORIZED) {
            signOut(this)
            return
        }
        webViewStripe?.showSnack(error.toString())
    }

    override fun onSuccessConnectStripe() {
        this.finish()
    }
}
