package com.buraq24.driver.ui.home.settings

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Toast
import com.buraq24.driver.MainActivity
import com.buraq24.driver.R
import com.buraq24.driver.WebViewActivity
import com.buraq24.driver.ui.home.AboutUsActivity
import com.buraq24.driver.ui.home.HomeActivity
import com.buraq24.driver.ui.signup.SignupActivity
import com.buraq24.driver.utils.getTermsAndCond
import com.buraq24.driver.utils.location.LocationUpdatesService
import com.buraq24.getLanguage
import com.buraq24.getLanguageId
import com.buraq24.showSWWerror
import com.buraq24.showSnack
import com.buraq24.utilities.*
import com.buraq24.utilities.constants.*
import com.buraq24.utilities.webservices.BASE_URL
import com.buraq24.utilities.webservices.models.AppDetail
import kotlinx.android.synthetic.main.activity_settings.*

class SettingsActivity : AppCompatActivity(), View.OnClickListener, SettingsContract.View {

    private var langPickerPopup: android.support.v7.widget.ListPopupWindow? = null
    private var presenter = SettingsPresenter()
    private var dialogIndeterminate: DialogIndeterminate? = null
    private var selectedLanguagePosition = 0

    private var tempLanguagePosition = 0

    override fun onSettingsApiSuccess() {
        if (LocaleManager.getLanguage(this@SettingsActivity) != getLanguage(tempLanguagePosition + 1)) {
            selectedLanguagePosition = tempLanguagePosition
            setNewLocale(getLanguage(selectedLanguagePosition + 1), true)
        }
    }

    override fun showLoader(isLoading: Boolean) {
        dialogIndeterminate?.show(isLoading)
    }

    override fun apiFailure() {
        rootView.showSWWerror()
        SharedPrefs.get().save(PREFS_LANGUAGE_ID, selectedLanguagePosition)
        initLanguagePicker(tvChangeLanguage)
    }

    override fun handleApiError(code: Int?, error: String?) {
        if (code == StatusCode.UNAUTHORIZED) {

        } else {
            rootView.showSnack(error ?: "")
            SharedPrefs.get().save(PREFS_LANGUAGE_ID, selectedLanguagePosition)
            initLanguagePicker(tvChangeLanguage)
        }
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.tvTermsConditions -> {
                startActivity(Intent(this, WebViewActivity::class.java).putExtra(WebViewActivity.EXTRA_URL,
                        getTermsAndCond(SharedPrefs.get().getInt(PREFS_LANGUAGE_ID, 1)))
                        .putExtra(WebViewActivity.EXTRA_TITLE, getString(R.string.terms_and_condition)))
            }
            R.id.tvPrivacy -> {
                startActivity(Intent(this, WebViewActivity::class.java).putExtra(WebViewActivity.EXTRA_URL,
                        "${BASE_URL}/PPolicy")
                        .putExtra(WebViewActivity.EXTRA_TITLE, getString(R.string.privacy)))
            }
            R.id.tvAboutUs -> {
                startActivity(Intent(this, AboutUsActivity::class.java).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP))
            }
            R.id.tvSignout -> {
                val builder = AlertDialog.Builder(this)
                builder.setMessage(getString(R.string.logout_msg))
                builder.setPositiveButton(getString(R.string.yes)) { dialog, _ ->
                    dialog?.dismiss()
                    if (!HomeActivity.isServiceGoing) {
                        presenter.logout()
                    } else {
                        Toast.makeText(this@SettingsActivity, getString(R.string.on_way_signout), Toast.LENGTH_SHORT).show()
                    }
                }
                builder.setNegativeButton(getString(R.string.no)) { dialog, _ -> dialog?.dismiss() }
                builder.show()
            }

        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)
        presenter.attachView(this)
        dialogIndeterminate = DialogIndeterminate(this)
        val notification = SharedPrefs.with(this).getObject(PROFILE, AppDetail::class.java)
                .notifications
        switchPush.isChecked = notification == "1"
        setListener()
        setLanguagae()
        initLanguagePicker(tvChangeLanguage)
    }

    private fun setLanguagae() {
        val lang = getLanguageId(LocaleManager.getLanguage(this)) - 1
        tvChangeLanguage.text = resources.getStringArray(R.array.language_list)[lang]
    }

    private fun setListener() {
        switchPush.setOnCheckedChangeListener { buttonView, isChecked ->
            presenter.updateSettingsApiCall("", if (isChecked) "1" else "0")
        }
        toolbar.setNavigationOnClickListener {
            onBackPressed()
        }

        tvChangeLanguage.setOnClickListener {
            openLangPicker()
        }
        tvTermsConditions.setOnClickListener(this)
        tvPrivacy.setOnClickListener(this)
        tvAboutUs.setOnClickListener(this)
        tvSignout.setOnClickListener(this)
    }


    private fun initLanguagePicker(anchorView: View) {
        langPickerPopup = android.support.v7.widget.ListPopupWindow(anchorView.context)
        langPickerPopup?.setAdapter(ArrayAdapter(this,
                R.layout.support_simple_spinner_dropdown_item, resources.getStringArray(R.array.language_list)))
        langPickerPopup?.isModal = true
        langPickerPopup?.anchorView = anchorView
        langPickerPopup?.width = android.support.v7.widget.ListPopupWindow.WRAP_CONTENT
        langPickerPopup?.height = android.support.v7.widget.ListPopupWindow.WRAP_CONTENT
        selectedLanguagePosition = getLanguageId(LocaleManager.getLanguage(this)) - 1
        langPickerPopup?.setSelection(getLanguageId(LocaleManager.getLanguage(this)) - 1)
        tempLanguagePosition = getLanguageId(LocaleManager.getLanguage(this)) - 1

        langPickerPopup?.setOnItemClickListener { _, _, position, _ ->
            tempLanguagePosition = position
            if (LocaleManager.getLanguage(this@SettingsActivity) != getLanguage(position + 1)) {
                SharedPrefs.get().save(PREFS_LANGUAGE_ID, position + 1)
                SharedPrefs.get().save(LANGUAGE_CHANGED, true)
                presenter.updateSettingsApiCall((position + 1).toString(), if (switchPush.isChecked) "1" else "0")
            }
            langPickerPopup?.dismiss()
        }
    }

    private fun openLangPicker() {
        langPickerPopup?.show()
    }

    private fun setNewLocale(language: String, restartProcess: Boolean): Boolean {
        LocaleManager.setNewLocale(this, language)

        val intent = Intent(this, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)

        if (restartProcess) {
            System.exit(0)
        }
        return true
    }

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(LocaleManager.setLocale(newBase))
    }

    override fun logoutSuccess() {
        AppSocket.get().off()
        AppSocket.get().disconnect()
        SharedPrefs.with(this@SettingsActivity).removeAllSharedPrefsChangeListeners()
        SharedPrefs.with(this@SettingsActivity).removeAll()
        finishAffinity()
        startActivity(Intent(this@SettingsActivity, SignupActivity::class.java))
        val intent = Intent(this, LocationUpdatesService::class.java)
        intent.putExtra("logged_out", true)
        startService(intent)
    }
}