package com.buraq24.driver.ui.signup.enterdetails


import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.FragmentManager
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.buraq24.Utils
import com.buraq24.driver.ImageUtils
import com.buraq24.driver.R
import com.buraq24.driver.ui.signup.addBankDetails.AddBankDetailsFragment
import com.buraq24.driver.utils.EmojiExcludeFilter
import com.buraq24.driver.utils.PermissionUtils
import com.buraq24.driver.webservices.models.Organization
import com.buraq24.driver.webservices.models.RequestPersonalDetails
import com.buraq24.driver.webservices.models.ResponseCommon
import com.buraq24.showSWWerror
import com.buraq24.showSnack
import com.buraq24.utilities.*
import com.buraq24.utilities.constants.*
import kotlinx.android.synthetic.main.fragment_add_mulkiya_details.*
import kotlinx.android.synthetic.main.fragment_add_personal_details.*
import kotlinx.android.synthetic.main.fragment_add_personal_details.fabNext
import kotlinx.android.synthetic.main.fragment_add_personal_details.ivProfile
import permissions.dispatcher.*
import java.util.*
import kotlin.collections.ArrayList

@RuntimePermissions
class AddPersonalDetailsFragment : Fragment(), AddPersonalDetailsContract.View, DateUtils.OnDateSelectedListener {

    private var date: String = ""

    override fun dateTimeSelected(dateCal: Calendar) {
        date = DateUtils.getFormattedDateForUTC(dateCal)
        tvDob.text = DateUtils.getFormattedTime(dateCal)
    }

    override fun timeSelected(dateCal: Calendar) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onOrgsSuccess(listOrg: ArrayList<Organization>) {
        if (this.listOrg.isEmpty()) {
            this.listOrg = listOrg
        }
        val adapter = ArrayAdapter(activity, android.R.layout.simple_spinner_item, listOrg)
        adapter.setDropDownViewResource(R.layout.item_capacity)
        spinner.adapter = adapter
    }

    private val presenter = AddPersonalDetailsPresenter()
    private var listOrg = ArrayList<Organization>()
    private var organizationId = 0L
    private var pathFrontImage = ""

    private lateinit var dialogIndeterminate: DialogIndeterminate

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_add_personal_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.attachView(this)
        date = DateUtils.getFormattedDateForUTC(Calendar.getInstance())
        etName.filters = arrayOf(EmojiExcludeFilter())
        dialogIndeterminate = DialogIndeterminate(activity)
        if (!textView6.isChecked) {
            setViewsOnOrganization(View.GONE)
        } else {
            setViewsOnOrganization(View.VISIBLE)
        }
        setListeners()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        presenter.detachView()
    }

    private fun setListeners() {
        textView5.isChecked = true
        setViewsOnOrganization(View.GONE)

        textView5.setOnClickListener {
            organizationId = 0
            textView6.isChecked = false
            textView5.isChecked = true
            setViewsOnOrganization(View.GONE)
        }

        textView6.setOnClickListener {
            textView5.isChecked = false
            textView6.isChecked = true
            setViewsOnOrganization(View.VISIBLE)
            if (listOrg.isEmpty()) {
                presenter.getOrganization()
            }
        }

        fabNext.setOnClickListener {
            val name = etName.text.toString().trim()
            val email = etEmail.text.toString().trim()
            if (pathFrontImage.isEmpty()){
                fabNext.showSnack(getString(R.string.please_select_img))
            }else if (name.isEmpty()) {
                fabNext.showSnack(getString(R.string.name_empty_validation_message))
            } else if (email.isEmpty() || !Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                fabNext.showSnack(getString(R.string.enter_your_email))
            }else if (tvDob.text.toString().trim().isEmpty()){
                fabNext.showSnack(getString(R.string.select_ur_dob))
            } else {
                if (pathFrontImage.isNotEmpty()) {
                    presenter.addPersonalDetails(RequestPersonalDetails(name, organizationId,
                            profile_pic = pathFrontImage,email = email,
                            date_of_birth = DateUtils.timeInMillisec(tvDob.text.toString().trim()).toString()))
                } else {
                    presenter.addPersonalDetails(RequestPersonalDetails(name, organizationId,email = email,date_of_birth = DateUtils.timeInMillisec(tvDob.text.toString().trim()).toString()))
                }
            }
        }

        ivProfile.setOnClickListener {
            activity?.supportFragmentManager?.popBackStack("backstack", FragmentManager.POP_BACK_STACK_INCLUSIVE)
        }

        imageView2.setOnClickListener {
            getStorageWithPermissionCheck()

        }

        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                organizationId = listOrg[position].organisationId?.toLong() ?: 0L
            }
        }

        tvDob.setOnClickListener {
            DateUtils.openDateDialogPast(it.context, this, false,
                    Calendar.getInstance())
        }
    }

    private fun setViewsOnOrganization(visibility: Int) {
        textView7.visibility = visibility
        spinner.visibility = visibility
    }

    override fun onApiSuccess(response: ResponseCommon?) {
        ACCESS_TOKEN = response?.AppDetail?.access_token ?: ""
        SharedPrefs.with(context).save(ACCESS_TOKEN_KEY, ACCESS_TOKEN)

        response?.services = response?.services?.filter {
            it.category_id == 7
        }

        SharedPrefs.with(activity).save(PROFILE, response)
        SharedPrefs.with(activity).save(STEP, 2)
        activity?.supportFragmentManager?.let { it1 ->
            Utils.addFragment(it1, AddBankDetailsFragment(),
                    R.id.container, AddBankDetailsFragment::class.java.simpleName)
        }
    }

    override fun showLoader(isLoading: Boolean) {
        dialogIndeterminate.show(isLoading)
    }

    override fun apiFailure() {
        fabNext.showSWWerror()
    }

    override fun handleApiError(code: Int?, error: String?) {
        fabNext.showSnack(error.toString())
    }

    @NeedsPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.CAMERA)
    fun getStorage() {
        ImageUtils.displayImagePicker(this)

    }

    @OnShowRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.CAMERA)
    fun showLocationRationale(request: PermissionRequest) {
        PermissionUtils.showRationalDialog(imageView2.context, R.string.permission_required_to_select_image, request)
    }

    @OnNeverAskAgain(Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.CAMERA)
    fun onNeverAskAgainRationale() {
        PermissionUtils.showAppSettingsDialog(imageView2.context,
                R.string.permission_required_to_select_image)
    }

    @SuppressLint("NeedOnRequestPermissionsResult")
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        onRequestPermissionsResult(requestCode, grantResults)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                ImageUtils.REQ_CODE_CAMERA_PICTURE -> {
                    val file = ImageUtils.getFile(activity)
                    Glide.with(imageView2.context).setDefaultRequestOptions(RequestOptions().circleCrop()).load(file).into(imageView2)
                    pathFrontImage = file.absolutePath
                }

                ImageUtils.REQ_CODE_GALLERY_PICTURE -> {
                    data?.let {
                        val file = ImageUtils.getImagePathFromGallery(activity, data.data)
                        Glide.with(imageView2.context).setDefaultRequestOptions(RequestOptions().circleCrop()).load(file)
                                .into(imageView2)
                        pathFrontImage = file.absolutePath
                    }

                }
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
    }
}
