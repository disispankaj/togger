package com.buraq24.driver.ui.home.earnings

import com.buraq24.driver.webservices.ApiResponse
import com.buraq24.driver.webservices.RestClient
import com.buraq24.driver.webservices.models.earning.EarningResultModel
import com.buraq24.getApiError
import com.buraq24.utilities.basearc.BasePresenterImpl
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class EarningsPresenter : BasePresenterImpl<EarningsContract.View>(), EarningsContract.Presenter {

    override fun getEarnings(startDate: String, endDate: String) {
        getView()?.showLoader(true)
        RestClient.get().earnings(startDate, endDate).enqueue(object : Callback<ApiResponse<ArrayList<EarningResultModel?>>> {

            override fun onResponse(call: Call<ApiResponse<ArrayList<EarningResultModel?>>>, response: Response<ApiResponse<ArrayList<EarningResultModel?>>>) {
                getView()?.showLoader(false)
                if (response.isSuccessful == true) {
                    if (response.body()?.statusCode == 200) {
                        getView()?.onApiSuccess(response.body()?.result ?: ArrayList())
                    } else {
                        getView()?.handleApiError(response.body()?.statusCode, response.body()?.msg)
                    }
                } else {
                    val errorModel = getApiError(response.errorBody()?.string())
                    getView()?.handleApiError(errorModel.statusCode, errorModel.msg)
                }
            }

            override fun onFailure(call: Call<ApiResponse<ArrayList<EarningResultModel?>>>, t: Throwable?) {
                getView()?.showLoader(false)
                getView()?.apiFailure()
            }
        })
    }

}