package com.buraq24.driver.ui.home.servicerequest

import com.buraq24.driver.webservices.API
import com.buraq24.driver.webservices.ApiResponse
import com.buraq24.driver.webservices.RestClient
import com.buraq24.driver.webservices.models.order.Order
import com.buraq24.getApiError
import com.buraq24.utilities.StatusCode
import com.buraq24.utilities.basearc.BasePresenterImpl
import com.google.android.gms.maps.model.LatLng
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import okhttp3.ResponseBody
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException
import java.util.*

class ServiceRequestPresenter : BasePresenterImpl<ServiceRequestContract.View>(), ServiceRequestContract.Presenter {


    override fun startOtherRide(orderId: Long) {
        getView()?.showLoader(true)
        RestClient.get().changeTurn(orderId).enqueue(object : Callback<Any> {

            override fun onResponse(call: Call<Any>?, response: Response<Any>?) {
                getView()?.showLoader(false)
                if (response?.isSuccessful == true) {
                    getView()?.onChangeTurnSuccess()
                } else {
                    val errorModel = getApiError(response?.errorBody()?.string())
                    getView()?.handleApiError(errorModel.statusCode, errorModel.msg)
                }
            }

            override fun onFailure(call: Call<Any>?, t: Throwable?) {
                getView()?.showLoader(false)
                getView()?.apiFailure()
            }
        })
    }

    override fun acceptRequest(lalLng: LatLng, orderId: Long) {
        getView()?.showLoader(true)
        RestClient.get().acceptService(lalLng.latitude, lalLng.longitude, orderId).enqueue(object : Callback<ApiResponse<Order>> {

            override fun onResponse(call: Call<ApiResponse<Order>>?, response: Response<ApiResponse<Order>>?) {
                getView()?.showLoader(false)
                if (response?.isSuccessful == true) {
                    getView()?.onAcceptApiSuccess(response.body()?.result)
                } else {
                    try {
                        val errorBody = response?.errorBody()?.string()
                        val errorResponse: ApiResponse<Order> = Gson().fromJson(errorBody, object : TypeToken<ApiResponse<Order>>() {}.type)
                        if(errorResponse.statusCode == StatusCode.BAD_REQUEST && errorResponse.result != null) {
                            getView()?.handleOrderError(errorResponse.result)
                        }else{
                            val errorModel = getApiError(errorBody)
                            getView()?.handleApiError(errorModel.statusCode, errorModel.msg)
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<ApiResponse<Order>>?, t: Throwable?) {
                getView()?.showLoader(false)
                getView()?.apiFailure()
            }
        })

    }

    override fun rejectRequest(lalLng: LatLng, orderId: Long) {
        getView()?.showLoader(true)
        RestClient.get().rejectService(lalLng.latitude, lalLng.longitude, orderId).enqueue(object : Callback<Any> {

            override fun onResponse(call: Call<Any>?, response: Response<Any>?) {
                getView()?.showLoader(false)
                if (response?.isSuccessful == true) {
                    getView()?.onRejectApiSuccess()
                } else {
                    val errorBody = response?.errorBody()?.string()
                    val errorResponse: ApiResponse<Order> = Gson().fromJson(errorBody, object : TypeToken<ApiResponse<Order>>() {}.type)
                    if(errorResponse.statusCode == StatusCode.BAD_REQUEST && errorResponse.result != null) {
                        getView()?.handleOrderError(errorResponse.result)
                    }else{
                        val errorModel = getApiError(errorBody)
                        getView()?.handleApiError(errorModel.statusCode, errorModel.msg)
                    }
                }
            }

            override fun onFailure(call: Call<Any>?, t: Throwable?) {
                getView()?.showLoader(false)
                getView()?.apiFailure()
            }
        })
    }

    override fun drawPolyLine(sourceLat: Double, sourceLong: Double, destLat: Double, destLong: Double) {
        val BASE_URL_for_map = "https://maps.googleapis.com/maps/api/"
        val retrofit = Retrofit.Builder().baseUrl(BASE_URL_for_map).addConverterFactory(GsonConverterFactory.create()).build()
        val api = retrofit.create(API::class.java)
        val service = api.getPolYLine(sourceLat.toString() + "," + sourceLong,
                destLat.toString() + "," + destLong, Locale.getDefault().language)
        service.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val responsePolyline = response.body()?.string()
                        val jsonRootObject = JSONObject(responsePolyline)
                        getView()?.polyLine(jsonRootObject)
                    } catch (e: IOException) {
                        e.printStackTrace()

                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                } else {
                    val errorModel = getApiError(response.errorBody()?.string())
                    getView()?.handleApiError(errorModel.statusCode, errorModel.msg)
                }
            }

            override fun onFailure(call: Call<ResponseBody>?, t: Throwable?) {
                getView()?.showLoader(false)
                getView()?.apiFailure()
            }
        })
    }

    override fun startRequest(lalLng: LatLng, distance: Double,orderId: Long) {
        getView()?.showLoader(true)
        RestClient.get().startService(lalLng.latitude, lalLng.longitude, distance,orderId).enqueue(object : Callback<Any> {

            override fun onResponse(call: Call<Any>?, response: Response<Any>?) {
                getView()?.showLoader(false)
                if (response?.isSuccessful == true) {
                    getView()?.onStartApiSuccess()
                } else {
                    val errorBody = response?.errorBody()?.string()
                    val errorResponse: ApiResponse<Order> = Gson().fromJson(errorBody, object : TypeToken<ApiResponse<Order>>() {}.type)
                    if(errorResponse.statusCode == StatusCode.BAD_REQUEST && errorResponse.result != null) {
                        getView()?.handleOrderError(errorResponse.result)
                    }else{
                        val errorModel = getApiError(errorBody)
                        getView()?.handleApiError(errorModel.statusCode, errorModel.msg)
                    }
                }
            }

            override fun onFailure(call: Call<Any>?, t: Throwable?) {
                getView()?.showLoader(false)
                getView()?.apiFailure()
            }
        })

    }


    override fun reachRequest(lalLng: LatLng, orderId: Long) {
        getView()?.showLoader(true)
        RestClient.get().reachedService(lalLng.latitude, lalLng.longitude, orderId).enqueue(object : Callback<Any> {

            override fun onResponse(call: Call<Any>?, response: Response<Any>?) {
                getView()?.showLoader(false)
                if (response?.isSuccessful == true) {
                    getView()?.onReachApiSuccess()
                } else {
                    val errorBody = response?.errorBody()?.string()
                    val errorResponse: ApiResponse<Order> = Gson().fromJson(errorBody, object : TypeToken<ApiResponse<Order>>() {}.type)
                    if(errorResponse.statusCode == StatusCode.BAD_REQUEST && errorResponse.result != null) {
                        getView()?.handleOrderError(errorResponse.result)
                    }else{
                        val errorModel = getApiError(errorBody)
                        getView()?.handleApiError(errorModel.statusCode, errorModel.msg)
                    }
                }
            }

            override fun onFailure(call: Call<Any>?, t: Throwable?) {
                getView()?.showLoader(false)
                getView()?.apiFailure()
            }
        })
    }

    override fun rejectRequest(lalLng: LatLng, orderId: Long, reason: String) {
        getView()?.showLoader(true)
        RestClient.get().cancelService(lalLng.latitude, lalLng.longitude, orderId, reason).enqueue(object : Callback<Any> {

            override fun onResponse(call: Call<Any>?, response: Response<Any>?) {
                getView()?.showLoader(false)
                if (response?.isSuccessful == true) {
                    getView()?.onRejectApiSuccess()
                } else {
                    val errorBody = response?.errorBody()?.string()
                    val errorResponse: ApiResponse<Order> = Gson().fromJson(errorBody, object : TypeToken<ApiResponse<Order>>() {}.type)
                    if(errorResponse.statusCode == StatusCode.BAD_REQUEST && errorResponse.result != null) {
                        getView()?.handleOrderError(errorResponse.result)
                    }else{
                        val errorModel = getApiError(errorBody)
                        getView()?.handleApiError(errorModel.statusCode, errorModel.msg)
                    }
                }
            }

            override fun onFailure(call: Call<Any>?, t: Throwable?) {
                getView()?.showLoader(false)
                getView()?.apiFailure()
            }
        })
    }

    override fun dropAtAddressRequest(lalLng: LatLng, orderId: Long) {
        getView()?.showLoader(true)
        RestClient.get().endService(lalLng.latitude, lalLng.longitude, orderId, "Cash").enqueue(object : Callback<Any> {

            override fun onResponse(call: Call<Any>?, response: Response<Any>?) {
                getView()?.showLoader(false)
                if (response?.isSuccessful == true) {
                    getView()?.onEndApiSuccess()
                } else {
                    val errorBody = response?.errorBody()?.string()
                    val errorResponse: ApiResponse<Order> = Gson().fromJson(errorBody, object : TypeToken<ApiResponse<Order>>() {}.type)
                    if(errorResponse.statusCode == StatusCode.BAD_REQUEST && errorResponse.result != null) {
                        getView()?.handleOrderError(errorResponse.result)
                    }else{
                        val errorModel = getApiError(errorBody)
                        getView()?.handleApiError(errorModel.statusCode, errorModel.msg)
                    }
                }
            }

            override fun onFailure(call: Call<Any>?, t: Throwable?) {
                getView()?.showLoader(false)
                getView()?.apiFailure()
            }
        })

    }

    override fun rateOrder(rating: Double, comments: String, orderId: Long) {
        getView()?.showLoader(true)
        RestClient.get().rateService(rating, orderId, comments).enqueue(object : Callback<Any> {
            override fun onResponse(call: Call<Any>?, response: Response<Any>?) {
                getView()?.showLoader(false)
                if (response?.isSuccessful == true) {
                    getView()?.onRateApiSuccess()
                } else {
                    val errorModel = getApiError(response?.errorBody()?.string())
                    getView()?.handleApiError(errorModel.statusCode, errorModel.msg)
                }
            }

            override fun onFailure(call: Call<Any>?, t: Throwable?) {
                getView()?.showLoader(false)
                getView()?.apiFailure()
            }
        })

    }

    override fun confirmScheduleRequest(lalLng: LatLng, orderId: Long, approve: Int) {
        getView()?.showLoader(true)
        RestClient.get().confirmUnConfirmService(lalLng.latitude, lalLng.longitude, orderId, approve).enqueue(object : Callback<ApiResponse<Order>> {

            override fun onResponse(call: Call<ApiResponse<Order>>?, response: Response<ApiResponse<Order>>?) {
                getView()?.showLoader(false)
                if (response?.isSuccessful == true) {
                    if (approve == 1) getView()?.onConfirmAcceptApiSuccess2(response.body()?.result)
                    else getView()?.onRejectApiSuccess()
                } else {
                    val errorBody = response?.errorBody()?.string()
                    val errorResponse: ApiResponse<Order> = Gson().fromJson(errorBody, object : TypeToken<ApiResponse<Order>>() {}.type)
                    if(errorResponse.statusCode == StatusCode.BAD_REQUEST && errorResponse.result != null) {
                        getView()?.handleOrderError(errorResponse.result)
                    }else{
                        val errorModel = getApiError(errorBody)
                        getView()?.handleApiError(errorModel.statusCode, errorModel.msg)
                    }
                }
            }

            override fun onFailure(call: Call<ApiResponse<Order>>?, t: Throwable?) {
                getView()?.showLoader(false)
                getView()?.apiFailure()
            }
        })

    }
}