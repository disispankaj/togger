package com.buraq24.driver.ui.home

import com.buraq24.driver.webservices.API
import com.buraq24.driver.webservices.ApiResponse
import com.buraq24.driver.webservices.RestClient
import com.buraq24.driver.webservices.models.DriverOnlineStatus
import com.buraq24.driver.webservices.models.OrderResponse
import com.buraq24.driver.webservices.models.RoadPoints
import com.buraq24.driver.webservices.models.order.Order
import com.buraq24.driver.webservices.models.order.OrderCommon
import com.buraq24.getApiError
import com.buraq24.utilities.basearc.BasePresenterImpl
import com.buraq24.utilities.webservices.models.LoginModel
import com.buraq24.utilities.webservices.models.Service
import okhttp3.ResponseBody
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class HomePresenter : BasePresenterImpl<HomeContract.View>(), HomeContract.Presenter {
    override fun logout() {
        getView()?.showLoader(true)
        RestClient.get().logout().enqueue(object : Callback<Any> {

            override fun onResponse(call: Call<Any>?, response: Response<Any>?) {
                getView()?.showLoader(false)
                if (response?.isSuccessful == true) {
                    getView()?.logoutSuccess()

                } else {
                    val errorModel = getApiError(response?.errorBody()?.string())
                    getView()?.handleApiError(errorModel.statusCode, errorModel.msg)
                }
            }

            override fun onFailure(call: Call<Any>?, t: Throwable?) {
                getView()?.showLoader(false)
                getView()?.apiFailure()
            }
        })
    }

    override fun driverStatusChange(status: String, showLoader: Boolean) {
        getView()?.showLoader(showLoader)
        RestClient.get().updateOnLineStatus(status).enqueue(object : Callback<DriverOnlineStatus> {

            override fun onResponse(call: Call<DriverOnlineStatus>?, response: Response<DriverOnlineStatus>?) {
                getView()?.showLoader(false)
                if (response?.isSuccessful == true) {
                    getView()?.onApiSuccess(response.body()?.online_status)

                } else {
                    val errorModel = getApiError(response?.errorBody()?.string())
                    getView()?.handleApiError(errorModel.statusCode, errorModel.msg)
                }
            }

            override fun onFailure(call: Call<DriverOnlineStatus>?, t: Throwable?) {
                getView()?.showLoader(false)
                getView()?.apiFailure()
            }
        })
    }


    override fun onGoingOrderApi() {
        getView()?.showLoader(true)
        RestClient.get().onGoingOrder().enqueue(object : Callback<OrderResponse> {

            override fun onResponse(call: Call<OrderResponse>?, response: Response<OrderResponse>?) {
                getView()?.showLoader(false)
                if (response?.isSuccessful == true) {
                    if (response.body()?.statusCode == 200) {
                        val allOrders = ArrayList<Order>()
                        allOrders.addAll(response.body()?.result ?: ArrayList())
                        allOrders.addAll(response.body()?.pendingOrders ?: ArrayList())
                        getView()?.onOrdersApiSuccess(allOrders)
                    } else {
                        getView()?.handleApiError(response.body()?.statusCode, response.body()?.msg)
                    }
                } else {
                    val errorModel = getApiError(response?.errorBody()?.string())
                    getView()?.handleApiError(errorModel.statusCode, errorModel.msg)
                }
            }

            override fun onFailure(call: Call<OrderResponse>?, t: Throwable?) {
                getView()?.showLoader(false)
                getView()?.apiFailure()
            }
        })
    }

    override fun getOrderDetails(orderId: Long) {
        //getView()?.showLoader(true)
        RestClient.get().getOrderDetails(orderId).enqueue(object : Callback<OrderCommon> {

            override fun onResponse(call: Call<OrderCommon>?, response: Response<OrderCommon>?) {
                getView()?.showLoader(false)
                if (response?.isSuccessful == true) {
                    if (response.body()?.statusCode == 200) {
                        getView()?.onOrderDetailsSuccess(response.body()?.order)
                    } else {
                        getView()?.handleApiError(response.body()?.statusCode, response.body()?.msg)
                    }
                } else {
                    val errorModel = getApiError(response?.errorBody()?.string())
                    getView()?.handleApiError(errorModel.statusCode, errorModel.msg)
                }
            }

            override fun onFailure(call: Call<OrderCommon>?, t: Throwable?) {
                getView()?.showLoader(false)
                getView()?.apiFailure()
            }
        })
    }

    override fun getSupportList() {
        getView()?.showLoader(true)
        RestClient.get().supportList().enqueue(object : Callback<ApiResponse<ArrayList<Service>>> {
            override fun onResponse(call: Call<ApiResponse<ArrayList<Service>>>?, response: Response<ApiResponse<ArrayList<Service>>>?) {
                getView()?.showLoader(false)
                if (response?.isSuccessful == true) {
                    if (response.body()?.statusCode == 200) {
                        getView()?.onSupportListApiSuccess(response.body()?.result)
                    } else {
                        getView()?.handleApiError(response.body()?.statusCode, response.body()?.msg)
                    }
                } else {
                    val errorModel = getApiError(response?.errorBody()?.string())
                    getView()?.handleApiError(errorModel.statusCode, errorModel.msg)
                }
            }

            override fun onFailure(call: Call<ApiResponse<ArrayList<Service>>>?, t: Throwable?) {
                getView()?.showLoader(false)
                getView()?.apiFailure()
            }

        })
    }

    var callPolyline: Call<ResponseBody>? = null
    var isPolylineApiCalling = false
    override fun drawPolyLine(sourceLat: Double, sourceLong: Double, destLat: Double, destLong: Double) {
        if (isPolylineApiCalling) return
        val BASE_URL_for_map = "https://maps.googleapis.com/maps/api/"
        val retrofit = Retrofit.Builder().baseUrl(BASE_URL_for_map).addConverterFactory(GsonConverterFactory.create()).build()
        val api = retrofit.create(API::class.java)
//        callPolyline?.cancel()
        isPolylineApiCalling = true
        callPolyline = api.getPolYLine(sourceLat.toString() + "," + sourceLong,
                destLat.toString() + "," + destLong, Locale.US.language)
        callPolyline?.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                isPolylineApiCalling = false
                if (response.isSuccessful) {
                    try {
                        val responsePolyline = response.body()?.string()
                        val jsonRootObject = JSONObject(responsePolyline)
                        getView()?.polyLine(jsonRootObject)
                    } catch (e: IOException) {
                        e.printStackTrace()

                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                }/* else {
                    val errorModel = getApiError(response.errorBody()?.string())
                    getView()?.handleApiError(errorModel.statusCode, errorModel.msg)
                }*/
            }

            override fun onFailure(call: Call<ResponseBody>?, t: Throwable?) {
                isPolylineApiCalling = false
                getView()?.showLoader(false)
//                getView()?.apiFailure()
            }
        })
    }

    var callRoadApi: Call<RoadPoints>? = null
    var isRoadApiCalling = false
    fun getRoadPoints(sourceLat: Double, sourceLong: Double) {
        if (isRoadApiCalling) return
        //getView()?.showLoader(true)
        callRoadApi?.cancel()
        val latlng = sourceLat.toString() + "," + sourceLong
        isRoadApiCalling = true
        callRoadApi = RestClient.get().getRoadPoints(latlng)
        callRoadApi?.enqueue(object : Callback<RoadPoints> {
            override fun onResponse(call: Call<RoadPoints>?,
                                    response: Response<RoadPoints>?) {
                //getView()?.showLoader(false)
                isRoadApiCalling = false
                getView()?.snappedPoints((response?.body()?.snappedPoints ?: ArrayList()))
            }

            override fun onFailure(call: Call<RoadPoints>?, t: Throwable?) {
                //getView()?.showLoader(false)
                getView()?.snappedPoints((ArrayList()))
                isRoadApiCalling = false
            }

        })
    }

    override fun verifyCode(map: HashMap<String, String>) {
        getView()?.showLoader(true)
        RestClient.get().verifyCode(map).enqueue(object : Callback<ApiResponse<LoginModel>> {

            override fun onResponse(call: Call<ApiResponse<LoginModel>>?, response: Response<ApiResponse<LoginModel>>?) {
                getView()?.showLoader(false)
                if (response?.isSuccessful == true) {
                    if (response.body()?.statusCode == 200) {
                        getView()?.onSuccessCode(response.body()?.result)
                    } else {
                        getView()?.handleApiError(response.body()?.statusCode, response.body()?.msg)
                    }
                } else {
                    val errorModel = getApiError(response?.errorBody()?.string())
                    getView()?.handleApiError(errorModel.statusCode, errorModel.msg)
                }
            }

            override fun onFailure(call: Call<ApiResponse<LoginModel>>?, t: Throwable?) {
                getView()?.showLoader(false)
                getView()?.apiFailure()
            }
        })
    }
}