package com.buraq24.driver.ui.home.serviceAfterStart

import com.buraq24.driver.webservices.RestClient
import com.buraq24.getApiError
import com.buraq24.utilities.basearc.BasePresenterImpl
import com.google.android.gms.maps.model.LatLng
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ServiceAfterStartPresenter : BasePresenterImpl<ServiceAfterStartContract.View>(), ServiceAfterStartContract.Presenter {
    override fun dropAtAddressRequest(lalLng: LatLng, orderId: Long) {
        getView()?.showLoader(true)
        RestClient.get().endService(lalLng.latitude,lalLng.longitude,orderId, "Cash").
                enqueue(object : Callback<Any> {

                    override fun onResponse(call: Call<Any>?, response: Response<Any>?) {
                        getView()?.showLoader(false)
                        if (response?.isSuccessful == true) {
                            getView()?.onApiSuccess()
                        }else {
                            val errorModel = getApiError(response?.errorBody()?.string())
                            getView()?.handleApiError(errorModel.statusCode, errorModel.msg)
                        }
                    }

                    override fun onFailure(call: Call<Any>?, t: Throwable?) {
                        getView()?.showLoader(false)
                        getView()?.apiFailure()
                    }
                })

    }

}