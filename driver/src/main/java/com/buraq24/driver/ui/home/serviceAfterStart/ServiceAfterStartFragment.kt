package com.buraq24.driver.ui.home.serviceAfterStart


import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions

import com.buraq24.driver.webservices.models.order.Order
import com.buraq24.driver.R
import com.buraq24.driver.utils.getFinalCharge
import com.buraq24.driver.ui.home.invoice.InvoiceFragment
import com.buraq24.driver.utils.getRatingSymbol
import com.buraq24.driver.utils.signOut
import com.buraq24.showSWWerror
import com.buraq24.showSnack
import com.buraq24.utilities.*
import com.buraq24.utilities.constants.PREF_CURRENT_LAT
import com.buraq24.utilities.constants.PREF_CURRENT_LNG
import com.buraq24.utilities.constants.PREF_DISTANCE
import com.google.android.gms.maps.model.LatLng
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_service_after_start.*


class ServiceAfterStartFragment : Fragment(), ServiceAfterStartContract.View {

    override fun onApiSuccess() {
        onEndService.onServiceEnd()
        val fragment = InvoiceFragment()
        val bundle = Bundle()
        bundle.putString("order", arguments?.getString("order"))
        fragment.arguments = bundle
        activity?.supportFragmentManager?.beginTransaction()?.
                replace(R.id.container,fragment, InvoiceFragment::class.java.name)
                ?.commitAllowingStateLoss()
    }

    private val presenter = ServiceAfterStartPresenter()

    private lateinit var orderDetail: Order


    private lateinit var onEndService : OnEndService

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_service_after_start, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity?.registerReceiver(receiver, IntentFilter("updateETA"))
        presenter.attachView(this)
        orderDetail = Gson().fromJson(arguments?.getString("order"), Order::class.java)
        setData()
        setListeners()
    }

    private fun setData() {
        val services= resources.getStringArray(R.array.services_list)
        tvProductNameStart.text = if (orderDetail.category_id == 1) {
             services[1]+" . " + orderDetail.brand?.name +" x "+orderDetail.payment?.productQuantity
        } else if(orderDetail.category_id ==3){
            services[3]+"."+ orderDetail.product?.name +" x "+orderDetail.payment?.productQuantity
        }
        else {
            orderDetail.brand?.brand_name +"."+ orderDetail.brand?.name +" x "+orderDetail.payment?.productQuantity
        }
        tvNameStart.text = orderDetail.user?.name
        if(orderDetail.user?.rating_avg?.isNotEmpty()== true) {
            ivRatingStart.visibility = View.VISIBLE
            textViewStart.visibility = View.VISIBLE
            ivRatingStart.setImageResource(getRatingSymbol(orderDetail.user?.rating_avg?.toInt() ?: 0))
            textViewStart.text = orderDetail.user?.rating_count?.toString() ?: "0"
        }
        else{
            ivRatingStart.visibility = View.GONE
            textViewStart.visibility = View.GONE
        }
        tvTotalStart.text = getFinalCharge(orderDetail,SharedPrefs.get().getInt(PREF_DISTANCE,1))+" "+getString(R.string.currency)
        tvDistanceStart.text =if(arguments?.containsKey("time")==true) arguments?.getString("time") ?:"" else ""
        tvTimeStart?.text =if(arguments?.containsKey("distance")==true) arguments?.getString("distance") ?:"" else ""
       Glide.with(imageViewStart.context).setDefaultRequestOptions(RequestOptions()
                .circleCrop().placeholder(R.drawable.ic_user_black)).load(orderDetail.user?.profile_pic_url)
                .into(imageViewStart)
    }

    private fun setListeners() {
        val lat= SharedPrefs.with(activity).getFloat(PREF_CURRENT_LAT,
               0.0f)
        val long = SharedPrefs.with(activity).
                getFloat(PREF_CURRENT_LNG,0.0f)
        val latlng= LatLng(lat.toDouble(),long.toDouble())
        tvDropAtAddressStart.setOnClickListener {
            if (CheckNetworkConnection.isOnline(activity)) {
                presenter.dropAtAddressRequest(latlng,orderDetail.order_id?.toLong()?:0)
            }
        }

        imageViewCallStart.setOnClickListener {
            val phone = orderDetail.user?.phone_number?.toString()?:""
            val intent = Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null))
            startActivity(intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP))
        }
    }

    override fun showLoader(isLoading: Boolean) {

    }

    override fun apiFailure() {
        view?.showSWWerror()
    }

    override fun handleApiError(code: Int?, error: String?) {
        if (code == StatusCode.UNAUTHORIZED) {
            signOut(activity)
            return
        }
        view?.showSnack(error.toString())
    }

    override fun onDestroyView() {
        super.onDestroyView()
        activity?.unregisterReceiver(receiver)
        presenter.detachView()
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        onEndService = context as OnEndService
    }
    interface OnEndService{
        fun onServiceEnd()
    }

    private val receiver = object : BroadcastReceiver(){
        override fun onReceive(context: Context?, intent: Intent?) {
            val time = intent?.getStringExtra("time")?:""
            val distance = intent?.getStringExtra("distance")?:""
            tvDistanceStart?.text = distance
            tvTimeStart?.text = time
        }
    }
}
