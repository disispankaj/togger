package com.buraq24.driver.ui.home

import com.buraq24.driver.webservices.models.RoadItem
import com.buraq24.driver.webservices.models.order.Order
import com.buraq24.utilities.basearc.BasePresenter
import com.buraq24.utilities.basearc.BaseView
import com.buraq24.utilities.webservices.models.LoginModel
import com.buraq24.utilities.webservices.models.Service
import org.json.JSONObject

class HomeContract {

    interface View : BaseView {
        fun onApiSuccess(status: String?)
        fun onSupportListApiSuccess(response: List<Service>?)
        fun onOrderDetailsSuccess(response: Order?)
        fun onOrdersApiSuccess(response: ArrayList<Order>?)
        fun polyLine(jsonRootObject: JSONObject)
        fun logoutSuccess()
        fun snappedPoints(response: List<RoadItem>?)
        fun onSuccessCode(response: LoginModel?)
    }

    interface Presenter : BasePresenter<View> {
        fun driverStatusChange(status: String, showLoader: Boolean)
        fun onGoingOrderApi()
        fun getSupportList()
        fun logout()
        fun getOrderDetails(orderId: Long)
        fun drawPolyLine(sourceLat: Double, sourceLong: Double, destLat: Double, destLong: Double)
        fun verifyCode(map: HashMap<String, String>)
    }

}