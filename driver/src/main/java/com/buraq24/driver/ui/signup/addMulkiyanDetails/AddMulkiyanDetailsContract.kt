package com.buraq24.driver.ui.signup.addMulkiyanDetails

import com.buraq24.driver.webservices.models.RequestPersonalDetails
import com.buraq24.driver.webservices.models.ResponseCommon
import com.buraq24.utilities.basearc.BasePresenter
import com.buraq24.utilities.basearc.BaseView

class AddMulkiyanDetailsContract{

    interface View: BaseView{
        fun onApiSuccess(response: ResponseCommon?)
    }

    interface Presenter: BasePresenter<View>{
        fun addMulkiyanDetails(requestPersonalDetails: RequestPersonalDetails)
    }

}