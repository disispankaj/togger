package com.buraq24.driver.ui.home.etokens.customers

import com.buraq24.driver.webservices.ApiResponse
import com.buraq24.driver.webservices.RestClient
import com.buraq24.driver.webservices.models.customers.CustomersModel
import com.buraq24.driver.webservices.models.order.Order
import com.buraq24.getApiError
import com.buraq24.utilities.basearc.BasePresenterImpl
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AreaCustomersPresenter : BasePresenterImpl<AreaCustomersContract.View>(), AreaCustomersContract.Presenter {

    override fun areaListingApi(map: HashMap<String, String>) {
        getView()?.showLoader(true)
        RestClient.get().customersList(map).enqueue(object : Callback<ApiResponse<CustomersModel>> {
            override fun onResponse(call: Call<ApiResponse<CustomersModel>>?, response: Response<ApiResponse<CustomersModel>>?) {
                getView()?.showLoader(false)
                if (response?.isSuccessful == true) {
                    getView()?.onApiSuccess(response.body()?.result)
                } else {
                    val errorModel = getApiError(response?.errorBody()?.string())
                    getView()?.handleApiError(errorModel.statusCode, errorModel.msg)
                }
            }

            override fun onFailure(call: Call<ApiResponse<CustomersModel>>?, t: Throwable?) {
                getView()?.showLoader(false)
                getView()?.apiFailure()
            }
        })
    }

    override fun initiateOrderApi(map: HashMap<String, String>) {
        RestClient.get().initiateWaterOrder(map).enqueue(object : Callback<ApiResponse<Order>> {
            override fun onResponse(call: Call<ApiResponse<Order>>?, response: Response<ApiResponse<Order>>?) {
                if (response?.isSuccessful == true) {
                    getView()?.onInitiateOrderApiSuccess(response.body()?.result)
                } else {
                    val errorModel = getApiError(response?.errorBody()?.string())
                    getView()?.handleApiError(errorModel.statusCode, errorModel.msg)
                }
            }

            override fun onFailure(call: Call<ApiResponse<Order>>?, t: Throwable?) {
                getView()?.apiFailure()
            }
        })
    }
}