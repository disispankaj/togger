package com.buraq24.driver

import android.content.Context
import android.database.DataSetObserver
import android.view.View
import android.view.ViewGroup
import android.widget.*
import android.databinding.adapters.TextViewBindingAdapter.setText
import android.view.LayoutInflater
import android.graphics.Color.parseColor
import android.databinding.adapters.TextViewBindingAdapter.setText
import android.graphics.Color
import android.databinding.adapters.TextViewBindingAdapter.setText
import android.databinding.adapters.TextViewBindingAdapter.setText
import android.databinding.adapters.TextViewBindingAdapter.setTextSize


internal class ColorListArrayAdapter(var context: Context) : BaseAdapter() {
    var colors: ArrayList<Int>

    override fun getCount(): Int {
        return colors.size
    }

    init {
        colors = ArrayList()
        val retrieve = context.resources.getIntArray(R.array.androidColors)
        for (re in retrieve) {
            colors.add(re)
        }
    }

 override   fun getItem(arg0: Int): Any {
        return colors[arg0]
    }

 override   fun getItemId(arg0: Int): Long {
        return arg0.toLong()
    }

 override   fun getView(pos: Int, view: View?, parent: ViewGroup): View {
        var viewInflated = view
        val inflater = LayoutInflater.from(context)
        viewInflated = inflater.inflate(android.R.layout.simple_spinner_dropdown_item, null)
        val txv = viewInflated.findViewById<View>(android.R.id.text1) as CheckedTextView
        txv.setBackgroundColor(colors[pos])
       // txv.textSize = 20f
       // txv.text = "Text  $pos"
        return viewInflated
    }

}