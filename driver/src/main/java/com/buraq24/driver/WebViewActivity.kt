package com.buraq24.driver

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.webkit.*

import kotlinx.android.synthetic.main.activity_web_view.*
import android.webkit.WebResourceError
import android.webkit.WebResourceRequest
import android.webkit.WebView
import com.buraq24.driver.utils.LocaleHelper
import com.buraq24.driver.utils.getPhoneLang
import com.buraq24.utilities.LocaleManager
import com.buraq24.utilities.SharedPrefs
import com.buraq24.utilities.constants.LANGUAGE_CODE


class WebViewActivity : AppCompatActivity() {

    companion object {
        const val EXTRA_URL = "EXTRA_URL"
        const val EXTRA_TITLE = "EXTRA_TITLE"

        fun start(context: Context, url: String, title: String) {
            context.startActivity(Intent(context, WebViewActivity::class.java)
                    .putExtra(EXTRA_URL, url).putExtra(EXTRA_TITLE, title)
                    .addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP))
        }
    }


    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(LocaleManager.setLocale(newBase))
    }

    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_web_view)

        toolbar?.setNavigationOnClickListener {
            finish()
        }

        if (intent.hasExtra(EXTRA_TITLE)) {
            toolbar.title = intent.getStringExtra(EXTRA_TITLE)
        }

        webView?.settings?.javaScriptEnabled = true
        webView?.settings?.allowFileAccess = true
        webView?.settings?.allowContentAccess = true
        webView?.settings?.allowFileAccessFromFileURLs = true
        webView?.settings?.allowUniversalAccessFromFileURLs = true

        // Setup web view
        webView?.webChromeClient = object : WebChromeClient() {
            override fun onProgressChanged(view: WebView, newProgress: Int) {
                progressBar.progress = newProgress
                super.onProgressChanged(view, newProgress)
            }


        }
        webView?.webViewClient = object : WebViewClient() {

            override fun onPageFinished(view: WebView, url: String) {
                progressBar.visibility = View.GONE
                super.onPageFinished(view, url)
            }

        }

        webView?.loadUrl(intent.getStringExtra(EXTRA_URL))

    }


    override fun onDestroy() {
        super.onDestroy()
        if (webView != null) {
            rlContainer.removeView(webView)
            webView.destroy()
        }
    }
}