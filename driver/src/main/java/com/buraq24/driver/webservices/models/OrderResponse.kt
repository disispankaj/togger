package com.buraq24.driver.webservices.models

import com.buraq24.driver.webservices.models.order.Order

class OrderResponse {
    val success: Int? = null
    val statusCode: Int? = null
    val msg: String? = null
    val result: ArrayList<Order>? = null
    val pendingOrders: ArrayList<Order>? = null
}