package com.buraq24.driver.webservices.models.order

data class OrderCommon( val success: Int? = null,
                        val statusCode: Int? = null,
                        val msg: String? = null,
                        var order: Order?= null)