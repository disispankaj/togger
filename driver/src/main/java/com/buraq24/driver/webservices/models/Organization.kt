package com.buraq24.driver.webservices.models

import com.google.gson.annotations.SerializedName

data class Organization(

	@field:SerializedName("organisation_id")
	val organisationId: Int? = null,

	@field:SerializedName("image")
	val image: String? = null,

	@field:SerializedName("licence_number")
	val licenceNumber: String? = null,

	@field:SerializedName("image_url")
	val imageUrl: String? = null,

	@field:SerializedName("name")
	val name: String? = null

){
	override fun toString(): String {
		return name?:""
	}
}