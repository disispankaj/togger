package com.buraq24.driver.webservices.models.homeapi

data class Category(
        var category_brand_id: Int?,
        var sort_order: Int?,
        var category_id: Int?,
        var name: String?,
        var image: String?,
        var image_url: String?,
        var products: List<Product>?
)