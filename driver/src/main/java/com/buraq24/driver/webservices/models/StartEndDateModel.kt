package com.buraq24.driver.webservices.models

import java.io.Serializable

data class StartEndDateModel(
        var startDate : String ? = null,
        var endDate : String ? = null,
        var isSelected : Boolean ? = false
) : Serializable