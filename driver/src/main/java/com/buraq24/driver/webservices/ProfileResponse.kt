package com.buraq24.driver.webservices

class ProfileResponse<T> {
    val success: Int? = null
    val statusCode: Int? = null
    val msg: String? = null
    val AppDetail: T? = null
}