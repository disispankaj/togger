package com.buraq24.driver.webservices.models

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class RatingBy(

	@field:SerializedName("order_rating_id")
	val orderRatingId: Int? = null,

	@field:SerializedName("comments")
	val comments: String? = null,

	@field:SerializedName("ratings")
	val ratings: Int? = null,

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("created_by")
	val createdBy: String? = null
) : Parcelable {
	constructor(parcel: Parcel) : this(
			parcel.readValue(Int::class.java.classLoader) as? Int,
			parcel.readString(),
			parcel.readValue(Int::class.java.classLoader) as? Int,
			parcel.readString(),
			parcel.readString())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
		parcel.writeValue(orderRatingId)
		parcel.writeString(comments)
		parcel.writeValue(ratings)
		parcel.writeString(createdAt)
		parcel.writeString(createdBy)
	}

	override fun describeContents(): Int {
		return 0
	}

	companion object CREATOR : Parcelable.Creator<RatingBy> {
		override fun createFromParcel(parcel: Parcel): RatingBy {
			return RatingBy(parcel)
		}

		override fun newArray(size: Int): Array<RatingBy?> {
			return arrayOfNulls(size)
		}
	}
}