package com.buraq24.driver.webservices.models.homeapi

data class Product(
        var category_brand_product_id: Int?,
        var name: String?,
        var description: String?,
        var sort_order: Int?,
        var actual_value: Float?,
        var alpha_price: Float?,
        var price_per_quantity: Float?,
        var price_per_distance: Float?,
        var price_per_weight: Float?
){
    override fun toString(): String {
        return name?:""
    }
}