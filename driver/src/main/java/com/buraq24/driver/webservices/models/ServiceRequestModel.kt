package com.buraq24.customer.webservices.models

class ServiceRequestModel {
    var category_id: Int? = -1
    var category_brand_id: Int? = -1
    var category_brand_product_id: Int? = -1
    var product_quantity: Int? = 0
    var pickup_address: String? = ""
    var productName: String? = ""
    var pickup_latitude: Double? = 0.0
    var pickup_longitude: Double? = 0.0
    var order_timings: String? = ""
    var future: String? = "0"
    var payment_type: String? = ""
    var distance: Int? = 100
    var price_per_item:Float? = 0f
}