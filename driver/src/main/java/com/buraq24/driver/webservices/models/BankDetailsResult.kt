package com.buraq24.driver.webservices.models

import com.buraq24.driver.webservices.models.homeapi.Product
import com.buraq24.utilities.webservices.models.AppDetail

class BankDetailsResult(val AppDetail:AppDetail,val products: List<Product>)