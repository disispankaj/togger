package com.buraq24.driver.webservices.models.order

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class Payment(

	@field:SerializedName("customer_user_type_id")
	val customerUserTypeId: Int? = null,

	@field:SerializedName("user_card_id")
	val userCardId: Int? = null,

	@field:SerializedName("product_weight")
	val productWeight: Int? = null,

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("final_charge")
	val finalCharge: Double? = null,

	@field:SerializedName("bottle_returned_value")
	val bottleReturnedValue: Int? = null,

	@field:SerializedName("product_quantity")
	val productQuantity: Int? = null,

	@field:SerializedName("updated_at")
	val updatedAt: String? = null,

	@field:SerializedName("product_per_weight_charge")
	val productPerWeightCharge: Double? = null,

	@field:SerializedName("payment_id")
	val paymentId: Int? = null,

	@field:SerializedName("bottle_charge")
	val bottleCharge: Double? = null,

	@field:SerializedName("seller_user_type_id")
	val sellerUserTypeId: Int? = null,

	@field:SerializedName("initial_charge")
	val initialCharge: Double? = null,

	@field:SerializedName("customer_organisation_id")
	val customerOrganisationId: Int? = null,

	@field:SerializedName("customer_user_detail_id")
	val customerUserDetailId: Int? = null,

	@field:SerializedName("transaction_id")
	val transactionId: String? = null,

	@field:SerializedName("admin_charge")
	val adminCharge: Double? = null,

	@field:SerializedName("product_per_quantity_charge")
	val productPerQuantityCharge: Double? = null,

	@field:SerializedName("refund_status")
	val refundStatus: String? = null,

	@field:SerializedName("payment_status")
	val paymentStatus: String? = null,

	@field:SerializedName("product_actual_value")
	val productActualValue: Int? = null,

	@field:SerializedName("order_distance")
	val orderDistance: Double? = null,

	@field:SerializedName("refund_id")
	val refundId: String? = null,

	@field:SerializedName("product_per_distance_charge")
	val productPerDistanceCharge: Double? = null,

	@field:SerializedName("seller_user_id")
	val sellerUserId: Int? = null,

	@field:SerializedName("payment_type")
	val paymentType: String? = null,

	@field:SerializedName("buraq_percentage")
	val buraqPercentage: Double? = null,

	@field:SerializedName("seller_organisation_id")
	val sellerOrganisationId: Int? = null,

	@field:SerializedName("seller_user_detail_id")
	val sellerUserDetailId: Int? = null,

	@field:SerializedName("product_alpha_charge")
	val productAlphaCharge: Double? = null,

	@field:SerializedName("customer_user_id")
	val customerUserId: Int? = null,

	@field:SerializedName("bank_charge")
	val bankCharge: Double? = null,

	@field:SerializedName("order_id")
	val orderId: Int? = null
) : Parcelable {
	constructor(parcel: Parcel) : this(
			parcel.readValue(Int::class.java.classLoader) as? Int,
			parcel.readValue(Int::class.java.classLoader) as? Int,
			parcel.readValue(Int::class.java.classLoader) as? Int,
			parcel.readString(),
			parcel.readValue(Double::class.java.classLoader) as? Double,
			parcel.readValue(Int::class.java.classLoader) as? Int,
			parcel.readValue(Int::class.java.classLoader) as? Int,
			parcel.readString(),
			parcel.readValue(Double::class.java.classLoader) as? Double,
			parcel.readValue(Int::class.java.classLoader) as? Int,
			parcel.readValue(Double::class.java.classLoader) as? Double,
			parcel.readValue(Int::class.java.classLoader) as? Int,
			parcel.readValue(Double::class.java.classLoader) as? Double,
			parcel.readValue(Int::class.java.classLoader) as? Int,
			parcel.readValue(Int::class.java.classLoader) as? Int,
			parcel.readString(),
			parcel.readValue(Double::class.java.classLoader) as? Double,
			parcel.readValue(Double::class.java.classLoader) as? Double,
			parcel.readString(),
			parcel.readString(),
			parcel.readValue(Int::class.java.classLoader) as? Int,
			parcel.readValue(Double::class.java.classLoader) as? Double,
			parcel.readString(),
			parcel.readValue(Double::class.java.classLoader) as? Double,
			parcel.readValue(Int::class.java.classLoader) as? Int,
			parcel.readString(),
			parcel.readValue(Double::class.java.classLoader) as? Double,
			parcel.readValue(Int::class.java.classLoader) as? Int,
			parcel.readValue(Int::class.java.classLoader) as? Int,
			parcel.readValue(Double::class.java.classLoader) as? Double,
			parcel.readValue(Int::class.java.classLoader) as? Int,
			parcel.readValue(Double::class.java.classLoader) as? Double,
			parcel.readValue(Int::class.java.classLoader) as? Int)

    override fun writeToParcel(parcel: Parcel, flags: Int) {
		parcel.writeValue(customerUserTypeId)
		parcel.writeValue(userCardId)
		parcel.writeValue(productWeight)
		parcel.writeString(createdAt)
		parcel.writeValue(finalCharge)
		parcel.writeValue(bottleReturnedValue)
		parcel.writeValue(productQuantity)
		parcel.writeString(updatedAt)
		parcel.writeValue(productPerWeightCharge)
		parcel.writeValue(paymentId)
		parcel.writeValue(bottleCharge)
		parcel.writeValue(sellerUserTypeId)
		parcel.writeValue(initialCharge)
		parcel.writeValue(customerOrganisationId)
		parcel.writeValue(customerUserDetailId)
		parcel.writeString(transactionId)
		parcel.writeValue(adminCharge)
		parcel.writeValue(productPerQuantityCharge)
		parcel.writeString(refundStatus)
		parcel.writeString(paymentStatus)
		parcel.writeValue(productActualValue)
		parcel.writeValue(orderDistance)
		parcel.writeString(refundId)
		parcel.writeValue(productPerDistanceCharge)
		parcel.writeValue(sellerUserId)
		parcel.writeString(paymentType)
		parcel.writeValue(buraqPercentage)
		parcel.writeValue(sellerOrganisationId)
		parcel.writeValue(sellerUserDetailId)
		parcel.writeValue(productAlphaCharge)
		parcel.writeValue(customerUserId)
		parcel.writeValue(bankCharge)
		parcel.writeValue(orderId)
	}

	override fun describeContents(): Int {
		return 0
	}

	companion object CREATOR : Parcelable.Creator<Payment> {
		override fun createFromParcel(parcel: Parcel): Payment {
			return Payment(parcel)
		}

		override fun newArray(size: Int): Array<Payment?> {
			return arrayOfNulls(size)
		}
	}
}