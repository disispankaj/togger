package com.buraq24.driver.webservices.models.order

import android.os.Parcel
import android.os.Parcelable

data class Brand(
        var category_brand_product_id: Int?,
        var category_id: Int?,
        var category_brand_id: Int?,
        var category_sub_type: String?,
        var si_unit: String?,
        var actual_value: Float?,
        var alpha_price: Float?,
        var price_per_quantity: Float?,
        var price_per_distance: Float?,
        var price_per_weight: Float?,
        var sort_order: Int?,
        var multiple: String?,
        var blocked: String?,
        var created_at: String?,
        var updated_at: String?,
        var name: String?,
        var description: String?,
        var category_name:String?,
        var brand_name: String?,
        var product_name: String?
) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readString(),
            parcel.readString(),
            parcel.readValue(Float::class.java.classLoader) as? Float,
            parcel.readValue(Float::class.java.classLoader) as? Float,
            parcel.readValue(Float::class.java.classLoader) as? Float,
            parcel.readValue(Float::class.java.classLoader) as? Float,
            parcel.readValue(Float::class.java.classLoader) as? Float,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString()
            )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(category_brand_product_id)
        parcel.writeValue(category_id)
        parcel.writeValue(category_brand_id)
        parcel.writeString(category_sub_type)
        parcel.writeString(si_unit)
        parcel.writeValue(actual_value)
        parcel.writeValue(alpha_price)
        parcel.writeValue(price_per_quantity)
        parcel.writeValue(price_per_distance)
        parcel.writeValue(price_per_weight)
        parcel.writeValue(sort_order)
        parcel.writeString(multiple)
        parcel.writeString(blocked)
        parcel.writeString(created_at)
        parcel.writeString(updated_at)
        parcel.writeString(name)
        parcel.writeString(description)
        parcel.writeString(category_name)
        parcel.writeString(brand_name)
        parcel.writeString(product_name)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Brand> {
        override fun createFromParcel(parcel: Parcel): Brand {
            return Brand(parcel)
        }

        override fun newArray(size: Int): Array<Brand?> {
            return arrayOfNulls(size)
        }
    }
}