package com.buraq24.customer.webservices.models

data class TrackingModel(
        var type: String?,
        var order_id: Int?,
        var driver_id: Int?,
        var latitude: Double?,
        var longitude: Double?,
        var order_status: String?,
        var bearing: Double?
)