package com.buraq24.driver.webservices

data class PolylineModel(
        var points: String?, // encoded polyline string
        var distanceText: String?,
        var distanceValue: Int?,
        var timeText: String?,
        var timeValue: Int?)
