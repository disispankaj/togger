package com.buraq24.driver.webservices.models.earning

import com.buraq24.driver.webservices.models.order.Brand
import com.buraq24.driver.webservices.models.order.Order
import java.io.Serializable

class EarningResultModel(
        var payment_id: String? = null,
        var final_charge: String? = null,
        var created_at: String? = null,
        var product_quantity: String? = null,
        var Order: Order? = null,
        var brand: Brand? = null
) : Serializable