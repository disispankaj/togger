package com.buraq24.driver.webservices

import com.buraq24.utilities.webservices.BaseRestClient

object RestClient : BaseRestClient() {

    private lateinit var restApi: API

    init {
        create()
    }

    private fun create() {
        val retrofit = createClient()
        restApi = retrofit.create(API::class.java)
    }

    fun recreate(): RestClient {
        create()
        return this
    }

    fun get(): API {
        return restApi
    }
}