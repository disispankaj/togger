package com.buraq24.driver.webservices.models.order

import android.os.Parcel
import android.os.Parcelable

data class Driver(
        var phone_number: Long?,
        var user_id: Int?,
        var name: String?,
        var user_detail_id: Int?,
        var user_type_id: Int?,
        var category_id: Int?,
        var category_brand_id: Int?,
        var profile_pic: String?,
        var latitude: Double?,
        var longitude: Double?,
        var profile_pic_url: String?,
        var rating_count: Int?,
        var rating_avg: String
) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readValue(Long::class.java.classLoader) as? Long,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readString(),
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readString(),
            parcel.readValue(Double::class.java.classLoader) as? Double,
            parcel.readValue(Double::class.java.classLoader) as? Double,
            parcel.readString(),
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readString())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(phone_number)
        parcel.writeValue(user_id)
        parcel.writeString(name)
        parcel.writeValue(user_detail_id)
        parcel.writeValue(user_type_id)
        parcel.writeValue(category_id)
        parcel.writeValue(category_brand_id)
        parcel.writeString(profile_pic)
        parcel.writeValue(latitude)
        parcel.writeValue(longitude)
        parcel.writeString(profile_pic_url)
        parcel.writeValue(rating_count)
        parcel.writeString(rating_avg)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Driver> {
        override fun createFromParcel(parcel: Parcel): Driver {
            return Driver(parcel)
        }

        override fun newArray(size: Int): Array<Driver?> {
            return arrayOfNulls(size)
        }
    }
}