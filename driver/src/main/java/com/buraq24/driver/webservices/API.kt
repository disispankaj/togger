package com.buraq24.driver.webservices

import com.buraq24.driver.ui.callModule.OpenTalkPresenter
import com.buraq24.driver.ui.callModule.OpenTalkSession
import com.buraq24.driver.webservices.models.*
import com.buraq24.driver.webservices.models.customers.Area
import com.buraq24.driver.webservices.models.customers.CustomersModel
import com.buraq24.driver.webservices.models.earning.EarningResultModel
import com.buraq24.driver.webservices.models.homeapi.Product
import com.buraq24.driver.webservices.models.order.Order
import com.buraq24.driver.webservices.models.order.OrderCommon
import com.buraq24.utilities.*
import com.buraq24.utilities.chatModel.ChatMessageListing
import com.buraq24.utilities.webservices.models.AppDetail
import com.buraq24.utilities.webservices.models.LoginModel
import com.buraq24.utilities.webservices.models.Service
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*

interface API {

    @FormUrlEncoded
    @POST("/service/sendOtp")
    fun sendOtp(@FieldMap map: Map<String, String>): Call<ApiResponse<SendOtp>>

    @FormUrlEncoded
    @POST("/service/verifyOTP")
    fun verifyOtp(@FieldMap map: Map<String, String>): Call<ApiResponse<LoginModel>>


    @POST("/common/support/list")
    fun supportList(): Call<ApiResponse<ArrayList<Service>>>

    @POST("/common/eContacts")
    fun eContactsList(): Call<ApiResponse<ArrayList<EContact>>>


    @POST("/service/addPersonalDetail")
    fun addPersonalDetails(@Body requestModel: RequestPersonalDetails): Call<ApiResponse<ResponseCommon>>


    @Multipart
    @POST("/service/addPersonalDetail")
    fun addPersonalDetails(@PartMap map: HashMap<String, RequestBody>): Call<ApiResponse<ResponseCommon>>

    @Multipart
    @POST("/service/picUpdate")
    fun picUpdate(@Part("profile_pic\"; filename=\"pp1.png\" ") pic: RequestBody): Call<ApiResponse<ResponseCommon>>

    @POST("/service/addBankDetail")
    fun addBankDetails(@Body requestModel: RequestPersonalDetails): Call<ApiResponse<BankDetailsResult>>

    @POST("/service/addMulkiyaDetail")
    fun addBankDetails2(@Body requestModel: RequestPersonalDetails): Call<ApiResponse<BankDetailsResult>>

    @POST("/service/addMulkiyaDetail")
    fun addMulkiyanDetails(@Body requestModel: RequestPersonalDetails): Call<ApiResponse<ResponseCommon>>

    @POST("/service/addMulkiyaDetail")
    fun addMulkiyanDetails(@Body requestModel: RequestBody): Call<ApiResponse<AppDetail>>

    @Multipart
    @POST("/service/addBankDetail")
    fun addMulkiyanDetails(@PartMap map: HashMap<String, RequestBody>,
                           @Part imageArray: List<MultipartBody.Part>): Call<ApiResponse<ResponseCommon>>

    @Multipart
    @POST("/service/profileUpdate")
    fun updateProfile(@PartMap map: HashMap<String, RequestBody>): Call<ProfileResponse<ResponseCommon>>

    @Multipart
    @POST("/service/profileUpdate")
    fun updateProfileWithImage(@PartMap map: HashMap<String, RequestBody>,
                               @Part imageArray: List<MultipartBody.Part>): Call<ProfileResponse<ResponseCommon>>

    @FormUrlEncoded
    @POST("/service/order/Accept")
    fun acceptService(@Field("latitude") lat: Double, @Field("longitude") lng: Double,
                      @Field("order_id") orderId: Long): Call<ApiResponse<Order>>

    @FormUrlEncoded
    @POST("/service/order/change/turn")
    fun changeTurn(@Field("order_id") orderId: Long): Call<Any>

    @FormUrlEncoded
    @POST("/service/order/confirm")
    fun confirmUnConfirmService(@Field("latitude") lat: Double, @Field("longitude") lng: Double,
                                @Field("order_id") orderId: Long,
                                @Field("approved") approved: Int): Call<ApiResponse<Order>>

    @FormUrlEncoded
    @POST("/service/order/Reject")
    fun rejectService(@Field("latitude") lat: Double, @Field("longitude") lng: Double,
                      @Field("order_id") orderId: Long): Call<Any>

    @FormUrlEncoded
    @POST("/service/order/Cancel")
    fun cancelService(@Field("latitude") lat: Double, @Field("longitude") lng: Double,
                      @Field("order_id") orderId: Long, @Field("cancel_reason") reason: String): Call<Any>

    @FormUrlEncoded
    @POST("/service/order/Start")
    fun startService(@Field("latitude") lat: Double, @Field("longitude") lng: Double,
                     @Field("distance") distance: Double,
                     @Field("order_id") orderId: Long): Call<Any>


    @FormUrlEncoded
    @POST("/service/order/reached")
    fun reachedService(@Field("latitude") lat: Double, @Field("longitude") lng: Double,
                     @Field("order_id") orderId: Long): Call<Any>

    @FormUrlEncoded
    @POST("/service/order/End")
    fun endService(@Field("latitude") lat: Double, @Field("longitude") lng: Double,
                   @Field("order_id") orderId: Long, @Field("payment_type") paymentType: String): Call<Any>

    @FormUrlEncoded
    @POST("/service/order/Rate")
    fun rateService(@Field("ratings") rating: Double,
                    @Field("order_id") orderId: Long, @Field("comments") comments: String): Call<Any>

    @FormUrlEncoded

    @POST("/service/other/order/history")
    fun getBookingsHistory(@Field("skip") skip: Int,
                           @Field("take") take: Int, @Field("type") type: Int): Call<ApiResponse<ArrayList<Order?>>>

    @FormUrlEncoded
    @POST("/common/updateOnline")
    fun updateOnLineStatus(@Field("online_status") status: String): Call<DriverOnlineStatus>

    @FormUrlEncoded
    @POST("/common/contactus")
    fun sendMessage(@Field("message") message: String): Call<Any>

    @POST("/common/logout")
    fun logout(): Call<Any>

    @POST("/service/order/Ongoing")
    fun onGoingOrder(): Call<OrderResponse>

    @FormUrlEncoded
    @POST("/service/other/order/details")
    fun getOrderDetails(@Field("order_id") orderId: Long): Call<OrderCommon>

    @FormUrlEncoded
    @POST("/common/brandProducts")
    fun getProductsByBrand(@Field("category_brand_id") id: Long): Call<ApiResponse<ArrayList<Product>>>

    @POST("/common/Orgs")
    fun searchOrg(): Call<ApiResponse<ArrayList<Organization>>>


    @GET("directions/json?sensor=false&mode=driving&alternatives=false&units=metric&key=AIzaSyCKx1fqDWY8Kdcqx5wknwjOlzB9Bej4U6Q")
    fun getPolYLine(@Query("origin") origin: String,
                    @Query("destination") destination: String,
                    @Query("language") language: String
    ): Call<ResponseBody>

    @GET("https://roads.googleapis.com/v1/snapToRoads?key=AIzaSyCKx1fqDWY8Kdcqx5wknwjOlzB9Bej4U6Q&interpolate=true")
    fun getRoadPoints(@Query("path") sourceDestLatLng: String): Call<RoadPoints>

    @FormUrlEncoded
    @POST("/common/settingUpdate")
    fun updateSettings(@Field("language_id") languageId: String?,
                       @Field("notifications") notificationsFlag: String?): Call<ApiResponse<Any>>

    @FormUrlEncoded
    @POST("/service/water/areas/list")
    fun areasList(@FieldMap map: Map<String, String>): Call<ApiResponse<List<Area>>>

    @FormUrlEncoded
    @POST("/service/water/area/customers/list")
    fun customersList(@FieldMap map: Map<String, String>): Call<ApiResponse<CustomersModel>>

    @FormUrlEncoded
    @POST("/service/water/initiate/order")
    fun initiateWaterOrder(@FieldMap map: Map<String, String>): Call<ApiResponse<Order>>

    @FormUrlEncoded
    @POST("/service/water/end/order")
    fun endWaterService(@FieldMap map: Map<String, String?>): Call<ApiResponse<Order>>


    @GET("/user/other/getChat")
    fun getChatMessages(@Header(AUTHORISATION) authorization: String?,
                        @Query(MESSAGE_ID) messageId: String,
                        @Query(RECEIVER_ID) receiverId: String,
                        @Query(LIMIT) limit: Int?,
                        @Query(SKIP) skip: Int?,
                        @Query(MESSAGE_ORDER) messageOrder: String): Call<ApiResponse<ArrayList<ChatMessageListing>>>

    @POST("/user/other/chatlogs")
    @FormUrlEncoded
    fun getChatLogs(@Header(AUTHORISATION) authorization: String?,
                    @Field(LIMIT) limit: Int?,
                    @Field(SKIP) skip: Int?): Call<ApiResponse<ArrayList<ChatMessageListing>>>


    @Multipart
    @POST("/user/other/mediafileUpload")
    fun uploadImageChat(@PartMap map: HashMap<String, @JvmSuppressWildcards RequestBody>): Call<ApiResponse<ChatMessageListing>>


    @POST("/user/other/makeCall")
    @FormUrlEncoded
    fun getSessionIdForOpenTalk(@Header(AUTHORISATION) authorization: String?,
                                @Field("receiver_id") limit: String): Call<ApiResponse<OpenTalkSession>>


    @FormUrlEncoded
    @POST("/service/verifyCode")
    fun verifyCode(@FieldMap map: Map<String, String>): Call<ApiResponse<LoginModel>>

    @FormUrlEncoded
    @POST("/service/connectStripe")
    fun connectStripe(@FieldMap map: Map<String, String>): Call<ApiResponse<LoginModel>>

    @FormUrlEncoded
    @POST("/service/other/earnings")
    fun earnings(@Field("start_dt") startDate: String,
                 @Field("end_dt") endDate: String): Call<ApiResponse<ArrayList<EarningResultModel?>>>

}