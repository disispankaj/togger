package com.buraq24.driver.webservices.models

import com.google.gson.annotations.SerializedName
import okhttp3.RequestBody

data class RequestPersonalDetails(
        var name: String?=null,
        var organisation_id: Long?=null,
        var profile_pic: String?=null,
        var category_id: String?=null,
        var vehicle_name: String?=null,
        var vehicle_color: String?=null,
        var category_brand_id: Long?=null,
        var bank_name: String?=null,
        var bank_account_number: String?=null,
        var mulkiya_number: String?=null,
        var mulkiya_validity: String?=null,
        var category_brand_product_ids: String?=null,
        @SerializedName("mulkiya[0]")
        var pic0: String?=null,
        @SerializedName("mulkiya[1]")
        var pic1: String?=null,
        var email: String? = null,
        var date_of_birth : String? = null
        )