package com.buraq24.driver.webservices.models.homeapi

import android.os.Parcel
import android.os.Parcelable

data class User(
        var user_id: Int,
        var organisation_id: Int,
        var stripe_customer_id: String,
        var stripe_connect_id: String,
        var name: String,
        var email: String,
        var phone_code: String,
        var rating_avg: String,
        var rating_count: Long,
        var rating_views: String,
        var phone_number: Long,
        var stripe_connect_token: String,
        var blocked: String,
        var created_by: String,
        var profile_pic_url: String,
        var created_at: String,
        var updated_at: String
) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.readInt(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readLong(),
            parcel.readString(),
            parcel.readLong(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(user_id)
        parcel.writeInt(organisation_id)
        parcel.writeString(stripe_customer_id)
        parcel.writeString(stripe_connect_id)
        parcel.writeString(name)
        parcel.writeString(email)
        parcel.writeString(phone_code)
        parcel.writeString(rating_avg)
        parcel.writeLong(rating_count)
        parcel.writeString(rating_views)
        parcel.writeLong(phone_number)
        parcel.writeString(stripe_connect_token)
        parcel.writeString(blocked)
        parcel.writeString(created_by)
        parcel.writeString(profile_pic_url)
        parcel.writeString(created_at)
        parcel.writeString(updated_at)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<User> {
        override fun createFromParcel(parcel: Parcel): User {
            return User(parcel)
        }

        override fun newArray(size: Int): Array<User?> {
            return arrayOfNulls(size)
        }
    }
}