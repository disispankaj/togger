package com.buraq24.driver.webservices.models.order

import android.os.Parcel
import android.os.Parcelable
import com.buraq24.driver.webservices.models.RatingBy
import com.buraq24.driver.webservices.models.homeapi.User

data class Order(
        var order_id: Long?,
        var order_token: String?,
        var customer_user_id: Int?,
        var customer_user_detail_id: Int?,
        var customer_organisation_id: Int?,
        var customer_user_type_id: Int?,
        var user_card_id: Int?,
        var driver_user_id: Int?,
        var driver_user_detail_id: Int?,
        var driver_organisation_id: Int?,
        var driver_user_type_id: Int?,
        var category_id: Int?,
        var category_brand_id: Int?,
        var category_brand_product_id: Int?,
        var continuous_order_id: Int?,
        var order_status: String?,
        var payment_status: String?,
        var refund_status: String?,
        var pickup_address: String?,
        var pickup_latitude: Double?,
        var pickup_longitude: Double?,
        var dropoff_address: String?,
        var dropoff_latitude: Double?,
        var dropoff_longitude: Double?,
        var category_type: String?,
        var order_type: String?,
        var created_by: String?,
        var order_timings: String?,
        var future: String?,
        var continouos_startdt: String?,
        var continuous_enddt: String?,
        var continuous_time: String?,
        var continuous: String?,
        var transaction_id: String?,
        var payment_type: String?,
        var cancel_reason: String?,
        var refund_id: String?,
        var cancelled_by: String?,
        var order_weight: String?,
        var order_distance: String?,
        var initial_charge: String?,
        var admin_charge: String?,
        var final_charge: String?,
        var bottle_charge: String?,
        var bottle_returned: String?,
        var bottle_returned_value: Int?,
        var bottle_first_time_value: String?,
        var product_quantity: Int?,
        var product_actual_value: String?,
        var product_alpha_price: String?,
        var product_price_per_quantity: String?,
        var product_price_per_distance: String?,
        var product_price_per_weight: String?,
        var organisation_coupon_user_id: Int?,
        var created_at: String?,
        var updated_at: String?,
        var brand: Brand?,
        var payment: Payment?=null,
        var product: Brand?,
        var brand_name: String?,
        var driver: Driver?,
        var user: User?,
        var customer: User?,
        var ratingByDriver: RatingBy?,
        var details: String?,
        var message: String?,
        var my_turn: String? ="0",
        var timeProgress: Int? =0,
        var order_images_url: List<OrderImageUrls?>?

) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readValue(Long::class.java.classLoader) as? Long,
            parcel.readString(),
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readValue(Double::class.java.classLoader) as? Double,
            parcel.readValue(Double::class.java.classLoader) as? Double,
            parcel.readString(),
            parcel.readValue(Double::class.java.classLoader) as? Double,
            parcel.readValue(Double::class.java.classLoader) as? Double,
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readString(),
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readString(),
            parcel.readString(),
            parcel.readParcelable(Brand::class.java.classLoader),
            parcel.readParcelable(Payment::class.java.classLoader),
            parcel.readParcelable(Brand::class.java.classLoader),
            parcel.readString(),
            parcel.readParcelable(Driver::class.java.classLoader),
            parcel.readParcelable(User::class.java.classLoader),
            parcel.readParcelable(User::class.java.classLoader),
            parcel.readParcelable(RatingBy::class.java.classLoader),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.createTypedArrayList(OrderImageUrls))

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(order_id)
        parcel.writeString(order_token)
        parcel.writeValue(customer_user_id)
        parcel.writeValue(customer_user_detail_id)
        parcel.writeValue(customer_organisation_id)
        parcel.writeValue(customer_user_type_id)
        parcel.writeValue(user_card_id)
        parcel.writeValue(driver_user_id)
        parcel.writeValue(driver_user_detail_id)
        parcel.writeValue(driver_organisation_id)
        parcel.writeValue(driver_user_type_id)
        parcel.writeValue(category_id)
        parcel.writeValue(category_brand_id)
        parcel.writeValue(category_brand_product_id)
        parcel.writeValue(continuous_order_id)
        parcel.writeString(order_status)
        parcel.writeString(payment_status)
        parcel.writeString(refund_status)
        parcel.writeString(pickup_address)
        parcel.writeValue(pickup_latitude)
        parcel.writeValue(pickup_longitude)
        parcel.writeString(dropoff_address)
        parcel.writeValue(dropoff_latitude)
        parcel.writeValue(dropoff_longitude)
        parcel.writeString(category_type)
        parcel.writeString(order_type)
        parcel.writeString(created_by)
        parcel.writeString(order_timings)
        parcel.writeString(future)
        parcel.writeString(continouos_startdt)
        parcel.writeString(continuous_enddt)
        parcel.writeString(continuous_time)
        parcel.writeString(continuous)
        parcel.writeString(transaction_id)
        parcel.writeString(payment_type)
        parcel.writeString(cancel_reason)
        parcel.writeString(refund_id)
        parcel.writeString(cancelled_by)
        parcel.writeString(order_weight)
        parcel.writeString(order_distance)
        parcel.writeString(initial_charge)
        parcel.writeString(admin_charge)
        parcel.writeString(final_charge)
        parcel.writeString(bottle_charge)
        parcel.writeString(bottle_returned)
        parcel.writeValue(bottle_returned_value)
        parcel.writeString(bottle_first_time_value)
        parcel.writeValue(product_quantity)
        parcel.writeString(product_actual_value)
        parcel.writeString(product_alpha_price)
        parcel.writeString(product_price_per_quantity)
        parcel.writeString(product_price_per_distance)
        parcel.writeString(product_price_per_weight)
        parcel.writeValue(organisation_coupon_user_id)
        parcel.writeString(created_at)
        parcel.writeString(updated_at)
        parcel.writeParcelable(brand, flags)
        parcel.writeParcelable(payment, flags)
        parcel.writeParcelable(product, flags)
        parcel.writeString(brand_name)
        parcel.writeParcelable(driver, flags)
        parcel.writeParcelable(user, flags)
        parcel.writeParcelable(customer, flags)
        parcel.writeParcelable(ratingByDriver, flags)
        parcel.writeString(details)
        parcel.writeString(message)
        parcel.writeString(my_turn)
        parcel.writeValue(timeProgress)
        parcel.writeTypedList(order_images_url)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Order> {
        override fun createFromParcel(parcel: Parcel): Order {
            return Order(parcel)
        }

        override fun newArray(size: Int): Array<Order?> {
            return arrayOfNulls(size)
        }
    }

}