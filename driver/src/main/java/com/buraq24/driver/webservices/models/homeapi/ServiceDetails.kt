package com.buraq24.driver.webservices.models.homeapi

import com.buraq24.driver.webservices.models.order.Order

data class ServiceDetails(
        var categories: List<Category>,
        var drivers: List<Any>,
        var Details: Details,
        var currentOrders: List<Order>
)