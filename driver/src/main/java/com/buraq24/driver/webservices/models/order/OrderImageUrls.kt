package com.buraq24.driver.webservices.models.order

import android.os.Parcel
import android.os.Parcelable

data class OrderImageUrls(
        var order_id: Long?,
        var image: String?,
        var image_url: String
) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readValue(Long::class.java.classLoader) as? Long,
            parcel.readString(),
            parcel.readString())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(order_id)
        parcel.writeString(image)
        parcel.writeString(image_url)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<OrderImageUrls> {
        override fun createFromParcel(parcel: Parcel): OrderImageUrls {
            return OrderImageUrls(parcel)
        }

        override fun newArray(size: Int): Array<OrderImageUrls?> {
            return arrayOfNulls(size)
        }
    }
}