package com.buraq24.driver.webservices.models

data class VersioningDetail(
        var force: Float,
        var normal: Float,
        var driver : VersioningDetail
)