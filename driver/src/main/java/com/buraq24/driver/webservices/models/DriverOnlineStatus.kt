package com.buraq24.driver.webservices.models

data class DriverOnlineStatus(
        var success: Int,
        var statusCode: Int,
        var msg: String,
        var online_status: String
)