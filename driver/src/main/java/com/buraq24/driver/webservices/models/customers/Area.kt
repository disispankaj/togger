package com.buraq24.driver.webservices.models.customers

data class Area(
        var organisation_area_id: Int?,
        var organisation_id: Int?,
        var label: String?,
        var address: String?,
        var address_latitude: Double?,
        var address_longitude: Double?,
        var distance: Double?,
        var category_id: Int,
        var sunday_service: Int,
        var monday_service: Int,
        var tuesday_service: Int,
        var wednesday_service: Int,
        var thursday_service: Int,
        var friday_service: Int,
        var saturday_service: Int,
        var deleted: String,
        var blocked: String,
        var created_at: String,
        var updated_at: String

)