package com.buraq24.driver.webservices.models.customers

data class CustomersModel(
        var area: Area,
        var customers: List<Customer>
)