package com.buraq24.driver.webservices

class ApiResponse<T> {
    val success: Int? = null
    val statusCode: Int? = null
    val msg: String? = null
    val result: T? = null
}