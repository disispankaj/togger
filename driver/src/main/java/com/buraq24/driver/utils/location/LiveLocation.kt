package com.buraq24.driver.utils.location

data class LiveLocation (var userId: String?,
                         var driverId: String?,
                         var lat: Double?,
                         var long: Double?,
                         var orderId: String?,
var bearing: Float?)