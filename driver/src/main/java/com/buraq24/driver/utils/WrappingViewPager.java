package com.buraq24.driver.utils;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;

public class WrappingViewPager extends ViewPager {

    private Boolean mAnimStarted = false;

    public WrappingViewPager(Context context) {
        super(context);
    }

    public WrappingViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        if (!mAnimStarted && null != getAdapter() && getAdapter().getCount() > 0) {
            int height = 0;
            View child = ((GeneralFragmentPagerAdapter) getAdapter()).getItem(getCurrentItem()).getView();

            if (child != null) {
                child.measure(widthMeasureSpec, MeasureSpec.makeMeasureSpec(0,
                        MeasureSpec.UNSPECIFIED));
                height = child.getMeasuredHeight();
                if (height < getMinimumHeight()) {
                    height = getMinimumHeight();
                }
                child.setClickable(false);
            }

            // Not the best place to put this animation, but it works pretty good.
            int newHeight = MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY);
            heightMeasureSpec = newHeight+150;

        }
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

}
