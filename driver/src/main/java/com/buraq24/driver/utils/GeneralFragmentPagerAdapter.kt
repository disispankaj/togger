package com.buraq24.driver.utils

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import android.support.v4.view.PagerAdapter
import com.buraq24.driver.webservices.models.order.Order
import com.google.gson.Gson

class GeneralFragmentPagerAdapter(fragmentManager: FragmentManager) :
        FragmentStatePagerAdapter(fragmentManager) {
    private val fragments = ArrayList<Fragment>()
    private val titles = ArrayList<String>()
    private val orderIdList = ArrayList<Long>()
    private val orderList = ArrayList<Order?>()

    fun addFragment(fragment: Fragment, title: String) {
        fragments.add(fragment)
        titles.add(title)
    }

    fun addFragment(fragment: Fragment, title: String, orderId: Long , order: Order?) {
        fragments.add(fragment)
        orderIdList.add(orderId)
        orderList.add(order)
        titles.add(title)
        val data =Bundle()
        data.putString("order",Gson().toJson(order))
        fragment.arguments = data
    }

    override fun getPageTitle(position: Int): CharSequence = titles[position]

    override fun getItem(position: Int): Fragment = fragments[position]

    override fun getCount(): Int = fragments.size

    override fun getItemPosition(`object`: Any): Int {
        val position = (fragments.indexOf(((`object` as Fragment))))
        return PagerAdapter.POSITION_NONE
        //return position

    }

    fun getFragments():  ArrayList<Fragment> {
        return fragments
    }

    fun removeFragment(orderId : Long?) {
        val item =orderIdList.find { it == orderId}
        val itemIndex =orderIdList.indexOf(item)
        if(itemIndex != -1) {
            fragments.removeAt(itemIndex)
            orderIdList.removeAt(itemIndex)
        }
    }

    fun removeFragmentAll() {
        fragments.clear()
        orderIdList.clear()
    }


}