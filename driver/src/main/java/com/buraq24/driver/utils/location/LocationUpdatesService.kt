package com.buraq24.driver.utils.location

import android.app.ActivityManager
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.location.Location
import android.os.Binder
import android.os.Build
import android.os.Handler
import android.os.HandlerThread
import android.os.IBinder
import android.os.Looper
import android.support.v4.app.NotificationCompat
import android.support.v4.content.LocalBroadcastManager
import android.util.Log
import android.widget.Toast
import com.buraq24.driver.MainActivity
import com.buraq24.driver.ui.home.HomeActivity
import com.buraq24.driver.ui.home.servicerequest.ServiceAllRequestFragment

import com.buraq24.driver.utils.BubbleOverlay
import com.buraq24.driver.webservices.models.order.Order
import com.buraq24.utilities.*
import com.buraq24.utilities.constants.ACCESS_TOKEN
import com.buraq24.utilities.constants.ACCESS_TOKEN_KEY
import com.buraq24.utilities.constants.PREFS_LANGUAGE_ID
import com.buraq24.utilities.constants.PROFILE
import com.buraq24.utilities.webservices.models.AppDetail
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.model.LatLng
import com.google.firebase.iid.FirebaseInstanceId
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import io.socket.client.Ack

import io.socket.emitter.Emitter
import org.jetbrains.anko.runOnUiThread
import org.json.JSONObject
import java.util.*
import kotlin.collections.ArrayList

/**
 * A bound and started service that is promoted to a foreground service when location updates have
 * been requested and all clients unbind.
 *
 *
 * For apps running in the background on "O" devices, location is computed only once every 10
 * minutes and delivered batched every 30 minutes. This restriction applies even to apps
 * targeting "N" or lower which are run on "O" devices.
 *
 *
 * This sample show how to use a long-running service for location updates. When an activity is
 * bound to this service, frequent location updates are permitted. When the activity is removed
 * from the foreground, the service promotes itself to a foreground service, and location updates
 * continue. When the activity comes back to the foreground, the foreground service stops, and the
 * notification assocaited with that service is removed.
 */
class LocationUpdatesService : Service() {

    private var isServiceGoing = true

    private val mBinder = LocalBinder()

    /**
     * Used to check whether the bound activity has really gone away and not unbound as part of an
     * orientation change. We create a foreground service notification only if the former takes
     * place.
     */
    private var mChangingConfiguration = false

    private var mNotificationManager: NotificationManager? = null

    /**
     * Contains parameters used by [com.google.android.gms.location.FusedLocationProviderApi].
     */
    private var mLocationRequest: LocationRequest? = null

    /**
     * Provides access to the Fused Location Provider API.
     */
    private var mFusedLocationClient: FusedLocationProviderClient? = null

    /**
     * Callback for changes in location.
     */
    private var mLocationCallback: LocationCallback? = null

    private var mServiceHandler: Handler? = null

    /**
     * The current location.
     */
    private var mLocation: Location? = null

    private var liveLocation: LiveLocation? = null

    private var bubbleOverlay: BubbleOverlay? = null

    /**
     * Returns the [NotificationCompat] used as part of the foreground service.
     */
    /*private val notification: Notification get() {
            val intent = Intent(this, HomeActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            val pendingIntent = PendingIntent.getActivity(this, 0 *//* Request code *//*, intent,
                    PendingIntent.FLAG_ONE_SHOT)

            val builder = NotificationCompat.Builder(this, CHANNEL_ID)
                    .setContentText(getString(com.buraq24.driver.R.string.app_running_in_the_background))
                    .setContentTitle(applicationContext.getString(R.string.app_name))
                    .setOngoing(true)
                    .setPriority(Notification.PRIORITY_LOW)
                    .setSmallIcon(R.drawable.ic_satellite)
                    .setWhen(System.currentTimeMillis())
                    .setOnlyAlertOnce(true)
                    .setContentIntent(pendingIntent)

            return builder.build()
        }*/

    private val orderEventListener = Emitter.Listener { args ->
        if (JSONObject(args[0].toString()).has("message")) {
            matchTypeAndDoAccordingly(JSONObject(args[0].toString()).getString("type"),
                    JSONObject(args[0].toString()).get("order").toString(), JSONObject(args[0].toString()).get("message").toString())
        } else {
            matchTypeAndDoAccordingly(JSONObject(args[0].toString()).getString("type"),
                    JSONObject(args[0].toString()).get("order").toString(), "")
        }

    }

    override fun onCreate() {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)

        mLocationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                super.onLocationResult(locationResult)
                locationResult?.lastLocation?.let { onNewLocation(it) }
            }
        }
        bubbleOverlay = BubbleOverlay(this)
        createLocationRequest()
        getLastLocation()

        val handlerThread = HandlerThread(TAG)
        handlerThread.start()
        mServiceHandler = Handler(handlerThread.looper)
        mNotificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        // Android O requires a Notification Channel.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = "Location Service"
            // Create the channel for the notification
            val mChannel = NotificationChannel(CHANNEL_ID, name, NotificationManager.IMPORTANCE_DEFAULT)
            mChannel.setShowBadge(false)
            mChannel.importance = NotificationManager.IMPORTANCE_LOW
            // Set the Notification Channel for the Notification Manager.
            mNotificationManager?.createNotificationChannel(mChannel)
        }
    }

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        Log.i(TAG, "Service started")
        val startedFromNotification = intent.getBooleanExtra(EXTRA_STARTED_FROM_NOTIFICATION,
                false)
        val isLoggedOut = intent.getBooleanExtra("logged_out", false)
        // We got here because the user decided to remove location updates from the notification.
        if (startedFromNotification || isLoggedOut) {
            removeLocationUpdates()
            stopSelf()
        }
        // Tells the system to not try to recreate the service after it has been killed.
        return Service.START_NOT_STICKY
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        mChangingConfiguration = true
    }

    override fun onBind(intent: Intent): IBinder? {
        /* Called when a client (MainActivity in case of this sample) comes to the foreground
        and binds with this service. The service should cease to be a foreground service
        when that happens. */
        Log.i(TAG, "in onBind()")
        bubbleOverlay?.dismiss()
        stopOrderEventListener()
        stopForeground(true)
        mChangingConfiguration = false
        return mBinder
    }

    override fun onRebind(intent: Intent) {
        /* Called when a client (MainActivity in case of this sample) returns to the foreground
        and binds once again with this service. The service should cease to be a foreground
        service when that happens.*/
        Log.i(TAG, "in onRebind()")
        bubbleOverlay?.dismiss()
        stopOrderEventListener()
        stopForeground(true)
        mChangingConfiguration = false
        super.onRebind(intent)
    }

    override fun onUnbind(intent: Intent): Boolean {
        Log.i(TAG, "Last client unbound from service")
        /*Called when the last client (MainActivity in case of this sample) unbinds from this
        service. If this method is called due to a configuration change in MainActivity, we
        do nothing. Otherwise, we make this service a foreground service.*/
        if (!mChangingConfiguration && LocationUtils.requestingLocationUpdates(this)) {
            Log.i(TAG, "Starting foreground service")
            /*
            // TODO(developer). If targeting O, use the following code.
            if (Build.VERSION.SDK_INT == Build.VERSION_CODES.O) {
                mNotificationManager.startServiceInForeground(new Intent(this,
                        LocationUpdatesService.class), NOTIFICATION_ID, getNotification());
            } else {
                startForeground(NOTIFICATION_ID, getNotification());
            }
             */
            isServiceGoing = true
            startOrderEventListener()
            //startForeground(NOTIFICATION_ID, notification)
            bubbleOverlay?.show()

        }
        return true // Ensures onRebind() is called when a client re-binds.
    }

    private fun sendDataToSocket(latLng: LatLng, currentBearing: Float) {
        try {
            val dataJson1 = JSONObject()
            dataJson1.put("type", "DCurrentOrders")
            dataJson1.put("access_token", ACCESS_TOKEN)
            dataJson1.put("latitude", latLng.latitude)
            dataJson1.put("longitude", latLng.longitude)
            if (currentBearing == Float.NaN) {
                dataJson1.put("bearing", 0.0f)
            } else {
                dataJson1.put("bearing", currentBearing)
            }
            AppSocket.get()?.emit("CommonEvent", dataJson1, Ack {
                try {
                    if (JSONObject(it[0].toString()).getInt("statusCode") == StatusCode.UNAUTHORIZED) {
                        removeLocationUpdates()
                    }
                    val json = JSONObject(it[0].toString()).getJSONObject("result").getJSONArray("orders")
                    val orders: ArrayList<Order> = Gson().fromJson(json.toString(), object :
                            TypeToken<ArrayList<Order>>() {}.type)
                    isServiceGoing = orders.isNotEmpty()
                } catch (exception: Exception) {
                    exception.printStackTrace()
                }
            })
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun sendIdleDataToSocket(latLng: LatLng, currentBearing: Float) {

        val dataJson = JSONObject()
        dataJson.put("type", "UpdateData")
        dataJson.put("access_token", SharedPrefs.with(this).getString(ACCESS_TOKEN_KEY, ""))
        dataJson.put("latitude", latLng.latitude)
        dataJson.put("longitude", latLng.longitude)
        dataJson.put("bearing", currentBearing)
        dataJson.put("language_id", SharedPrefs.get().getInt(PREFS_LANGUAGE_ID, 1).toString())
        dataJson.put("timezone", TimeZone.getDefault().id)
        FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener {
            it?.token?.let { token ->
                dataJson.put("fcm_id", token)
                AppSocket.get()?.emit("CommonEvent", dataJson, Ack { args ->
                    try {
                        if (JSONObject(args[0].toString()).getInt("statusCode") == StatusCode.UNAUTHORIZED) {
                            removeLocationUpdates()
                            return@Ack
                        }
                        val resultJson = JSONObject(args[0].toString()).get("AppDetail").toString()
                        val response = Gson().fromJson<AppDetail>(resultJson, AppDetail::class.java)
                        SharedPrefs.get().save(PROFILE, response)
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                })
            }
        }


    }

    private fun matchTypeAndDoAccordingly(type: String?, orderJsonString: String, message: String) {
        when (type) {
            OrderEventType.SERVICE_CANCEL,
            OrderEventType.CUSTOMER_CANCEL,
            OrderEventType.SYSTEM_CANCELLED -> {
                runOnUiThread {
                    val profile = SharedPrefs.with(this).getObject(PROFILE, AppDetail::class.java)
                    val order = Gson().fromJson(orderJsonString, Order::class.java)
                    if (order?.driver_user_detail_id == profile?.user_detail_id) {
                        Toast.makeText(this, getString(com.buraq24.driver.R.string.your_ride_canelled), Toast.LENGTH_LONG).show()
                        val intent = Intent(this, MainActivity::class.java)
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                        startActivity(intent)
                    }
                }
            }
        }
    }

    override fun onDestroy() {
        bubbleOverlay?.dismiss()
        mServiceHandler?.removeCallbacksAndMessages(null)
    }

    /**
     * Makes a request for location updates. Note that in this sample we merely log the
     * [SecurityException].
     */
    fun requestLocationUpdates() {
        Log.i(TAG, "Requesting location updates")
        LocationUtils.setRequestingLocationUpdates(this, true)
        startService(Intent(applicationContext, LocationUpdatesService::class.java))
        try {
            mFusedLocationClient?.requestLocationUpdates(mLocationRequest,
                    mLocationCallback, Looper.myLooper())
        } catch (unlikely: SecurityException) {
            LocationUtils.setRequestingLocationUpdates(this, false)
            Log.e(TAG, "Lost location permission. Could not request updates. $unlikely")
        }
    }

    /**
     * Removes location updates. Note that in this sample we merely log the
     * [SecurityException].
     */
    fun removeLocationUpdates() {
        bubbleOverlay?.dismiss()
        Log.i(TAG, "Removing location updates")
        try {
            mFusedLocationClient?.removeLocationUpdates(mLocationCallback)
            LocationUtils.setRequestingLocationUpdates(this, false)
            stopSelf()
        } catch (unlikely: SecurityException) {
            LocationUtils.setRequestingLocationUpdates(this, true)
            Log.e(TAG, "Lost location permission. Could not remove updates. $unlikely")
        }

    }

    private fun getLastLocation() {
        try {
            mFusedLocationClient?.lastLocation?.addOnCompleteListener { task ->
                if (task.isSuccessful && task.result != null) {
                    mLocation = task.result
                } else {
                    Log.w(TAG, "Failed to get location.")
                }
            }
        } catch (unlikely: SecurityException) {
            Log.e(TAG, "Lost location permission.$unlikely")
        }

    }

    fun setLiveLocation(liveLocation: LiveLocation) {
        this.liveLocation = liveLocation
    }

    /* Send location to backend in this method*/
    private fun onNewLocation(location: Location) {
        Log.i(TAG, "New location: $location")
        mLocation = location
        // Notify anyone listening for broadcasts about the new location.
        val intent = Intent(ACTION_BROADCAST)
        intent.putExtra(EXTRA_LOCATION, location)
        LocalBroadcastManager.getInstance(applicationContext).sendBroadcast(intent)
        val latLng = LatLng(location.latitude, location.longitude)
        // Update notification content if running as a foreground service.
        if (serviceIsRunningInForeground(this)) {
            if (isServiceGoing) {
                sendDataToSocket(latLng, location.bearing)
            } else {
                sendIdleDataToSocket(latLng, location.bearing)
            }
           // mNotificationManager?.notify(NOTIFICATION_ID, notification)

        }
        if (liveLocation != null) {
            liveLocation?.lat = location.latitude
            liveLocation?.long = location.longitude
            liveLocation?.bearing = location.bearing
            //            AppSocket.get().sendLiveLocation(liveLocation);
        }
    }

    private fun startOrderEventListener() {
        AppSocket.get().on("OrderEvent", orderEventListener)
    }

    private fun stopOrderEventListener() {
        AppSocket.get().off("OrderEvent", orderEventListener)
    }


    /**
     * Sets the location request parameters.
     */
    private fun createLocationRequest() {
        mLocationRequest = LocationRequest()
        mLocationRequest?.interval = UPDATE_INTERVAL_IN_MILLISECONDS
        mLocationRequest?.fastestInterval = FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS
        mLocationRequest?.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
    }

    /**
     * Class used for the client Binder.  Since this service runs in the same process as its
     * clients, we don't need to deal with IPC.
     */
    inner class LocalBinder : Binder() {
        val service: LocationUpdatesService
            get() = this@LocationUpdatesService
    }

    /**
     * Returns true if this is a foreground service.
     *
     * @param context The [Context].
     */
    fun serviceIsRunningInForeground(context: Context): Boolean {
        val manager = context.getSystemService(
                Context.ACTIVITY_SERVICE) as ActivityManager
        for (service in manager.getRunningServices(
                Integer.MAX_VALUE)) {
            if (javaClass.name == service.service.className) {
                if (service.foreground) {
                    return true
                }
            }
        }
        return false
    }

    companion object {

        private val PACKAGE_NAME = "com.google.android.gms.location.sample.locationupdatesforegroundservice"

        private val TAG = LocationUpdatesService::class.java.simpleName

        /**
         * The name of the channel for notifications.
         */
        private val CHANNEL_ID = "channel_01"

        val ACTION_BROADCAST = "$PACKAGE_NAME.broadcast"

        val EXTRA_LOCATION = "$PACKAGE_NAME.location"
        private val EXTRA_STARTED_FROM_NOTIFICATION = "$PACKAGE_NAME.started_from_notification"

        /**
         * The desired interval for location updates. Inexact. Updates may be more or less frequent.
         */
        private val UPDATE_INTERVAL_IN_MILLISECONDS: Long = 10000

        /**
         * The fastest rate for active location updates. Updates will never be more frequent
         * than this value.
         */
        private val FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = UPDATE_INTERVAL_IN_MILLISECONDS

        /**
         * The identifier for the notification displayed for the foreground service.
         */
        private val NOTIFICATION_ID = 12345678
    }
}