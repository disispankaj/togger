package com.buraq24.driver.utils

import android.content.Context
import android.graphics.Point
import android.support.v7.widget.Toolbar
import android.util.AttributeSet
import android.util.DisplayMetrics
import android.view.Display
import android.view.WindowManager
import android.widget.TextView

import com.buraq24.driver.R

//
// CenteredTitleToolbar
//
// Created by Ben De La Haye on 25/05/2016.
//
class CenteredTitleToolbar : Toolbar {

    private var _titleTextView: TextView? = null
    private var _screenWidth: Int = 0
    private var _centerTitle = true

    private val screenSize: Point
        get() {
            val wm = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
            val display = wm.defaultDisplay
            val screenSize = Point()
            display.getSize(screenSize)

            return screenSize
        }

    constructor(context: Context) : super(context) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init()
    }

    private fun init() {
        _screenWidth = DisplayMetrics().xdpi.toInt()

        _titleTextView = TextView(context)
        //_titleTextView.setTextAppearance(getContext(), R.style.ToolbarTitleText);
        _titleTextView!!.text = "Title"
        addView(_titleTextView)
    }

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        super.onLayout(changed, left, top, right, bottom)

        if (_centerTitle) {
            val location = IntArray(2)
            _titleTextView!!.getLocationOnScreen(location)
            _titleTextView!!.translationX = _titleTextView!!.translationX + (-location[0] + _screenWidth / 2 - _titleTextView!!.width / 2)
        }
    }

    override fun setTitle(title: CharSequence) {
        _titleTextView!!.text = title
        requestLayout()
    }

    override fun setTitle(titleRes: Int) {
        _titleTextView!!.setText(titleRes)
        requestLayout()
        setTitleCentered(true)
    }

    fun setTitleCentered(centered: Boolean) {
        _centerTitle = centered
        requestLayout()
    }
}