package com.buraq24.driver.utils

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.support.annotation.DrawableRes
import com.buraq24.driver.R
import com.buraq24.driver.ui.signup.SignupActivity
import com.buraq24.driver.utils.location.LocationUpdatesService
import com.buraq24.driver.webservices.models.order.Order
import com.buraq24.utilities.PaymentType
import com.buraq24.utilities.SharedPrefs
import com.buraq24.utilities.constants.LANGUAGE_CODE
import com.buraq24.utilities.webservices.BASE_URL
import java.text.DecimalFormat
import java.text.NumberFormat
import java.util.*

fun getFinalCharge(orderDetail: Order?, distance: Int): String {
    val price = ((orderDetail?.payment?.productQuantity
            ?: 1).times(orderDetail?.payment?.productPerQuantityCharge ?: 0.00))
            .plus((orderDetail?.payment?.productPerDistanceCharge
                    ?: 0.00).times(orderDetail?.payment?.orderDistance ?: 0.0))
//            .plus((orderDetail?.payment?.productPerWeightCharge
//                    ?: 0.00).times(orderDetail?.payment?.productWeight ?: 0))
            .plus(orderDetail?.payment?.productAlphaCharge
                    ?: 0.00)
            .plus(orderDetail?.payment?.adminCharge ?: 0.00)
    when (orderDetail?.category_id) {
        1 -> {

        }

        2 -> {

        }

        3 -> {

        }
    }
    return getFormattedPrice(price)
}


@DrawableRes
fun getRatingSymbol(rating: Int?): Int {
    rating?.let {
        return when {
            rating == 1 -> {
                R.drawable.ic_1
            }
            rating == 2 -> {
                R.drawable.ic_2
            }
            rating == 3 -> {
                R.drawable.ic_3
            }
            rating == 4 -> {
                R.drawable.ic_4
            }
            rating == 5 -> {
                R.drawable.ic_5
            }
            rating > 5 -> {
                R.drawable.ic_5
            }
            else -> {
                R.drawable.ic_1
            }
        }
    }
    return R.drawable.ic_1

}

fun getLanguageCodeFromId(language: Int): String {
    return when (language) {
        2 -> "hi"
        3 -> "ur"
        4 -> "ch"
        5 -> "ar"
        else -> "en"
    }
}

fun getTermsAndCond(language: Int): String {
//    return BASE_URL + "/pages/Service/" + language.toString()
    return "$BASE_URL${"/pages/Customer/"}$language"
}

fun getServiceName(orderDetail: Order?, services: Array<String>): String {
    return when (orderDetail?.category_id) {
        1 -> services[1]
        2 -> services[2]
        3 -> services[3]
        else -> orderDetail?.brand?.brand_name ?: ""
    }
}

fun getPhoneLang(storeLang: String): String {
    val lang = SharedPrefs.get().getString(LANGUAGE_CODE, Locale.getDefault().language)
//    val langCode = if (storeLang == "en") {
//        when (lang) {
//            "hi" -> "hi"
//            "ur" -> "ur"
//            "ch" -> "ch"
//            "ar" -> "ar"
//            else -> "en"
//
//        }
//    } else {
//        storeLang
//    }
    SharedPrefs.get().save(LANGUAGE_CODE, lang)
    return lang
}

fun getFormattedPrice(value: Double?): String {
    value?.let {
        val nf = NumberFormat.getNumberInstance(Locale.ENGLISH)
        val formatter = nf as DecimalFormat
        formatter.applyPattern("#0.00")
        return formatter.format(value)
    }
    return value.toString()
}

fun getPaymentStringId(paymentType: String): Int {
    return when (paymentType) {
        PaymentType.CARD -> R.string.card
        PaymentType.CASH -> R.string.cash
        PaymentType.E_TOKEN -> R.string.e_token
        else -> R.string.card
    }
}

fun signOut(context: Context?) {
    SharedPrefs.with(context).removeAllSharedPrefsChangeListeners()
    SharedPrefs.with(context).removeAll()
    (context as? Activity)?.finishAffinity()
    context?.startActivity(Intent(context, SignupActivity::class.java))
    val intent = Intent(context, LocationUpdatesService::class.java)
    intent.putExtra("logged_out", true)
    context?.startService(intent)
}




