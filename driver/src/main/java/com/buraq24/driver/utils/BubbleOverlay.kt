package com.buraq24.driver.utils

import android.animation.ValueAnimator
import android.annotation.SuppressLint
import android.content.Context
import android.content.Context.WINDOW_SERVICE
import android.content.Intent
import android.graphics.PixelFormat
import android.os.Build
import android.provider.Settings
import android.util.DisplayMetrics
import android.view.*
import android.view.animation.BounceInterpolator
import com.buraq24.Utils
import com.buraq24.driver.MainActivity
import com.buraq24.driver.R
import java.util.*


class BubbleOverlay(private val context: Context) {

    private var prevTouchTime = Calendar.getInstance().timeInMillis

    private var bubbleView: View? = null

    private var mWindowManager: WindowManager? = null

    private var params: WindowManager.LayoutParams? = null

    private var animator: ValueAnimator? = null

    init {
        val type = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY
        else
            WindowManager.LayoutParams.TYPE_PHONE

        bubbleView = LayoutInflater.from(context).inflate(R.layout.layout_bubble_system_overlay, null, false)
        params = WindowManager.LayoutParams(
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                type,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSLUCENT)
        params?.gravity = Gravity.TOP or Gravity.START
        params?.x = 0
        params?.y = 100
        mWindowManager = context.getSystemService(WINDOW_SERVICE) as WindowManager
    }

    @SuppressLint("ClickableViewAccessibility")
    fun show() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !Settings.canDrawOverlays(context)) return
        dismiss()
        if (bubbleView?.windowToken == null)
            mWindowManager?.addView(bubbleView, params)
        bubbleView?.setOnTouchListener(object : View.OnTouchListener {
            private var lastAction: Int = 0
            private var initialX: Int? = 0
            private var initialY: Int? = 0
            private var initialTouchX: Float = 0.toFloat()
            private var initialTouchY: Float = 0.toFloat()

            override fun onTouch(v: View, event: MotionEvent): Boolean {
                when (event.action) {
                    MotionEvent.ACTION_DOWN -> {
                        prevTouchTime = Calendar.getInstance().timeInMillis
                        //remember the initial position.
                        initialX = params?.x
                        initialY = params?.y

                        //get the touch location
                        initialTouchX = event.rawX
                        initialTouchY = event.rawY

                        lastAction = event.action
                        return true
                    }
                    MotionEvent.ACTION_UP -> {
                        if ((Math.abs(event.rawX - initialTouchX)) < Utils.dpToPx(4) || (Math.abs(event.rawY - initialTouchY)) < Utils.dpToPx(4) || lastAction == MotionEvent.ACTION_DOWN) {
                            val intent = Intent(context, MainActivity::class.java)
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                            context.startActivity(intent)
                        }
                        dropToNearestEdge()
                        lastAction = event.action
                        return true
                    }
                    MotionEvent.ACTION_MOVE -> {
                        if ((Math.abs(event.rawX - initialTouchX)) < Utils.dpToPx(4) || (Math.abs(event.rawY - initialTouchY)) < Utils.dpToPx(4)) return false
//                        if (Calendar.getInstance().timeInMillis - prevTouchTime < 100) return false
                        animator?.cancel()
                        animator?.removeAllUpdateListeners()
                        //Calculate the X and Y coordinates of the view.
                        params?.x = initialX?.plus(event.rawX - initialTouchX)?.toInt()
                        params?.y = initialY?.plus(event.rawY - initialTouchY)?.toInt()

                        //Update the layout with new X & Y coordinate
                        mWindowManager?.updateViewLayout(bubbleView, params)
                        lastAction = event.action
                        return true
                    }
                }
                return false
            }
        })
    }

    private fun dropToNearestEdge() {
        val locationOnScreen = IntArray(2)
        val screenWidth = getScreenWidth()
        val viewWidth = bubbleView?.width ?: 0
        bubbleView?.getLocationOnScreen(locationOnScreen)
        val animateValue: Float = if (locationOnScreen[0].plus(viewWidth.div(2)) <= screenWidth / 2) {
            0f
        } else {
            screenWidth - viewWidth
        }
        animator = ValueAnimator.ofFloat(locationOnScreen[0].toFloat(), animateValue)
        animator?.addUpdateListener {
            try {
                params?.x = (it?.animatedValue as Float).toInt()
                mWindowManager?.updateViewLayout(bubbleView, params)
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }
        animator?.duration = 400
        animator?.interpolator = BounceInterpolator()
        animator?.start()

    }

    private fun getScreenWidth(): Float {
        val displayMetrics = DisplayMetrics()
        mWindowManager?.defaultDisplay?.getMetrics(displayMetrics)
        return displayMetrics.widthPixels.toFloat()
    }

    fun dismiss() {
        try {
            (context.getSystemService(WINDOW_SERVICE) as WindowManager).removeViewImmediate(bubbleView)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

}