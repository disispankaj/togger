package com.buraq24.driver.fcm

import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import io.fabric.sdk.android.services.settings.IconRequest.build
import android.app.NotificationManager
import android.app.NotificationChannel
import android.os.Build
import android.content.Context.NOTIFICATION_SERVICE
import android.media.RingtoneManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import com.buraq24.driver.R
import android.support.v4.app.NotificationCompat
import android.util.Log
import com.buraq24.driver.ui.home.HomeActivity
import com.buraq24.utilities.*
import com.buraq24.utilities.constants.FCM_TOKEN


class AppFirebaseMessagingService : FirebaseMessagingService() {

    private val tag = AppFirebaseMessagingService::class.java.simpleName

    override fun onMessageReceived(remoteMessage: RemoteMessage?) {
        super.onMessageReceived(remoteMessage)
        System.out.println("NOTIFICATION_DATA "+remoteMessage.toString())
        if (remoteMessage?.notification !== null) {
            Logger.e(tag, "Message data payload: " + remoteMessage.notification)
//            sendNotification(remoteMessage.notification?.body ?: "")
        }

        if (remoteMessage?.data?.isNotEmpty() == true) {
            Logger.e(tag, "Message data payload: " + remoteMessage.data["message"])

            if (remoteMessage.data["type"] != null) {
                val intent = Intent()
                intent.action = Broadcast.NOTIFICATION
                intent.putExtra("order_id", remoteMessage.data["order_id"])
                intent.putExtra("type", remoteMessage.data["type"])
                sendBroadcast(intent)

                sendNotification(remoteMessage)
            } else {
                var sessionId = remoteMessage.data["session_id"]
                var token = remoteMessage.data["token"]
                var userID = remoteMessage.data["user_id"]
                var name = remoteMessage.data["name"]
                var profile_pic = remoteMessage.data["profile_pic"]
                var dateTime = remoteMessage.data["date"]

                val intent = Intent(this, CustomBroadCastReceiver::class.java)
                        .setAction("myaction")
                intent.putExtra("session_id", sessionId)
                intent.putExtra("token", token)
                intent.putExtra("dateTime", dateTime)
                intent.putExtra(RECEIVER_ID, userID)
                intent.putExtra(USER_NAME, name)
                intent.putExtra(PROFILE_PIC_URL, profile_pic)
                sendBroadcast(intent)


            }


//            sendNotification(remoteMessage.data.get("message") ?: "")
        }
    }

    override fun onNewToken(token: String?) {
        super.onNewToken(token)
        SharedPrefs.with(applicationContext).save(FCM_TOKEN, token)
    }

    private fun sendNotification(messageBody: RemoteMessage) {
        val intent = Intent(this, HomeActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        val pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT)

        val channelId = getString(com.buraq24.driver.R.string.default_notification_channel_id)
        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val notificationBuilder = NotificationCompat.Builder(this, channelId)
                .setSmallIcon(R.drawable.togger_launcher)
                .setContentTitle(messageBody.data["title"]?:"")
                .setContentText(messageBody.data["body"]?:"")
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent)

        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(channelId,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_DEFAULT)
            notificationManager.createNotificationChannel(channel)
        }

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build())
    }
}