package com.buraq24.customer.fcm

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.buraq24.customer.ui.callingTokBox.AudioChatActivity
import com.buraq24.utilities.PROFILE_PIC_URL
import com.buraq24.utilities.RECEIVER_ID
import com.buraq24.utilities.USER_NAME


class CustomBroadCastReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context?, intent: Intent?) {
        var sessionId = intent!!.getStringExtra("session_id")
        var token = intent.getStringExtra("token")
        var dateTime = intent.getStringExtra("dateTime")
        var receiverID = intent.getStringExtra(RECEIVER_ID)
        var name = intent.getStringExtra(USER_NAME)
        var profile_pic = intent.getStringExtra(PROFILE_PIC_URL)

        val i = Intent(context, AudioChatActivity::class.java)
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        i.addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT)
        i.putExtra("session_id", sessionId)
        i.putExtra("token", token)
        i.putExtra("dateTime", dateTime)
        i.putExtra(RECEIVER_ID, receiverID)
        i.putExtra(USER_NAME, name)
        i.putExtra(PROFILE_PIC_URL, profile_pic)
        context!!.startActivity(i)
    }
}