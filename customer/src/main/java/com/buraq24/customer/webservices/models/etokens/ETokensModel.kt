package com.buraq24.customer.webservices.models.etokens

data class ETokensModel(
        var history: List<History>?,
        var brands: List<Brand>?
)