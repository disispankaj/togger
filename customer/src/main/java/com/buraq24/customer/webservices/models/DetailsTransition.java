package com.buraq24.customer.webservices.models;

import android.support.transition.ChangeBounds;
import android.support.transition.ChangeImageTransform;
import android.support.transition.ChangeTransform;
import android.support.transition.TransitionSet;

public class DetailsTransition extends TransitionSet {
    public DetailsTransition() {
        addTransition(new ChangeBounds());
    }
}