package com.buraq24.customer.webservices.models.paymentdetails

data class PaymentDetail(
        var etoken_sum: Int?,
        var brand: Brand?
)