package com.buraq24.customer.webservices.models.homeapi

import java.math.BigInteger

class Category{
    val category_brand_id: Int?= null
    val sort_order: Int?= null
    val category_id: Int?= null
    val name: String?= null
    val image: String?= null
    val image_url: String?= null
    val products: List<Product>?= null
    var isSelected: Boolean? = false
}