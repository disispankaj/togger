package com.buraq24.customer.webservices.models

import com.buraq24.utilities.constants.REQUEST_DISTANCE
import okhttp3.MultipartBody

class ServiceRequestModel {
    companion object {
        val BOOK_NOW = "0"
        val SCHEDULE = "1"
    }

    var category_id: Int? = -1
    var category_brand_id: Int? = -1
    var category_brand_product_id: Int? = -1
    var product_quantity: Int? = 1
    var pickup_address: String? = ""
    var dropoff_address: String? = ""
    var brandName: String? = ""
    var productName: String? = ""
    var pickup_latitude: Double? = 0.0
    var pickup_longitude: Double? = 0.0
    var dropoff_latitude: Double? = 0.0
    var dropoff_longitude: Double? = 0.0
    var order_timings: String? = ""
    var order_timings_text: String? = ""
    var future: String? = "0"
    var payment_type: String? = ""
    var distance: Int? = REQUEST_DISTANCE
    var price_per_item: Float? = 0f
    var final_charge: Double? = 0.0
    var organisation_coupon_user_id: Int? = null
    var coupon_user_id: Int? = null
    var isCurrentLocation = false
    var material_details: String? = ""
    var details: String? = ""
    var product_weight: Int? = 0
    var order_distance: Float? = 0f
    var images: ArrayList<String> = ArrayList()
    var recieverName:String?=""
    var recieverPhone:String?=""
    var reciptDetail:String?=""
    var pickup_person_name:String?=""
    var pickup_person_phone:String?=""
    var invoice_number:String?=""
    var delivery_person_name:String?=""
    var order_mins :String =""

}