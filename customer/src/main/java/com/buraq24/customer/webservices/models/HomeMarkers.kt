package com.buraq24.customer.webservices.models

import android.animation.ValueAnimator
import com.google.android.gms.maps.model.Marker

class HomeMarkers {
    var marker: Marker? = null
    var moveValueAnimate: ValueAnimator? = null
    var rotateValueAnimator: ValueAnimator? = null
}