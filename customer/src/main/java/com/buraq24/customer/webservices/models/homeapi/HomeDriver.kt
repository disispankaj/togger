package com.buraq24.customer.webservices.models.homeapi

data class HomeDriver(
        var user_detail_id: Int?,
        var user_type_id: Int?,
        var category_id: Int?,
        var category_brand_id: Int?,
        var organisation_id: Int?,
        var latitude: Double?,
        var longitude: Double?,
        var bearing: Float?,
        var distance: Double?,
        var o_blocked: String?,
        var o_serving: Int?
)