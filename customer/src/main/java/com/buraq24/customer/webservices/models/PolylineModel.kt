package com.buraq24.customer.webservices.models

data class PolylineModel(
        var points: String?,
        var distanceText: String?,
        var distanceValue: Int?,
        var timeText: String?,
        var timeValue: Int?)
