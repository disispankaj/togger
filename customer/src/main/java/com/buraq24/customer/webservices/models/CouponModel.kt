package com.buraq24.customer.webservices.models

import java.io.Serializable

class CouponModel(
        var coupon_id : String ? = null,
        var code:String?=null,
        var amount_value :String?=null,
        var rides_value:String ?= null,
        var coupon_type :String ?= null,
        var price : String ?= null,
        var purchase_counts :String ?= null,
        var coupon_user_id : String ?= null
):Serializable