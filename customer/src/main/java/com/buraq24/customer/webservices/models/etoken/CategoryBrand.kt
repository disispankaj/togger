package com.buraq24.customer.webservices.models.etoken

data class CategoryBrand(
        var category_brand_detail_id: Int?,
        var category_brand_id: Int?,
        var name: String?,
        var image: String?,
        var image_url: String?,
        var description: String?
)