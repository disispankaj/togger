package com.buraq24.customer.webservices.models

data class VersioningDetail(
        var force: Float,
        var normal: Float,
        var user: VersioningDetail
)