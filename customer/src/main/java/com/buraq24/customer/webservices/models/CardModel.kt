package com.buraq24.customer.webservices.models

import java.io.Serializable

class CardModel(
        var user_card_id: String? = null,
        var user_id: String? = null,
        var stripe_customer_id: String? = null,
        var stripe_connect_id: String? = null,
        var fingerprint: String? = null,
        var brand: String? = null,
        var last4: String? = null,
        var exp_month: String? = null,
        var exp_year: String? = null,
        var card_holder_name: String? = null,
        var card_id: String? = null,
        var isSelected: Boolean? = null
) : Serializable