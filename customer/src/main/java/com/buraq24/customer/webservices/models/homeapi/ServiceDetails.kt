package com.buraq24.customer.webservices.models.homeapi

import com.buraq24.customer.webservices.models.Versioning
import com.buraq24.customer.webservices.models.order.Order

data class ServiceDetails(
        val categories: List<Category>?,
        val drivers: List<HomeDriver>?,
        val Details: Details?,
        val currentOrders: List<Order>?,
        val lastCompletedOrders: List<Order>?,
        val Versioning: Versioning?
)