package com.buraq24.customer.webservices

import com.buraq24.utilities.SharedPrefs
import com.buraq24.utilities.constants.LANGUAGE_CHANGED
import com.buraq24.utilities.webservices.BaseRestClient

object RestClient : BaseRestClient() {

    private lateinit var restApi: API

    init {
        create()
    }

    private fun create() {
        val retrofit = createClient()
        restApi = retrofit.create(API::class.java)
    }

    fun recreate(): RestClient {
        create()
        return this
    }

    fun get(): API {
        if (SharedPrefs.get().getBoolean(LANGUAGE_CHANGED, true)) {
            recreate()
            SharedPrefs.get().save(LANGUAGE_CHANGED, false)
        }
        return restApi
    }
}