package com.buraq24.customer.webservices.models.order

data class Payment(
        var payment_id: Int?,
        var payment_type: String?,
        var payment_status: String?,
        var refund_status: String?,
        var transaction_id: String?,
        var refund_id: String?,
        var buraq_percentage: Float?,
        var product_actual_value: String?,
        var product_quantity: Int?,
        var product_weight: String?,
        var order_distance: String?,
        var product_alpha_charge: String?,
        var product_per_quantity_charge: String?,
        var product_per_weight_charge: String?,
        var product_per_distance_charge: String?,
        var initial_charge: String?,
        var admin_charge: String?,
        var bank_charge: String?,
        var final_charge: String?,
        var bottle_charge: String?,
        var bottle_returned_value: Int?,
        var product_per_hr_charge: String?,
        var product_per_sq_mt_charge: String?
)