package com.buraq24.customer.webservices.models.order

data class Rating(
        var order_rating_id: Int,
        var ratings: Int,
        var comments: String,
        var created_by: String,
        var created_at: String
)