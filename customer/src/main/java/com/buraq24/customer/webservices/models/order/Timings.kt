package com.buraq24.customer.webservices.models.order

data class Timings(
        var accepted_at: String?,
        var confirmed_at: String?,
        var started_at: String?,
        var updated_at: String?
)