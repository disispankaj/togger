package com.buraq24.customer.webservices.models.etoken

data class ETokensModel(
        var org: Org?,
        var etokens: List<Etoken>?
)