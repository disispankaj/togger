package com.buraq24.customer.webservices

import com.buraq24.customer.ui.callingTokBox.OpenTalkSession
import com.buraq24.customer.webservices.models.*
import com.buraq24.customer.webservices.models.etoken.PurchasedEToken
import com.buraq24.customer.webservices.models.etokens.ETokensModel
import com.buraq24.customer.webservices.models.homeapi.Category
import com.buraq24.customer.webservices.models.homeapi.ServiceDetails
import com.buraq24.customer.webservices.models.order.Order
import com.buraq24.customer.webservices.models.paymentdetails.PaymentDetail
import com.buraq24.customer.webservices.models.nearestroad.RoadPoints
import com.buraq24.utilities.*
import com.buraq24.utilities.chatModel.ChatMessageListing
import com.buraq24.utilities.webservices.models.AppDetail
import com.buraq24.utilities.webservices.models.LoginModel
import com.buraq24.utilities.webservices.models.Service
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*
import java.math.BigInteger

interface API {

    @FormUrlEncoded
    @POST("/user/sendOtp")
    fun sendOtp(@FieldMap map: Map<String, String>): Call<ApiResponse<SendOtp>>

    @FormUrlEncoded
    @POST("/user/verifyOTP")
    fun verifyOtp(@FieldMap map: Map<String, String>): Call<ApiResponse<LoginModel>>

    @FormUrlEncoded
    @POST("/user/addName")
    fun addName(@Field("name") name: String,@Field("email") email: String,@Field("date_of_birth") dob: String): Call<ApiResponse<AppDetail>>

    @FormUrlEncoded
    @POST("/user/service/homeApi")
    fun homeApi(@FieldMap map: Map<String, String>): Call<ApiResponse<ServiceDetails>>

    @Multipart
    @POST("/user/service/Request")
    fun requestService(@PartMap map: Map<String, @JvmSuppressWildcards RequestBody>, @Part imageArray: List<MultipartBody.Part>): Call<ApiResponse<Order>>

    @FormUrlEncoded
    @POST("/user/service/Cancel")
    fun cancelService(@FieldMap map: Map<String, String>): Call<ApiResponse<Any>>

    @POST("/user/service/Ongoing")
    fun onGoingOrder(): Call<ApiResponse<List<Order>>>

    @GET("directions/json?sensor=false&mode=driving&alternatives=true&units=metric&key=AIzaSyCKx1fqDWY8Kdcqx5wknwjOlzB9Bej4U6Q")
    fun getPolYLine(@Query("origin") origin: String,
                    @Query("destination") destination: String,
                    @Query("language") language: String?): Call<ResponseBody>

    @FormUrlEncoded
    @POST("/user/service/ETokens")
    fun eTokens(@FieldMap map: Map<String, String>): Call<ApiResponse<ETokensModel>>

    @FormUrlEncoded
    @POST("/user/service/eToken/buy")
    fun buyETokens(@Field("organisation_coupon_id") coupon_id: String?): Call<ApiResponse<Any>>

    @FormUrlEncoded
    @POST("/user/other/payment/details")
    fun paymentDetail(@Field("category_brand_id") categoryBrandId: String?,
                      @Field("category_brand_product_id") categoryBrandProductId: String?)
            : Call<ApiResponse<PaymentDetail>>

    @FormUrlEncoded
    @POST("/user/service/Rate")
    fun rateService(@Field("ratings") rating: Int?,
                    @Field("order_id") orderId: BigInteger?,
                    @Field("comments") comments: String?): Call<Any>

    @FormUrlEncoded
    @POST("/common/updateData")
    fun updateData(@FieldMap map: Map<String, String>): Call<ApiResponse<LoginModel>>

    @FormUrlEncoded
    @POST("/user/other/order/history")
    fun getBookingsHistory(@Field("skip") skip: Int,
                           @Field("take") take: Int, @Field("type") type: Int): Call<ApiResponse<ArrayList<Order?>>>

    @FormUrlEncoded
    @POST("/user/other/order/details")
    fun getOrderDetails(@Field("order_id") orderId: Long): Call<ApiResponse<Order>>

    @POST("/common/eContacts")
    fun eContactsList(): Call<ApiResponse<ArrayList<EContact>>>

    @FormUrlEncoded
    @POST("/common/contactus")
    fun sendMessage(@Field("message") message: String): Call<Any>

    @Multipart
    @POST("/user/profileUpdate")
    fun updateProfile(@PartMap map: HashMap<String, RequestBody>): Call<ApiResponse<AppDetail>>

    @FormUrlEncoded
    @POST("/common/settingUpdate")
    fun updateSettings(@Field("language_id") languageId: String?,
                       @Field("notifications") notificationsFlag: String?): Call<ApiResponse<Any>>

    @POST("/common/support/list")
    fun supportList(): Call<ApiResponse<ArrayList<Service>>>

    @POST("/common/logout")
    fun logout(): Call<Any>

    @FormUrlEncoded
    @POST("/common/serviceBrands")
    fun serviceBrands(@Field("category_id") categoryId: String?): Call<ApiResponse<ArrayList<Category>>>

    @FormUrlEncoded
    @POST("/user/water/companies/list")
    fun companiesList(@FieldMap map: HashMap<String, String>?): Call<ApiResponse<ArrayList<Company>>>

    @FormUrlEncoded
    @POST("/user/water/etokens/list")
    fun eTokensList(@FieldMap map: HashMap<String, String>?): Call<ApiResponse<com.buraq24.customer.webservices.models.etoken.ETokensModel>>

    @FormUrlEncoded
    @POST("/user/water/etoken/purchase")
    fun purchaseEToken(@FieldMap map: HashMap<String, String>?): Call<ApiResponse<Any>>

    @FormUrlEncoded
    @POST("/user/water/purchases/list")
    fun purchaseETokenList(@FieldMap map: HashMap<String, String>?): Call<ApiResponse<ArrayList<PurchasedEToken>>>

    @GET("https://roads.googleapis.com/v1/nearestRoads?key=AIzaSyCKx1fqDWY8Kdcqx5wknwjOlzB9Bej4U6Q")
    fun getRoadPoints(@Query("points") points: String): Call<RoadPoints>

    @FormUrlEncoded
    @POST("/user/water/confirm/order")
    fun confirmETokenOrder(@Field("order_id") orderId: Long,
                           @Field("status") status: String): Call<ApiResponse<Any>>


    @GET("/user/other/getChat")
    fun getChatMessages(@Header(AUTHORISATION) authorization: String?,
                        @Query(MESSAGE_ID) messageId: String,
                        @Query(RECEIVER_ID) receiverId: String,
                        @Query(LIMIT) limit: Int?,
                        @Query(SKIP) skip: Int?,
                        @Query(MESSAGE_ORDER) messageOrder: String): Call<ApiResponse<ArrayList<ChatMessageListing>>>

    @POST("/user/other/chatlogs")
    @FormUrlEncoded
    fun getChatLogs(@Header(AUTHORISATION) authorization: String?,
                    @Field(LIMIT) limit: Int?,
                    @Field(SKIP) skip: Int?): Call<ApiResponse<ArrayList<ChatMessageListing>>>


    @Multipart
    @POST("/user/other/mediafileUpload")
    fun uploadImageChat(@PartMap map: HashMap<String, @JvmSuppressWildcards RequestBody>): Call<ApiResponse<ChatMessageListing>>


    @POST("/user/other/makeCall")
    @FormUrlEncoded
    fun getSessionIdForOpenTalk(@Header(AUTHORISATION) authorization: String?,
                    @Field("receiver_id") limit: String): Call<ApiResponse<OpenTalkSession>>

    @FormUrlEncoded
    @POST("/user/other/generateToken")
    fun addCreditCard(@FieldMap map: HashMap<String, Any>): Call<ApiResponse<CardModel>>

    @GET("/user/other/getPaymentCard")
    fun getCreditCardList(): Call<ApiResponse<ArrayList<CardModel>>>

    @FormUrlEncoded
    @POST("/user/other/defaultCard")
    fun setDefaultCard(@FieldMap map: HashMap<String, Any>): Call<ApiResponse<CardModel>>


    @POST("/user/other/coupons")
    fun getCounpons(): Call<ApiResponse<ArrayList<CouponModel>>>

    @FormUrlEncoded
    @POST("/user/other/coupon/buy")
    fun buyCoupon(@FieldMap map: HashMap<String, Any>): Call<ApiResponse<CouponModel>>
}