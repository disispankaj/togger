package com.buraq24.customer.ui.home.orderdetails.heavyloads

import android.Manifest
import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.transition.Explode
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.RotateAnimation
import com.buraq24.customer.R
import com.buraq24.customer.ui.home.HomeActivity
import com.buraq24.customer.ui.home.ScheduleFragment
import com.buraq24.customer.ui.home.comfirmbooking.ConfirmBookingFragment
import com.buraq24.customer.utils.ImageUtils
import com.buraq24.customer.utils.PermissionUtils
import com.buraq24.customer.webservices.models.ServiceRequestModel
import com.buraq24.utilities.CategoryId
import com.buraq24.showSnack
import kotlinx.android.synthetic.main.fragment_enter_order_details2.*
import permissions.dispatcher.*


@RuntimePermissions
class EnterOrderDetailsFragment : Fragment(), AddImageAdapter.AddImageCallback {


    private val imageList = ArrayList<String>()

    private var adapter: AddImageAdapter? = null

    private var serviceRequest: ServiceRequestModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            enterTransition = Explode()
            exitTransition = Explode()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_enter_order_details2, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        serviceRequest = (activity as HomeActivity).serviceRequestModel
        rvCompanies.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        adapter = AddImageAdapter(imageList)
        adapter?.settingCallback(this)
        rvCompanies.adapter = adapter
        if (serviceRequest?.category_id == CategoryId.MINERAL_WATER) {
            tvNext.text = getString(R.string.order_now)
        } else {
            tvNext.text = getString(R.string.book_now)
        }
        setListeners()
    }

    private fun setListeners() {

        tvNext.setOnClickListener {
            setRequestDetails()
            serviceRequest?.future = ServiceRequestModel.BOOK_NOW
            fragmentManager?.beginTransaction()?.replace(R.id.container, ConfirmBookingFragment())?.addToBackStack("backstack")?.commit()
        }

        tvSchedule.setOnClickListener {
            setRequestDetails()
            serviceRequest?.future = ServiceRequestModel.SCHEDULE
            fragmentManager?.beginTransaction()?.replace(R.id.container, ScheduleFragment())?.addToBackStack("backstack")?.commit()
        }

        group?.visibility=View.GONE

        tv_add_more.setOnClickListener {

            group?.clearAnimation()

            group?.animate()?.translationY(group.height.toFloat())
                    ?.alpha(if (group?.visibility == View.VISIBLE) 0.0f else 1.0f)
                    ?.setDuration(if (group?.visibility == View.VISIBLE) 600 else 200)
                    ?.setListener(object : AnimatorListenerAdapter() {
                        override fun onAnimationEnd(animation: Animator) {
                            super.onAnimationEnd(animation)

                            group.visibility = if (group?.visibility == View.VISIBLE) View.GONE else View.VISIBLE
                            if (group?.visibility == View.VISIBLE) animateExpand() else animateCollapse()
                        }
                    })

        }
    }


    private fun animateExpand() {
        val rotate = RotateAnimation(360f, 180f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f)
        rotate.duration = 300
        rotate.fillAfter = true
        imageView2.animation = rotate
    }

    private fun animateCollapse() {
        val rotate = RotateAnimation(180f, 360f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f)
        rotate.duration = 300
        rotate.fillAfter = true
        imageView2.animation = rotate
    }

    private fun setRequestDetails() {
        serviceRequest?.material_details = etTypeOfMaterial.text.toString()
        try {
            serviceRequest?.product_weight = etWeight.text.toString().toInt()
        } catch (e: Exception) {
            serviceRequest?.product_weight = 0
        }
        serviceRequest?.details = etAdditionalInfo.text.toString()
        serviceRequest?.pickup_person_name = etDeliverpersonName.text.toString()
        serviceRequest?.pickup_person_phone = etPhone.text.toString()
        serviceRequest?.invoice_number = etInvoice.text.toString()
        serviceRequest?.delivery_person_name = etDeliverPerson.text.toString()
        serviceRequest?.images = imageList

        serviceRequest?.recieverName=etDeliverPerson.text.toString()
        serviceRequest?.recieverPhone=etPhone.text.toString()
        serviceRequest?.reciptDetail=etInvoice.text.toString()
    }


    @NeedsPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    fun getStorage() {
        if (imageList.size < 2) {
            ImageUtils.displayImagePicker(this)
        } else {
            rootView.showSnack(getString(R.string.max_images_validation_msg))
        }
    }

    @OnShowRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    fun showLocationRationale(request: PermissionRequest) {
        activity?.let { PermissionUtils.showRationalDialog(it, R.string.permission_required_to_select_image, request) }
    }

    @OnNeverAskAgain(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    fun onNeverAskAgainRationale() {
        activity?.let {
            PermissionUtils.showAppSettingsDialog(it,
                    R.string.permission_required_to_select_image)
        }
    }

    @SuppressLint("NeedOnRequestPermissionsResult")
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        onRequestPermissionsResult(requestCode, grantResults)
    }

    private var imagePath: String? = ""

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                ImageUtils.REQ_CODE_CAMERA_PICTURE -> {
                    val file = ImageUtils.getFile(activity)
                    imagePath = file.absolutePath
                    addImage(imagePath)
                }

                ImageUtils.REQ_CODE_GALLERY_PICTURE -> {
                    data?.let {
                        val file = ImageUtils.getImagePathFromGallery(activity, data.data)
//                        ivProfilePic.setRoundProfilePic(file)
                        imagePath = file.absolutePath
                        addImage(imagePath)
                    }
                }
            }
        }
    }

    private fun addImage(imagePath: String?) {
        imageList.add(imagePath.toString())
        adapter?.notifyDataSetChanged()
    }


    override fun addImageHeader() {
        getStorageWithPermissionCheck()
    }
}
