package com.buraq24.customer.ui.home.deliverystarts


import android.animation.ValueAnimator
import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.SystemClock
import android.support.v4.app.FragmentManager
import android.support.v4.content.ContextCompat
import android.view.*
import android.view.animation.LinearInterpolator
import android.widget.*
import com.buraq24.customer.R
import com.buraq24.Utils
import com.buraq24.customer.ui.callingTokBox.AudioChatActivity
import com.buraq24.customer.ui.chatModule.RQ_CODE_CHAT
import com.buraq24.customer.ui.chatModule.chatMessage.ChatActivity
import com.buraq24.setRoundProfileUrl
import com.buraq24.showSWWerror
import com.buraq24.showSnack
import com.buraq24.customer.ui.home.HomeActivity
import com.buraq24.customer.ui.home.invoice.InvoiceFragment
import com.buraq24.customer.ui.home.services.ServicesFragment
import com.buraq24.customer.utils.AppUtils
import com.buraq24.customer.webservices.ApiResponse
import com.buraq24.customer.webservices.models.PolylineModel
import com.buraq24.customer.webservices.models.ServiceRequestModel
import com.buraq24.customer.webservices.models.TrackingModel
import com.buraq24.customer.webservices.models.nearestroad.RoadItem
import com.buraq24.customer.webservices.models.order.Order
import com.buraq24.utilities.*
import com.buraq24.utilities.constants.ACCESS_TOKEN
import com.buraq24.utilities.constants.PREF_MAP_VIEW
import com.buraq24.utilities.constants.SUCCESS_CODE
import com.buraq24.utilities.webservices.BASE_URL_LOAD_IMAGE
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.*
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import io.socket.client.Ack
import io.socket.client.Socket
import io.socket.emitter.Emitter
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.content_home.*
import kotlinx.android.synthetic.main.fragment_delivery_starts.*
import org.json.JSONArray
import org.json.JSONObject
import java.util.*


class DeliveryStartsFragment : BaseFragment(), DeliveryStartsContract.View {

    private val presenter = DeliveryStartsPresenter()

    private var line: Polyline? = null

    private var mMap: GoogleMap? = null

    private var order: Order? = null

    private var pathArray = ArrayList<String>()

    private var list = ArrayList<LatLng>()

    private var sourceLatLong: LatLng? = null

    private var destLong: LatLng? = null

    /* Checks if all events are idle for the time */
    private var checkIdleTimer = Timer()

    private var isPaused = false

    private var isHeavyLoadsVehicle = false

    private var mapType: Int? = null

    var valueAnimatorMove: ValueAnimator? = null

    private val ratingDrawables = listOf(R.drawable.ic_1,
            R.drawable.ic_2,
            R.drawable.ic_3,
            R.drawable.ic_4,
            R.drawable.ic_5)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_delivery_starts, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.attachView(this)
        ivSupport?.isEnabled = false
        presenter.onGoingOrderApi()
        setupMap()
        setListeners()
    }


    override fun onResume() {
        super.onResume()
        isPaused = false
        if (AppSocket.get().isConnected) {
            restartCheckIdleTimer(0)
        }
        AppSocket.get().on(Events.ORDER_EVENT, orderEventListener)
        AppSocket.get().on(Socket.EVENT_CONNECT, onSocketConnected)
        AppSocket.get().on(Socket.EVENT_DISCONNECT, onSocketDisconnected)
    }

    override fun onPause() {
        super.onPause()
        isPaused = true
        checkIdleTimer.cancel()
        AppSocket.get().off(Events.ORDER_EVENT, orderEventListener)
        AppSocket.get().off(Socket.EVENT_CONNECT, onSocketConnected)
        AppSocket.get().off(Socket.EVENT_DISCONNECT, onSocketDisconnected)
    }

    override fun onDestroyView() {
        dialog?.cancel()
        AppSocket.get().off(Events.ORDER_EVENT)
        AppSocket.get().off(Events.COMMON_EVENT)
        presenter.detachView()
        super.onDestroyView()

    }

    override fun onNetworkConnected() {

    }

    override fun onNetworkDisconnected() {

    }

    private val onSocketConnected = Emitter.Listener {
        activity?.runOnUiThread {
            if (!isPaused) {
                restartCheckIdleTimer(0)
            }
        }
    }

    private val onSocketDisconnected = Emitter.Listener {
        checkIdleTimer.cancel()
    }

    private fun setupMap() {
        mMap = (activity as HomeActivity).googleMapHome
        if (map != null) {
            mMap?.clear()
        }
        mapType = SharedPrefs.with(activity).getInt(PREF_MAP_VIEW, GoogleMap.MAP_TYPE_NORMAL)
        tvMapNormal.isSelected = mapType == GoogleMap.MAP_TYPE_NORMAL
        tvSatellite.isSelected = mapType == GoogleMap.MAP_TYPE_HYBRID
        order = Gson().fromJson(arguments?.getString("order"), Order::class.java)
        (activity as HomeActivity).ivMarker.visibility = View.GONE
        sourceLatLong = LatLng(order?.driver?.latitude ?: 0.0, order?.driver?.longitude ?: 0.0)
        when (order?.category_id) {
            CategoryId.GAS, CategoryId.MINERAL_WATER, CategoryId.WATER_TANKER -> {
                isHeavyLoadsVehicle = false
                destLong = LatLng(order?.dropoff_latitude ?: 0.0, order?.dropoff_longitude ?: 0.0)
            }
            CategoryId.FREIGHT, CategoryId.TOW, CategoryId.FREIGHT_2 -> {
                isHeavyLoadsVehicle = true
                if (order?.order_status == OrderStatus.CONFIRMED) {
                    destLong = LatLng(order?.pickup_latitude ?: 0.0, order?.pickup_longitude ?: 0.0)
                } else {
                    destLong = LatLng(order?.dropoff_latitude ?: 0.0, order?.dropoff_longitude
                            ?: 0.0)
                }
            }
        }
        showMarker(sourceLatLong, destLong)
        reFocusMapCamera()
    }

    private fun setListeners() {
        tvMapNormal.setOnClickListener {
            tvMapNormal.isSelected = true
            tvSatellite.isSelected = false
            line?.color = activity?.let { it1 -> ContextCompat.getColor(it1, R.color.text_dark) }
                    ?: Color.BLACK
            mMap?.mapType = GoogleMap.MAP_TYPE_NORMAL
            mapType = GoogleMap.MAP_TYPE_NORMAL
            SharedPrefs.with(activity).save(PREF_MAP_VIEW, GoogleMap.MAP_TYPE_NORMAL)
        }
        tvSatellite.setOnClickListener {
            tvMapNormal.isSelected = false
            tvSatellite.isSelected = true
            mMap?.mapType = GoogleMap.MAP_TYPE_HYBRID
            mapType = GoogleMap.MAP_TYPE_HYBRID
            line?.color = activity?.let { it1 -> ContextCompat.getColor(it1, R.color.white) }
                    ?: Color.WHITE
            SharedPrefs.with(activity).save(PREF_MAP_VIEW, GoogleMap.MAP_TYPE_HYBRID)
        }
        fabMyLocation.setOnClickListener {
            reFocusMapCamera()
        }
        tvCancel.setOnClickListener {
            showCancellationDialog()
        }
        ivProfile.setOnClickListener {
            activity?.drawer_layout?.openDrawer(Gravity.START)
        }

        ivSupport.setOnClickListener {
            activity?.drawer_layout?.openDrawer(Gravity.END)
        }

        fabSatellite.setOnClickListener {
            if (mapType == GoogleMap.MAP_TYPE_NORMAL) {
                mMap?.mapType = GoogleMap.MAP_TYPE_HYBRID
                mapType = GoogleMap.MAP_TYPE_HYBRID
                SharedPrefs.with(activity).save(PREF_MAP_VIEW, GoogleMap.MAP_TYPE_HYBRID)
                fabSatellite.setImageResource(R.drawable.ic_satellite_blue)
            } else {
                mMap?.mapType = GoogleMap.MAP_TYPE_NORMAL
                mapType = GoogleMap.MAP_TYPE_NORMAL
                SharedPrefs.with(activity).save(PREF_MAP_VIEW, GoogleMap.MAP_TYPE_NORMAL)
                fabSatellite.setImageResource(R.drawable.ic_satellite)
            }
        }

        tvCall.setOnClickListener {
            val phone = order?.driver?.phone_number.toString()
            val intent = Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null))
            startActivity(intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP))
        }


        btn_chat.setOnClickListener {
            val intent = Intent(context, ChatActivity::class.java)
            intent.putExtra(RECEIVER_ID, order?.driver?.user_id?.toString())
            intent.putExtra(USER_NAME, order?.driver?.name)
            intent.putExtra(PROFILE_PIC_URL, BASE_URL_LOAD_IMAGE + order?.driver?.profile_pic)
            startActivity(intent)
        }

        btn_call.setOnClickListener {
            var intent = Intent(context, AudioChatActivity::class.java)
            intent.putExtra(RECEIVER_ID, order?.driver?.user_id.toString())
            intent.putExtra(USER_NAME, order?.driver?.name)
            intent.putExtra(PROFILE_PIC_URL, BASE_URL_LOAD_IMAGE + order?.driver?.profile_pic)
            startActivity(intent)
        }

    }

    // Socket Listener for orders events
    private val orderEventListener = Emitter.Listener { args ->
        Logger.e("orderEventListener", args[0].toString())
        restartCheckIdleTimer(10000)
        activity?.runOnUiThread {

            when (JSONObject(args[0].toString()).getString("type")) {
                OrderEventType.SERVICE_COMPLETE -> {
                    val orderJson = JSONObject(args[0].toString()).getJSONArray("order").get(0).toString()
                    val orderModel = Gson().fromJson(orderJson, Order::class.java)
                    if (orderModel.order_id == order?.order_id && !isPaused) {
                        openInvoiceFragment(orderJson)
                    }
                }

                OrderEventType.CURRENT_ORDERS -> {
                    val track = Gson().fromJson(args[0].toString(), TrackingModel::class.java)
                            ?: null
                    if (track?.order_id == order?.order_id && !isPaused) {
                        sourceLatLong = LatLng(track?.latitude ?: 0.0, track?.longitude ?: 0.0)
                        /*         if (track?.polyline != null && track.polyline?.points?.isEmpty() == false) {
                                    drawPolylineFromDriver(track.polyline)
                                     if (sourceLatLong != null) {
                                         animateMarker(sourceLatLong, track.bearing?.toFloat())
                                     }
                                     if (isHeavyLoadsVehicle && track.order_status == OrderStatus.ONGOING) {
                                         destLong = LatLng(order?.dropoff_latitude
                                                 ?: 0.0, order?.dropoff_longitude ?: 0.0)
                                         destMarker?.position = LatLng(destLong?.latitude
                                                 ?: 0.0, destLong?.longitude ?: 0.0)
                                         destMarker?.setIcon((BitmapDescriptorFactory.fromResource(R.drawable.ic_drop_location_mrkr)))
                                     }
                                 } else {*/
                        presenter.getRoadPoints(track)
                        //    }
                        if (track?.order_status == OrderStatus.CONFIRMED) {
                            tvDriverStatus.text = getString(R.string.driver_accepted_request)
                            tvTime.visibility = View.VISIBLE
                        } else if (track?.order_status == OrderStatus.ONGOING) {
                            if (track.my_turn == "1") {
                                tvDriverStatus.text = getString(R.string.driver_is_on_the_way)
                                tvTime.visibility = View.VISIBLE
                            } else {
                                tvDriverStatus.text = getString(R.string.driver_completing_nearby_order)
                                tvTime.visibility = View.INVISIBLE
                            }
                        } else if (track?.order_status == OrderStatus.REACHED) {
                            tvDriverStatus.text = getString(R.string.driver_reached)
                            tvTime.visibility = View.VISIBLE
                        }
                    }
                }

                OrderStatus.DRIVER_CANCELLED -> {
                    openServiceFragment()
                    Toast.makeText(activity, getString(R.string.ongoing_request_cancelled_by_driver), Toast.LENGTH_LONG).show()
                }
            }
        }
    }

    override fun snappedPoints(response: List<RoadItem>?, trackingModel: TrackingModel?) {
        if ((response?.size ?: 0) > 0) {
            sourceLatLong = LatLng(response?.get(0)?.location?.latitude ?: 0.0,
                    response?.get(0)?.location?.longitude ?: 0.0)
            if (isHeavyLoadsVehicle && trackingModel?.order_status == OrderStatus.ONGOING) {
                destLong = LatLng(order?.dropoff_latitude
                        ?: 0.0, order?.dropoff_longitude ?: 0.0)
                destMarker?.position = LatLng(destLong?.latitude
                        ?: 0.0, destLong?.longitude ?: 0.0)
                destMarker?.setIcon((BitmapDescriptorFactory.fromResource(R.drawable.ic_drop_location_mrkr)))
            }
            presenter.drawPolyLine(sourceLat = trackingModel?.latitude
                    ?: 0.0, sourceLong = trackingModel?.longitude ?: 0.0,
                    destLat = destLong?.latitude ?: 0.0, destLong = destLong?.longitude
                    ?: 0.0, language = Locale.US.language)
            if (sourceLatLong != null) {
                animateMarker(sourceLatLong, trackingModel?.bearing?.toFloat())
//                            carMarker?.let { animateMarker(it, sourceLatLong!!, false) }
//                            carMarker?.let {
//                                rotateMarker(it, track.bearing?.toFloat() ?: 0f, startRotation)
//                            }
            }
        }
    }


    override fun onApiSuccess(response: List<Order>?) {
        if (response?.isNotEmpty() == true) {
            tvDriverName?.text = response[0].driver?.name
            if (response[0].driver?.rating_count ?: 0 > 0 && response[0].driver?.rating_avg?.toInt() ?: 0 != 0) {
                val rating = if ((response[0].driver?.rating_avg?.toInt() ?: 0) <= 5) {
                    response[0].driver?.rating_avg?.toInt()
                } else {
                    5
                }
                ivRating.setImageResource(ratingDrawables[rating?.minus(1) ?: 0])
                tvRating.text = " · " + response.get(0).driver?.rating_count.toString()
            } else {
                ivRating.visibility = View.GONE
                tvRating.visibility = View.GONE
            }
            order = response[0]
            ivDriverImage.setRoundProfileUrl(response[0].driver?.profile_pic_url)

            tv_price.text = getString(R.string.currency) + " " + getFormattedPrice(response[0].payment?.final_charge)

            tv_vehicleType.text = response[0].brand?.brand_name

            tv_total_person.text = response[0].brand?.name

            //  textView12.text = response[0].brand?.brand_name + " , " + response[0].brand?.name + " , " + getFormattedPrice(response[0]?.payment?.final_charge) + " " + getString(R.string.currency)
            if (order?.order_status == OrderStatus.CONFIRMED) {
                tvDriverStatus.text = getString(R.string.driver_accepted_request)
                //  textView12.visibility = View.VISIBLE

            } else if (order?.order_status == OrderStatus.ONGOING) {
                if (order?.my_turn == "1") {
                    tvDriverStatus.text = getString(R.string.driver_is_on_the_way)
                } else {
                    tvDriverStatus.text = getString(R.string.driver_completing_nearby_order)
                }
            } else if (order?.order_status == OrderStatus.REACHED) {
                tvDriverStatus.text = getString(R.string.driver_reached)
            }
        } else {
            fragmentManager?.popBackStack("backstack", FragmentManager.POP_BACK_STACK_INCLUSIVE)
            val fragment = ServicesFragment()
            (activity as HomeActivity).serviceRequestModel = ServiceRequestModel()
            (activity as HomeActivity).servicesFragment = fragment
            fragmentManager?.beginTransaction()?.add(R.id.container, fragment)?.addToBackStack("backstack")?.commit()
            Toast.makeText(activity, getString(R.string.order_cancelled), Toast.LENGTH_LONG).show()
        }
    }

    private fun getFormattedPrice(price: String?): String {
        return String.format(Locale.US, "%.2f", price?.toDouble())
    }

    override fun showLoader(isLoading: Boolean) {

    }

    override fun apiFailure() {
        rootView.showSWWerror()
    }

    override fun handleApiError(code: Int?, error: String?) {
        if (code == StatusCode.UNAUTHORIZED) {
            AppUtils.logout(activity as Activity)
        } else {
            rootView.showSnack(error.toString())
        }
    }

    override fun onCancelApiSuccess() {
        openServiceFragment()
    }

    override fun polyLine(jsonRootObject: JSONObject) {
        line?.remove()
        val routeArray = jsonRootObject.getJSONArray("routes")
        if (routeArray.length() == 0) {
            return
        }
        var routes: JSONObject?
        routes = routeArray.getJSONObject(0)
        val overviewPolylines = routes.getJSONObject("overview_polyline")
        val encodedString = overviewPolylines.getString("points")
        pathArray.add(encodedString)
        list = decodePoly(encodedString)
        val listSize = list.size
        sourceLatLong?.let { list.add(0, it) }
        destLong?.let { list.add(listSize + 1, it) }
        line = mMap?.addPolyline(PolylineOptions()
                .addAll(list)
                .width(8f)
                .color(if (mapType == GoogleMap.MAP_TYPE_NORMAL) activity?.let { ContextCompat.getColor(it, R.color.text_dark) }
                        ?: 0 else activity?.let { ContextCompat.getColor(it, R.color.white) } ?: 0)
                .geodesic(true))
        /* To calculate the estimated distance*/
        /*val estimatedDistance = (((routes?.get("legs") as JSONArray).get(0) as JSONObject).get("distance") as JSONObject).get("text") as String*/
        val estimatedTime = (((routes?.get("legs") as JSONArray).get(0) as JSONObject).get("duration") as JSONObject).get("text") as String
        tvTime.text = estimatedTime
    }

    private fun drawPolylineFromDriver(model: PolylineModel?) {
        line?.remove()
        val encodedString = model?.points ?: ""
        pathArray.add(encodedString)
        list = decodePoly(encodedString)
        val listSize = list.size
        sourceLatLong?.let { list.add(0, it) }
        destLong?.let { list.add(listSize + 1, it) }
        line = mMap?.addPolyline(PolylineOptions()
                .addAll(list)
                .width(8f)
                .color(if (mapType == GoogleMap.MAP_TYPE_NORMAL) activity?.let { ContextCompat.getColor(it, R.color.text_dark) }
                        ?: 0 else activity?.let { ContextCompat.getColor(it, R.color.white) }
                        ?: 0)
                .geodesic(true))
        /*         val builder = LatLngBounds.Builder()
                 val arr = ArrayList<Marker?>()
                 arr.add(carMarker)
                 arr.add(destMarker)
                 for (marker in arr) {
                     builder.include(marker?.position)
                 }
                 val bounds = builder.build()
                 val cu = CameraUpdateFactory.newLatLngBounds(bounds,
                         activity?.let { Utils.getScreenWidth(it) } ?: 0,
                         activity?.let { Utils.getScreenWidth(it) }?.minus(Utils.dpToPx(24).toInt())
                                 ?: 0,
                         Utils.dpToPx(56).toInt())
             mMap?.animateCamera(cu)*/

        //  val estimatedDistance = (((routes?.get("legs") as JSONArray).get(0) as JSONObject).get("distance") as JSONObject).get("text") as String
        tvTime.text = model?.timeText

    }

    private fun reFocusMapCamera() {
        val builder = LatLngBounds.Builder()
        val arr = ArrayList<Marker?>()
        arr.add(carMarker)
        arr.add(destMarker)
        for (marker in arr) {
            builder.include(marker?.position)
        }
        val bounds = builder.build()
        val cu = CameraUpdateFactory.newLatLngBounds(bounds,
                activity?.let { Utils.getScreenWidth(it) } ?: 0,
                activity?.let { Utils.getScreenWidth(it) }?.minus(Utils.dpToPx(24).toInt())
                        ?: 0, Utils.dpToPx(56).toInt())
        mMap?.animateCamera(cu)
    }

    private fun decodePoly(encoded: String): ArrayList<LatLng> {

        val poly = java.util.ArrayList<LatLng>()
        var index = 0
        val len = encoded.length
        var lat = 0
        var lng = 0
        while (index < len) {
            var b: Int
            var shift = 0
            var result = 0
            do {
                b = encoded[index++].toInt() - 63
                result = result or (b and 0x1f shl shift)
                shift += 5
            } while (b >= 0x20)
            val dlat = if (result and 1 != 0) (result shr 1).inv() else result shr 1
            lat += dlat
            shift = 0
            result = 0
            do {
                b = encoded[index++].toInt() - 63
                result = result or (b and 0x1f shl shift)
                shift += 5
            } while (b >= 0x20)
            val dlng = if (result and 1 != 0) (result shr 1).inv() else result shr 1
            lng += dlng
            val p = LatLng(lat.toDouble() / 1E5,
                    lng.toDouble() / 1E5)
            poly.add(p)
        }
        return poly
    }

    var carMarker: Marker? = null
    var destMarker: Marker? = null
    private fun showMarker(sourceLatLong: LatLng?, destLong: LatLng?) {
        /*add marker for both source and destination*/
        carMarker = mMap?.addMarker(this.sourceLatLong?.let { MarkerOptions().position(it) })
        when (order?.category_id) {
            CategoryId.GAS -> carMarker?.setIcon((BitmapDescriptorFactory.fromResource(R.drawable.ic_mini_truck_m_gas)))
            CategoryId.MINERAL_WATER -> carMarker?.setIcon((BitmapDescriptorFactory.fromResource(R.drawable.ic_mini_truck_water_m)))
            CategoryId.WATER_TANKER -> carMarker?.setIcon((BitmapDescriptorFactory.fromResource(R.drawable.ic_truck_water_tank_m)))
            CategoryId.FREIGHT, CategoryId.FREIGHT_2 -> carMarker?.setIcon((BitmapDescriptorFactory.fromResource(R.drawable.ic_pin)))
            CategoryId.TOW -> carMarker?.setIcon((BitmapDescriptorFactory.fromResource(R.drawable.ic_mini_truck_water_m)))
        }
        carMarker?.isFlat = true
        carMarker?.setAnchor(0.5f, 0.5f)
        destMarker = mMap?.addMarker(this.destLong?.let { MarkerOptions().position(it) })
        if (isHeavyLoadsVehicle && order?.order_status == OrderStatus.CONFIRMED) {
            destMarker?.setIcon((BitmapDescriptorFactory.fromResource(R.drawable.ic_pickup_location_mrkr)))
        } else {
            destMarker?.setIcon((BitmapDescriptorFactory.fromResource(R.drawable.ic_drop_location_mrkr)))
        }

        this.sourceLatLong?.latitude?.let {
            this.sourceLatLong?.longitude?.let { it1 ->
                this.destLong?.latitude?.let { it2 ->
                    this.destLong?.longitude?.let { it3 ->
                        presenter.drawPolyLine(sourceLat = it, sourceLong = it1,
                                destLat = it2, destLong = it3, language = Locale.US.language)
                    }
                }
            }
        }
    }

    fun animateMarker(latLng: LatLng?, bearing: Float?) {
        if (carMarker != null) {
            val startPosition = carMarker?.position
            val endPosition = LatLng(latLng?.latitude ?: 0.0, latLng?.longitude ?: 0.0)
            val startRotation = carMarker?.rotation
            val latLngInterpolator = LatLngInterpolator.LinearFixed()
            valueAnimatorMove?.cancel()
            valueAnimatorMove = ValueAnimator.ofFloat(0f, 1f)
            valueAnimatorMove?.duration = 10000 // duration 1 second
            valueAnimatorMove?.interpolator = LinearInterpolator()
            valueAnimatorMove?.addUpdateListener(object : ValueAnimator.AnimatorUpdateListener {
                override fun onAnimationUpdate(animation: ValueAnimator) {
                    try {
                        val v = animation.animatedFraction
                        val newPosition = latLngInterpolator.interpolate(v, startPosition
                                ?: LatLng(0.0, 0.0), endPosition)
                        carMarker?.position = newPosition
//                        carMarker?.setRotation(computeRotation(v, startRotation?:0f, currentBearing))
                    } catch (ex: Exception) {
                        ex.printStackTrace()
                    }
                }
            })
            valueAnimatorMove?.start()
            rotateMarker(latLng, bearing)
        }
    }

    var valueAnimatorRotate: ValueAnimator? = null
    private fun rotateMarker(latLng: LatLng?, bearing: Float?) {
        if (carMarker != null) {
            val startPosition = carMarker?.position
            val endPosition = LatLng(latLng?.latitude ?: 0.0, latLng?.longitude ?: 0.0)

            val latLngInterpolator = LatLngInterpolator.LinearFixed()
            valueAnimatorRotate?.cancel()
            val startRotation = carMarker?.rotation
            valueAnimatorRotate = ValueAnimator.ofFloat(0f, 1f)
//            valueAnimatorRotate?.startDelay = 200
            valueAnimatorRotate?.duration = 5000 // duration 1 second
            valueAnimatorRotate?.interpolator = LinearInterpolator()
            valueAnimatorRotate?.addUpdateListener { animation ->
                try {
                    val v = animation.animatedFraction
                    val newPosition = latLngInterpolator.interpolate(v, startPosition
                            ?: LatLng(0.0, 0.0), endPosition)
                    //                        carMarker?.setPosition(newPosition)
                    carMarker?.rotation = computeRotation(v, startRotation ?: 0f, bearing
                            ?: 0f)
                } catch (ex: Exception) {
                    ex.printStackTrace()
                }
            }
            valueAnimatorRotate?.start()

        }
    }

    private fun computeRotation(fraction: Float, start: Float, end: Float): Float {
        val normalizeEnd = end - start // rotate start to 0
        val normalizedEndAbs = (normalizeEnd + 360) % 360

        val direction = (if (normalizedEndAbs > 180) -1 else 1).toFloat() // -1 = anticlockwise, 1 = clockwise
        val rotation: Float
        if (direction > 0) {
            rotation = normalizedEndAbs
        } else {
            rotation = normalizedEndAbs - 360
        }

        val result = fraction * rotation + start
        return (result + 360) % 360
    }

    private interface LatLngInterpolator {
        fun interpolate(fraction: Float, a: LatLng, b: LatLng): LatLng
        class LinearFixed : LatLngInterpolator {
            override fun interpolate(fraction: Float, a: LatLng, b: LatLng): LatLng {
                val lat = (b.latitude - a.latitude) * fraction + a.latitude
                var lngDelta = b.longitude - a.longitude
                // Take the shortest path across the 180th meridian.
                if (Math.abs(lngDelta) > 180) {
                    lngDelta -= Math.signum(lngDelta) * 360
                }
                val lng = lngDelta * fraction + a.longitude
                return LatLng(lat, lng)
            }
        }
    }

    private fun animateMarker(marker: Marker, toPosition: LatLng, hideMarker: Boolean) {
        val handler = Handler()
        val start = SystemClock.uptimeMillis()
        val proj = mMap?.projection
        val startPoint = proj?.toScreenLocation(marker.position)
        val startLatLng = proj?.fromScreenLocation(startPoint)
        val duration: Long = 2000
        val interpolator = LinearInterpolator()

        handler.post(object : Runnable {
            override fun run() {
                val elapsed = SystemClock.uptimeMillis() - start
                val t = interpolator.getInterpolation(elapsed.toFloat() / duration)
                val lng = t * toPosition.longitude + (1 - t) * (startLatLng?.longitude ?: 0.0)
                val lat = t * toPosition.latitude + (1 - t) * (startLatLng?.latitude ?: 0.0)
                marker.position = LatLng(lat, lng)
                if (t < 1.0) {
                    // Post again 16ms later.
                    handler.postDelayed(this, 16)
                } else {
                    marker.isVisible = !hideMarker
                }
            }
        })
    }

    private var startRotation = 0f

    private fun rotateMarker(marker: Marker, toRotation: Float, st: Float) {
        val handler = Handler()
        val start = SystemClock.uptimeMillis()
        val duration = 1555L
        val startRotation = marker.rotation
        val interpolator = LinearInterpolator()
        handler.post(object : Runnable {
            override fun run() {
                val elapsed = SystemClock.uptimeMillis() - start
                val t = interpolator.getInterpolation(elapsed.toFloat() / duration)
                val rot = t * toRotation + (1 - t) * startRotation
                marker.rotation = if (-rot > 180) rot / 2 else rot
                this@DeliveryStartsFragment.startRotation = if (-rot > 180) rot / 2 else rot
                if (t < 1.0) {
                    // Post again 16ms later.
                    handler.postDelayed(this, 16)
                }
            }
        })
    }

    private var dialog: Dialog? = null
    private fun showCancellationDialog() {
        dialog = activity?.let { Dialog(it) }
        dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog?.setCancelable(true)
        dialog?.setCanceledOnTouchOutside(true)
        dialog?.setContentView(R.layout.dialog_cancel_reason)
        dialog?.window?.setBackgroundDrawable(ColorDrawable(android.graphics.Color.TRANSPARENT))

        //     val etMessage = dialog?.findViewById(R.id.etMessage) as EditText
//        val cancelSpinner = dialog?.findViewById<Spinner>(R.id.cancelSpinner)
//
//        val cancelReason = arrayOf("Driver told to cancel", "ETA was too long",
//                                                "Driver's call not reachable", "Booked by mistake")
//
//        if (cancelSpinner != null) {
//            val arrayAdapter = ArrayAdapter(context, android.R.layout.simple_spinner_item, cancelReason)
//            cancelSpinner.adapter = arrayAdapter
//            cancelSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
//                override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
//                    cancelReason[position]
//                    (view as TextView).setTextColor(Color.BLACK)
//                    // Toast.makeText(this@MainActivity, getString(R.string.selected_item) + " " + personNames[position], Toast.LENGTH_SHORT).show()
//                }
//
//                override fun onNothingSelected(parent: AdapterView<*>) {
//                    // Code to perform some action when nothing is selected
//                }
//            }
//        }

//        val cancelReason1 = dialog?.findViewById<TextView>(R.id.reason1)

        val cancelGroup = dialog?.findViewById<RadioGroup>(R.id.cancelGroup)
        val btn1 = dialog?.findViewById<RadioButton>(R.id.btn1)
        val btn2 = dialog?.findViewById<RadioButton>(R.id.btn2)
        val btn3 = dialog?.findViewById<RadioButton>(R.id.btn3)


        val tvSubmit = dialog?.findViewById(R.id.tvSubmit) as TextView
        tvSubmit.setOnClickListener {
            if (cancelGroup?.checkedRadioButtonId.toString().trim().isNotEmpty()) {
                val map = HashMap<String, String>()
                map["order_id"] = order?.order_id.toString()
                map["cancel_reason"] = cancelGroup?.checkedRadioButtonId.toString().trim()
                if (CheckNetworkConnection.isOnline(activity)) {
                    presenter.requestCancelApiCall(map)
                    dialog?.dismiss()
                } else {
                    CheckNetworkConnection.showNetworkError(rootView)
                }
            } else {
                activity?.let { Toast.makeText(it, getString(R.string.cancellation_reason_validation_text), Toast.LENGTH_SHORT).show() }
            }
        }

        dialog?.show()
        dialog?.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
    }

    private fun restartCheckIdleTimer(checkIdleTimeInterval: Long) {
        checkIdleTimer.cancel()
        checkIdleTimer = Timer()
        checkIdleTimer.schedule(object : TimerTask() {
            override fun run() {
                if (CheckNetworkConnection.isOnline(activity)) {
                    checkOrderStatus()
                }
                restartCheckIdleTimer(20000L)
            }
        }, checkIdleTimeInterval)
    }

    private fun checkOrderStatus() {
        val request = JSONObject()
        request.put("type", OrderEventType.CUSTOMER_SINGLE_ORDER)
        request.put("access_token", ACCESS_TOKEN)
        request.put("order_token", order?.order_token)
        AppSocket.get().emit(Events.COMMON_EVENT, request, Ack {
            val response = Gson().fromJson<ApiResponse<Order>>(it[0].toString(),
                    object : TypeToken<ApiResponse<Order>>() {}.type)
            if (response?.statusCode == SUCCESS_CODE) {
                val orderModel = response.result
                val orderJson = JSONObject(it[0].toString()).getString("result")
                activity?.runOnUiThread {
                    when (orderModel?.order_status) {

                        OrderStatus.SERVICE_COMPLETE -> {
                            openInvoiceFragment(orderJson)
                        }

                        OrderStatus.REACHED -> {
                            tvDriverStatus.text = getString(R.string.driver_reached)
                        }

                        OrderStatus.ONGOING -> {
                            tvDriverStatus.text = getString(R.string.driver_is_on_the_way)
                        }

                        OrderStatus.DRIVER_CANCELLED -> {
                            openServiceFragment()
                            Toast.makeText(activity, getString(R.string.ongoing_request_cancelled_by_driver), Toast.LENGTH_LONG).show()
                        }

                        OrderStatus.CUSTOMER_CANCEL -> {
                            openServiceFragment()
                            Toast.makeText(activity, getString(R.string.request_was_cancelled), Toast.LENGTH_LONG).show()
                        }

                        else -> {
                            Logger.e("CUSTOMER_SINGLE_ORDER", "This status not handeled :" + orderModel?.order_status)

                        }
                    }
                }
            } else if (response.statusCode == StatusCode.UNAUTHORIZED) {
                AppUtils.logout(activity)
            }
        })
    }

    private fun openInvoiceFragment(orderJson: String?) {
        val fragment = InvoiceFragment()
        val bundle = Bundle()
        bundle.putString("order", orderJson)
        fragment.arguments = bundle
        fragmentManager?.popBackStack("backstack", FragmentManager.POP_BACK_STACK_INCLUSIVE)
        fragmentManager?.beginTransaction()?.add(R.id.container, fragment)?.addToBackStack("backstack")?.commit()
    }

    private fun openServiceFragment() {
        fragmentManager?.popBackStack("backstack", FragmentManager.POP_BACK_STACK_INCLUSIVE)
        val fragment = ServicesFragment()
        (activity as? HomeActivity)?.servicesFragment = fragment
        fragmentManager?.beginTransaction()?.add(R.id.container, fragment)?.addToBackStack("backstack")?.commit()
    }

}
