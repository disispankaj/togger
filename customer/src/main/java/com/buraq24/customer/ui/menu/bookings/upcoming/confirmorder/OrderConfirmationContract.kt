package com.buraq24.customer.ui.menu.bookings.upcoming.confirmorder

import com.buraq24.utilities.basearc.BasePresenter
import com.buraq24.utilities.basearc.BaseView

class OrderConfirmationContract {

    interface View : BaseView {
        fun onApiSuccess(status: String)
    }

    interface Presenter : BasePresenter<View> {
        fun confirmOrder(orderId: Long, status: String)
    }
}