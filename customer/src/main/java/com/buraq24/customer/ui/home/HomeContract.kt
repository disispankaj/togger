package com.buraq24.customer.ui.home

import com.buraq24.utilities.basearc.BasePresenter
import com.buraq24.utilities.basearc.BaseView
import com.buraq24.utilities.webservices.models.LoginModel
import com.buraq24.utilities.webservices.models.Service
import com.google.android.gms.maps.model.LatLng
import org.json.JSONObject

interface HomeContract {

    interface View: BaseView {
        fun onApiSuccess(login: LoginModel?)
        fun logoutSuccess()
        fun onSupportListApiSuccess(response: List<Service>?)
        fun polyLine(jsonRootObject: JSONObject,sourceLatLng: LatLng?, destLatLng: LatLng?)
    }

    interface Presenter: BasePresenter<View> {
        fun updateDataApi(map: HashMap<String, String>)
        fun drawPolyLine(sourceLatLng: LatLng?, destLatLng: LatLng?, language: String?)
        fun logout()
        fun getSupportList()
    }

}