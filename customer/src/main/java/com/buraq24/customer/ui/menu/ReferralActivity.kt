package com.buraq24.customer.ui.menu

import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_referral.*
import android.content.Intent
import com.buraq24.customer.R
import com.buraq24.utilities.LocaleManager


class ReferralActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_referral)
        tvBack.setOnClickListener {
            onBackPressed()
        }

        tvShare.setOnClickListener {
            shareLink()
        }
    }

    private fun shareLink() {
        val share = Intent(android.content.Intent.ACTION_SEND)
        share.type = "text/plain"
        share.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
        share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        share.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.share) + " " + getString(R.string.app_name))
        share.putExtra(Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=com.buraq24.customer&hl=en_US")
        startActivity(share)
    }

    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(LocaleManager.setLocale(newBase))
    }
}