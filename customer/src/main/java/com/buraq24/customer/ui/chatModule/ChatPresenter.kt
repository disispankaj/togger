package com.buraq24.customer.ui.chatModule


import com.buraq24.customer.webservices.ApiResponse
import com.buraq24.customer.webservices.RestClient
import com.buraq24.getApiError
import com.buraq24.utilities.basearc.BasePresenterImpl
import com.buraq24.utilities.chatModel.ChatMessageListing
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ChatPresenter : BasePresenterImpl<ChatContract.View>(), ChatContract.Presenter {

    override fun getChatLogsApiCall(authorization: String, limit: Int, skip: Int) {
        RestClient.get().getChatLogs(authorization, limit, skip).enqueue(object : Callback<ApiResponse<ArrayList<ChatMessageListing>>> {
            override fun onFailure(call: Call<ApiResponse<ArrayList<ChatMessageListing>>>?, t: Throwable?) {
                getView()?.apiFailure()
            }

            override fun onResponse(call: Call<ApiResponse<ArrayList<ChatMessageListing>>>?, response: Response<ApiResponse<ArrayList<ChatMessageListing>>>?) {
                if (response?.isSuccessful == true) {
                    if (response.body()?.statusCode == 200) {
                        getView()?.chatLogsApiSuccess(response.body()?.result)
                    } else {
                        getView()?.handleApiError(response.body()?.statusCode, response.body()?.msg)
                    }
                } else {
                    val errorModel = getApiError(response?.errorBody()?.string())
                    getView()?.handleApiError(errorModel.statusCode, errorModel.msg)
                }
            }

        })
    }
}