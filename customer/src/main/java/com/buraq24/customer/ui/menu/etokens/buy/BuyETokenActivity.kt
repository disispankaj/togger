package com.buraq24.customer.ui.menu.etokens.buy

import android.app.Activity
import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.*
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.buraq24.customer.R
import com.buraq24.Utils
import com.buraq24.showSWWerror
import com.buraq24.showSnack
import com.buraq24.customer.utils.AppUtils
import com.buraq24.customer.webservices.models.etoken.ETokensModel
import com.buraq24.customer.webservices.models.etoken.Etoken
import com.buraq24.utilities.*
import com.buraq24.utilities.constants.*
import kotlinx.android.synthetic.main.activity_buy_etoken.*
import kotlinx.android.synthetic.main.dialog_etoken_confirm_booking.*
import java.util.*


class BuyETokenActivity : AppCompatActivity(), BuyEtokenContract.View {

    val presenter = BuyETokenPresenter()

    private val eTokensList = ArrayList<Etoken>()

    private lateinit var quantityAdapter: ArrayAdapter<String>

    private var dialogIndeterminate: DialogIndeterminate? = null

    private var eTokensModel: ETokensModel? = null

    private var selectedQuantity: Int? = 1

    companion object {
        var RQ_BUY_ETOKEN = 700
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_buy_etoken)
        presenter.attachView(this)
        setData()
    }

    private fun setData() {
        dialogIndeterminate = DialogIndeterminate(this)
        val arr = Array(10) { i -> (i + 1).toString() }.toMutableList()
        arr.add(getString(R.string.other))
        quantityAdapter = ArrayAdapter(this, R.layout.layout_spinner_capacity, arr)
        quantityAdapter.setDropDownViewResource(R.layout.item_capacity)
        spinnerQuantity.adapter = quantityAdapter

        /*  *//* Restrict the height of the spinner*//*
        try {
            val popup = Spinner::class.java.getDeclaredField("mPopup")
            popup.isAccessible = true
            val popupWindow = popup.get(spinnerQuantity) as android.widget.ListPopupWindow
            popupWindow.height = Utils.dpToPx(240).toInt()
        } catch (e: Exception) {
            e.printStackTrace()
        }*/
        spinnerQuantity.visibility = View.VISIBLE
        etQuantity.visibility = View.GONE
        rvCompanies.layoutManager = LinearLayoutManager(this)
        rvCompanies.adapter = BuyETokensAdapter(eTokensList)
        tvDeliveryDays.text = intent.getStringExtra(DAYS_TO_DELIVER)
        tvBrandName.text = intent.getStringExtra(BRAND_NAME)
        var requestOptions = RequestOptions()
        requestOptions = requestOptions.transforms(RoundedCorners(Utils.pxToDp(this, 6)))
        Glide.with(this).load(intent.getStringExtra(CATEGORY_IMAGE)).apply(requestOptions).into(ivBrand)
        if (CheckNetworkConnection.isOnline(this)) {
            getETokenListApi()
        } else {
            CheckNetworkConnection.showNetworkError(rootView)
        }

        setListeners()
    }

    private fun setListeners() {
        tvBack.setOnClickListener { onBackPressed() }
        spinnerQuantity.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                // Do Nothing
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                if (position < spinnerQuantity?.adapter?.count?.minus(1) ?: 0) {
                    selectedQuantity = parent?.getItemAtPosition(position)?.toString()?.toInt()
                } else {
                    selectedQuantity = 0
                    etQuantity.setText("")
                    etQuantity.visibility = View.VISIBLE
                    spinnerQuantity.visibility = View.GONE
                }
            }
        }
    }

    override fun onDestroy() {
        presenter.detachView()
        super.onDestroy()
    }

    private fun getETokenListApi() {
        val map = HashMap<String, String>()
        map["organisation_id"] = intent?.getIntExtra(ORGANISATION_ID, 0).toString()
        map["category_brand_id"] = intent?.getIntExtra(CATEGORY_BRAND_ID, 0).toString()
        map["skip"] = "0"
        map["take"] = "1000"
        presenter.getETokensApiCall(map)
    }

    override fun onETokenApiSuccess(eTokensModel: ETokensModel?) {
        this.eTokensModel = eTokensModel
        eTokensList.clear()
        eTokensModel?.etokens?.let { eTokensList.addAll(it) }
        rvCompanies?.adapter?.notifyDataSetChanged()
    }

    override fun onETokenBuyApiSuccess() {
        Toast.makeText(this, R.string.etoken_purchased_success_msg, Toast.LENGTH_SHORT).show()
        setResult(Activity.RESULT_OK)
        finish()
    }

    override fun showLoader(isLoading: Boolean) {
        dialogIndeterminate?.show(isLoading)
    }

    override fun apiFailure() {
        rootView?.showSWWerror()
    }

    override fun handleApiError(code: Int?, error: String?) {
        if (code == StatusCode.UNAUTHORIZED) {
            AppUtils.logout(this)
        } else {
            rootView?.showSnack(error ?: "")
        }
    }

    fun showConfirmationDialog(eToken: Etoken?) {
        if (selectedQuantity == 0 && etQuantity?.text?.isEmpty() == true) {
            rootView?.showSnack(R.string.please_enter_quantity)
            return
        }
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(true)
        dialog.setCanceledOnTouchOutside(true)
        dialog.setContentView(R.layout.dialog_etoken_confirm_booking)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.radioGroup.setOnCheckedChangeListener { radioGroup, id ->
            when (id) {
                R.id.rbYes -> {
                    val percentage = eToken?.price?.times(eTokensModel?.org?.buraq_percentage
                            ?: 0)?.div(100)
                    dialog.tvTotal.text = "${getFormattedDecimal(eToken?.price?.plus(percentage
                            ?: 0f))} ${getString(R.string.currency)}"
                }

                R.id.rbNo -> {

                    val percentage = eToken?.price?.times(eTokensModel?.org?.buraq_percentage
                            ?: 0)?.div(100)
                    dialog.tvTotal.text = "${getFormattedDecimal(eToken?.price?.plus(percentage
                            ?: 0f)?.plus(eTokensModel?.org?.bottle_charge?.toFloat()
                            ?: 0f))} ${getString(R.string.currency)}"
                }
            }
        }
        dialog.tvCash?.setOnClickListener {
            buyETokenApi(eToken, PaymentType.CASH, if (dialog.rbYes?.isChecked == true) "1" else "0")
            dialog.dismiss()
        }
        dialog.tvCard?.setOnClickListener {
            //            buyETokenApi(eToken, PaymentType.CARD, if (dialog.rbYes?.isChecked == true) "1" else "0")
//            dialog.dismiss()
            Toast.makeText(this@BuyETokenActivity, R.string.coming_soon, Toast.LENGTH_SHORT).show()
        }
        dialog.show()
        dialog.tvBrandAvailable?.text = "${getString(R.string.do_you_have)} ${intent.getStringExtra(BRAND_NAME)}\n${getString(R.string.bottles_with_you)}"
        val percentage = eToken?.price?.times(eTokensModel?.org?.buraq_percentage ?: 0)?.div(100)
        dialog.tvTotal.text = "${getFormattedDecimal(eToken?.price?.plus(percentage
                ?: 0f))} ${getString(R.string.currency)}"
        dialog.tvBrand?.text = eToken?.categoryBrand?.name
        dialog.tvProduct?.text = "${eToken?.categoryBrandProduct?.name}/ ${getString(R.string.e_token)}"
        dialog.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
    }

    private fun buyETokenApi(eToken: Etoken?, paymentType: String, haveBottles: String) {
        val map = HashMap<String, String>()
        map["organisation_coupon_id"] = eToken?.organisation_coupon_id.toString()
        map["buraq_percentage"] = eTokensModel?.org?.buraq_percentage.toString()
        map["bottle_returned_value"] = haveBottles
        map["bottle_charge"] = eTokensModel?.org?.bottle_charge ?: "0"
        map["quantity"] = selectedQuantity.toString()
        map["payment_type"] = paymentType
        map["price"] = eToken?.price.toString()
        map["eToken_quantity"] = eToken?.quantity.toString()
        map["address"] = intent?.getStringExtra(Constants.ADDRESS) ?: ""
        map["address_latitude"] = intent?.getDoubleExtra(Constants.LATITUDE, 0.0).toString()
        map["address_longitude"] = intent?.getDoubleExtra(Constants.LONGITUDE, 0.0).toString()
        if (CheckNetworkConnection.isOnline(this)) {
            presenter.buyEtokenApiCall(map)
        } else {
            CheckNetworkConnection.showNetworkError(rootView)
        }
    }

    private fun getFormattedDecimal(num: Float?): String? {
        return String.format(Locale.US, "%.2f", num)
    }
}
