package com.buraq24.customer.ui.home.submodels


import android.os.Build
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.transition.Explode
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.buraq24.customer.R
import com.buraq24.customer.ui.home.HomeActivity
import com.buraq24.customer.ui.home.comfirmbooking.ConfirmBookingFragment
import com.buraq24.customer.ui.home.orderdetails.heavyloads.EnterOrderDetailsFragment
import com.buraq24.customer.webservices.models.ServiceRequestModel
import com.buraq24.customer.webservices.models.homeapi.Product
import com.buraq24.utilities.SharedPrefs
import com.buraq24.utilities.constants.SERVICES
import com.buraq24.showSnack
import com.buraq24.utilities.webservices.models.Service
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.fragment_select_vehicle_type.*


class SelectSubModelsFragment : Fragment() {

    private val products: ArrayList<Product?>? = ArrayList()

    private var adapter: SelectSubModelsAdapter? = null

    private lateinit var serviceRequest: ServiceRequestModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        serviceRequest = (activity as HomeActivity).serviceRequestModel
        serviceRequest.category_brand_product_id = -1
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            enterTransition = Explode()
            exitTransition = Explode()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_select_vehicle_type, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        tvTitle.text = getString(R.string.select_sub_model)
        products?.clear()
        products?.addAll(Gson().fromJson(arguments?.getString("submodels", ""), object : TypeToken<List<Product>>() {}.type)
                ?: ArrayList())
        if (products?.isEmpty() == false) {
            val selectedPosition = getSelectedPosition(serviceRequest.category_brand_product_id
                    ?: 0)
            adapter = SelectSubModelsAdapter(products, selectedPosition)
            rvCompanies?.layoutManager = GridLayoutManager(activity, 3, GridLayoutManager.VERTICAL, false)
            rvCompanies?.adapter = adapter
        }
        setListeners()
    }

    private fun setListeners() {
        tvNext.setOnClickListener {
            if (products?.isEmpty() == true) {
                rootView.showSnack(R.string.no_vehicles_available)
                return@setOnClickListener
            }
            serviceRequest.category_brand_product_id = adapter?.getSelectedBrandProductId()
            serviceRequest.productName = adapter?.getSelectedBrandProductName()
            serviceRequest.final_charge = getFinalCharge()
            serviceRequest.images = ArrayList()
            serviceRequest.product_weight = 0
            serviceRequest.material_details = ""
            serviceRequest.details = ""

            if (serviceRequest.category_id == 7) {
                serviceRequest.future = ServiceRequestModel.BOOK_NOW   // for cab booking cat id 7
                fragmentManager?.beginTransaction()?.replace(R.id.container, ConfirmBookingFragment())?.addToBackStack("backstack")?.commit()
            } else {
                val fragment = EnterOrderDetailsFragment()
                fragmentManager?.beginTransaction()?.replace(R.id.container, fragment)?.addToBackStack("backstack")?.commit()
            }

        }
    }

    private fun getSelectedPosition(categoryBrandId: Int): Int {
        for (i in products.orEmpty().indices) {
            if (products?.get(i)?.category_brand_product_id == categoryBrandId) {
                return i
            }
        }
        return 0
    }

    private fun getFinalCharge(): Double? {
        val service = Gson().fromJson<List<Service>>(SharedPrefs.with(activity).getString(SERVICES, ""), object : TypeToken<List<Service>>() {}.type)
        val product = adapter?.getSelectedProduct()
        val price = product?.price_per_quantity?.times(serviceRequest.product_quantity ?: 1)
                ?.plus(product.alpha_price?.toDouble() ?: 0.0)
                ?.plus((product.price_per_distance?.toDouble() ?: 0.0)
                        .times(serviceRequest.order_distance ?: 0f))
        var buraqPercentage = 0f
        service.forEach {
            if (it.category_id == serviceRequest.category_id) {
                buraqPercentage = it.buraq_percentage ?: 0f
            }
        }
        return price?.div(100)?.times(buraqPercentage)?.plus(price)

    }

}
