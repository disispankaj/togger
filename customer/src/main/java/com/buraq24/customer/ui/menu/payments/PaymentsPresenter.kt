package com.buraq24.customer.ui.menu.payments

import com.buraq24.customer.webservices.ApiResponse
import com.buraq24.customer.webservices.RestClient
import com.buraq24.customer.webservices.models.CardModel
import com.buraq24.customer.webservices.models.CouponModel
import com.buraq24.customer.webservices.models.paymentdetails.PaymentDetail
import com.buraq24.utilities.basearc.BasePresenterImpl
import com.buraq24.getApiError
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PaymentsPresenter : BasePresenterImpl<PaymentsContract.View>(), PaymentsContract.Presenter {

    override fun paymentDetail(categoryId: String?, categoryProductId: String?) {
        getView()?.showLoader(true)
        RestClient.get().paymentDetail(categoryId, categoryProductId).enqueue(object : Callback<ApiResponse<PaymentDetail>> {

            override fun onResponse(call: Call<ApiResponse<PaymentDetail>>?, response: Response<ApiResponse<PaymentDetail>>?) {
                getView()?.showLoader(false)
                if (response?.isSuccessful == true) {
                    if (response.body()?.statusCode == 200) {
                        getView()?.onPaymentDetailsApiSuccess(response.body()?.result)
                    } else {
                        getView()?.handleApiError(response.body()?.statusCode, response.body()?.msg)
                    }
                } else {
                    val errorModel = getApiError(response?.errorBody()?.string())
                    getView()?.handleApiError(errorModel.statusCode, errorModel.msg)
                }
            }

            override fun onFailure(call: Call<ApiResponse<PaymentDetail>>?, t: Throwable?) {
                getView()?.showLoader(false)
                getView()?.apiFailure()
            }
        })
    }

    override fun addUserCard(map: HashMap<String, Any>) {
        getView()?.showLoader(true)
        RestClient.get().addCreditCard(map).enqueue(object :
                Callback<ApiResponse<CardModel>> {

            override fun onResponse(call: Call<ApiResponse<CardModel>>?, response: Response<ApiResponse<CardModel>>?) {
                getView()?.showLoader(false)
                if (response?.isSuccessful == true) {
                    if (response.body()?.statusCode == 200) {
                        getView()?.onAddCardSuccess(response.body()?.result)
                    } else {
                        getView()?.handleApiError(response.body()?.statusCode, response.body()?.msg)
                    }
                } else {
                    val errorModel = getApiError(response?.errorBody()?.string())
                    getView()?.handleApiError(errorModel.statusCode, errorModel.msg)
                }
            }

            override fun onFailure(call: Call<ApiResponse<CardModel>>?, t: Throwable?) {
                getView()?.showLoader(false)
                getView()?.apiFailure()
            }
        })
    }

    override fun getCardList() {
        getView()?.showLoader(true)
        RestClient.get().getCreditCardList().enqueue(object :
                Callback<ApiResponse<ArrayList<CardModel>>> {

            override fun onResponse(call: Call<ApiResponse<ArrayList<CardModel>>>?, response: Response<ApiResponse<ArrayList<CardModel>>>?) {
                getView()?.showLoader(false)
                if (response?.isSuccessful == true) {
                    if (response.body()?.statusCode == 200) {
                        getView()?.onGetCardSuccess(response.body()?.result)
                    } else {
                        getView()?.handleApiError(response.body()?.statusCode, response.body()?.msg)
                    }
                } else {
                    val errorModel = getApiError(response?.errorBody()?.string())
                    getView()?.handleApiError(errorModel.statusCode, errorModel.msg)
                }
            }

            override fun onFailure(call: Call<ApiResponse<ArrayList<CardModel>>>?, t: Throwable?) {
                getView()?.showLoader(false)
                getView()?.apiFailure()
            }
        })
    }

    override fun setDefaultCard(map: HashMap<String, Any>) {
        getView()?.showLoader(true)
        RestClient.get().setDefaultCard(map).enqueue(object :
                Callback<ApiResponse<CardModel>> {

            override fun onResponse(call: Call<ApiResponse<CardModel>>?, response: Response<ApiResponse<CardModel>>?) {
                getView()?.showLoader(false)
                if (response?.isSuccessful == true) {
                    if (response.body()?.statusCode == 200) {
                        getView()?.onSetDefaultCard(response.body()?.result)
                    } else {
                        getView()?.handleApiError(response.body()?.statusCode, response.body()?.msg)
                    }
                } else {
                    val errorModel = getApiError(response?.errorBody()?.string())
                    getView()?.handleApiError(errorModel.statusCode, errorModel.msg)
                }
            }

            override fun onFailure(call: Call<ApiResponse<CardModel>>?, t: Throwable?) {
                getView()?.showLoader(false)
                getView()?.apiFailure()
            }
        })
    }

    override fun buyCoupons(map: HashMap<String, Any>) {
        getView()?.showLoader(true)
        RestClient.get().buyCoupon(map).enqueue(object :
                Callback<ApiResponse<CouponModel>> {
            override fun onResponse(call: Call<ApiResponse<CouponModel>>?, response: Response<ApiResponse<CouponModel>>?) {
                getView()?.showLoader(false)
                if (response?.isSuccessful == true) {
                    if (response.body()?.statusCode == 200) {
                        getView()?.onBuyCounponSuccess(response.body()?.result)
                    } else {
                        getView()?.handleApiError(response.body()?.statusCode, response.body()?.msg)
                    }
                } else {
                    val errorModel = getApiError(response?.errorBody()?.string())
                    getView()?.handleApiError(errorModel.statusCode, errorModel.msg)
                }
            }

            override fun onFailure(call: Call<ApiResponse<CouponModel>>?, t: Throwable?) {
                getView()?.showLoader(false)
                getView()?.apiFailure()
            }
        })
    }
}