package com.buraq24.customer.ui.home.rating


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.buraq24.customer.R
import com.buraq24.setRoundProfileUrl
import com.buraq24.showSWWerror
import com.buraq24.showSnack
import com.buraq24.customer.ui.home.HomeActivity
import com.buraq24.customer.ui.home.services.ServicesFragment
import com.buraq24.customer.utils.AppUtils
import com.buraq24.customer.webservices.models.ServiceRequestModel
import com.buraq24.customer.webservices.models.order.Order
import com.buraq24.utilities.*
import com.buraq24.utilities.constants.ORDER
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_rating.*

class RatingFragment : Fragment(), RatingContract.View {

    private var prevSelectedRating: TextView? = null

    private var rating = 0

    private var presenter = RatingPresenter()

    private var order: Order? = null

    private var dialogIndeterminate: DialogIndeterminate? = null

    private val ratingUnselectedDrawables = listOf(R.drawable.ic_1off, R.drawable.ic_2off, R.drawable.ic_3off, R.drawable.ic_4off, R.drawable.ic_5off)

    private val ratingselectedDrawables = listOf(R.drawable.ic_1, R.drawable.ic_2, R.drawable.ic_3, R.drawable.ic_4, R.drawable.ic_5)


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_rating, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.attachView(this)
        dialogIndeterminate = DialogIndeterminate(activity)
        setData()
        setListeners()
    }

    private fun setData() {
        order = Gson().fromJson(arguments?.getString(ORDER), Order::class.java)
        ivDriverImage.setRoundProfileUrl(order?.driver?.profile_pic_url)
        tvDriverName.text = order?.driver?.name
    }

    private fun setListeners() {
        tvRate1.setOnClickListener(ratingsClickListener)
        tvRate2.setOnClickListener(ratingsClickListener)
        tvRate3.setOnClickListener(ratingsClickListener)
        tvRate4.setOnClickListener(ratingsClickListener)
        tvRate5.setOnClickListener(ratingsClickListener)
        tvSubmit.setOnClickListener { checkValidations() }
    }

    private val ratingsClickListener = View.OnClickListener {
        prevSelectedRating?.setCompoundDrawablesWithIntrinsicBounds(0, ratingUnselectedDrawables[prevSelectedRating?.tag.toString().toInt() - 1], 0, 0)
        rating = it.tag.toString().toInt()
        (it as TextView).setCompoundDrawablesWithIntrinsicBounds(0, ratingselectedDrawables[rating - 1], 0, 0)
        prevSelectedRating = it
        tvRate1.isSelected = rating == 1
        tvRate2.isSelected = rating == 2
        tvRate3.isSelected = rating == 3
        tvRate4.isSelected = rating == 4
        tvRate5.isSelected = rating == 5
    }

    private fun checkValidations() {
        presenter.checkValidations(rating, tvAddComment.text.toString().trim())
    }

    override fun onValidationsResult(isSuccess: Boolean?, message: Int?) {
        if (isSuccess == true) {
            rateApiCall()
        } else {
            rootView.showSnack(message ?: 0)
        }
    }

    private fun rateApiCall() {
        if (CheckNetworkConnection.isOnline(activity)) {
            presenter.rateOrder(rating, tvAddComment.text.toString().trim(), order?.order_id)
        } else {
            CheckNetworkConnection.showNetworkError(rootView)
        }
    }


    override fun onApiSuccess() {
        if (order?.payment?.payment_type == PaymentType.E_TOKEN) {
            activity?.finish()
        } else {
            fragmentManager?.popBackStack("backstack", FragmentManager.POP_BACK_STACK_INCLUSIVE)
            val fragment = ServicesFragment()
            (activity as? HomeActivity)?.serviceRequestModel = ServiceRequestModel()
            (activity as? HomeActivity)?.servicesFragment = fragment
            fragmentManager?.beginTransaction()?.add(R.id.container, fragment)?.addToBackStack("backstack")?.commit()
        }
    }

    override fun showLoader(isLoading: Boolean) {
        dialogIndeterminate?.show(isLoading)
    }

    override fun apiFailure() {
        rootView?.showSWWerror()
    }

    override fun handleApiError(code: Int?, error: String?) {
        if (code == StatusCode.UNAUTHORIZED) {
            AppUtils.logout(activity)
        } else {
            rootView?.showSnack(error ?: "")
        }
    }

}
