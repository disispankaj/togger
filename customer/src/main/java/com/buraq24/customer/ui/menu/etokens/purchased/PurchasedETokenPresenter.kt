package com.buraq24.customer.ui.menu.etokens.purchased

import com.buraq24.customer.webservices.ApiResponse
import com.buraq24.customer.webservices.RestClient
import com.buraq24.customer.webservices.models.etoken.PurchasedEToken
import com.buraq24.utilities.basearc.BasePresenterImpl
import com.buraq24.getApiError
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PurchasedETokenPresenter : BasePresenterImpl<PurchasedETokenContract.View>(), PurchasedETokenContract.Presenter {

    override fun purchasedETokenListApi(map: HashMap<String, String>) {
        getView()?.showLoader(true)
        RestClient.get().purchaseETokenList(map).enqueue(object : Callback<ApiResponse<ArrayList<PurchasedEToken>>> {

            override fun onResponse(call: Call<ApiResponse<ArrayList<PurchasedEToken>>>?, response: Response<ApiResponse<ArrayList<PurchasedEToken>>>?) {
                getView()?.showLoader(false)
                if (response?.isSuccessful == true) {
                    if (response.body()?.statusCode == 200) {
                        getView()?.onPurchasedETokensApiSuccess(response.body()?.result)
                    } else {
                        getView()?.handleApiError(response.body()?.statusCode, response.body()?.msg)
                    }
                } else {
                    val errorModel = getApiError(response?.errorBody()?.string())
                    getView()?.handleApiError(errorModel.statusCode, errorModel.msg)
                }
            }

            override fun onFailure(call: Call<ApiResponse<ArrayList<PurchasedEToken>>>?, t: Throwable?) {
                getView()?.showLoader(false)
                getView()?.apiFailure()
            }
        })
    }

}