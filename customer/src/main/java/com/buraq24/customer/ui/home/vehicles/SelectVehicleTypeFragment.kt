package com.buraq24.customer.ui.home.vehicles


import android.os.Build
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.transition.Explode
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast

import com.buraq24.customer.R
import com.buraq24.customer.ui.home.HomeActivity
import com.buraq24.customer.ui.home.comfirmbooking.ConfirmBookingFragment
import com.buraq24.customer.ui.home.submodels.SelectSubModelsFragment
import com.buraq24.customer.webservices.models.ServiceRequestModel
import com.buraq24.customer.webservices.models.homeapi.Category
import com.buraq24.customer.webservices.models.homeapi.Product
import com.buraq24.utilities.SharedPrefs
import com.buraq24.utilities.constants.SERVICES
import com.buraq24.showSnack
import com.buraq24.utilities.DateUtils
import com.buraq24.utilities.webservices.models.Service
import com.google.android.gms.maps.GoogleMap
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.fragment_login.*
import kotlinx.android.synthetic.main.fragment_select_vehicle_type.*
import kotlinx.android.synthetic.main.fragment_select_vehicle_type.rootView

class SelectVehicleTypeFragment : Fragment() {

    private var categoriesList: ArrayList<Category>? = ArrayList()

    private lateinit var serviceRequest: ServiceRequestModel

    private var adapter: SelectVehicleTypeAdapter? = null

    private var map: GoogleMap? = null
    var timeDurationList = arrayOf("15","30","45","60")
    var selectedTimeDuration = timeDurationList[0]

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        serviceRequest = (activity as HomeActivity).serviceRequestModel
        serviceRequest.category_brand_id = -1
        serviceRequest.category_brand_product_id = -1
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            enterTransition = Explode()
            exitTransition = Explode()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_select_vehicle_type, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        rvCompanies.visibility = View.VISIBLE

        map = (activity as HomeActivity).googleMapHome

        categoriesList?.clear()



        val service = Gson().fromJson<List<Service>>(SharedPrefs.with(activity).getString(SERVICES, ""), object : TypeToken<List<Service>>() {}.type)?.filter {
            it.category_id == 7
        }


        categoriesList?.addAll((activity as HomeActivity).serviceDetails?.categories as? ArrayList
                ?: ArrayList())
        if (categoriesList?.isEmpty() == false) {
            val selectedPosition = getSelectedPosition(serviceRequest.category_brand_id ?: 0)
            rvCompanies?.layoutManager = LinearLayoutManager(activity,  RecyclerView.HORIZONTAL,false)
            adapter = SelectVehicleTypeAdapter(categoriesList, selectedPosition)
            rvCompanies?.adapter = adapter
        }

        this.map?.uiSettings.let {
            it?.isZoomGesturesEnabled = true
            it?.setAllGesturesEnabled(true)
            it?.isScrollGesturesEnabled=true

        }

        setListeners()
        setSpinner()
        rvCompanies.visibility = View.GONE
    }

    override fun onDestroyView() {
        super.onDestroyView()
        if (categoriesList?.isNotEmpty() == true) {
            categoriesList?.get(adapter?.getSelectedPosition() ?: 0)?.isSelected = false
        }
    }

    private fun setListeners() {
        tvNext.setOnClickListener {
            if (categoriesList?.isEmpty() == true) {
                rootView.showSnack(R.string.no_vehicles_available)
                return@setOnClickListener
            }

            serviceRequest.category_brand_id = adapter?.getSelectedCategoryBrandId()
            serviceRequest.brandName = adapter?.getSelectedCategoryBrandName()

            if (serviceRequest.category_id == 7) {

                adapter?.getSelectedPosition().let {
                    if (categoriesList?.get(it!!)?.products?.isNotEmpty() == true) {
                        serviceRequest.category_brand_product_id = categoriesList?.get(it!!)?.products!![0].category_brand_product_id
                        serviceRequest.productName = categoriesList?.get(it!!)?.products!![0].name
                        serviceRequest.final_charge = getFinalCharge(categoriesList?.get(it!!)?.products!![0])
                    }
                }


                serviceRequest.images = ArrayList()
                serviceRequest.product_weight = 0
                serviceRequest.material_details = ""
                serviceRequest.details = ""

                serviceRequest.future = ServiceRequestModel.BOOK_NOW
                serviceRequest.order_mins = selectedTimeDuration
                fragmentManager?.beginTransaction()?.replace(R.id.container, ConfirmBookingFragment())?.addToBackStack("backstack")?.commit()
            } else {
                val fragment = SelectSubModelsFragment()
                val bundle = Bundle()
                bundle.putString("submodels", Gson().toJson(adapter?.getSelectedCategoryProducts()))
                fragment.arguments = bundle
                fragmentManager?.beginTransaction()?.replace(R.id.container, fragment)?.addToBackStack("backstack")?.commit()
            }


        }

        ivSchdule.setOnClickListener {
            Toast.makeText(activity,"coming soon",Toast.LENGTH_SHORT).show()
        }


    }

    private fun getFinalCharge(product: Product?): Double? {
        val service = Gson().fromJson<List<Service>>(SharedPrefs.with(activity).getString(SERVICES, ""), object : TypeToken<List<Service>>() {}.type)
        val price = product?.price_per_quantity?.times(serviceRequest.product_quantity ?: 1)
                ?.plus(product.alpha_price?.toDouble() ?: 0.0)
                ?.plus((product.price_per_distance?.toDouble() ?: 0.0)
                        .times(serviceRequest.order_distance ?: 0f))
        var buraqPercentage = 0f
        service.forEach {
            if (it.category_id == serviceRequest.category_id) {
                buraqPercentage = it.buraq_percentage ?: 0f
            }
        }
        return 25.0//price?.div(100)?.times(buraqPercentage)?.plus(price)

    }


    private fun getSelectedPosition(categoryBrandId: Int): Int {
        for (i in categoriesList.orEmpty().indices) {
            if (categoriesList?.get(i)?.category_brand_id == categoryBrandId) {
                return i
            }
        }
        return 0
    }

    private fun setSpinner(){
        tvBookingTime.text = DateUtils.getCurrentTimeFormat()
        val timeDurationAdatper =  ArrayAdapter(activity,android.R.layout.simple_spinner_item,timeDurationList);
        timeDurationAdatper.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnTimeDuration.adapter = timeDurationAdatper
        spnTimeDuration.setSelection(0)
        spnTimeDuration.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                selectedTimeDuration = timeDurationList[position]
            }

        }
    }
}
