package com.buraq24.customer.ui.signup.login

import com.buraq24.customer.webservices.models.SendOtp
import com.buraq24.utilities.basearc.BasePresenter
import com.buraq24.utilities.basearc.BaseView

class LoginContract{

    interface View: BaseView{
        fun onApiSuccess(response: SendOtp?, map: Map<String, String>)
    }

    interface Presenter: BasePresenter<View>{
        fun sendOtpApiCall(map: Map<String, String>)
    }

}