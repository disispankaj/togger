package com.buraq24.customer.ui.menu.etokens.buy

import com.buraq24.customer.webservices.ApiResponse
import com.buraq24.customer.webservices.RestClient
import com.buraq24.customer.webservices.models.etoken.ETokensModel
import com.buraq24.utilities.basearc.BasePresenterImpl
import com.buraq24.getApiError
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class BuyETokenPresenter : BasePresenterImpl<BuyEtokenContract.View>(), BuyEtokenContract.Presenter {

    override fun buyEtokenApiCall(map: HashMap<String, String>?) {
        getView()?.showLoader(true)
        RestClient.get().purchaseEToken(map).enqueue(object : Callback<ApiResponse<Any>> {

            override fun onResponse(call: Call<ApiResponse<Any>>?, response: Response<ApiResponse<Any>>?) {
                getView()?.showLoader(false)
                if (response?.isSuccessful == true) {
                    if (response.body()?.statusCode == 200) {
                        getView()?.onETokenBuyApiSuccess()
                    } else {
                        getView()?.handleApiError(response.body()?.statusCode, response.body()?.msg)
                    }
                } else {
                    val errorModel = getApiError(response?.errorBody()?.string())
                    getView()?.handleApiError(errorModel.statusCode, errorModel.msg)
                }
            }

            override fun onFailure(call: Call<ApiResponse<Any>>?, t: Throwable?) {
                getView()?.showLoader(false)
                getView()?.apiFailure()
            }
        })
    }

    override fun getETokensApiCall(map: HashMap<String, String>?) {
        getView()?.showLoader(true)
        RestClient.get().eTokensList(map).enqueue(object : Callback<ApiResponse<ETokensModel>> {

            override fun onResponse(call: Call<ApiResponse<ETokensModel>>?, response: Response<ApiResponse<ETokensModel>>?) {
                getView()?.showLoader(false)
                if (response?.isSuccessful == true) {
                    if (response.body()?.statusCode == 200) {
                        getView()?.onETokenApiSuccess(response.body()?.result)
                    } else {
                        getView()?.handleApiError(response.body()?.statusCode, response.body()?.msg)
                    }
                } else {
                    val errorModel = getApiError(response?.errorBody()?.string())
                    getView()?.handleApiError(errorModel.statusCode, errorModel.msg)
                }
            }

            override fun onFailure(call: Call<ApiResponse<ETokensModel>>?, t: Throwable?) {
                getView()?.showLoader(false)
                getView()?.apiFailure()
            }
        })
    }
}