package com.buraq24.customer.ui.signup.login

import com.buraq24.customer.webservices.ApiResponse
import com.buraq24.customer.webservices.RestClient
import com.buraq24.customer.webservices.models.SendOtp
import com.buraq24.utilities.basearc.BasePresenterImpl
import com.buraq24.utilities.constants.SUCCESS_CODE
import com.buraq24.getApiError
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginPresenter: BasePresenterImpl<LoginContract.View>(), LoginContract.Presenter {

    override fun sendOtpApiCall(map: Map<String, String>) {
        getView()?.showLoader(true)
        RestClient.get().sendOtp(map).enqueue(object : Callback<ApiResponse<SendOtp>>{

            override fun onResponse(call: Call<ApiResponse<SendOtp>>?, response: Response<ApiResponse<SendOtp>>?) {
                getView()?.showLoader(false)
                if (response?.isSuccessful == true){
                    if (response.body()?.statusCode == SUCCESS_CODE) {
                        getView()?.onApiSuccess(response.body()?.result, map)
                    }else{
                        getView()?.handleApiError(response.body()?.statusCode, response.body()?.msg)
                    }
                }else{
                    val errorModel = getApiError(response?.errorBody()?.string())
                    getView()?.handleApiError(errorModel.statusCode, errorModel.msg)
                }
            }

            override fun onFailure(call: Call<ApiResponse<SendOtp>>?, t: Throwable?) {
                getView()?.showLoader(false)
                getView()?.apiFailure()
            }
        })
    }
}