package com.buraq24.customer.ui.home.rating

import android.support.annotation.IdRes
import android.support.annotation.StringRes
import com.buraq24.utilities.basearc.BasePresenter
import com.buraq24.utilities.basearc.BaseView
import com.google.android.gms.maps.model.LatLng
import java.math.BigInteger

class RatingContract{

    interface View: BaseView{
        fun onApiSuccess()
        fun onValidationsResult(isSuccess: Boolean?, @StringRes message: Int?)
    }

    interface Presenter: BasePresenter<View>{
        fun rateOrder(rating: Int?,comments: String?, orderId: BigInteger?)
        fun checkValidations(rating: Int?,comments: String?)
    }
}