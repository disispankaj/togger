package com.buraq24.customer.ui.menu.etokens.purchased

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.buraq24.customer.R
import com.buraq24.customer.webservices.models.etoken.PurchasedEToken
import com.buraq24.Utils
import kotlinx.android.synthetic.main.item_purchased_etokens.view.*
import java.util.*

class PurchasedETokensAdapter(private val purchasedETokensList: ArrayList<PurchasedEToken>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_purchased_etokens, parent, false))
    }

    override fun getItemCount(): Int {
        return purchasedETokensList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ViewHolder).onBind(purchasedETokensList.get(position))
    }

    inner class ViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {
        fun onBind(eToken: PurchasedEToken?) = with(itemView) {
            var requestOptions = RequestOptions()
            requestOptions = requestOptions.transforms(RoundedCorners(context?.let { Utils.pxToDp(it, 6) }
                    ?: 6))
            context?.let { Glide.with(it).load(eToken?.categoryBrand?.image_url).apply(requestOptions).into(ivBrand) }
            tvBrandName?.text = eToken?.categoryBrand?.name
            tvETokenInfo.text = "${eToken?.quantity} ${context?.getString(R.string.e_tokens)}\n${eToken?.categoryBrandProduct?.name}/${context?.getString(R.string.e_token)}\n${getFormattedPrice(eToken?.paidPaymentAmount?.plus(eToken.pendingPaymentAmount
                    ?: 0.0))} ${context?.getString(R.string.currency)}"
            tvAvailableCount.text = eToken?.quantity_left?.toString()
            tvUsedCount.text = eToken?.quantity?.minus(eToken.quantity_left ?: 0).toString()
        }

        private fun getFormattedPrice(price: Double?): String {
            return String.format(Locale.US, "%.2f", price)
        }
    }
}