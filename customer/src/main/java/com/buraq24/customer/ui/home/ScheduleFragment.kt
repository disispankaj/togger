package com.buraq24.customer.ui.home


import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.os.Build
import android.os.Bundle
import android.support.v4.app.Fragment
import android.transition.Explode
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast

import com.buraq24.customer.R
import com.buraq24.customer.ui.home.comfirmbooking.ConfirmBookingFragment
import com.buraq24.customer.webservices.models.ServiceRequestModel
import com.buraq24.utilities.CategoryId
import com.buraq24.getFormatFromDate
import com.buraq24.getFormatFromDateUtc
import kotlinx.android.synthetic.main.fragment_schedule.*
import java.util.*

/**
 * A simple [Fragment] subclass.
 *
 */
class ScheduleFragment : Fragment(), View.OnClickListener {

    private var serviceRequest: ServiceRequestModel? = null

    private var selectedDate = Calendar.getInstance(Locale.getDefault())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            enterTransition = Explode()
            exitTransition = Explode()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_schedule, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        serviceRequest = (activity as HomeActivity).serviceRequestModel
        selectedDate = Calendar.getInstance(Locale.getDefault())
        selectedDate.add(Calendar.HOUR_OF_DAY, 1)
        setDateTimeViews()
        setListeners()
    }

    private fun setListeners() {
        tvDate.setOnClickListener(this)
        tvTime.setOnClickListener(this)
        tvNext.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.tvDate -> {
                showDatePicker()
            }
            R.id.tvTime -> {
                showTimePicker()
            }
            R.id.tvNext -> {
                val tempCal = Calendar.getInstance()
                tempCal.add(Calendar.HOUR_OF_DAY, 1)
                if (selectedDate.before(tempCal)) {
                    Toast.makeText(activity, getString(R.string.schedule_time_selection_validation_msg), Toast.LENGTH_LONG).show()
                } else {
                    serviceRequest?.order_timings = getFormatFromDateUtc(selectedDate.time, "yyyy-MM-dd HH:mm:ss")
                    serviceRequest?.order_timings_text = getFormatFromDate(selectedDate.time, "EEE, MMM d h:mm a")
                    fragmentManager?.beginTransaction()?.replace(R.id.container, ConfirmBookingFragment())?.addToBackStack("backstack")?.commit()
                }
            }
        }
    }

    private fun showDatePicker() {
        val datePicker = DatePickerDialog(activity, DatePickerDialog.OnDateSetListener { view, year, month, dayOfMonth ->
            selectedDate.set(Calendar.YEAR, year)
            selectedDate.set(Calendar.MONTH, month)
            selectedDate.set(Calendar.DAY_OF_MONTH, dayOfMonth)

            val tempCalendar = Calendar.getInstance()
            tempCalendar.add(Calendar.HOUR_OF_DAY, 1)
            if (selectedDate.time.before(tempCalendar.time)) {
                selectedDate = tempCalendar.clone() as Calendar
            }
            setDateTimeViews()
        }, selectedDate.get(Calendar.YEAR),
                selectedDate.get(Calendar.MONTH),
                selectedDate.get(Calendar.DAY_OF_MONTH))

        datePicker.setCanceledOnTouchOutside(true)
        val tempCal = Calendar.getInstance()
        datePicker.datePicker.minDate = tempCal.timeInMillis
        if (serviceRequest?.category_id == CategoryId.MINERAL_WATER) {
            tempCal.add(Calendar.DAY_OF_MONTH, 6)
            tempCal.set(Calendar.HOUR_OF_DAY, 23)
            tempCal.set(Calendar.MINUTE, 59)
            tempCal.set(Calendar.SECOND, 0)
            datePicker.datePicker.maxDate = tempCal.timeInMillis
        }
        datePicker.show()
    }

    private fun showTimePicker() {
        val timePicker = TimePickerDialog(activity, TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
            val selectedTimeCalendar = selectedDate.clone() as Calendar
            selectedTimeCalendar.set(Calendar.HOUR_OF_DAY, hourOfDay)
            selectedTimeCalendar.set(Calendar.MINUTE, minute)
            selectedTimeCalendar.set(Calendar.SECOND, 0)
            val tempCalendar = Calendar.getInstance()
            tempCalendar.add(Calendar.HOUR_OF_DAY, 1)
            tempCalendar.add(Calendar.MINUTE, 1)
            tempCalendar.set(Calendar.SECOND, 0)
            if (selectedTimeCalendar.time.before(tempCalendar.time)) {
                Toast.makeText(activity, getString(R.string.schedule_time_selection_validation_msg), Toast.LENGTH_LONG).show()
            } else {
                selectedDate = selectedTimeCalendar.clone() as Calendar
                setDateTimeViews()
            }
        }, selectedDate.get(Calendar.HOUR_OF_DAY),
                selectedDate.get(Calendar.MINUTE),
                false)
        timePicker.setCanceledOnTouchOutside(true)
        timePicker.show()
    }

    private fun setDateTimeViews() {
        tvDate.text = getFormatFromDate(selectedDate.time, "EEE, dd MMM")
        tvTime.text = getFormatFromDate(selectedDate.time, "h:mm a")
    }
}
