package com.buraq24.customer.ui.menu.etokens.purchased


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.buraq24.customer.R
import com.buraq24.customer.utils.AppUtils
import com.buraq24.customer.webservices.models.etoken.PurchasedEToken
import com.buraq24.utilities.CheckNetworkConnection
import com.buraq24.utilities.StatusCode
import com.buraq24.showSWWerror
import com.buraq24.showSnack
import kotlinx.android.synthetic.main.fragment_purchased_etokens.*

class PurchasedETokensFragment : Fragment(), PurchasedETokenContract.View {

    private val presenter = PurchasedETokenPresenter()

    val purchasedETokensList = ArrayList<PurchasedEToken>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_purchased_etokens, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.attachView(this)
        recyclerView?.layoutManager = LinearLayoutManager(activity)
        recyclerView.adapter = PurchasedETokensAdapter(purchasedETokensList)
        setListeners()
    }

    override fun onResume() {
        super.onResume()
        purchasedETokensApiCall()
    }

    private fun setListeners() {
        swipeRefreshLayout?.setOnRefreshListener { purchasedETokensApiCall() }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        presenter.detachView()
    }

    private fun purchasedETokensApiCall() {
        if (!CheckNetworkConnection.isOnline(activity)) {
            CheckNetworkConnection.showNetworkError(rootView)
            return
        }
        val map = HashMap<String, String>()
        map["skip"] = "0"
        map["take"] = "1000"
        presenter.purchasedETokenListApi(map)
    }

    override fun onPurchasedETokensApiSuccess(purchasedETokensList: ArrayList<PurchasedEToken>?) {
        this.purchasedETokensList.clear()
        purchasedETokensList?.let { this.purchasedETokensList.addAll(it) }
        if (this.purchasedETokensList.isEmpty()) {
            tvNoData.visibility = View.VISIBLE
        } else {
            tvNoData.visibility = View.GONE
        }
        recyclerView?.adapter?.notifyDataSetChanged()
    }

    override fun showLoader(isLoading: Boolean) {
        swipeRefreshLayout.isRefreshing = isLoading
    }

    override fun apiFailure() {
        rootView.showSWWerror()
    }

    override fun handleApiError(code: Int?, error: String?) {
        rootView?.showSnack(error ?: "")
        if (code == StatusCode.UNAUTHORIZED) {
            AppUtils.logout(activity)
        } else {
            rootView?.showSnack(error ?: "")
        }
    }

}
