package com.buraq24.customer.ui.menu.bookings.upcoming.confirmorder

import com.buraq24.customer.webservices.ApiResponse
import com.buraq24.customer.webservices.RestClient
import com.buraq24.utilities.basearc.BasePresenterImpl
import com.buraq24.getApiError
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class OrderConfirmationPresenter : BasePresenterImpl<OrderConfirmationContract.View>(), OrderConfirmationContract.Presenter {

    override fun confirmOrder(orderId: Long, status: String) {
        getView()?.showLoader(true)
        RestClient.get().confirmETokenOrder(orderId, status).enqueue(object : Callback<ApiResponse<Any>> {

            override fun onResponse(call: Call<ApiResponse<Any>>?, response: Response<ApiResponse<Any>>?) {
                getView()?.showLoader(false)
                if (response?.isSuccessful == true) {
                    if (response.body()?.statusCode == 200) {
                        getView()?.onApiSuccess(status)
                    } else {
                        getView()?.handleApiError(response.body()?.statusCode, response.body()?.msg)
                    }
                } else {
                    val errorModel = getApiError(response?.errorBody()?.string())
                    getView()?.handleApiError(errorModel.statusCode, errorModel.msg)
                }
            }

            override fun onFailure(call: Call<ApiResponse<Any>>?, t: Throwable?) {
                getView()?.showLoader(false)
                getView()?.apiFailure()
            }
        })
    }
}