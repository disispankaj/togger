package com.buraq24.customer.ui.home.etokens

import android.content.Context
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.buraq24.customer.R
import com.buraq24.customer.webservices.models.etokens.Brand
import kotlinx.android.synthetic.main.item_tokens_offers.view.*

class OffersETokenAdapter(private val context: Context?, private val offersList: ArrayList<Brand>?) : RecyclerView.Adapter<OffersETokenAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OffersETokenAdapter.ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_tokens_offers, parent, false))
    }

    override fun getItemCount(): Int {
        return offersList?.size ?: 0
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind(offersList?.get(position))
    }

    inner class ViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {
        fun onBind(brand: Brand?) = with(itemView) {
            var requestOptions = RequestOptions()
            requestOptions = requestOptions.transforms(CenterCrop(), RoundedCorners(6))
            context?.let { Glide.with(it).load(brand?.image_url).apply(requestOptions).into(ivBrandImage) }
            tvBrandName.text = brand?.category_brand_name
            tvOffersCount.text = """${brand?.etokens_count.toString()} ${context.getString(R.string.e_tokens)} ${context.getString(R.string.available)}"""
            val tokensAdapter = OffersListAdapter(context, brand?.etokens)
            rvOffers?.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            rvOffers.adapter = tokensAdapter
        }
    }

}