package com.buraq24.customer.ui.menu.payments

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Toast
import com.buraq24.customer.R
import com.buraq24.customer.utils.AppUtils
import com.buraq24.customer.webservices.models.CardModel
import com.buraq24.customer.webservices.models.CouponModel
import com.buraq24.customer.webservices.models.paymentdetails.PaymentDetail
import com.buraq24.showSnack
import com.buraq24.utilities.CheckNetworkConnection
import com.buraq24.utilities.DialogIndeterminate
import com.buraq24.utilities.StatusCode
import kotlinx.android.synthetic.main.activity_promo.*

class PromoActivity : AppCompatActivity(), PaymentsContract.View, View.OnClickListener {

    private val presenter = PaymentsPresenter()
    private var dialogIndeterminate: DialogIndeterminate? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_promo)
        initializer()
        setListener()
    }

    private fun initializer() {
        presenter.attachView(this)
        dialogIndeterminate = DialogIndeterminate(this)
    }

    fun setListener() {
        ivBack.setOnClickListener(this)
        tvPromoSelected.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            ivBack.id -> onBackPressed()

            tvPromoSelected.id -> validator()
        }
    }

    fun validator() {
        if (!(etPromo.text.toString().trim().isEmpty())) {
            if (CheckNetworkConnection.isOnline(this)) {
                var map = HashMap<String, Any>()
                map.put("code", etPromo.text.toString().trim())
                presenter.buyCoupons(map)
            } else {
                CheckNetworkConnection.showNetworkError(rootViewPromo)
            }
        } else {
            tvPromoSelected.showSnack(getString(R.string.enter_promo_code))
        }
    }

    override fun onPaymentDetailsApiSuccess(response: PaymentDetail?) {

    }

    override fun onAddCardSuccess(response: CardModel?) {

    }

    override fun onGetCardSuccess(response: ArrayList<CardModel>?) {

    }

    override fun onSetDefaultCard(response: CardModel?) {

    }

    override fun onBuyCounponSuccess(response: CouponModel?) {
        Toast.makeText(this,getString(R.string.promo_applied_successfully),Toast.LENGTH_SHORT).show()
        finish()
    }

    override fun showLoader(isLoading: Boolean) {
        dialogIndeterminate?.show(isLoading)
    }

    override fun apiFailure() {

    }

    override fun handleApiError(code: Int?, error: String?) {
        if (code == StatusCode.UNAUTHORIZED) {
            AppUtils.logout(this)
        }
    }

}
