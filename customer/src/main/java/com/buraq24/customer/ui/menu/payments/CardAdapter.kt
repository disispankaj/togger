package com.buraq24.customer.ui.menu.payments

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.TextView
import com.buraq24.customer.R
import com.buraq24.customer.webservices.models.CardModel

class CardAdapter(val cardList: ArrayList<CardModel>, context: Context, val onClickDelete: OnClickDelete) : RecyclerView.Adapter<CardAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.card_item_layout, parent, false))
    }

    override fun getItemCount(): Int {
        return cardList.size
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        val model = cardList.get(position)
        viewHolder.tvCardNum?.text = "XXXX XXXX XXXX "+model.last4
//        viewHolder.tvCardName?.text = model.card_holder_name
//        viewHolder.tvCardDate?.text = model.exp_month + "/" + model.exp_year

        viewHolder.ivDeleteCard?.setOnClickListener {
            onClickDelete.onClickDeleteListener(position)
        }

        viewHolder.chkBoxCardNum?.setOnClickListener {
            onClickDelete.onCardSelected(position)
        }

        viewHolder.chkBoxCardNum?.isChecked = model.isSelected == true
    }


    inner class ViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {

        val chkBoxCardNum = itemView?.findViewById<CheckBox>(R.id.chkBoxCardNum)
        val tvCardNum = itemView?.findViewById<TextView>(R.id.tvCardNum)
        val tvCardName = itemView?.findViewById<TextView>(R.id.tvCardName)
        val tvCardDate = itemView?.findViewById<TextView>(R.id.tvCardDate)
        var ivDeleteCard = itemView?.findViewById<ImageView>(R.id.ivDeleteCard)

    }

    interface OnClickDelete {
        fun onClickDeleteListener(position: Int)

        fun onCardSelected(position: Int)
    }
}