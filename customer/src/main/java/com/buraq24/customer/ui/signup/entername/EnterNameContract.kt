package com.buraq24.customer.ui.signup.entername

import com.buraq24.utilities.basearc.BasePresenter
import com.buraq24.utilities.basearc.BaseView
import com.buraq24.utilities.webservices.models.AppDetail
import com.buraq24.utilities.webservices.models.LoginModel

class EnterNameContract{

    interface View: BaseView{
        fun onApiSuccess(response: AppDetail?)
    }

    interface Presenter: BasePresenter<View>{
        fun addNameApiCall(name: String,email : String,dob : String)
    }

}