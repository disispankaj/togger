package com.buraq24.customer.ui.chatModule.chatMessage

import com.buraq24.customer.webservices.ApiResponse
import com.buraq24.customer.webservices.RestClient
import com.buraq24.utilities.basearc.BasePresenterImpl
import com.buraq24.utilities.chatModel.ChatMessageListing
import com.buraq24.getApiError
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ChatMessagesPresenter : BasePresenterImpl<ChatMessageContract.View>(), ChatMessageContract.Presenter {

    private var call: Call<ApiResponse<ArrayList<ChatMessageListing>>>? = null

    override fun getChatMessagesApiCall(authorization: String, messageId: String, receiverId: String, limit: Int, skip: Int, messageOrder: String) {
        if (call != null) {
            call?.cancel()
        }
        getView()?.showLoader(true)
        call = RestClient.get().getChatMessages(authorization, messageId, receiverId, limit, skip, messageOrder)
        call?.enqueue(object : Callback<ApiResponse<ArrayList<ChatMessageListing>>> {
            override fun onFailure(call: Call<ApiResponse<ArrayList<ChatMessageListing>>>?, t: Throwable?) {
                getView()?.showLoader(false)
                getView()?.apiFailure()
            }

            override fun onResponse(call: Call<ApiResponse<ArrayList<ChatMessageListing>>>?, response: Response<ApiResponse<ArrayList<ChatMessageListing>>>?) {
                getView()?.showLoader(false)
                if (response?.isSuccessful == true) {
                    if (response.body()?.statusCode == 200) {
                        getView()?.chatMessagesApiSuccess(response.body()?.result, messageOrder)
                    } else {
                        getView()?.handleApiError(response.body()?.statusCode, response.body()?.msg)
                    }
                } else {
                    val errorModel = getApiError(response?.errorBody()?.string())
                    getView()?.handleApiError(errorModel.statusCode, errorModel.msg)
                }
            }
        })
    }

}