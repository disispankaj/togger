package com.buraq24.customer.ui.menu.etokens

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.location.Address
import android.location.Location
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import com.buraq24.customer.R
import com.buraq24.hideKeyboard
import com.buraq24.showSnack
import com.buraq24.utilities.*
import com.buraq24.utilities.location.LocationProvider
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.places.GeoDataClient
import com.google.android.gms.location.places.PlaceBufferResponse
import com.google.android.gms.location.places.Places
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.RuntimeRemoteException
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.OnSuccessListener
import kotlinx.android.synthetic.main.activity_get_address.*

class GetAddressActivity : AppCompatActivity(), OnMapReadyCallback, GoogleMap.OnCameraMoveStartedListener, GoogleMap.OnCameraIdleListener, View.OnClickListener {

    private var mMap: GoogleMap? = null

    private var mGeoDataClient: GeoDataClient? = null

    private var mapFragment: SupportMapFragment? = null

    private lateinit var locationProvider: LocationProvider

    private lateinit var adapterDropOff: PlaceAutocompleteAdapter

    private var latitude = 0.0

    private var longitude = 0.0

    private var address = ""

    private var isCurrentLocation = false

    private var cameraMoveReason: Int? = 0

    private var currentLocation: Location? = null

    private var dTempAddress: String? = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_get_address)
        mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        locationProvider = LocationProvider.CurrentLocationBuilder(this).build()
        locationProvider.getLastKnownLocation(OnSuccessListener {
            currentLocation = it
            mapFragment?.getMapAsync(this)
        })
        setListeners()
    }

    private fun init() {
        mGeoDataClient = Places.getGeoDataClient(this)
        latitude = intent?.getDoubleExtra(Constants.LATITUDE, 0.0) ?: 0.0
        longitude = intent?.getDoubleExtra(Constants.LONGITUDE, 0.0) ?: 0.0
        address = intent?.getStringExtra(Constants.ADDRESS) ?: ""
        val latLngBounds = LatLngBounds.builder()
        latLngBounds.include(LatLng(latitude + 1, longitude + 1))
        latLngBounds.include(LatLng(latitude - 1, longitude - 1))
        acDropOffAddress.setText(address)
        if (latitude != 0.0 && longitude != 0.0) {
            mMap?.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(latitude, longitude), 16f))
        }
        acDropOffAddress?.isFocusable = false
        acDropOffAddress?.isFocusableInTouchMode = false
        adapterDropOff = PlaceAutocompleteAdapter(this, mGeoDataClient, latLngBounds.build(), null)
        acDropOffAddress.setAdapter(adapterDropOff)
        acDropOffAddress.onItemClickListener = onDropOffItemClickListener
    }

    private fun setListeners() {
        fabMyLocation.setOnClickListener(this)
        ivNext.setOnClickListener(this)
        tvBack.setOnClickListener(this)
        acDropOffAddress.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {

            R.id.ivNext -> {
                if (latitude == 0.0 && longitude == 0.0 || acDropOffAddress.text.isEmpty() || dTempAddress != acDropOffAddress.text.toString().trim()) {
                    rootView.showSnack(getString(R.string.dropoff_address_validation_message))
                } else {
                    val intent = Intent()
                    intent.putExtra(Constants.LATITUDE, latitude)
                    intent.putExtra(Constants.LONGITUDE, longitude)
                    intent.putExtra(Constants.ADDRESS, address)
                    setResult(Activity.RESULT_OK, intent)
                    finish()
                }
            }

            R.id.tvBack -> {
                onBackPressed()
            }

            R.id.fabMyLocation -> {
                locationProvider.getLastKnownLocation(OnSuccessListener {
                    currentLocation = it
                    if (it != null) {
                        val cameraUpdate = CameraUpdateFactory.newLatLngZoom(LatLng(it.latitude, it.longitude), 16f)
                        mMap?.animateCamera(cameraUpdate)
                        cameraMoveReason = GoogleMap.OnCameraMoveStartedListener.REASON_GESTURE
                    } else {
                        startLocationUpdates()
                    }
                })
            }

            R.id.acDropOffAddress -> {
                acDropOffAddress.isFocusable = true
                acDropOffAddress?.isFocusableInTouchMode = true
//                acDropOffAddress?.requestFocus()
            }
        }
    }

    @SuppressLint("MissingPermission")
    override fun onMapReady(googleMap: GoogleMap?) {
        mMap = googleMap
        init()
        mMap?.uiSettings?.isTiltGesturesEnabled = false
        mMap?.uiSettings?.isMyLocationButtonEnabled = false
        mMap?.isMyLocationEnabled = true
        mMap?.setOnCameraMoveStartedListener(this)
        mMap?.setOnCameraIdleListener(this)
    }

    override fun onCameraMoveStarted(reason: Int) {
        cameraMoveReason = reason
        if (reason == GoogleMap.OnCameraMoveStartedListener.REASON_GESTURE) {
            latitude = 0.0
            longitude = 0.0
            address = ""
            acDropOffAddress.setText(address)
            acDropOffAddress.hideKeyboard()
        }
        isCurrentLocation = false
    }

    override fun onCameraIdle() {
        if (cameraMoveReason != GoogleMap.OnCameraMoveStartedListener.REASON_GESTURE) return
        latitude = mMap?.cameraPosition?.target?.latitude ?: 0.0
        longitude = mMap?.cameraPosition?.target?.longitude ?: 0.0
        address = ""
        locationProvider.getAddressFromLatLng(latitude, longitude, dropOffAddressListener)

        isCurrentLocation = if (MapUtils.getDistanceBetweenTwoPoints(LatLng(mMap?.cameraPosition?.target?.latitude
                        ?: 0.0, mMap?.cameraPosition?.target?.longitude
                        ?: 0.0), LatLng(currentLocation?.latitude
                        ?: 0.0, currentLocation?.longitude ?: 0.0)) < 1) {
            fabMyLocation.setImageResource(R.drawable.ic_my_location_blue)
            true
        } else {
            fabMyLocation.setImageResource(R.drawable.ic_my_location)
            false
        }
    }

    private val onDropOffItemClickListener = AdapterView.OnItemClickListener { parent, view, position, id ->
        val item = adapterDropOff.getItem(position)
        val placeId = item?.placeId
        val placeResult = mGeoDataClient?.getPlaceById(placeId)
        placeResult?.addOnCompleteListener(mUpdateDropOffAddressCallback)
        dTempAddress = acDropOffAddress.text.toString().trim()
    }

    private val mUpdateDropOffAddressCallback = OnCompleteListener<PlaceBufferResponse> { task ->
        try {
            val places = task.result
            // Get the Place object from the buffer.
            val place = places.get(0)
            val array = ArrayList<Double>()
            array.add(place.latLng.longitude)
            array.add(place.latLng.latitude)
            latitude = place.latLng.latitude
            longitude = place.latLng.longitude
            address = place.address.toString()
            focusOnLocation()
            places.release()
        } catch (e: RuntimeRemoteException) {
            // Request did not complete successfully
            Log.e("EnterTransportAddress", "Place query did not complete.", e)
            return@OnCompleteListener
        }
    }

    private val dropOffAddressListener = object : LocationProvider.OnAddressListener {
        override fun getAddress(address: String, result: List<Address>) {
            this@GetAddressActivity.address = address
            acDropOffAddress?.isFocusable = false
            acDropOffAddress?.isFocusableInTouchMode = false
            acDropOffAddress?.setText(address)
            acDropOffAddress?.requestFocus()
            acDropOffAddress?.isFocusable = true
            acDropOffAddress?.isFocusableInTouchMode = true
            dTempAddress = address
        }
    }

    private fun focusOnLocation() {
        val cameraUpdate = CameraUpdateFactory.newLatLng(LatLng(latitude, longitude))
        mMap?.animateCamera(cameraUpdate)
    }

    private fun startLocationUpdates() {
        locationProvider = LocationProvider.LocationUpdatesBuilder(this).apply {
            interval = 1000
            fastestInterval = 1000
        }.build()
        locationProvider.startLocationUpdates(object : LocationCallback() {
            override fun onLocationResult(locationRes: LocationResult?) {
                super.onLocationResult(locationRes)
                val cameraUpdate = CameraUpdateFactory.newLatLng(LatLng(locationRes?.lastLocation?.latitude
                        ?: 0.0, locationRes?.lastLocation?.longitude ?: 0.0))
                mMap?.animateCamera(cameraUpdate)
                mapFragment?.getMapAsync(this@GetAddressActivity)
                locationProvider.stopLocationUpdates(this)
            }
        })
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        locationProvider.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        locationProvider.onActivityResult(requestCode, resultCode, data)
    }

    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(LocaleManager.setLocale(newBase))
    }
}
