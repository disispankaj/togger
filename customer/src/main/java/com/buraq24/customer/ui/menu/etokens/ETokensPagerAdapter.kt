package com.buraq24.customer.ui.menu.etokens

import android.content.Context
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.buraq24.customer.R

class ETokensPagerAdapter(fragmentManager: FragmentManager, private var fragmentList: ArrayList<Fragment>?, private var context: Context?) : FragmentPagerAdapter(fragmentManager) {

    override fun getItem(position: Int): Fragment? {
        return fragmentList?.get(position)
    }

    override fun getCount(): Int {
        return fragmentList?.size ?: 0
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return when (position) {
            0 -> context?.getString(R.string.new_label)
            1 -> context?.getString(R.string.purchased)
            else->""
        }
    }

}