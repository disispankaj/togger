package com.buraq24.customer.ui.callingTokBox


import com.buraq24.customer.webservices.ApiResponse
import com.buraq24.customer.webservices.RestClient
import com.buraq24.getApiError
import com.buraq24.utilities.basearc.BasePresenterImpl
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class OpenTalkPresenter : BasePresenterImpl<AudioContract.View>(), AudioContract.Presenter {

    override fun getSessionIDApi(authorization: String, receiverId: String) {
        RestClient.get().getSessionIdForOpenTalk(authorization, receiverId).enqueue(object : Callback<ApiResponse<OpenTalkSession>> {
            override fun onFailure(call: Call<ApiResponse<OpenTalkSession>>?, t: Throwable?) {
                getView()?.apiFailure()
            }

            override fun onResponse(call: Call<ApiResponse<OpenTalkSession>>?, response: Response<ApiResponse<OpenTalkSession>>?) {
                if (response?.isSuccessful == true) {
                    if (response.body()?.statusCode == 200) {
                        getView()?.sessionApiSuccess(response.body()?.result)
                    } else {
                        getView()?.handleApiError(response.body()?.statusCode, response.body()?.msg)
                    }
                } else {
                    val errorModel = getApiError(response?.errorBody()?.string())
                    getView()?.handleApiError(errorModel.statusCode, errorModel.msg)
                }
            }

        })
    }
}