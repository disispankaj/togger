package com.buraq24.customer.ui.chatModule

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.text.format.DateUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.buraq24.convertDateTimeInMillis
import com.buraq24.customer.R
import com.buraq24.customer.ui.chatModule.chatMessage.ChatActivity
import com.buraq24.existsInWeek
import com.buraq24.getFormatFromDate
import com.buraq24.isYesterday
import com.buraq24.utilities.*
import com.buraq24.utilities.chatModel.ChatMessageListing
import com.buraq24.utilities.chatModel.UserId
import com.buraq24.utilities.constants.PROFILE
import com.buraq24.utilities.webservices.BASE_URL_LOAD_IMAGE
import kotlinx.android.synthetic.main.item_chat_list.view.*
import java.util.*

class ChatListAdapter(private var context: Context,
                      private val chatListing: ArrayList<ChatMessageListing>?,
                      private val chatsFragment: ChatUserListActivity) : RecyclerView.Adapter<ChatListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChatListAdapter.ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_chat_list, parent, false))
    }

    override fun getItemCount(): Int {
        return chatListing?.size ?: 0
    }

    override fun onBindViewHolder(holder: ChatListAdapter.ViewHolder, position: Int) {
        holder.bind(chatListing?.get(position))
    }

    inner class ViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {

        init {
            itemView?.setOnClickListener {
                val intent = Intent(context, ChatActivity::class.java)
                intent.putExtra(RECEIVER_ID, chatListing?.get(adapterPosition)?.oppositionId)
                intent.putExtra(USER_NAME, chatListing?.get(adapterPosition)?.name)
                intent.putExtra(PROFILE_PIC_URL, BASE_URL_LOAD_IMAGE+chatListing?.get(adapterPosition)?.profile_pic)
                chatsFragment.startActivity(intent)
                notifyDataSetChanged()
            }
        }

        @SuppressLint("SetTextI18n")
        fun bind(chat: ChatMessageListing?) = with(itemView) {
            val isMyMessage: Boolean = chat?.send_by == SharedPrefs.with(context).getObject(PROFILE, UserId::class.java)._id
            tvName.text = chat?.name
             Glide.with(context).load(BASE_URL_LOAD_IMAGE+chat?.profile_pic).apply(RequestOptions().circleCrop()).into(ivProfilePic)
            if (chat?.chat_type == ChatType.IMAGE) {
                if (isMyMessage) {
                    tvMessage.text = """${context.getString(R.string.you)} ${context.getString(R.string.sent_an_image)}"""
                } else {
                    tvMessage.text = """${chat.name} ${context.getString(R.string.sent_an_image)}"""
                }
            } else {
                tvMessage.text = chat?.text
            }
            val calendar = Calendar.getInstance()
            chat?.sent_at?.let { calendar.timeInMillis = convertDateTimeInMillis(it) }
            val dateString: String
            val time = getFormatFromDate(calendar.time, "h:mm a")
            dateString = when {
                DateUtils.isToday(calendar.timeInMillis) -> String.format("%s", time)
                isYesterday(calendar) -> String.format("%s", context.getString(R.string.yesterday))
                existsInWeek(calendar) -> getFormatFromDate(calendar.time, "EEE")!!
                else -> getFormatFromDate(calendar.time, "MMM dd")!!
            }
            tvTime.text = dateString
            /* if (chat?.unDeliverCount ?: 0 > 0) {
                 tvUnreadCount.visibility = View.VISIBLE
                 if (chat?.unDeliverCount ?: 0 > 99) {
                     tvUnreadCount.text = context.getString(R.string.nintynine_plus)
                 } else {
                     tvUnreadCount.text = chat?.unDeliverCount.toString()
                 }
             } else {
                 tvUnreadCount.visibility = View.GONE
             }*/
        }
    }
}