package com.buraq24.customer.ui.menu.etokens

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.Fragment
import com.buraq24.customer.R
import com.buraq24.customer.ui.menu.etokens.buy.BuyETokenActivity
import com.buraq24.customer.ui.menu.etokens.newt.NewETokensFragment
import com.buraq24.customer.ui.menu.etokens.purchased.PurchasedETokensFragment
import com.buraq24.utilities.LocaleManager
import kotlinx.android.synthetic.main.activity_etoken.*

/**
 * Activity to buy and view E-Token to buy mineral water
 */

class ETokenActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_etoken)
        setUpViewPager()
        setListeners()
    }

    /* Sets the viewpager to show new and purchased E-Tokens*/
    private fun setUpViewPager() {
        val fragments = ArrayList<Fragment>()
        fragments.add(NewETokensFragment())
        fragments.add(PurchasedETokensFragment())
        val adapter = ETokensPagerAdapter(supportFragmentManager, fragments, this)
        viewPager.adapter = adapter
        tabLayout.setupWithViewPager(viewPager)
    }

    private fun setListeners() {
        tvBack.setOnClickListener { onBackPressed() }
    }

    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(LocaleManager.setLocale(newBase))
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == BuyETokenActivity.RQ_BUY_ETOKEN && resultCode == Activity.RESULT_OK) {
            viewPager.currentItem = 1
        }
    }
}
