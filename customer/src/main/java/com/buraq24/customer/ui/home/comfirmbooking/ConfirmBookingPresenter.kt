package com.buraq24.customer.ui.home.comfirmbooking

import com.buraq24.customer.webservices.ApiResponse
import com.buraq24.customer.webservices.RestClient
import com.buraq24.customer.webservices.models.ServiceRequestModel
import com.buraq24.customer.webservices.models.order.Order
import com.buraq24.utilities.basearc.BasePresenterImpl
import com.buraq24.getApiError
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File

class ConfirmBookingPresenter : BasePresenterImpl<ConfirmBookingContract.View>(), ConfirmBookingContract.Presenter {

    override fun requestServiceApiCall(requestModel: ServiceRequestModel) {
        getView()?.showLoader(true)
        val orderImages: ArrayList<MultipartBody.Part> = ArrayList()
        val map = HashMap<String, RequestBody>()
        map["category_id"] = RequestBody.create(MultipartBody.FORM, requestModel.category_id.toString())
        map["category_brand_id"] = RequestBody.create(MultipartBody.FORM, requestModel.category_brand_id.toString())
        map["category_brand_product_id"] = RequestBody.create(MultipartBody.FORM, requestModel.category_brand_product_id.toString())
        map["product_quantity"] = RequestBody.create(MultipartBody.FORM, requestModel.product_quantity.toString())
        map["pickup_address"] = RequestBody.create(MultipartBody.FORM, requestModel.pickup_address.toString())
        map["pickup_latitude"] = RequestBody.create(MultipartBody.FORM, requestModel.pickup_latitude.toString())
        map["pickup_longitude"] = RequestBody.create(MultipartBody.FORM, requestModel.pickup_longitude.toString())
        map["dropoff_address"] = RequestBody.create(MultipartBody.FORM, requestModel.pickup_address.toString())
        map["dropoff_latitude"] = RequestBody.create(MultipartBody.FORM, requestModel.pickup_latitude.toString())
        map["dropoff_longitude"] = RequestBody.create(MultipartBody.FORM, requestModel.pickup_longitude.toString())
        map["order_timings"] = RequestBody.create(MultipartBody.FORM, requestModel.order_timings.toString())
        map["future"] = RequestBody.create(MultipartBody.FORM, requestModel.future.toString())
        map["payment_type"] = RequestBody.create(MultipartBody.FORM, requestModel.payment_type.toString())
        map["distance"] = RequestBody.create(MultipartBody.FORM, requestModel.distance.toString())
        map["organisation_coupon_user_id"] = RequestBody.create(MultipartBody.FORM, requestModel.organisation_coupon_user_id.toString())
        map["product_weight"] = RequestBody.create(MultipartBody.FORM, requestModel.product_weight.toString())
        map["details"] = RequestBody.create(MultipartBody.FORM, requestModel.details.toString())
        map["material_details"] = RequestBody.create(MultipartBody.FORM, requestModel.material_details.toString())
        map["order_distance"] = RequestBody.create(MultipartBody.FORM, requestModel.order_distance.toString())
        map["pickup_person_name"] = RequestBody.create(MultipartBody.FORM, requestModel.pickup_person_name.toString())
        map["pickup_person_phone"] = RequestBody.create(MultipartBody.FORM, requestModel.pickup_person_phone.toString())
        map["invoice_number"] = RequestBody.create(MultipartBody.FORM, requestModel.invoice_number.toString())
        map["delivery_person_name"] = RequestBody.create(MultipartBody.FORM, requestModel.delivery_person_name.toString())
        map["order_mins"] = RequestBody.create(MultipartBody.FORM, requestModel.order_mins.toString())


        if (requestModel.images.isNotEmpty()) {
            requestModel.images.forEach {
                val file = File(it)
                val image = RequestBody.create(MediaType.parse("image/jpeg"), file)
                val data = MultipartBody.Part.createFormData("order_images",
                        file.name, image)
                orderImages.add(data)
            }
        }

        RestClient.get().requestService(map, orderImages).enqueue(object : Callback<ApiResponse<Order>> {

            override fun onResponse(call: Call<ApiResponse<Order>>?, response: Response<ApiResponse<Order>>?) {
                getView()?.showLoader(false)
                if (response?.isSuccessful == true) {
                    if (response.body()?.statusCode == 200) {
                        getView()?.onApiSuccess(response.body()?.result)
                    } else {
                        getView()?.handleApiError(response.body()?.statusCode, response.body()?.msg)
                    }
                } else {
                    val errorModel = getApiError(response?.errorBody()?.string())
                    getView()?.handleApiError(errorModel.statusCode, errorModel.msg)
                }
            }

            override fun onFailure(call: Call<ApiResponse<Order>>?, t: Throwable?) {
                getView()?.showLoader(false)
                getView()?.apiFailure()
            }
        })
    }

}