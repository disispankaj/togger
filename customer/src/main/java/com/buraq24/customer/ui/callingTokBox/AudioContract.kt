package com.buraq24.customer.ui.callingTokBox

import com.buraq24.utilities.basearc.BasePresenter
import com.buraq24.utilities.basearc.BaseView

class AudioContract{

    interface View : BaseView {
        fun sessionApiSuccess(chatList: OpenTalkSession?)
    }

    interface Presenter : BasePresenter<View> {
        fun getSessionIDApi(authorization : String , limit: String)
    }
}