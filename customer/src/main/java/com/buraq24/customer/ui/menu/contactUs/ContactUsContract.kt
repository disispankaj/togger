package com.buraq24.driver.ui.home.contactUs

import com.buraq24.utilities.basearc.BasePresenter
import com.buraq24.utilities.basearc.BaseView
import com.google.android.gms.maps.model.LatLng

class ContactUsContract{

    interface View: BaseView{
        fun onApiSuccess()
    }

    interface Presenter: BasePresenter<View>{
        fun sendMsg(msg: String)
    }
}