package com.buraq24.customer.ui.home.support

import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.OrientationHelper
import com.buraq24.customer.R
import com.buraq24.customer.ui.home.SupportServicesAdapter
import com.buraq24.customer.utils.GridSpacingItemDecoration
import com.buraq24.utilities.CheckNetworkConnection
import com.buraq24.utilities.LocaleManager
import com.buraq24.showSWWerror
import com.buraq24.showSnack
import com.buraq24.utilities.webservices.models.Service
import kotlinx.android.synthetic.main.activity_support.*


class SupportActivity : AppCompatActivity(), SupportContract.View {

    private var supportList = ArrayList<Service>()

    private val presenter = SupportPresenter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_support)
        presenter.attachView(this)
        setAdapter()
        if (CheckNetworkConnection.isOnline(this)) {
            presenter.getSupportList()
        } else {
            CheckNetworkConnection.showNetworkError(rootView)
        }
        setListeners()
    }

    private fun setListeners() {
        tvBack.setOnClickListener { onBackPressed() }
    }

    private fun setAdapter() {
        rvSupportList.layoutManager = GridLayoutManager(this, 2, OrientationHelper.VERTICAL, false)
        rvSupportList.adapter = SupportServicesAdapter(supportList)
        val spacingInPixels = resources.getDimensionPixelSize(R.dimen.fab_margin)
        rvSupportList.addItemDecoration(GridSpacingItemDecoration(2, spacingInPixels, true))

    }

    override fun onSupportListApiSuccess(response: List<Service>?) {
        if (response?.isNotEmpty() == true) {
            flipperSupport.displayedChild = 1
            supportList.addAll(response)
            rvSupportList.adapter.notifyDataSetChanged()
        } else {
            flipperSupport.displayedChild = 2
        }
    }

    override fun showLoader(isLoading: Boolean) {

    }

    override fun apiFailure() {
        rvSupportList?.showSWWerror()
    }

    override fun handleApiError(code: Int?, error: String?) {
        rvSupportList?.showSnack(error.toString())
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.detachView()
    }

    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(LocaleManager.setLocale(newBase))
    }
}