package com.buraq24.customer.ui.signup.entername

import com.buraq24.customer.webservices.ApiResponse
import com.buraq24.customer.webservices.RestClient
import com.buraq24.utilities.basearc.BasePresenterImpl
import com.buraq24.utilities.constants.SUCCESS_CODE
import com.buraq24.getApiError
import com.buraq24.utilities.DateUtils
import com.buraq24.utilities.webservices.models.AppDetail
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class EnterNamePresenter : BasePresenterImpl<EnterNameContract.View>(), EnterNameContract.Presenter {

    override fun addNameApiCall(name: String,email : String,dob : String) {
        getView()?.showLoader(true)
        RestClient.recreate().get().addName(name,email,DateUtils.timeInMillisec(dob).toString()).enqueue(object : Callback<ApiResponse<AppDetail>> {
            override fun onResponse(call: Call<ApiResponse<AppDetail>>?, response: Response<ApiResponse<AppDetail>>?) {
                getView()?.showLoader(false)
                if (response?.isSuccessful == true) {
                    if (response.body()?.statusCode == SUCCESS_CODE) {
                        getView()?.onApiSuccess(response.body()?.result)
                    } else {
                        getView()?.handleApiError(response.body()?.statusCode, response.body()?.msg)
                    }
                } else {
                    val errorModel = getApiError(response?.errorBody()?.string())
                    getView()?.handleApiError(errorModel.statusCode, errorModel.msg)
                }
            }

            override fun onFailure(call: Call<ApiResponse<AppDetail>>?, t: Throwable?) {
                getView()?.showLoader(false)
                getView()?.apiFailure()
            }

        })
    }

}