package com.buraq24.customer.ui.menu.bookings

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.*
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.buraq24.customer.R
import com.buraq24.showSWWerror
import com.buraq24.showSnack
import com.buraq24.customer.utils.AppUtils
import com.buraq24.customer.webservices.models.order.Driver
import com.buraq24.customer.webservices.models.order.Order
import com.buraq24.utilities.*
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_booking_details.*
import java.util.*

class BookingDetailsActivity : AppCompatActivity(), BookingContract.View {

    private val presenter = BookingsPresenter()

    private var order: Order? = null

    private val ratingDrawables = listOf(R.drawable.ic_1, R.drawable.ic_2, R.drawable.ic_3, R.drawable.ic_4, R.drawable.ic_5)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter.attachView(this)
        setContentView(R.layout.activity_booking_details)
        tvCancel.visibility = View.GONE
        ivBack.setOnClickListener {
            onBackPressed()
        }
        val orderData = Gson().fromJson(intent.getStringExtra("data"), Order::class.java)
        setBookingData(orderData)
        setUserData(orderData?.driver)
        setPaymentData(orderData)
        if (CheckNetworkConnection.isOnline(this)) {
            presenter.getOrderDetails(orderData?.order_id?.toLong() ?: 0L)
        } else {
            CheckNetworkConnection.isOnline(this)
        }

        setListeners()
    }

    private fun setListeners() {
        tvCancel.setOnClickListener {
            showCancellationDialog()
        }

        ivCall.setOnClickListener {
            val phone = order?.driver?.phone_number.toString()
            val intent = Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null))
            startActivity(intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP))
        }
    }

    private fun setPaymentData(order: Order?) {
        tvServiceName.text = when {
            order?.category_id == CategoryId.GAS -> {
                tvQuantity.text = String.format("%s × %s", order.brand?.name, order.payment?.product_quantity)
                getString(R.string.gas)
            }
            order?.category_id == CategoryId.MINERAL_WATER -> {
                tvQuantity.text = String.format("%s × %s", order.brand?.name, order.payment?.product_quantity)
                order.brand?.brand_name
            }
            order?.category_id == CategoryId.WATER_TANKER -> {
                tvQuantity.text = String.format("%s × %s", order.brand?.name, order.payment?.product_quantity)
                getString(R.string.water_tanker)
            }
            order?.category_id == CategoryId.FREIGHT || order?.category_id == CategoryId.FREIGHT_2 -> {
                tvQuantity.text = "${order.brand?.name}"
                order.brand?.brand_name
            }
            else -> order?.brand?.brand_name
        }

        tvPrice.text = "${getFormattedPrice(order?.payment?.final_charge)} ${getString(R.string.currency)}" // base fair
        tvTex.text = "${getFormattedPrice("0.0")} ${getString(R.string.currency)}"
        tvGst.text = "${getFormattedPrice("0.0")} ${getString(R.string.currency)}"
        tvTotal.text = getFormattedPrice(order?.payment?.final_charge) + " " + getString(R.string.currency)
    }

    private fun getFormattedPrice(price: String?): String {
        return String.format(Locale.US, "%.2f", price?.toDouble())
    }

    private fun setUserData(driver: Driver?) {
        if (driver?.name == null) {
            rlProfile.visibility = View.GONE
            return
        } else {
            rlProfile.visibility = View.VISIBLE
        }
        var requestOptions = RequestOptions()
        requestOptions = requestOptions.transforms(CircleCrop()).placeholder(R.drawable.ic_bg_menu)
                .placeholder(R.drawable.profile_pic_placeholder)
        Glide.with(this).applyDefaultRequestOptions(requestOptions).load(driver.profile_pic_url).into(imageView)
        tvName.text = driver.name
        tvComments.text = order?.ratingByUser?.comments
    }

    private fun setBookingData(orderData: Order?) {

        if(orderData?.category_id==4)
        {
            tvDropOff.text=getString(R.string.deliver_loc)
        }


        var requestOptions = RequestOptions()
        requestOptions = requestOptions.transforms(CenterCrop(), RoundedCorners(8))
        var lat = orderData?.pickup_latitude ?: 0.0
        var lng = orderData?.pickup_longitude ?: 0.0
        if (orderData?.dropoff_latitude?.toInt() != 0 && orderData?.dropoff_latitude?.toInt() != 0) {
            lat = orderData?.dropoff_latitude ?: 0.0
            lng = orderData?.dropoff_longitude ?: 0.0
        }

        Glide.with(this).applyDefaultRequestOptions(requestOptions).load(MapUtils.getStaticMapWithMarker(this, lat, lng)).into(ivMap)
        tvDateTime.text = DateUtils.getFormattedTimeForBooking(orderData?.order_timings ?: "")
        val paymentType = getString(AppUtils.getPaymentStringId(orderData?.payment?.payment_type
                ?: "0"))
        tvPaymentTypeAmount.text = "$paymentType · ${getFormattedPrice(order?.payment?.final_charge)} ${getString(R.string.currency)}"
        tvBookingId.text = "ID:${orderData?.order_token}"
        tvBookingStatus.text = when (orderData?.order_status) {
            OrderStatus.SERVICE_COMPLETE -> {
                if (order?.future == "1") {
                    tvStartTime.text = DateUtils.getFormattedTimeForBooking(orderData.cRequest?.started_at
                            ?: "")
                } else {
                    tvStartTime.text = DateUtils.getFormattedTimeForBooking(orderData.cRequest?.accepted_at
                            ?: "")
                }
                tvCompletedTime.text = DateUtils.getFormattedTimeForBooking(orderData.cRequest?.updated_at
                        ?: "")
                showDateTimeViews(View.VISIBLE)
                getString(R.string.completed)
            }
            OrderStatus.CUSTOMER_CONFIRMATION_PENDING_ETOKEN, OrderStatus.ONGOING -> {
                showDateTimeViews(View.GONE)
                getString(R.string.ongoing)
            }
            OrderStatus.CONFIRMED -> {
                showDateTimeViews(View.GONE)
               getString(R.string.confirmed)
            }
            OrderStatus.REACHED -> {
                showDateTimeViews(View.GONE)
                getString(R.string.reached)
            }
            OrderStatus.SCHEDULED,
            OrderStatus.DRIVER_PENDING,
            OrderStatus.DRIVER_APPROVED -> {
                showDateTimeViews(View.GONE)
                getString(R.string.Scheduled)
            }
            else -> {
                showDateTimeViews(View.GONE)
                getString(R.string.cancelled)
            }
        }
        tvDropOffLocation.text = if (orderData?.dropoff_address.isNullOrEmpty() == true) orderData?.pickup_address else orderData?.dropoff_address
    }

    private fun showDateTimeViews(visibility: Int) {
        tvStartedAt.visibility = visibility
        tvStartTime.visibility = visibility
        tvCompletedAt.visibility = visibility
        tvCompletedTime.visibility = visibility
    }


    private fun getFinalCharge(orderDetail: Order?): String {
        val price = (orderDetail?.payment?.product_quantity
                ?: 1).times(orderDetail?.payment?.product_per_quantity_charge?.toDouble()
                ?: 1.00).plus(orderDetail?.payment?.product_per_distance_charge?.toDouble() ?: 0.00)
                .plus(orderDetail?.payment?.product_per_weight_charge?.toDouble() ?: 0.00)
                .plus(orderDetail?.payment?.product_alpha_charge?.toDouble() ?: 0.00)
        when (orderDetail?.category_id) {
            1 -> {

            }

            2 -> {

            }

            3 -> {

            }
        }
        return String.format("%.2f", price)
    }

    override fun onApiSuccess(listBookings: ArrayList<Order?>) {

    }

    override fun onOrderDetailsSuccess(response: Order?) {
        order = response
        if (response?.order_status == OrderStatus.SCHEDULED
                || response?.order_status == OrderStatus.DRIVER_PENDING
                || response?.order_status == OrderStatus.DRIVER_APPROVED) {
            tvCancel.visibility = View.VISIBLE
        }
        response?.brand?.name = response?.brand?.product_name
        setUserData(response?.driver)
        setPaymentData(response)
        setBookingData(response)
//        tvBookingStatus.text = when (response?.order_status) {
//            OrderStatus.SERVICE_COMPLETE -> {
//                tvStartTime.text = DateUtils.getFormattedTimeForBooking(response.cRequest?.started_at
//                        ?: "")
//                tvCompletedTime.text = DateUtils.getFormattedTimeForBooking(response.cRequest?.updated_at
//                        ?: "")
//                showDateTimeViews(View.VISIBLE)
//                getString(R.string.completed)
//            }
//            OrderStatus.SCHEDULED, OrderStatus.DRIVER_PENDING, OrderStatus.DRIVER_APPROVED -> {
//                showDateTimeViews(View.GONE)
//                getString(R.string.Scheduled)
//            }
//            else -> {
//                showDateTimeViews(View.GONE)
//                getString(R.string.cancelled)
//            }
//        }
        viewFlipperExtraDetails.displayedChild = 1
//        if (response?.ratingByUser == null || response.ratingByUser?.ratings == null || response.ratingByUser?.ratings == 0) {
//            rlProfile.visibility = View.GONE
//        } else {
        val rating = if (response?.ratingByUser?.ratings ?: 0 <= 5) {
            response?.ratingByUser?.ratings ?: 0
        } else {
            5
        }
        if (rating == 0) {
            ivRating.visibility = View.INVISIBLE
        } else {
            ivRating.visibility = View.VISIBLE
            ivRating?.setImageResource(ratingDrawables[rating - 1])
        }

//        }
    }

    override fun showLoader(isLoading: Boolean) {

    }

    override fun apiFailure() {
        viewFlipperExtraDetails.displayedChild = 2
        rootView.showSWWerror()
    }

    override fun handleApiError(code: Int?, error: String?) {
        viewFlipperExtraDetails.displayedChild = 2
        if (code == StatusCode.UNAUTHORIZED) {
            AppUtils.logout(this)
        } else {
            rootView.showSnack(error ?: "")
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.detachView()
    }

    private fun showCancellationDialog() {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(true)
        dialog.setCanceledOnTouchOutside(true)
        dialog.setContentView(R.layout.dialog_cancel_reason)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

       // val etMessage = dialog.findViewById(R.id.etMessage) as EditText
//        val cancelSpinner = dialog?.findViewById<Spinner>(R.id.cancelSpinner)
//
//        val cancelReason = arrayOf("driver told to cancel", "ETA was too long",
//                "driver's call not reachable", "Booked by mistake")
//        if (cancelSpinner != null) {
//            val arrayAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, cancelReason)
//            cancelSpinner.adapter = arrayAdapter
//
//            cancelSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
//                override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
//                    cancelReason[position]
//                    // Toast.makeText(this@MainActivity, getString(R.string.selected_item) + " " + personNames[position], Toast.LENGTH_SHORT).show()
//                }
//
//                override fun onNothingSelected(parent: AdapterView<*>) {
//                    // Code to perform some action when nothing is selected
//                }
//            }
//        }

        val cancelGroup = dialog.findViewById<RadioGroup>(R.id.cancelGroup)
        val btn1 = dialog.findViewById<RadioButton>(R.id.btn1)
        val btn2 = dialog.findViewById<RadioButton>(R.id.btn2)
        val btn3 = dialog.findViewById<RadioButton>(R.id.btn3)

        val tvSubmit = dialog.findViewById(R.id.tvSubmit) as TextView
        tvSubmit.setOnClickListener {
            if (cancelGroup?.checkedRadioButtonId.toString().trim().isNotEmpty()) {
                val map = HashMap<String, String>()
                map["order_id"] = order?.order_id.toString()
                map["cancel_reason"] = cancelGroup?.checkedRadioButtonId.toString().trim()
                if (CheckNetworkConnection.isOnline(this)) {
                    presenter.requestCancelApiCall(map)
                    dialog.dismiss()
                } else {
                    CheckNetworkConnection.showNetworkError(rootView)
                }
            } else {
                Toast.makeText(this, getString(R.string.cancellation_reason_validation_text), Toast.LENGTH_SHORT).show()
            }
        }

        dialog.show()
        dialog.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
    }

    override fun onCancelApiSuccess() {
        Toast.makeText(this, R.string.order_cancelled, Toast.LENGTH_LONG).show()
        finish()
    }

    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(LocaleManager.setLocale(newBase))
    }
}
