package com.buraq24.customer.ui.chatModule

import com.buraq24.utilities.basearc.BasePresenter
import com.buraq24.utilities.basearc.BaseView
import com.buraq24.utilities.chatModel.ChatListing
import com.buraq24.utilities.chatModel.ChatMessageListing

class ChatContract{

    interface View : BaseView {
        fun chatLogsApiSuccess(chatList: ArrayList<ChatMessageListing>?)
    }

    interface Presenter : BasePresenter<View> {
        fun getChatLogsApiCall(authorization : String , limit: Int, skip: Int)
    }
}