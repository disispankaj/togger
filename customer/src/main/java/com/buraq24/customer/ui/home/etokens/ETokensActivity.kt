package com.buraq24.customer.ui.home.etokens

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.buraq24.customer.R
import com.buraq24.getCurentDateStringUtc
import com.buraq24.showSWWerror
import com.buraq24.showSnack
import com.buraq24.customer.webservices.models.etokens.Brand
import com.buraq24.customer.webservices.models.etokens.ETokensModel
import com.buraq24.customer.webservices.models.etokens.History
import com.buraq24.utilities.*
import com.buraq24.utilities.constants.CATEGORY_BRAND_ID
import com.buraq24.utilities.constants.REQUEST_DISTANCE
import com.buraq24.utilities.constants.TOKEN_ID
import com.buraq24.utilities.location.LocationProvider
import com.google.android.gms.tasks.OnSuccessListener
import kotlinx.android.synthetic.main.activity_etokens.*

class ETokensActivity : AppCompatActivity(), ETokensContract.View {

    private var offersAdapter: OffersETokenAdapter? = null

    private var purchasedAdapter: PurchasedETokensAdapter? = null

    private val purchasedList: ArrayList<History>? = ArrayList()

    private val offersList: ArrayList<Brand>? = ArrayList()

    private val presenter = ETokensPresenter()

    private var locationProvider: LocationProvider? = null

    private var categoryBrandId: Int? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_etokens)
        presenter.attachView(this)
        locationProvider = LocationProvider.CurrentLocationBuilder(this).build()
        setAdapters()
        setListeners()
        if (intent.getBooleanExtra("want_selection", false)) {
            categoryBrandId = intent.getIntExtra(CATEGORY_BRAND_ID, 0)
            tvSelectBrand.visibility = View.VISIBLE
        } else {
            tvSelectBrand.visibility = View.GONE
        }
        eTokenApiCalls()
    }

    private fun setListeners() {
        tvBack.setOnClickListener {
            onBackPressed()
        }
        swipeRefreshLayout.setOnRefreshListener {
            eTokenApiCalls()
        }

        tvSelectBrand.setOnClickListener {
            val intent = Intent()
            intent.putExtra(TOKEN_ID, purchasedList?.get(purchasedAdapter?.prevSelectedPosition
                    ?: 0)?.organisation_coupon_user_id)
            setResult(Activity.RESULT_OK, intent)
            finish()
        }
    }

    private fun setAdapters() {
        purchasedAdapter = PurchasedETokensAdapter(this, purchasedList)
        rvPurchasedToken.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        rvPurchasedToken.adapter = purchasedAdapter
        offersAdapter = OffersETokenAdapter(this, offersList)
        rvOffers.layoutManager = LinearLayoutManager(this)
        rvOffers.adapter = offersAdapter
    }

    private fun eTokenApiCalls() {
        locationProvider?.getLastKnownLocation(OnSuccessListener {
            if (CheckNetworkConnection.isOnline(this)) {
                val map = HashMap<String, String>()
                map["category_id"] = CategoryId.MINERAL_WATER.toString()
                if (categoryBrandId != null) {
                    map["category_brand_id"] = categoryBrandId.toString()
                }
                map["latitude"] = it.latitude.toString()
                map["longitude"] = it.longitude.toString()
                map["distance"] = REQUEST_DISTANCE.toString()
                map["take"] = Int.MAX_VALUE.toString()
                map["order_timings"] = getCurentDateStringUtc().toString()
                presenter.eTokensApi(map)
            } else {
                CheckNetworkConnection.showNetworkError(rootView)
            }
        })
    }

    override fun onApiSuccess(response: ETokensModel?) {
        purchasedList?.clear()
        offersList?.clear()
        response?.history?.let { purchasedList?.addAll(it) }
        response?.brands?.let { offersList?.addAll(it) }
        if (purchasedList?.isEmpty() == true) {
            tvPurchased.visibility = View.GONE
            tvSelectBrand.visibility = View.GONE
        } else {
            purchasedList?.get(0)?.isSelected = true
            tvPurchased.visibility = View.VISIBLE
            tvSelectBrand.visibility = View.VISIBLE
        }

        if (offersList?.isEmpty() == true) {
            tvNoOffersAvailable.visibility = View.VISIBLE
        } else {
            tvNoOffersAvailable.visibility = View.GONE
        }
        purchasedAdapter?.notifyDataSetChanged()
        offersAdapter?.notifyDataSetChanged()
    }

    override fun showLoader(isLoading: Boolean) {
        swipeRefreshLayout.isRefreshing = isLoading
    }

    override fun apiFailure() {
        rootView?.showSWWerror()
    }

    override fun handleApiError(code: Int?, error: String?) {
        rootView?.showSnack(error.toString())
    }

    fun buyEToken(organisation_coupon_id: Int?) {
        AlertDialogUtil.getInstance().createOkCancelDialog(this, R.string.buy_etoken,
                R.string.buy_etoken_confirmation, R.string.yes, R.string.no,
                true, object : AlertDialogUtil.OnOkCancelDialogListener {
            override fun onOkButtonClicked() {
                if (CheckNetworkConnection.isOnline(this@ETokensActivity)) {
                    presenter.buyETokenApi(organisation_coupon_id.toString())
                } else {
                    CheckNetworkConnection.showNetworkError(rootView)
                }
            }

            override fun onCancelButtonClicked() {
                // Cancel automatically
            }

        }).show()
    }

    override fun onBuyETokenApiSuccess() {
        eTokenApiCalls()
    }

    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(LocaleManager.setLocale(newBase))
    }
}
