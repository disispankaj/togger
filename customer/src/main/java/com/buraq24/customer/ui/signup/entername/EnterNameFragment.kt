package com.buraq24.customer.ui.signup.entername


import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.buraq24.customer.R
import com.buraq24.showSWWerror
import com.buraq24.showSnack
import com.buraq24.customer.ui.home.HomeActivity
import com.buraq24.utilities.*
import com.buraq24.utilities.constants.ACCESS_TOKEN
import com.buraq24.utilities.constants.ACCESS_TOKEN_KEY
import com.buraq24.utilities.constants.PROFILE
import com.buraq24.utilities.webservices.models.AppDetail
import kotlinx.android.synthetic.main.fragment_enter_name.*
import java.util.*

/**
 * Fragment to get the user name after successful otp verification to complete the sign up process
 * */
class EnterNameFragment : Fragment(), EnterNameContract.View, DateUtils.OnDateSelectedListener {

    private var date: String = ""

    override fun dateTimeSelected(dateCal: Calendar) {
        date = DateUtils.getFormattedDateForUTC(dateCal)
        tvDob.text = DateUtils.getFormattedTime(dateCal)
    }

    override fun timeSelected(dateCal: Calendar) {

    }

    private val presenter = EnterNamePresenter()

    private lateinit var dialogIndeterminate: DialogIndeterminate

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_enter_name, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.attachView(this)
        date = DateUtils.getFormattedDateForUTC(Calendar.getInstance())
        dialogIndeterminate = DialogIndeterminate(activity)
        setListeners()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        presenter.detachView()
    }

    private fun setListeners() {
        fabNext.setOnClickListener {
            val name = etName.text.toString().trim()
            val email = etEmail.text.toString().trim()
            val dob = tvDob.text.toString().trim()
            if (name.isEmpty()) {
                fabNext.showSnack(getString(R.string.name_empty_validation_message)) // User name is not filled by the user
            } else if (email.isEmpty() || !Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                fabNext.showSnack(getString(R.string.enter_your_email))
            } else if (dob.isEmpty()) {
                fabNext.showSnack(getString(R.string.select_ur_dob))
            } else {
                if (CheckNetworkConnection.isOnline(activity)) {
                    presenter.addNameApiCall(name, email, dob) // Add name api call
                } else {
                    CheckNetworkConnection.showNetworkError(rootView)
                }
            }
        }
        ivBack.setOnClickListener { fragmentManager?.popBackStack("backstack", FragmentManager.POP_BACK_STACK_INCLUSIVE) }

        tvDob.setOnClickListener {
            DateUtils.openDateDialogPast(it.context, this, false,
                    Calendar.getInstance())
        }
    }

    /* Api success for add name api*/
    override fun onApiSuccess(response: AppDetail?) {
        /* Sign up successful. Redirects user to home screen*/
        ACCESS_TOKEN = response?.access_token ?: ""
        SharedPrefs.with(activity).save(ACCESS_TOKEN_KEY, response?.access_token ?: "")
        SharedPrefs.with(activity).save(PROFILE, response)
        activity?.finishAffinity()
        startActivity(Intent(activity, HomeActivity::class.java))
    }

    override fun showLoader(isLoading: Boolean) {
        dialogIndeterminate.show(isLoading)
    }

    override fun apiFailure() {
        fabNext.showSWWerror()
    }

    override fun handleApiError(code: Int?, error: String?) {
        fabNext.showSnack(error.toString())
    }

}
