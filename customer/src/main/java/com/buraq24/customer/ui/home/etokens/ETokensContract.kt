package com.buraq24.customer.ui.home.etokens

import com.buraq24.customer.webservices.models.etokens.ETokensModel
import com.buraq24.customer.webservices.models.order.Order
import com.buraq24.utilities.basearc.BasePresenter
import com.buraq24.utilities.basearc.BaseView
import org.json.JSONObject

class ETokensContract{

    interface View: BaseView{
        fun onApiSuccess(response: ETokensModel?)
        fun onBuyETokenApiSuccess()
    }

    interface Presenter: BasePresenter<View>{
        fun eTokensApi(map: Map<String, String>)
        fun buyETokenApi(tokenId: String?)
    }
}