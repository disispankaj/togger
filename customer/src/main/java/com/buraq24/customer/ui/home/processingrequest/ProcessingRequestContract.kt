package com.buraq24.customer.ui.home.processingrequest

import com.buraq24.customer.webservices.models.ServiceRequestModel
import com.buraq24.customer.webservices.models.homeapi.ServiceDetails
import com.buraq24.customer.webservices.models.order.Order
import com.buraq24.utilities.basearc.BasePresenter
import com.buraq24.utilities.basearc.BaseView

class ProcessingRequestContract{

    interface View: BaseView{
        fun onApiSuccess()
        fun handleOrderError(order: Order?)
    }

    interface Presenter: BasePresenter<View>{
        fun requestCancelApiCall(map: HashMap<String, String>)
    }
}