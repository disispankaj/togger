package com.buraq24.customer.ui.home.processingrequest

import com.buraq24.customer.webservices.ApiResponse
import com.buraq24.customer.webservices.RestClient
import com.buraq24.customer.webservices.models.order.Order
import com.buraq24.utilities.StatusCode
import com.buraq24.utilities.basearc.BasePresenterImpl
import com.buraq24.getApiError
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ProcessingRequestPresenter : BasePresenterImpl<ProcessingRequestContract.View>(), ProcessingRequestContract.Presenter {

    override fun requestCancelApiCall(map: HashMap<String, String>) {
        getView()?.showLoader(true)
        RestClient.get().cancelService(map).enqueue(object : Callback<ApiResponse<Any>> {

            override fun onResponse(call: Call<ApiResponse<Any>>?, response: Response<ApiResponse<Any>>?) {
                getView()?.showLoader(false)
                if (response?.isSuccessful == true) {
                    if (response.body()?.statusCode == 200) {
                        getView()?.onApiSuccess()
                    } else {
                        val errorBody = response.errorBody()?.string()
                        val errorResponse: ApiResponse<Order> = Gson().fromJson(errorBody, object : TypeToken<ApiResponse<Order>>() {}.type)
                        if(errorResponse.statusCode == StatusCode.BAD_REQUEST && errorResponse.result != null) {
                            getView()?.handleOrderError(errorResponse.result)
                        }else{
                            val errorModel = getApiError(errorBody)
                            getView()?.handleApiError(errorModel.statusCode, errorModel.msg)
                        }
                    }
                } else {
                    val errorBody = response?.errorBody()?.string()
                    val errorResponse: ApiResponse<Order> = Gson().fromJson(errorBody, object : TypeToken<ApiResponse<Order>>() {}.type)
                    if(errorResponse.statusCode == StatusCode.BAD_REQUEST && errorResponse.result != null) {
                        getView()?.handleOrderError(errorResponse.result)
                    }else{
                        val errorModel = getApiError(errorBody)
                        getView()?.handleApiError(errorModel.statusCode, errorModel.msg)
                    }
                }
            }

            override fun onFailure(call: Call<ApiResponse<Any>>?, t: Throwable?) {
                getView()?.showLoader(false)
                getView()?.apiFailure()
            }
        })
    }

}