package com.buraq24.customer.ui.home.processingrequest


import android.app.Activity
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.*
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.content.LocalBroadcastManager
import android.transition.Explode
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast

import com.buraq24.customer.R
import com.buraq24.showSWWerror
import com.buraq24.showSnack
import com.buraq24.customer.ui.home.HomeActivity
import com.buraq24.customer.ui.home.deliverystarts.DeliveryStartsFragment
import com.buraq24.customer.ui.home.invoice.InvoiceFragment
import com.buraq24.customer.ui.home.services.ServicesFragment
import com.buraq24.customer.utils.AppUtils
import com.buraq24.customer.webservices.ApiResponse
import com.buraq24.customer.webservices.models.ServiceRequestModel
import com.buraq24.customer.webservices.models.order.Order
import com.buraq24.utilities.*
import com.buraq24.utilities.constants.ACCESS_TOKEN
import com.buraq24.utilities.constants.ORDER
import com.buraq24.utilities.constants.SUCCESS_CODE
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import io.socket.client.Ack
import io.socket.client.Socket
import io.socket.emitter.Emitter
import kotlinx.android.synthetic.main.fragment_processing_request.*
import org.json.JSONObject


class ProcessingRequestFragment : Fragment(), ProcessingRequestContract.View {

    private val timeLimit = 50000L

    private lateinit var serviceRequest: ServiceRequestModel

    private val presenter = ProcessingRequestPresenter()

    private var orderDetail: Order? = null

    private var dialogIndeterminate: DialogIndeterminate? = null

    private var isPaused = false

    private var isTimerFinished = false

    private var handlerCheckOrder = Handler()

    private var isServicesAccepted = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            enterTransition = Explode()
            exitTransition = Explode()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_processing_request, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.attachView(this)
        dialogIndeterminate = DialogIndeterminate(activity)
        setData()
        progressBar.max = timeLimit.toInt()
        if (arguments?.getBoolean("changeTimeOut", false) == true) {
            progressBar?.isIndeterminate = true
            isTimerFinished = true
//            checkOrderStatus(0)
        } else {
            startTimer()
        }
        progressBar.visibility = View.VISIBLE
        setListeners()
    }

    override fun onResume() {
        super.onResume()
        AppSocket.get().on(Socket.EVENT_CONNECT, onConnect)
        isPaused = false
        checkOrderStatus(0)
//        if (!isTimerFinished) {
        startEventSocket()
        context?.let {
            LocalBroadcastManager
                    .getInstance(it)
                    .registerReceiver(notificationBroadcast, IntentFilter(Broadcast.NOTIFICATION))
        }
//        }
    }

    override fun onPause() {
        super.onPause()
        AppSocket.get().off(Socket.EVENT_CONNECT, onConnect)
        isPaused = true
        stopSocketEvent()
    }

    private val onConnect = Emitter.Listener {
        //        if (isTimerFinished) {
        checkOrderStatus(0)
//        }
    }

    private val notificationBroadcast = object : BroadcastReceiver() {

        override fun onReceive(context: Context?, intent: Intent?) {
            if (intent?.getIntExtra("order_id", 0) == orderDetail?.order_id?.toInt() && !isServicesAccepted) {
                checkOrderStatus(0)
            }
        }
    }

    private fun startEventSocket() {
        AppSocket.get().on(Events.ORDER_EVENT) {
            if (JSONObject(it[0].toString()).has("type")) {
                when (JSONObject(it[0].toString()).getString("type")) {

                    OrderEventType.SERVICE_REACHED , OrderEventType.SERVICE_ACCEPT -> {
                        isServicesAccepted = true
                        try {
                            val orderJson = JSONObject(it[0].toString()).getJSONArray("order").get(0).toString()
                            val orderModel = Gson().fromJson(orderJson, Order::class.java)
                            if (orderModel.order_id == orderDetail?.order_id) {
                                timer.cancel()
                                Logger.e(Events.ORDER_EVENT, it[0].toString())
                                Handler(Looper.getMainLooper()).post {
                                    if (orderModel.future == ServiceRequestModel.SCHEDULE) {
                                        showScheduledConfirmationDialog()
                                    } else {
                                        openBookingConfirmedFragment(orderJson)
                                    }
                                }
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }
                    OrderEventType.SERVICE_REJECT -> {
                        isServicesAccepted = true
                        Handler(Looper.getMainLooper()).post {
                            if (arguments?.getBoolean("changeTimeOut", false) == false) {
                                openBookingFragment()
                            } else {
                                fragmentManager?.popBackStack("backstack", FragmentManager.POP_BACK_STACK_INCLUSIVE)
                                val fragment = ServicesFragment()
                                (activity as HomeActivity).serviceRequestModel = ServiceRequestModel()
                                (activity as HomeActivity).servicesFragment = fragment
                                fragmentManager?.beginTransaction()?.add(R.id.container, fragment)?.addToBackStack("backstack")?.commit()
                            }
                        }
                    }
                }
            }
        }
    }

    private fun stopSocketEvent() {
        AppSocket.get().off(Events.ORDER_EVENT)
    }

    private fun setData() {
        orderDetail = Gson().fromJson(arguments?.getString(ORDER), Order::class.java)
        serviceRequest = (activity as HomeActivity).serviceRequestModel
        tvProductName.text = when (orderDetail?.category_id) {
            CategoryId.GAS -> {
                tvQuantity.text = String.format("%s × %s", orderDetail?.brand?.name, orderDetail?.payment?.product_quantity)
                getString(R.string.gas)
            }
            CategoryId.MINERAL_WATER -> {
                tvQuantity.text = String.format("%s × %s", orderDetail?.brand?.name, orderDetail?.payment?.product_quantity)
                serviceRequest.brandName
            }
            CategoryId.WATER_TANKER -> {
                tvQuantity.text = String.format("%s × %s", orderDetail?.brand?.name, orderDetail?.payment?.product_quantity)
                getString(R.string.water_tanker)
            }
            CategoryId.FREIGHT,CategoryId.FREIGHT_2 -> {
                tvQuantity.text = """${orderDetail?.brand?.name}"""
                serviceRequest.brandName
            }
            else -> {
                "Default"
            }
        }
        tvLocation.text = orderDetail?.dropoff_address
        var total = 0.0
        when (orderDetail?.payment?.payment_type) {
            PaymentType.CASH -> {
                total = orderDetail?.payment?.final_charge?.toDouble() ?: 0.0
            }
            PaymentType.CARD -> {
                total = orderDetail?.payment?.final_charge?.toDouble() ?: 0.0
            }
            PaymentType.E_TOKEN -> {
                total = 0.0
            }
        }
        tvTotal.text = String.format("%.2f %s", total, getString(R.string.currency))
    }

    private fun setListeners() {
        tvCancel.setOnClickListener {
            if (CheckNetworkConnection.isOnline(activity)) {
                val map = HashMap<String, String>()
                map["order_id"] = orderDetail?.order_id.toString()
                map["cancel_reason"] = Constants.DEFAULT_REASON_CUSTOMER_CANCELLED
                presenter.requestCancelApiCall(map)
            } else {
                CheckNetworkConnection.showNetworkError(rootView)
            }
        }
    }


    private val timer = object : CountDownTimer(timeLimit, 16) {
        override fun onFinish() {
            isTimerFinished = true
            if (!isPaused) {
                checkOrderStatus(0)
            }
        }

        override fun onTick(millisUntilFinished: Long) {
            progressBar.progress = timeLimit.toInt() - millisUntilFinished.toInt()
        }

    }

    private fun startTimer() {
        timer.start()
    }

    private fun checkOrderStatus(delay: Long) {
        try {
            handlerCheckOrder.removeCallbacks(runnable)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        handlerCheckOrder.postDelayed(runnable, delay)
    }

    private val runnable = Runnable { if (isTimerFinished) {
            checkOrderStatus(5000)
        }
        val request = JSONObject()
        request.put("type", OrderEventType.CUSTOMER_SINGLE_ORDER)
        request.put("access_token", ACCESS_TOKEN)
        request.put("order_token", orderDetail?.order_token)
        AppSocket.get().emit(Events.COMMON_EVENT, request, Ack {
            val response = Gson().fromJson<ApiResponse<Order>>(it[0].toString(),
                    object : TypeToken<ApiResponse<Order>>() {}.type)
            if (response.statusCode == SUCCESS_CODE) {
                val orderModel = response.result
                val orderJson = JSONObject(it[0]?.toString()).getString("result")
                Handler(Looper.getMainLooper()).post {
                    when (orderModel?.order_status) {

                        OrderStatus.ONGOING, OrderStatus.CONFIRMED,OrderStatus.REACHED -> {
                            openBookingConfirmedFragment(orderJson)
                        }

                        OrderStatus.SERVICE_COMPLETE -> {
                            openInvoiceFragment(orderJson)
                        }

                        OrderStatus.SEARCHING -> {
                            /* Keep searching */
                        }

                        OrderStatus.SCHEDULED -> {
                            showScheduledConfirmationDialog()
                        }
                        else -> {
                            if (arguments?.getBoolean("changeTimeOut", false) == false) {
                                openBookingFragment()
                            } else {
                                fragmentManager?.popBackStack("backstack", FragmentManager.POP_BACK_STACK_INCLUSIVE)
                                val fragment = ServicesFragment()
                                (activity as HomeActivity).serviceRequestModel = ServiceRequestModel()
                                (activity as HomeActivity).servicesFragment = fragment
                                fragmentManager?.beginTransaction()?.add(R.id.container, fragment)?.addToBackStack("backstack")?.commit()
                            }
                        }
                    }
                }
            } else if (response.statusCode == StatusCode.UNAUTHORIZED) {
                AppUtils.logout(activity as Activity)
            }
        })
    }

    private fun openBookingConfirmedFragment(orderJson: String?) {
        val fragment = DeliveryStartsFragment()
        val bundle = Bundle()
        bundle.putString("order", orderJson)
        fragment.arguments = bundle
        try {
            fragmentManager?.popBackStack("backstack", FragmentManager.POP_BACK_STACK_INCLUSIVE)
            fragmentManager?.beginTransaction()?.add(R.id.container, fragment)?.addToBackStack("backstack")?.commitAllowingStateLoss()
        }catch (e:java.lang.Exception){
            e.printStackTrace()
        }
    }

    private fun openInvoiceFragment(orderJson: String?) {
        val fragment = InvoiceFragment()
        val bundle = Bundle()
        bundle.putString("order", orderJson)
        fragment.arguments = bundle
        fragmentManager?.popBackStack("backstack", FragmentManager.POP_BACK_STACK_INCLUSIVE)
        fragmentManager?.beginTransaction()?.add(R.id.container, fragment)?.addToBackStack("backstack")?.commit()
    }

    private fun openBookingFragment() {
        if (activity != null) {
            Toast.makeText(activity, R.string.currently_no_driver_available, Toast.LENGTH_SHORT).show()
        }
        fragmentManager?.popBackStackImmediate()
    }

    override fun onApiSuccess() {
        timer.cancel()
        Toast.makeText(activity, R.string.request_cancelled_successfully, Toast.LENGTH_SHORT).show()
        fragmentManager?.popBackStackImmediate()
    }

    override fun showLoader(isLoading: Boolean) {
        dialogIndeterminate?.show(isLoading)
    }

    override fun apiFailure() {
        rootView.showSWWerror()
    }

    override fun handleApiError(code: Int?, error: String?) {
        if (code == StatusCode.UNAUTHORIZED) {
            AppUtils.logout(activity as Activity)
        } else {
            rootView.showSnack(error.toString())
        }
    }

    override fun handleOrderError(order: Order?) {
        when (order?.order_status) {
            OrderStatus.CUSTOMER_CANCEL, OrderStatus.DRIVER_CANCELLED, OrderStatus.DRIVER_SCHEDULED_CANCELLED, OrderStatus.SYS_SCHEDULED_CANCELLED, OrderStatus.SERVICE_TIMEOUT -> {
                timer.cancel()
                Toast.makeText(activity, R.string.request_cancelled_successfully, Toast.LENGTH_SHORT).show()
                fragmentManager?.popBackStackImmediate()
            }

            OrderStatus.SERVICE_COMPLETE->{
                openInvoiceFragment(Gson().toJson(orderDetail))
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        timer.cancel()
        dialogIndeterminate?.show(false)
        handlerCheckOrder.removeCallbacks(runnable)
        presenter.detachView()

    }

    private fun showScheduledConfirmationDialog() {
        AlertDialogUtil.getInstance().createOkCancelDialog(activity, R.string.congratulations,
                R.string.service_booked_successfully, R.string.okay, 0,
                false, object : AlertDialogUtil.OnOkCancelDialogListener {
            override fun onOkButtonClicked() {
                openHomeFragment()
            }

            override fun onCancelButtonClicked() {
                openHomeFragment()
            }
        }).show()
    }

    private fun openHomeFragment() {
        fragmentManager?.popBackStack("backstack", FragmentManager.POP_BACK_STACK_INCLUSIVE)
        val fragment = ServicesFragment()
        (activity as HomeActivity).serviceRequestModel = ServiceRequestModel()
        (activity as HomeActivity).servicesFragment = fragment
        fragmentManager?.beginTransaction()?.add(R.id.container, fragment)?.addToBackStack("backstack")?.commit()
    }

}
