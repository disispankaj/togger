package com.buraq24.customer.ui.signup.login


import android.content.Context
import android.content.Intent
import android.graphics.Typeface
import android.os.Bundle
import android.support.v4.app.Fragment
import android.text.SpannableString
import android.text.style.StyleSpan
import android.text.style.UnderlineSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.buraq24.customer.MainActivity
import com.buraq24.customer.R
import com.buraq24.Utils
import com.buraq24.getLanguage
import com.buraq24.getLanguageId
import com.buraq24.showSnack
import com.buraq24.customer.ui.home.WebViewActivity
import com.buraq24.customer.ui.signup.verifytop.OtpFragment
import com.buraq24.customer.webservices.models.SendOtp
import com.buraq24.utilities.*
import com.buraq24.utilities.constants.*
import com.buraq24.utilities.location.LocationProvider
import com.buraq24.utilities.webservices.BASE_URL
import com.google.android.gms.tasks.OnSuccessListener
import com.hbb20.CountryCodePicker
import kotlinx.android.synthetic.main.fragment_login.*
import java.util.*
import kotlin.collections.HashMap

/**
 * A Fragment to get the details for the login process(if the user is already registered)
 * or sign up process(if the user is not registered)
 * */

class LoginFragment : Fragment(), View.OnClickListener, LoginContract.View {

    private lateinit var locationProvider: LocationProvider

    private val presenter = LoginPresenter()

    private lateinit var dialogIndeterminate: DialogIndeterminate

    private var languagesAdapter: ArrayAdapter<String>? = null

    private var languages: Array<String>? = null

    private var isPhoneNoValid = false

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        /* signupCCP" is the ccp_selectionMemoryTag for Country code picker to remember previous
         country code selection*/
        val ccpPrefs = context?.getSharedPreferences("CCP_PREF_FILE", Context.MODE_PRIVATE)
        ccpPrefs?.edit()?.putString("signupCCP", "")?.apply()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        activity?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)
        return inflater.inflate(R.layout.fragment_login, container, false) // Inflate the layout for this fragment
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.attachView(this) // attach presenter
        dialogIndeterminate = DialogIndeterminate(activity)
        setLanguageAdapter()
        countryCodePicker.registerCarrierNumberEditText(etPhone) // registers the EditText with the country code picker
        setSpannableTermsAndConditions()
        setListeners()
        locationProvider = LocationProvider.CurrentLocationBuilder(activity, this).build()
        locationProvider.getLastKnownLocation(OnSuccessListener {
            SharedPrefs.with(activity).save(Constants.LATITUDE, it?.latitude ?: 0.0)
            SharedPrefs.with(activity).save(Constants.LONGITUDE, it?.longitude ?: 0.0)
        })
    }

    /* Make terms and conditions text spannable with underline and bold*/
    private fun setSpannableTermsAndConditions() {
        val content = SpannableString(getString(R.string.terms_and_conditions))
        content.setSpan(UnderlineSpan(), 0, content.length, 0)
        content.setSpan(StyleSpan(Typeface.BOLD), 0, content.length, 0)
        tvTermsConditions.text = content
    }

    private fun setListeners() {
        fabNext.setOnClickListener(this)
        countryCodePicker.setCountryForPhoneCode(91)
        countryCodePicker.setNumberAutoFormattingEnabled(false)
        countryCodePicker.setPhoneNumberValidityChangeListener(phoneNoValidationChangeListener)

        tvTermsConditions.setOnClickListener {
            val intent = Intent(activity, WebViewActivity::class.java)
            intent.putExtra(TITLE, getString(R.string.terms_and_conditions))
            intent.putExtra(URL_TO_LOAD, "${BASE_URL}/pages/Customer/" +
                    getLanguageId(LocaleManager.getLanguage(activity)))
            startActivity(intent)
        }
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.fabNext -> {
                if (isPhoneNoValid) {
                    if (isValidationOk()) {
                        locationProvider.getLastKnownLocation(OnSuccessListener {
                            SharedPrefs.with(activity).save(Constants.LATITUDE, it?.latitude ?: 0.0)
                            SharedPrefs.with(activity).save(Constants.LONGITUDE, it?.longitude
                                    ?: 0.0)
                            val map = HashMap<String, String>()
                            map["phone_code"] = countryCodePicker.selectedCountryCodeWithPlus
                            map["phone_number"] = etPhone.text.toString().replace(Regex("[^0-9]"), "")
                            map["timezone"] = TimeZone.getDefault().id
                            map["latitude"] = it?.latitude?.toString() ?: "0.0"
                            map["longitude"] = it?.longitude?.toString() ?: "0.0"
                            map["device_type"] = "Android"
                            if (CheckNetworkConnection.isOnline(activity)) {
                                presenter.sendOtpApiCall(map)
                            } else {
                                CheckNetworkConnection.showNetworkError(rootView)
                            }
                        })
                    } // if we are good with the validations
                }else {
                    fabNext?.showSnack(getString(R.string.phone_validation_message))}
            }
        }
    }
    /*Checks for the validations*/
    private fun isValidationOk(): Boolean {
        return when {
            etPhone.text.toString().trim().isEmpty() -> {
                fabNext?.showSnack(getString(R.string.phone_validation_message))// Invalid phone number
                false
            }
            cbTermsConditions?.isChecked == false -> {
                fabNext?.showSnack(getString(R.string.terms_and_conditions_validation_message)) // User not agreed to the terms and conditions
                false
            }
            else -> true
        }
    }

    /* Prepares the list of languages to be shown on the language selector*/
    private fun setLanguageAdapter() {
        languages = resources.getStringArray(R.array.languages)
        languagesAdapter = ArrayAdapter(activity, R.layout.layout_spinner_languages, languages)
        languagesAdapter?.setDropDownViewResource(R.layout.item_languages)
        spinnerLanguages.adapter = languagesAdapter
        /* sets selection of the spinner to the default or currently selected language*/
        spinnerLanguages.setSelection(getLanguageId(LocaleManager.getLanguage(activity)) - 1)
        spinnerLanguages.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                if (LocaleManager.getLanguage(activity) != getLanguage(position + 1)) {
                    SharedPrefs.get().save(PREFS_LANGUAGE_ID, position + 1)
                    SharedPrefs.get().save(LANGUAGE_CHANGED, true)
                    setNewLocale(getLanguage(position + 1), true)
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {

            }
        }
    }

    private val phoneNoValidationChangeListener = CountryCodePicker.PhoneNumberValidityChangeListener {
        isPhoneNoValid = it
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        locationProvider.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        locationProvider.onActivityResult(requestCode, resultCode, data)
    }

    /* Api success callback for send OTP api*/
    override fun onApiSuccess(response: SendOtp?, map: Map<String, String>) {
        SharedPrefs.with(activity).save(ACCESS_TOKEN_KEY, response?.access_token)
        ACCESS_TOKEN = response?.access_token ?: ""
        val fragment = OtpFragment()
        val bundle = Bundle()
        bundle.putString("phone_number", countryCodePicker.selectedCountryCodeWithPlus + "-" + etPhone.text.toString().replace(Regex("[^0-9]"), ""))
        bundle.putString("otp", response?.otp.toString())
        bundle.putString("access_token", response?.access_token)
        bundle.putSerializable("map", map as HashMap)
        fragment.arguments = bundle
        fragmentManager?.let { it1 ->
            Utils.replaceFragment(it1, fragment, R.id.container, OtpFragment::class.java.simpleName)
        }
    }


    override fun showLoader(isLoading: Boolean) {
        dialogIndeterminate.show(isLoading)
    }

    override fun apiFailure() {
        fabNext.showSnack(getString(R.string.sww_error))
    }

    override fun handleApiError(code: Int?, error: String?) {
        fabNext.showSnack(error.toString())
    }

    override fun onDestroyView() {
        super.onDestroyView()
        presenter.detachView() //detach the persenter
    }

    /* Sets the selected locale and reloads the views with new language*/
    private fun setNewLocale(language: String, restartProcess: Boolean): Boolean {
        LocaleManager.setNewLocale(activity, language)

        val intent = Intent(activity, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)

        if (restartProcess) {
            System.exit(0) //exits from the current process
        }
        return true
    }
}
