package com.buraq24.customer.ui.home.deliverystarts

import com.buraq24.customer.webservices.models.TrackingModel
import com.buraq24.customer.webservices.models.nearestroad.RoadItem
import com.buraq24.customer.webservices.models.order.Order
import com.buraq24.utilities.basearc.BasePresenter
import com.buraq24.utilities.basearc.BaseView
import org.json.JSONObject

class DeliveryStartsContract{

    interface View: BaseView{
        fun onApiSuccess(response: List<Order>?)
        fun polyLine(jsonRootObject: JSONObject)
        fun onCancelApiSuccess()
        fun snappedPoints(response: List<RoadItem>?, trackingModel: TrackingModel?)
    }

    interface Presenter: BasePresenter<View>{
        fun onGoingOrderApi()
        fun drawPolyLine(sourceLat: Double, sourceLong: Double, destLat: Double, destLong: Double, language: String?)
        fun requestCancelApiCall(map: HashMap<String, String>)
        fun getRoadPoints(trackingModel: TrackingModel?)
    }
}