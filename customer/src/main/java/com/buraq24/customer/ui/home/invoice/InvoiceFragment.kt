package com.buraq24.customer.ui.home.invoice


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.buraq24.customer.R
import com.buraq24.customer.ui.home.HomeActivity
import com.buraq24.customer.ui.home.rating.RatingFragment
import com.buraq24.customer.ui.home.services.ServicesFragment
import com.buraq24.customer.utils.AppUtils
import com.buraq24.customer.webservices.models.ServiceRequestModel
import com.buraq24.customer.webservices.models.order.Order
import com.buraq24.utilities.CategoryId
import com.buraq24.utilities.Constants
import com.buraq24.utilities.constants.ORDER
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_invoice.*
import java.util.*

class InvoiceFragment : Fragment() {

    private var order: Order? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_invoice, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setData()
        setListeners()
    }

    private fun setListeners() {
        tvNext.setOnClickListener {
            openRatingFragment()
        }
    }

    private fun setData() {
        order = Gson().fromJson(arguments?.getString("order"), Order::class.java)
        tvServiceName.text = when (order?.brand?.category_id) {
            CategoryId.GAS -> {
                tvQuantity.text = String.format("%s × %s", order?.brand?.name, order?.payment?.product_quantity)
                getString(R.string.gas)
            }
            CategoryId.MINERAL_WATER -> {
                tvQuantity.text = String.format("%s × %s", order?.brand?.name, order?.payment?.product_quantity)
                order?.brand?.brand_name
            }
            CategoryId.WATER_TANKER -> {
                tvQuantity.text = String.format("%s × %s", order?.brand?.name, order?.payment?.product_quantity)
                getString(R.string.water_tanker)
            }
            CategoryId.FREIGHT,CategoryId.FREIGHT_2 -> {
                tvQuantity.text = order?.brand?.name
                order?.brand?.brand_name
            }
            else -> {
                getString(R.string.default_category_name)
            }
        }

//        tvPrice.text = "${AppUtils.getFinalCharge(order)} ${getString(R.string.currency)}"
//        tvTotal.text = AppUtils.getFinalCharge(order) + " " + getString(R.string.currency)
        tvPrice.text = "${getFormattedDecimal(order?.payment?.final_charge?.toDouble()
                ?: 0.0)} ${getString(R.string.currency)}"
        tvTax.text = "${getFormattedDecimal(0.0)} ${getString(R.string.currency)}"
        tvGst.text = "${getFormattedDecimal(0.0)} ${getString(R.string.currency)}"
        tvTotal.text = "${getFormattedDecimal(order?.payment?.final_charge?.toDouble()
                ?: 0.0)} ${getString(R.string.currency)}"
    }

    private fun openRatingFragment() {
        fragmentManager?.popBackStack("backstack", FragmentManager.POP_BACK_STACK_INCLUSIVE)
        val fragment = RatingFragment()
        val bundle = Bundle()
        bundle.putString(ORDER, Gson().toJson(order))
        fragment.arguments = bundle
        fragmentManager?.beginTransaction()?.add(R.id.container, fragment)?.addToBackStack("backstack")?.commit()
    }

    private fun getFormattedDecimal(num: Double): String? {
        return String.format(Locale.US, "%.2f", num)
    }

}
