package com.buraq24.customer.ui.home.comfirmbooking

import com.buraq24.customer.webservices.models.ServiceRequestModel
import com.buraq24.customer.webservices.models.homeapi.ServiceDetails
import com.buraq24.customer.webservices.models.order.Order
import com.buraq24.utilities.basearc.BasePresenter
import com.buraq24.utilities.basearc.BaseView

class ConfirmBookingContract{

    interface View: BaseView{
        fun onApiSuccess(response: Order?)
    }

    interface Presenter: BasePresenter<View>{
        fun requestServiceApiCall(requestModel: ServiceRequestModel)
    }
}