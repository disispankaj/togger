package com.buraq24.customer.ui.home.services

import com.buraq24.customer.webservices.ApiResponse
import com.buraq24.customer.webservices.RestClient
import com.buraq24.customer.webservices.models.homeapi.HomeDriver
import com.buraq24.customer.webservices.models.homeapi.ServiceDetails
import com.buraq24.customer.webservices.models.nearestroad.RoadPoints
import com.buraq24.utilities.Logger
import com.buraq24.utilities.basearc.BasePresenterImpl
import com.buraq24.getApiError
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ServicePresenter : BasePresenterImpl<ServiceContract.View>(), ServiceContract.Presenter {

    private var api: Call<ApiResponse<ServiceDetails>>? = null

    override fun homeApi(map: Map<String, String>) {
        getView()?.showLoader(true)
        api?.cancel()
        api = RestClient.recreate().get().homeApi(map)
        api?.enqueue(object : Callback<ApiResponse<ServiceDetails>> {

            override fun onResponse(call: Call<ApiResponse<ServiceDetails>>?, response: Response<ApiResponse<ServiceDetails>>?) {
                getView()?.showLoader(false)
                if (response?.isSuccessful == true) {
                    if (response.body()?.statusCode == 200) {
                        getView()?.onApiSuccess(response.body()?.result)
                    } else {
                        getView()?.handleApiError(response.body()?.statusCode, response.body()?.msg)
                    }
                } else {
                    val errorModel = getApiError(response?.errorBody()?.string())
                    getView()?.handleApiError(errorModel.statusCode, errorModel.msg)
                }
            }

            override fun onFailure(call: Call<ApiResponse<ServiceDetails>>?, t: Throwable?) {
                if (t?.message.toString() != "Canceled" && t?.message.toString() != "Socket closed") {
                    getView()?.showLoader(false)
                    getView()?.apiFailure()
                    Logger.e("homeApi?Error", t?.message.toString())
                }
            }
        })
    }

    override fun getNearestRoadPoints(drivers: List<HomeDriver>?) {
        var pointsString = ""
        drivers?.forEach {
            pointsString = "$pointsString${it.latitude},${it.longitude}|"
        }
        pointsString = pointsString.removeSuffix("|")
        RestClient.get().getRoadPoints(pointsString).enqueue(object : Callback<RoadPoints> {

            override fun onResponse(call: Call<RoadPoints>?, response: Response<RoadPoints>?) {
                if (response?.isSuccessful == true) {
                    response.body()?.snappedPoints?.forEachIndexed { index, roadItem ->
                        drivers?.get(roadItem.originalIndex?:0)?.latitude = roadItem.location?.latitude
                        drivers?.get(roadItem.originalIndex?:0)?.longitude = roadItem.location?.longitude
                    }
                    getView()?.snappedDrivers(drivers)
                } /*else {
                    val errorModel = getApiError(response?.errorBody()?.string())
                    getView()?.handleApiError(errorModel.statusCode, errorModel.msg)
                }*/
            }

            override fun onFailure(call: Call<RoadPoints>?, t: Throwable?) {
//                if (t?.message.toString() != "Canceled" && t?.message.toString() != "Socket closed") {
//                    getView()?.showLoader(false)
//                    getView()?.apiFailure()
//                    Logger.e("homeApi?Error", t?.message.toString())
//                }
            }

        })
    }

}