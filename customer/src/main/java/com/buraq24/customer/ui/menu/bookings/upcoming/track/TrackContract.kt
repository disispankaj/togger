package com.buraq24.customer.ui.menu.bookings.upcoming.track

import com.buraq24.customer.webservices.models.TrackingModel
import com.buraq24.customer.webservices.models.nearestroad.RoadItem
import com.buraq24.customer.webservices.models.order.Order
import com.buraq24.utilities.basearc.BasePresenter
import com.buraq24.utilities.basearc.BaseView
import org.json.JSONObject

class TrackContract{

    interface View: BaseView{
        fun polyLine(jsonRootObject: JSONObject)
        fun snappedPoints(response: List<RoadItem>?, trackingModel: TrackingModel?)
    }

    interface Presenter: BasePresenter<View>{
        fun drawPolyLine(sourceLat: Double, sourceLong: Double, destLat: Double, destLong: Double, language: String?)
        fun getRoadPoints(trackingModel: TrackingModel?)
    }
}