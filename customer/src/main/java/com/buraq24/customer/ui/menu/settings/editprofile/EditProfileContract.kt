package com.buraq24.customer.ui.menu.settings.editprofile

import com.buraq24.utilities.basearc.BasePresenter
import com.buraq24.utilities.basearc.BaseView
import com.buraq24.utilities.webservices.models.AppDetail
import java.io.File

class EditProfileContract{

    interface View: BaseView{
        fun onApiSuccess(response: AppDetail?)
    }

    interface Presenter: BasePresenter<View>{
        fun updateProfileApiCall(name: String?, profilePicFile: File?,userData : AppDetail?)
    }

}