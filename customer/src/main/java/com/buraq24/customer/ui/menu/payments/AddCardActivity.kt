package com.buraq24.customer.ui.menu.payments

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.Toast
import com.buraq24.customer.R
import com.buraq24.customer.webservices.models.CardModel
import com.buraq24.customer.webservices.models.CouponModel
import com.buraq24.customer.webservices.models.paymentdetails.PaymentDetail
import com.buraq24.showSnack
import com.buraq24.utilities.CheckNetworkConnection
import com.buraq24.utilities.DialogIndeterminate
import com.buraq24.utilities.StatusCode.GET_NEW_CARD
import com.cooltechworks.creditcarddesign.CardEditActivity
import com.cooltechworks.creditcarddesign.CreditCardUtils
import kotlinx.android.synthetic.main.activity_add_card.*
import kotlinx.android.synthetic.main.activity_payments.*

class AddCardActivity : AppCompatActivity(), View.OnClickListener, PaymentsContract.View {

    // testing connecta- key_KH7QNPz32iZVDNyqSNNqybA
    var cardHolderName: String = ""
    var cardNumber: String = ""
    var expiry: String = ""
    var cvv: String = ""
    var monthCard: String = ""
    var yearCard: String = ""
    var isAddCard: Boolean = false
    private var dialogIndeterminate: DialogIndeterminate? = null

    private val presenter = PaymentsPresenter()

    companion object {
        fun start(context: Context) {
            var intent = Intent(context, AddCardActivity::class.java)
            context.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_card)

        presenter.attachView(this)
        dialogIndeterminate = DialogIndeterminate(this)
        editCard()
        initListener()
    }

    fun editCard() {
        val intent = Intent(this@AddCardActivity, CardEditActivity::class.java)
        startActivityForResult(intent, GET_NEW_CARD)
    }

    fun initListener() {
        ivBack.setOnClickListener(this)
        tvAddCard.setOnClickListener(this)
        cardForm.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            ivBack.id -> onBackPressed()

            tvAddCard.id -> validateCardDetail()

            cardForm.id -> editCard()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            cardHolderName = data?.getStringExtra(CreditCardUtils.EXTRA_CARD_HOLDER_NAME) ?: ""
            cardNumber = data?.getStringExtra(CreditCardUtils.EXTRA_CARD_NUMBER) ?: ""
            expiry = data?.getStringExtra(CreditCardUtils.EXTRA_CARD_EXPIRY) ?: ""
            cvv = data?.getStringExtra(CreditCardUtils.EXTRA_CARD_CVV) ?: ""
            cardForm.cardHolderName = cardHolderName
            cardForm.cardNumber = cardNumber
            cardForm.setCardExpiry(expiry)
            cardForm.cvv = cvv
        }
    }

    fun validateCardDetail() {
        when {
            cardHolderName.isEmpty() || cardNumber.isEmpty() || expiry.isEmpty() || cvv.isEmpty()
            -> tvAddCard.showSnack(getString(R.string.error_card))

            !expiry.contains("/") -> {
                tvAddCard.showSnack(getString(R.string.error_card))
            }

            else -> {
                val parts = expiry.split("/".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                val month = parts[0]
                val year = parts[1]
                monthCard = month
                yearCard = year
                if (CheckNetworkConnection.isOnline(this)) {
                    var map = HashMap<String, Any>()
                    map.put("cardNumber", cardNumber)
                    map.put("cvc", cvv)
                    map.put("month", month)
                    map.put("year", year)
                    presenter.addUserCard(map)
                } else {
                    CheckNetworkConnection.showNetworkError(rootView)
                }
            }
        }
    }

    override fun onPaymentDetailsApiSuccess(response: PaymentDetail?) {

    }

    override fun onAddCardSuccess(response: CardModel?) {
        setResult(Activity.RESULT_OK, Intent())
        finish()
    }

    override fun onGetCardSuccess(response: ArrayList<CardModel>?) {

    }

    override fun showLoader(isLoading: Boolean) {
        dialogIndeterminate?.show(isLoading)
    }

    override fun apiFailure() {

    }

    override fun handleApiError(code: Int?, error: String?) {

    }

    override fun onSetDefaultCard(response: CardModel?) {

    }

    override fun onBuyCounponSuccess(response: CouponModel?) {

    }
}
