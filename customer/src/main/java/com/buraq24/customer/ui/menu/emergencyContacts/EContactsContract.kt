package com.buraq24.driver.ui.home.emergencyContacts

import com.buraq24.customer.webservices.models.EContact
import com.buraq24.utilities.basearc.BasePresenter
import com.buraq24.utilities.basearc.BaseView

class EContactsContract{

    interface View: BaseView{
        fun onApiSuccess(response: List<EContact>?)
    }

    interface Presenter: BasePresenter<View>{
        fun getContactsList()
    }

}