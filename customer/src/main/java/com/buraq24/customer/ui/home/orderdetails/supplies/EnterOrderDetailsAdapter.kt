package com.buraq24.customer.ui.home.orderdetails.supplies

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.buraq24.customer.R
import com.buraq24.customer.webservices.models.homeapi.Category
import kotlinx.android.synthetic.main.item_select_brands.view.*
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.buraq24.Utils


class EnterOrderDetailsAdapter(private val context: Context?, private val categories: List<Category>?, private var prevSelectedPosition: Int) : RecyclerView.Adapter<EnterOrderDetailsAdapter.ViewHolder>() {

    private var itemSelectedListener: ItemSelectedListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_select_brands, parent, false))
    }

    override fun getItemCount(): Int {
        return categories?.size ?: 0
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(categories?.get(position))
    }


    inner class ViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {

        init {
            categories?.get(prevSelectedPosition)?.isSelected = true
            itemView?.imageView?.setOnClickListener {
                categories?.get(prevSelectedPosition)?.isSelected = false
                categories?.get(adapterPosition)?.isSelected = true
                prevSelectedPosition = adapterPosition
                itemSelectedListener?.onItemSelected(adapterPosition)
                notifyDataSetChanged()
            }
        }

        fun bind(categories: Category?) {
            var requestOptions = RequestOptions()
            requestOptions = requestOptions.transforms(RoundedCorners(context?.let { Utils.pxToDp(it, 6) }
                    ?: 6))
            context?.let { Glide.with(it).load(categories?.image_url).apply(requestOptions).into(itemView.imageView) }
            itemView?.imageView?.isSelected = categories?.isSelected ?: false
        }
    }

    fun setItemSelectedListener(listener: ItemSelectedListener?) {
        itemSelectedListener = listener
    }

    interface ItemSelectedListener {
        fun onItemSelected(position: Int)
    }
}
