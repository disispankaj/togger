package com.buraq24.customer.ui.menu.payments

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.LinearLayout
import com.buraq24.customer.R
import com.buraq24.showSnack
import com.buraq24.customer.ui.home.etokens.ETokensActivity
import com.buraq24.customer.utils.AppUtils
import com.buraq24.customer.webservices.models.CardModel
import com.buraq24.customer.webservices.models.CouponModel
import com.buraq24.customer.webservices.models.paymentdetails.PaymentDetail
import com.buraq24.utilities.*
import com.buraq24.utilities.Constants.FOR_ADD_CARD
import com.buraq24.utilities.constants.*
import kotlinx.android.synthetic.main.activity_payments.*

class PaymentsActivity : AppCompatActivity(), View.OnClickListener, PaymentsContract.View, CardAdapter.OnClickDelete {

    private var selectedPaymentType: String? = PaymentType.CASH

    private val presenter = PaymentsPresenter()

    private val RQ_SELECT_ETOKEN = 401
    lateinit var adapter: CardAdapter
    private lateinit var cardList: ArrayList<CardModel>
    private var dialogIndeterminate: DialogIndeterminate? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payments)
        presenter.attachView(this)
        dialogIndeterminate = DialogIndeterminate(this)
        selectedPaymentType = if (intent.hasExtra(PAYMENT_TYPE)) {
            intent.getStringExtra(PAYMENT_TYPE)
        } else {
            SharedPrefs.with(this).getString(PAYMENT_TYPE, PaymentType.CASH)
        }
        setSelectedPaymentMethod(selectedPaymentType)
        //    Uncomment to enable old etoken module
//        if (intent.hasExtra(CATEGORY_ID)) {
            tvViewToken.visibility = View.GONE

//            if (intent.getIntExtra(CATEGORY_ID, 0) != CategoryId.MINERAL_WATER) {
            tvToken.visibility = View.GONE
            rbToken.visibility = View.GONE
            tvViewToken.visibility = View.GONE
//
//            }
//        } else {
//            tvViewToken.visibility = View.VISIBLE
//            tvViewToken.text = getString(R.string.view_e_tokens)
//        }
        if (selectedPaymentType == PaymentType.E_TOKEN) {
            tvViewToken.visibility = View.VISIBLE
            tvViewToken.text = getString(R.string.select_e_token)
        }
        setListener()
        initCardAdapter()
    }

    private fun paymentDetailApiCall() {
        presenter.paymentDetail("", "")
    }

    override fun onResume() {
        super.onResume()
        if (CheckNetworkConnection.isOnline(this)) {
            presenter.getCardList()
        } else {
            CheckNetworkConnection.showNetworkError(rootView)
        }
    }

    private fun setListener() {
        tvBack.setOnClickListener(this)
        tvToken.setOnClickListener(this)
        rbToken.setOnClickListener(this)
        tvCod.setOnClickListener(this)
        rbCod.setOnClickListener(this)
        tvCard.setOnClickListener(this)
        rbCard.setOnClickListener(this)
        tvViewToken.setOnClickListener(this)
        tvAddCardPayment.setOnClickListener(this)
        tvPayNow.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.tvBack -> {
                finish()
            }
            R.id.tvToken, R.id.rbToken -> {
                if (intent.hasExtra(CATEGORY_ID)) {
                    setSelectedPaymentMethod(PaymentType.E_TOKEN)
                    tvViewToken.visibility = View.VISIBLE
                    tvViewToken.text = getString(R.string.select_e_token)
//                    setResult()
                } else {
                    rootView.showSnack(R.string.default_selecting_token_error)
                    setSelectedPaymentMethod(selectedPaymentType)
                }
            }
            R.id.tvCod, R.id.rbCod -> {
                setSelectedPaymentMethod(PaymentType.CASH)
                saveDefaultPaymentMethod()
                setResult()
            }
            R.id.tvCard, R.id.rbCard -> {
                setSelectedPaymentMethod(selectedPaymentType)
                rootView.showSnack(R.string.coming_soon)
//                setSelectedPaymentMethod(PaymentType.CARD)
//                saveDefaultPaymentMethod()
//                setResult()
            }
            R.id.tvViewToken -> {
                val intentToken = Intent(this, ETokensActivity::class.java)
                if (intent.hasExtra(CATEGORY_BRAND_ID)) {
                    intentToken.putExtra("want_selection", true)
                    intentToken.putExtra(CATEGORY_BRAND_ID, intent.getIntExtra(CATEGORY_BRAND_ID, 0))
                    startActivityForResult(intentToken, RQ_SELECT_ETOKEN)
                } else {
                    intentToken.putExtra("want_selection", false)
                    startActivity(intentToken)
                }
            }

            tvAddCardPayment.id ->{
                startActivityForResult(Intent(this, AddCardActivity::class.java), FOR_ADD_CARD)
            }

            tvPayNow.id ->{
                var isCardSelected = false
                var cardId = ""
                for (model in cardList) {
                    if (model.isSelected == true) {
                        isCardSelected = true
                        cardId = model.card_id ?: ""
                    }
                }

                if (isCardSelected) {
                    if (CheckNetworkConnection.isOnline(this)) {
                        var map = HashMap<String,Any>()
                        map.put("card_id",cardId)
                        presenter.setDefaultCard(map)
                    }
                } else
                    tvPayNow.showSnack(getString(R.string.error_in_card_selection))
            }
        }
    }

    private fun setSelectedPaymentMethod(paymentType: String?) {
        selectedPaymentType = paymentType
        rbToken.isChecked = paymentType == PaymentType.E_TOKEN
        rbCod.isChecked = paymentType == PaymentType.CASH
        rbCard.isChecked = paymentType == PaymentType.CARD
    }

    private fun saveDefaultPaymentMethod() {
        SharedPrefs.with(this).save(PAYMENT_TYPE, selectedPaymentType)
    }


    private fun setResult() {
        if (intent.hasExtra(CATEGORY_ID)) {
            val intent = Intent()
            intent.putExtra(PAYMENT_TYPE, selectedPaymentType)
            intent.putExtra(TOKEN_ID, tokenId)
            setResult(Activity.RESULT_OK, intent)
            finish()
        }
    }

    private var tokenId: Int? = null

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RQ_SELECT_ETOKEN && resultCode == Activity.RESULT_OK && data != null) {
            tokenId = data.getIntExtra(TOKEN_ID, 0)
            setResult()
        }
    }

    override fun onPaymentDetailsApiSuccess(response: PaymentDetail?) {
        response?.etoken_sum
    }

    override fun showLoader(isLoading: Boolean) {
        dialogIndeterminate?.show(isLoading)
    }

    override fun apiFailure() {

    }

    override fun handleApiError(code: Int?, error: String?) {
        if (code == StatusCode.UNAUTHORIZED) {
            AppUtils.logout(this)
        }
    }

    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(LocaleManager.setLocale(newBase))
    }

    override fun onAddCardSuccess(response: CardModel?) {

    }

    override fun onGetCardSuccess(response: ArrayList<CardModel>?) {
        cardList.clear()
        cardList.addAll(response ?: emptyList())
        for (i in 0..(cardList.size - 1)) {
            cardList.get(i).isSelected = i == 0
        }

        adapter.notifyDataSetChanged()

        if (cardList.size > 0)
            tvNoCards.visibility = View.GONE
        else {
            if (rbCard.isChecked) {
                tvNoCards.visibility = View.VISIBLE
            } else
                tvNoCards.visibility = View.GONE
        }
    }

    private fun initCardAdapter(){
        cardList = ArrayList<CardModel>()
        adapter = CardAdapter(cardList, this, this)
        val layoutManaget = LinearLayoutManager(this, LinearLayout.VERTICAL, false)
        rvCardList.layoutManager = layoutManaget
        rvCardList.adapter = adapter
    }

    override fun onClickDeleteListener(position: Int) {

    }

    override fun onCardSelected(position: Int) {
        for (i in 0..(cardList.size - 1)) {
            cardList.get(i).isSelected = i == position
        }
        adapter.notifyDataSetChanged()
    }

    override fun onSetDefaultCard(response: CardModel?) {
        val intent = Intent()
        intent.putExtra(PAYMENT_TYPE, selectedPaymentType)
        intent.putExtra(TOKEN_ID, tokenId)
        intent.putExtra(CARD_ID, "")
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

    override fun onBuyCounponSuccess(response: CouponModel?) {

    }
}
