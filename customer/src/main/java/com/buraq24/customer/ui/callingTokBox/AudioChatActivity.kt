package com.buraq24.customer.ui.callingTokBox

import android.Manifest
import android.app.AlertDialog
import android.content.Context
import android.opengl.GLSurfaceView
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Toast

import com.buraq24.customer.R
import com.buraq24.getAccessToken
import com.buraq24.showSWWerror
import com.buraq24.showSnack
import com.buraq24.utilities.RECEIVER_ID
import com.buraq24.utilities.USER_NAME
import kotlinx.android.synthetic.main.activity_chatvideo.*
import kotlinx.android.synthetic.main.fragment_chats.*

import pub.devrel.easypermissions.AfterPermissionGranted
import pub.devrel.easypermissions.AppSettingsDialog
import pub.devrel.easypermissions.EasyPermissions
import android.media.AudioManager.MODE_IN_CALL
import android.media.AudioManager
import android.os.CountDownTimer
import android.view.View
import android.view.WindowManager
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.buraq24.utilities.PROFILE_PIC_URL
import com.opentok.android.*

class AudioChatActivity : AppCompatActivity(),
        AudioContract.View,
        EasyPermissions.PermissionCallbacks,
        Session.SessionListener,
        PublisherKit.PublisherListener,
        SubscriberKit.SubscriberListener {

    private var mSession: Session? = null
    private var mPublisher: Publisher? = null
    private var mSubscriber: Subscriber? = null

    private val presenter = OpenTalkPresenter()

    var receiverID: String = ""
    var sessionId: String = ""
    var sessionToken: String = ""
    var dateTime: String = ""

    var isConnected = false

    lateinit var timerCounDown: CountDownTimer

    override fun onCreate(savedInstanceState: Bundle?) {
        window.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED)
        window.addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD)
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chatvideo)
        presenter.attachView(this)

        receiverID = intent.getStringExtra(RECEIVER_ID)
        sessionId = intent.getStringExtra("session_id") ?: ""
        sessionToken = intent.getStringExtra("token") ?: ""
        dateTime = intent!!.getStringExtra("dateTime") ?: ""

        if (!sessionId.equals("")) {
            countDounwTime((System.currentTimeMillis() + 30 * 1000) - dateTime.toLong())
            tv_status.text = getText(R.string.incoming_call)
            linear_inprogress.visibility = View.GONE
            linear_incoming.visibility = View.VISIBLE
        } else {
            linear_inprogress.visibility = View.VISIBLE
            linear_incoming.visibility = View.GONE
        }

        tv_username.text = intent.getStringExtra(USER_NAME) ?: ""

        Glide.with(this).load(intent.getStringExtra(PROFILE_PIC_URL) ?: "")
                .apply(RequestOptions().circleCrop()).into(userImage)


        val audioManager = getSystemService(Context.AUDIO_SERVICE) as AudioManager
        audioManager.mode = MODE_IN_CALL

        if (!audioManager.isMicrophoneMute) {
            btn_mute.setBackgroundResource(R.drawable.ic_mic_off_white_24dp)
        } else btn_mute.setBackgroundResource(R.drawable.ic_mic_on_white_24dp)

        if (!audioManager.isSpeakerphoneOn) {
            btn_speaker.setBackgroundResource(R.drawable.ic_loude_speaker_off_24dp)
        } else btn_speaker.setBackgroundResource(R.drawable.ic_loude_speaker_24dp)

        btn_mute.setOnClickListener {
            if (!audioManager.isMicrophoneMute) {
                audioManager.isMicrophoneMute = true
                btn_mute.setBackgroundResource(R.drawable.ic_mic_on_white_24dp)
            } else {
                audioManager.isMicrophoneMute = false
                btn_mute.setBackgroundResource(R.drawable.ic_mic_off_white_24dp)
            }
        }

        btn_disconnect.setOnClickListener {
            if (mSession != null) {
                btn_disconnect.isEnabled = false
                mSession!!.disconnect()
            } else finish()
        }

        btn_speaker.setOnClickListener {
            if (!audioManager.isSpeakerphoneOn) {
                audioManager.isSpeakerphoneOn = true
                btn_speaker.setBackgroundResource(R.drawable.ic_loude_speaker_24dp)
            } else {
                audioManager.isSpeakerphoneOn = false
                btn_speaker.setBackgroundResource(R.drawable.ic_loude_speaker_off_24dp)
            }
        }


        btn_connect.setOnClickListener {
            tv_status.text = getText(R.string.connecting)
            linear_inprogress.visibility = View.VISIBLE
            linear_incoming.visibility = View.GONE
            Log.d(LOG_TAG, " SessionId: " + sessionId + " Token: " + sessionToken)
            initializeSession(OpenTokConfig.API_KEY, sessionId, sessionToken)
        }

        btn_dnd.setOnClickListener {
            onBackPressed()
        }

        requestPermissions()
    }


    fun countDounwTime(time: Long) {
        timerCounDown =  object : CountDownTimer(time, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                println("TIMELEFT $millisUntilFinished")
            }

            override fun onFinish() {
                if (!isConnected) {
                    if (mSession != null && mSession!!.sessionId != null) {
                        mSession!!.disconnect()
                    }
                    if (mPublisher != null) {
                        mPublisher!!.destroy()
                    }
                    if (mSubscriber != null) {
                        mSubscriber = null
                    }
                    finish()
                }
            }
        }.start()
    }

    /* Activity lifecycle methods */
    override fun onPause() {
        Log.d(LOG_TAG, "onPause")
        super.onPause()
       /* if (mSession != null && mSession!!.sessionId != null) {
            mSession!!.onPause()
        }*/
    }

    override fun onResume() {
        Log.d(LOG_TAG, "onResume")
        super.onResume()
      /*  if (mSession != null && mSession!!.sessionId != null) {
            mSession!!.onResume()
        }*/
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
    }

    override fun onPermissionsGranted(requestCode: Int, perms: List<String>) {
        Log.d(LOG_TAG, "onPermissionsGranted:" + requestCode + ":" + perms.size)
        requestPermissions()
    }

    override fun onPermissionsDenied(requestCode: Int, perms: List<String>) {
        Log.d(LOG_TAG, "onPermissionsDenied:" + requestCode + ":" + perms.size)
        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            AppSettingsDialog.Builder(this)
                    .setTitle(getString(R.string.title_settings_dialog))
                    .setRationale(getString(R.string.rationale_ask_again))
                    .setPositiveButton(getString(R.string.settings))
                    .setNegativeButton(getString(R.string.cancel))
                    .setRequestCode(RC_SETTINGS_SCREEN_PERM)
                    .build()
                    .show()
        }
    }

    @AfterPermissionGranted(RC_VIDEO_APP_PERM)
    private fun requestPermissions() {
        val perms = arrayOf(Manifest.permission.INTERNET, Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO)
        if (EasyPermissions.hasPermissions(this, *perms)) {
            // if there is no server URL set
            if (sessionId.equals("")) {
                presenter.getSessionIDApi(getAccessToken(this), receiverID)
            }
        } else {
            EasyPermissions.requestPermissions(this, getString(R.string.rationale_permission), RC_VIDEO_APP_PERM, *perms)
        }
    }

    private fun initializeSession(apiKey: String, sessionId: String, token: String) {
        var customAudioDevice = CustomAudioDevice(this)
        AudioDeviceManager.setAudioDevice(customAudioDevice)
        mSession = Session.Builder(this, apiKey, sessionId).build()
        mSession!!.setSessionListener(this)
        mSession!!.connect(token)
    }


    /* Session Listener methods */

    override fun onConnected(session: Session) {

        Log.d(LOG_TAG, "onConnected: Connected to session: " + session.sessionId)
        // initialize Publisher and set this object to listen to Publisher events
        mPublisher = Publisher.Builder(this).build()
        mPublisher!!.setPublisherListener(this)

        // set publisher video style to fill view
        mPublisher!!.renderer.setStyle(BaseVideoRenderer.STYLE_VIDEO_SCALE,
                BaseVideoRenderer.STYLE_VIDEO_FILL)
        tv_status.text = getString(R.string.connected)

        if (mPublisher!!.view is GLSurfaceView) {
            (mPublisher!!.view as GLSurfaceView).setZOrderOnTop(true)
        }

        mSession!!.publish(mPublisher)
    }

    override fun onDisconnected(session: Session) {
        Log.d(LOG_TAG, "onDisconnected: Disconnected from session: " + session.sessionId)
        if (mPublisher != null) {
            mPublisher!!.destroy()
        }
        if (mSubscriber != null) {
            mSubscriber = null
        }
        finish()
    }

    override fun onStreamReceived(session: Session, stream: Stream) {
        Log.d(LOG_TAG, "onStreamReceived: New Stream Received " + stream.streamId + " in session: " + session.sessionId)
        if (mSubscriber == null) {
            isConnected = true
            mSubscriber = Subscriber.Builder(this, stream).build()
            mSubscriber!!.renderer.setStyle(BaseVideoRenderer.STYLE_VIDEO_SCALE, BaseVideoRenderer.STYLE_VIDEO_FILL)
            mSubscriber!!.setSubscriberListener(this)
            mSession!!.subscribe(mSubscriber)
        }
    }

    override fun onStreamDropped(session: Session, stream: Stream) {
        tv_status.text = getString(R.string.disconnected)
        Log.d(LOG_TAG, "onStreamDropped: Stream Dropped: " + stream.streamId + " in session: " + session.sessionId)
        if (mSubscriber != null) {
            mSubscriber = null
        }
        /*if (mSession != null) {
            mSession!!.disconnect()
        }*/
        onBackPressed()

    }

    override fun onError(session: Session, opentokError: OpentokError) {
        Log.e(LOG_TAG, "onError: " + opentokError.errorDomain + " : " +
                opentokError.errorCode + " - " + opentokError.message + " in session: " + session.sessionId)

        showOpenTokError(opentokError)
    }

    /* Publisher Listener methods */

    override fun onStreamCreated(publisherKit: PublisherKit, stream: Stream) {

        if (timerCounDown != null) {
            timerCounDown.cancel()
        }

        isConnected = true
        Log.d(LOG_TAG, "onStreamCreated: Publisher Stream Created. Own stream " + stream.streamId)

    }

    override fun onStreamDestroyed(publisherKit: PublisherKit, stream: Stream) {
        Log.d(LOG_TAG, "onStreamDestroyed: Publisher Stream Destroyed. Own stream ")
        finish()
    }

    override fun onError(publisherKit: PublisherKit, opentokError: OpentokError) {

        Log.e(LOG_TAG, "onError: " + opentokError.errorDomain + " : " +
                opentokError.errorCode + " - " + opentokError.message)

        showOpenTokError(opentokError)
    }

    override fun onConnected(subscriberKit: SubscriberKit) {

        Log.d(LOG_TAG, "onConnected: Subscriber connected. Stream: " + subscriberKit.stream.streamId)
    }

    override fun onDisconnected(subscriberKit: SubscriberKit) {
        finish()
        //Log.d(LOG_TAG, "onDisconnected: Subscriber disconnected. Stream: " + subscriberKit.stream.streamId)
    }

    override fun onError(subscriberKit: SubscriberKit, opentokError: OpentokError) {

        Log.e(LOG_TAG, "onError: " + opentokError.errorDomain + " : " +
                opentokError.errorCode + " - " + opentokError.message)

        showOpenTokError(opentokError)
    }

    private fun showOpenTokError(opentokError: OpentokError) {

        Toast.makeText(this, opentokError.errorDomain.name + ": " + opentokError.message + " Please, see the logcat.", Toast.LENGTH_LONG).show()
        finish()
    }

    private fun showConfigError(alertTitle: String, errorMessage: String) {
        Log.e(LOG_TAG, "Error $alertTitle: $errorMessage")
        AlertDialog.Builder(this)
                .setTitle(alertTitle)
                .setMessage(errorMessage)
                .setPositiveButton("ok") { dialog, which -> this@AudioChatActivity.finish() }
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show()
    }

    companion object {

        private val LOG_TAG = AudioChatActivity::class.java.simpleName
        private val RC_SETTINGS_SCREEN_PERM = 123
        private const val RC_VIDEO_APP_PERM = 124
    }

    override fun sessionApiSuccess(chatList: OpenTalkSession?) {
        countDounwTime(30000)
        Log.d(LOG_TAG, " SessionId: " + chatList?.data?.session_id + " Token: " + chatList?.data?.token)
        initializeSession(OpenTokConfig.API_KEY, chatList!!.data.session_id, chatList.data.token)
    }

    override fun showLoader(isLoading: Boolean) {
    }

    override fun apiFailure() {
        rootView.showSWWerror()
        onBackPressed()
    }

    override fun handleApiError(code: Int?, error: String?) {
        error?.let { rootView.showSnack(it) }
    }


}
