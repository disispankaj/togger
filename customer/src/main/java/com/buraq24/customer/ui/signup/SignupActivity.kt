package com.buraq24.customer.ui.signup

import android.content.Context
import android.os.Bundle
import android.support.v4.app.FragmentManager
import android.support.v7.app.AppCompatActivity
import com.buraq24.customer.R
import com.buraq24.customer.ui.signup.login.LoginFragment
import com.buraq24.utilities.LocaleManager

/**
 * This activity acts as the container for the whole login process
 * */

class SignupActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signup)
        supportFragmentManager.beginTransaction().add(R.id.container, LoginFragment()).commit()
    }

    override fun onBackPressed() {
        when {
            supportFragmentManager.backStackEntryCount > 0 -> supportFragmentManager?.popBackStack("backstack", FragmentManager.POP_BACK_STACK_INCLUSIVE)
            else -> super.onBackPressed()
        }

    }

    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(LocaleManager.setLocale(newBase))
    }
}
