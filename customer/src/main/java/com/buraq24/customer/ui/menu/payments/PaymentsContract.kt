package com.buraq24.customer.ui.menu.payments

import com.buraq24.customer.webservices.models.CardModel
import com.buraq24.customer.webservices.models.CouponModel
import com.buraq24.customer.webservices.models.paymentdetails.PaymentDetail
import com.buraq24.utilities.basearc.BasePresenter
import com.buraq24.utilities.basearc.BaseView

class PaymentsContract{

    interface View: BaseView{
        fun onPaymentDetailsApiSuccess(response: PaymentDetail?)

        fun onAddCardSuccess(response: CardModel?)

        fun onGetCardSuccess(response: ArrayList<CardModel>?)

        fun onSetDefaultCard(response: CardModel?)

//        fun onGetCouponSuccess(response: ArrayList<CouponModel>?)

        fun onBuyCounponSuccess(response: CouponModel?)
    }

    interface Presenter: BasePresenter<View>{
        fun paymentDetail(categoryId: String?, categoryProductId: String?)

        fun addUserCard(map:HashMap<String,Any>)

        fun getCardList()

        fun setDefaultCard(map:HashMap<String,Any>)

//        fun getCoupons()

        fun buyCoupons(map:HashMap<String,Any>)
    }
}