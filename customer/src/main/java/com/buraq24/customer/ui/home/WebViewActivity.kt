package com.buraq24.customer.ui.home

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v4.content.ContextCompat.startActivity
import android.view.View
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import com.buraq24.customer.R
import com.buraq24.utilities.constants.TITLE
import com.buraq24.utilities.constants.URL_TO_LOAD
import com.buraq24.utilities.webservices.BASE_URL
import kotlinx.android.synthetic.main.activity_web_view.*
import android.webkit.WebChromeClient
import com.buraq24.customer.R.id.webView
import com.buraq24.utilities.LocaleManager


class WebViewActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_web_view)
        val title = intent?.getStringExtra(TITLE)
        if (title?.isNotEmpty() == true) {
            tvTitle.text = title
        }
        progressbar.max = 100
        webView.webChromeClient = object : WebChromeClient() {
            override fun onProgressChanged(view: WebView, progress: Int) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    progressbar.setProgress(progress, true)
                } else {
                    progressbar.progress = progress
                }
            }

        }
        webView.webViewClient = MyWebViewClient()
        webView.loadUrl(intent?.getStringExtra(URL_TO_LOAD))
        setListeners()
    }

    private fun setListeners() {
        tvBack.setOnClickListener { onBackPressed() }
    }

    override fun onBackPressed() {
        if (webView.canGoBack()) {
            webView.goBack()
        } else {
            super.onBackPressed()
        }
    }

    inner class MyWebViewClient : WebViewClient() {

        override fun onPageFinished(view: WebView?, url: String?) {
            super.onPageFinished(view, url)
            progressbar.visibility = View.GONE
        }

        override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
            super.onPageStarted(view, url, favicon)
            progressbar.progress = 0
            progressbar.visibility = View.VISIBLE
        }

        @Suppress("OverridingDeprecatedMember")
        override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
            val uri = Uri.parse(url)
            return handleUri(uri)
        }

        @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
        override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {

            val uri = request?.url
            return handleUri(uri)
        }

        private fun handleUri(uri: Uri?): Boolean {
            val host = uri?.host
            // Based on some condition you need to determine if you are going to load the url
            // in your web view itself or in a browser.
            // You can use `host` or `scheme` or any part of the `uri` to decide.
            return if (host == BASE_URL) {
                // Returning false means that you are going to load this url in the webView itself
                false
            } else {
                // Returning true means that you need to handle what to do with the url
                // e.g. open web page in a Browser
                val intent = Intent(Intent.ACTION_VIEW, uri)
                startActivity(intent)
                true
            }
        }

    }

    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(LocaleManager.setLocale(newBase))
    }
}


