package com.buraq24.customer.ui.home.support

import com.buraq24.customer.webservices.ApiResponse
import com.buraq24.customer.webservices.RestClient
import com.buraq24.utilities.basearc.BasePresenterImpl
import com.buraq24.getApiError
import com.buraq24.utilities.webservices.models.Service
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SupportPresenter: BasePresenterImpl<SupportContract.View>(), SupportContract.Presenter {

    override fun getSupportList() {
        getView()?.showLoader(true)
        RestClient.get().supportList().enqueue(object : Callback<ApiResponse<ArrayList<Service>>> {
            override fun onResponse(call: Call<ApiResponse<ArrayList<Service>>>?, response: Response<ApiResponse<ArrayList<Service>>>?) {
                getView()?.showLoader(false)
                if (response?.isSuccessful == true) {
                    if (response.body()?.statusCode == 200) {
                        getView()?.onSupportListApiSuccess(response.body()?.result)
                    } else {
                        getView()?.handleApiError(response.body()?.statusCode, response.body()?.msg)
                    }
                } else {
                    val errorModel = getApiError(response?.errorBody()?.string())
                    getView()?.handleApiError(errorModel.statusCode, errorModel.msg)
                }
            }

            override fun onFailure(call: Call<ApiResponse<ArrayList<Service>>>?, t: Throwable?) {
                getView()?.showLoader(false)
                getView()?.apiFailure()
            }

        })
    }
}