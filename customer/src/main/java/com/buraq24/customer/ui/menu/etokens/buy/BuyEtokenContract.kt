package com.buraq24.customer.ui.menu.etokens.buy

import com.buraq24.customer.webservices.models.Company
import com.buraq24.customer.webservices.models.etoken.ETokensModel
import com.buraq24.customer.webservices.models.homeapi.Category
import com.buraq24.utilities.basearc.BasePresenter
import com.buraq24.utilities.basearc.BaseView

class BuyEtokenContract {

    interface View : BaseView {
        fun onETokenApiSuccess(eTokensModel: ETokensModel?)
        fun onETokenBuyApiSuccess()
    }

    interface Presenter : BasePresenter<View> {
        fun getETokensApiCall(map: HashMap<String, String>?)
        fun buyEtokenApiCall(map: HashMap<String, String>?)
    }
}