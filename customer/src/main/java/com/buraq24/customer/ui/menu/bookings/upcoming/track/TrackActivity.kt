package com.buraq24.customer.ui.menu.bookings.upcoming.track

import android.animation.ValueAnimator

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.view.animation.LinearInterpolator
import android.widget.Toast
import com.buraq24.customer.R
import com.buraq24.Utils
import com.buraq24.showSWWerror
import com.buraq24.showSnack
import com.buraq24.customer.utils.AppUtils
import com.buraq24.customer.webservices.ApiResponse
import com.buraq24.customer.webservices.models.PolylineModel
import com.buraq24.customer.webservices.models.TrackingModel
import com.buraq24.customer.webservices.models.nearestroad.RoadItem
import com.buraq24.customer.webservices.models.order.Order
import com.buraq24.utilities.*
import com.buraq24.utilities.constants.ACCESS_TOKEN
import com.buraq24.utilities.constants.PREF_MAP_VIEW
import com.buraq24.utilities.constants.SUCCESS_CODE
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import io.socket.client.Ack
import io.socket.client.Socket
import io.socket.emitter.Emitter
import kotlinx.android.synthetic.main.activity_track.*
import org.json.JSONArray
import org.json.JSONObject
import java.util.*

class TrackActivity : AppCompatActivity(), OnMapReadyCallback, TrackContract.View {

    private var mMap: GoogleMap? = null

    private var carMarker: Marker? = null

    private var destMarker: Marker? = null

    private var order: Order? = null

    private var sourceLatLong: LatLng? = null

    private var destLong: LatLng? = null

    private var isHeavyLoadsVehicle = false

    private var presenter = TrackPresenter()

    /* Checks if all events are idle for the time */
    private var checkIdleTimer = Timer()

    private var isPaused = false

    private var line: Polyline? = null

    private var pathArray = ArrayList<String>()

    private var list = ArrayList<LatLng>()

    private var mapType: Int? = null

    private var confirmationDialog: AlertDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_track)
        presenter.attachView(this)
        mapType = SharedPrefs.with(this).getInt(PREF_MAP_VIEW, GoogleMap.MAP_TYPE_NORMAL)
        tvMapNormal.isSelected = mapType == GoogleMap.MAP_TYPE_NORMAL
        tvSatellite.isSelected = mapType == GoogleMap.MAP_TYPE_HYBRID
        order = Gson().fromJson(intent?.getStringExtra("order"), Order::class.java)
        sourceLatLong = LatLng(order?.driver?.latitude ?: 0.0, order?.driver?.longitude ?: 0.0)
        when (order?.category_id) {
            CategoryId.GAS, CategoryId.MINERAL_WATER, CategoryId.WATER_TANKER -> {
                isHeavyLoadsVehicle = false
                destLong = LatLng(order?.dropoff_latitude ?: 0.0, order?.dropoff_longitude ?: 0.0)
            }
            CategoryId.FREIGHT,CategoryId.FREIGHT_2, CategoryId.TOW -> {
                isHeavyLoadsVehicle = true
                if (order?.order_status == OrderStatus.CONFIRMED) {
                    destLong = LatLng(order?.pickup_latitude ?: 0.0, order?.pickup_longitude ?: 0.0)
                } else {
                    destLong = LatLng(order?.dropoff_latitude ?: 0.0, order?.dropoff_longitude
                            ?: 0.0)
                }
            }
        }
        mapType = SharedPrefs.with(this).getInt(PREF_MAP_VIEW, GoogleMap.MAP_TYPE_NORMAL)
        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        setListeners()
    }

    private fun setListeners() {
        tvMapNormal.setOnClickListener {
            tvMapNormal.isSelected = true
            tvSatellite.isSelected = false
            line?.color = ContextCompat.getColor(this, R.color.text_dark)
            mMap?.mapType = GoogleMap.MAP_TYPE_NORMAL
            SharedPrefs.with(this).save(PREF_MAP_VIEW, GoogleMap.MAP_TYPE_NORMAL)
        }
        tvSatellite.setOnClickListener {
            tvMapNormal.isSelected = false
            tvSatellite.isSelected = true
            mMap?.mapType = GoogleMap.MAP_TYPE_HYBRID
            line?.color = ContextCompat.getColor(this, R.color.white)
            SharedPrefs.with(this).save(PREF_MAP_VIEW, GoogleMap.MAP_TYPE_HYBRID)
        }

        fabMyLocation.setOnClickListener {
            reFocusMapCamera()
        }
        tvBack.setOnClickListener { onBackPressed() }
    }

    override fun onResume() {
        super.onResume()
        isPaused = false
        if (AppSocket.get().isConnected) {
            restartCheckIdleTimer(0)
        }
        AppSocket.get().on(Events.ORDER_EVENT, orderEventListener)
        AppSocket.get().on(Socket.EVENT_CONNECT, onSocketConnected)
        AppSocket.get().on(Socket.EVENT_DISCONNECT, onSocketDisconnected)
    }

    override fun onPause() {
        super.onPause()
        isPaused = true
        checkIdleTimer.cancel()
        AppSocket.get().off(Events.ORDER_EVENT, orderEventListener)
        AppSocket.get().off(Socket.EVENT_CONNECT, onSocketConnected)
        AppSocket.get().off(Socket.EVENT_DISCONNECT, onSocketDisconnected)
    }

    override fun onDestroy() {
        presenter.detachView()
        super.onDestroy()
    }

    override fun onMapReady(map: GoogleMap?) {
        mMap = map
        mMap?.clear()
        showMarker(sourceLatLong, destLong)
        reFocusMapCamera()
    }

    private val onSocketConnected = Emitter.Listener {
        runOnUiThread {
            if (!isPaused) {
                restartCheckIdleTimer(0)
            }
        }
    }

    private val onSocketDisconnected = Emitter.Listener {
        checkIdleTimer.cancel()
    }

    private fun showMarker(sourceLatLong: LatLng?, destLong: LatLng?) {
        /*add marker for both source and destination*/
        carMarker = mMap?.addMarker(this.sourceLatLong?.let { MarkerOptions().position(it) })
        when (order?.category_id) {
            CategoryId.GAS -> carMarker?.setIcon((BitmapDescriptorFactory.fromResource(R.drawable.ic_mini_truck_m_gas)))
            CategoryId.MINERAL_WATER -> carMarker?.setIcon((BitmapDescriptorFactory.fromResource(R.drawable.ic_mini_truck_water_m)))
            CategoryId.WATER_TANKER -> carMarker?.setIcon((BitmapDescriptorFactory.fromResource(R.drawable.ic_truck_water_tank_m)))
            CategoryId.FREIGHT,CategoryId.FREIGHT_2 -> carMarker?.setIcon((BitmapDescriptorFactory.fromResource(R.drawable.ic_truck_m_freights)))
            CategoryId.TOW -> carMarker?.setIcon((BitmapDescriptorFactory.fromResource(R.drawable.ic_mini_truck_m_tow)))
        }
        carMarker?.isFlat = true
        carMarker?.setAnchor(0.5f, 0.5f)
        destMarker = mMap?.addMarker(this.destLong?.let { MarkerOptions().position(it) })
        if (isHeavyLoadsVehicle && order?.order_status == OrderStatus.CONFIRMED) {
            destMarker?.setIcon((BitmapDescriptorFactory.fromResource(R.drawable.ic_pickup_location_mrkr)))
        } else {
            destMarker?.setIcon((BitmapDescriptorFactory.fromResource(R.drawable.ic_drop_location_mrkr)))
        }

        this.sourceLatLong?.latitude?.let {
            this.sourceLatLong?.longitude?.let { it1 ->
                this.destLong?.latitude?.let { it2 ->
                    this.destLong?.longitude?.let { it3 ->
                        presenter.drawPolyLine(sourceLat = it, sourceLong = it1,
                                destLat = it2, destLong = it3, language = Locale.US.language)
                    }
                }
            }
        }
    }

    private fun reFocusMapCamera() {
        val builder = LatLngBounds.Builder()
        val arr = ArrayList<Marker?>()
        arr.add(carMarker)
        arr.add(destMarker)
        for (marker in arr) {
            builder.include(marker?.position)
        }
        val bounds = builder.build()
        val cu = CameraUpdateFactory.newLatLngBounds(bounds,
                Utils.getScreenWidth(this),
                Utils.getScreenWidth(this).minus(Utils.dpToPx(24).toInt()), Utils.dpToPx(56).toInt())
        mMap?.animateCamera(cu)
    }

    var valueAnimatorMove: ValueAnimator? = null
    fun animateMarker(latLng: LatLng?, bearing: Float?) {
        if (carMarker != null) {
            val startPosition = carMarker?.position
            val endPosition = LatLng(latLng?.latitude ?: 0.0, latLng?.longitude ?: 0.0)
            val startRotation = carMarker?.rotation
            val latLngInterpolator = LatLngInterpolator.LinearFixed()
            valueAnimatorMove?.cancel()
            valueAnimatorMove = ValueAnimator.ofFloat(0f, 1f)
            valueAnimatorMove?.duration = 10000 // duration 1 second
            valueAnimatorMove?.interpolator = LinearInterpolator()
            valueAnimatorMove?.addUpdateListener(object : ValueAnimator.AnimatorUpdateListener {
                override fun onAnimationUpdate(animation: ValueAnimator) {
                    try {
                        val v = animation.animatedFraction
                        val newPosition = latLngInterpolator.interpolate(v, startPosition
                                ?: LatLng(0.0, 0.0), endPosition)
                        carMarker?.position = newPosition
//                        carMarker?.setRotation(computeRotation(v, startRotation?:0f, currentBearing))
                    } catch (ex: Exception) {
                        ex.printStackTrace()
                    }
                }
            })
            valueAnimatorMove?.start()
            rotateMarker(latLng, bearing)
        }
    }

    var valueAnimatorRotate: ValueAnimator? = null
    private fun rotateMarker(latLng: LatLng?, bearing: Float?) {
        if (carMarker != null) {
            val startPosition = carMarker?.position
            val endPosition = LatLng(latLng?.latitude ?: 0.0, latLng?.longitude ?: 0.0)

            val latLngInterpolator = LatLngInterpolator.LinearFixed()
            valueAnimatorRotate?.cancel()
            val startRotation = carMarker?.rotation
            valueAnimatorRotate = ValueAnimator.ofFloat(0f, 1f)
//            valueAnimatorRotate?.startDelay = 200
            valueAnimatorRotate?.duration = 5000 // duration 1 second
            valueAnimatorRotate?.interpolator = LinearInterpolator()
            valueAnimatorRotate?.addUpdateListener { animation ->
                try {
                    val v = animation.animatedFraction
                    val newPosition = latLngInterpolator.interpolate(v, startPosition
                            ?: LatLng(0.0, 0.0), endPosition)
                    //                        carMarker?.setPosition(newPosition)
                    carMarker?.rotation = computeRotation(v, startRotation ?: 0f, bearing
                            ?: 0f)
                } catch (ex: Exception) {
                    ex.printStackTrace()
                }
            }
            valueAnimatorRotate?.start()

        }
    }

    private fun computeRotation(fraction: Float, start: Float, end: Float): Float {
        val normalizeEnd = end - start // rotate start to 0
        val normalizedEndAbs = (normalizeEnd + 360) % 360

        val direction = (if (normalizedEndAbs > 180) -1 else 1).toFloat() // -1 = anticlockwise, 1 = clockwise
        val rotation: Float
        if (direction > 0) {
            rotation = normalizedEndAbs
        } else {
            rotation = normalizedEndAbs - 360
        }

        val result = fraction * rotation + start
        return (result + 360) % 360
    }

    private interface LatLngInterpolator {
        fun interpolate(fraction: Float, a: LatLng, b: LatLng): LatLng
        class LinearFixed : LatLngInterpolator {
            override fun interpolate(fraction: Float, a: LatLng, b: LatLng): LatLng {
                val lat = (b.latitude - a.latitude) * fraction + a.latitude
                var lngDelta = b.longitude - a.longitude
                // Take the shortest path across the 180th meridian.
                if (Math.abs(lngDelta) > 180) {
                    lngDelta -= Math.signum(lngDelta) * 360
                }
                val lng = lngDelta * fraction + a.longitude
                return LatLng(lat, lng)
            }
        }
    }

    override fun polyLine(jsonRootObject: JSONObject) {
        line?.remove()
        val routeArray = jsonRootObject.getJSONArray("routes")
        if (routeArray.length() == 0) {
            return
        }
        var routes: JSONObject? = null
        routes = routeArray.getJSONObject(0)
        val overviewPolylines = routes.getJSONObject("overview_polyline")
        val encodedString = overviewPolylines.getString("points")
        pathArray.add(encodedString)
        list = decodePoly(encodedString)
        val listSize = list.size
        sourceLatLong?.let { list.add(0, it) }
        destLong?.let { list.add(listSize + 1, it) }
        line = mMap?.addPolyline(PolylineOptions()
                .addAll(list)
                .width(8f)
                .color(if (mapType == GoogleMap.MAP_TYPE_NORMAL) ContextCompat.getColor(this, R.color.text_dark)
                else ContextCompat.getColor(this, R.color.white))
                .geodesic(true))

//            val builder = LatLngBounds.Builder()
//            val arr = ArrayList<Marker?>()
//            arr.add(carMarker)
//            arr.add(destMarker)
//            for (marker in arr) {
//                builder.include(marker?.position)
//            }
//            val bounds = builder.build()
//            val cu = CameraUpdateFactory.newLatLngBounds(bounds,
//                    activity?.let { Utils.getScreenWidth(it) } ?: 0,
//                    activity?.let { Utils.getScreenWidth(it) }?.minus(Utils.dpToPx(24).toInt())
//                            ?: 0,
//                    Utils.dpToPx(56).toInt())
//            googleMapHome?.animateCamera(cu)

//        val estimatedDistance = (((routes?.get("legs") as JSONArray).get(0) as JSONObject).get("distance") as JSONObject).get("text") as String
        val estimatedTime = (((routes?.get("legs") as JSONArray).get(0) as JSONObject).get("duration") as JSONObject).get("text") as String
//        tvTime.text = estimatedTime
    }

    private fun decodePoly(encoded: String): ArrayList<LatLng> {

        val poly = java.util.ArrayList<LatLng>()
        var index = 0
        val len = encoded.length
        var lat = 0
        var lng = 0
        while (index < len) {
            var b: Int
            var shift = 0
            var result = 0
            do {
                b = encoded[index++].toInt() - 63
                result = result or (b and 0x1f shl shift)
                shift += 5
            } while (b >= 0x20)
            val dlat = if (result and 1 != 0) (result shr 1).inv() else result shr 1
            lat += dlat
            shift = 0
            result = 0
            do {
                b = encoded[index++].toInt() - 63
                result = result or (b and 0x1f shl shift)
                shift += 5
            } while (b >= 0x20)
            val dlng = if (result and 1 != 0) (result shr 1).inv() else result shr 1
            lng += dlng
            val p = LatLng(lat.toDouble() / 1E5,
                    lng.toDouble() / 1E5)
            poly.add(p)
        }
        return poly
    }

    override fun snappedPoints(response: List<RoadItem>?, trackingModel: TrackingModel?) {
        if ((response?.size ?: 0) > 0) {
            sourceLatLong = LatLng(response?.get(0)?.location?.latitude ?: 0.0,
                    response?.get(0)?.location?.longitude ?: 0.0)
            if (isHeavyLoadsVehicle && trackingModel?.order_status == OrderStatus.ONGOING) {
                destLong = LatLng(order?.dropoff_latitude
                        ?: 0.0, order?.dropoff_longitude ?: 0.0)
                destMarker?.position = LatLng(destLong?.latitude
                        ?: 0.0, destLong?.longitude ?: 0.0)
                destMarker?.setIcon((BitmapDescriptorFactory.fromResource(R.drawable.ic_drop_location_mrkr)))
            }
            presenter.drawPolyLine(sourceLat = trackingModel?.latitude
                    ?: 0.0, sourceLong = trackingModel?.longitude ?: 0.0,
                    destLat = destLong?.latitude ?: 0.0, destLong = destLong?.longitude
                    ?: 0.0, language = Locale.US.language)
            if (sourceLatLong != null) {
                animateMarker(sourceLatLong, trackingModel?.bearing?.toFloat())
//                            carMarker?.let { animateMarker(it, sourceLatLong!!, false) }
//                            carMarker?.let {
//                                rotateMarker(it, track.bearing?.toFloat() ?: 0f, startRotation)
//                            }
            }
        }
    }

    override fun showLoader(isLoading: Boolean) {

    }

    override fun apiFailure() {
        rootView.showSWWerror()
    }

    override fun handleApiError(code: Int?, error: String?) {
        if (code == StatusCode.UNAUTHORIZED) {
            AppUtils.logout(this)
        } else {
            rootView.showSnack(error.toString())
        }
    }

    private val orderEventListener = Emitter.Listener { args ->
        Logger.e("orderEventListener", args[0].toString())
        restartCheckIdleTimer(20000)
        runOnUiThread {

            when (JSONObject(args[0].toString()).getString("type")) {
                OrderEventType.SERVICE_COMPLETE -> {
                    val orderJson = JSONObject(args[0].toString()).getJSONArray("order").get(0).toString()
                    val orderModel = Gson().fromJson(orderJson, Order::class.java)
                    if (orderModel.order_id == order?.order_id && !isPaused) {

                    }
                }

                OrderEventType.CUSTOMER_CONFIRMATION_PENDING_ETOKEN -> {
                    showOrderConfirmationAlert()
                }

                OrderEventType.CURRENT_ORDERS -> {
                    val track = Gson().fromJson(args[0].toString(), TrackingModel::class.java)
                            ?: null
                    if (track?.order_id == order?.order_id && !isPaused) {
                        sourceLatLong = LatLng(track?.latitude ?: 0.0, track?.longitude ?: 0.0)
//                        if (track?.polyline != null && track.polyline?.points?.isEmpty() == false) {
//                            drawPolylineFromDriver(track.polyline)
//                            if (sourceLatLong != null) {
//                                animateMarker(sourceLatLong, track.bearing?.toFloat())
//                            }
//                            if (isHeavyLoadsVehicle && track.order_status == OrderStatus.ONGOING) {
//                                destLong = LatLng(order?.dropoff_latitude
//                                        ?: 0.0, order?.dropoff_longitude ?: 0.0)
//                                destMarker?.position = LatLng(destLong?.latitude
//                                        ?: 0.0, destLong?.longitude ?: 0.0)
//                                destMarker?.setIcon((BitmapDescriptorFactory.fromResource(R.drawable.ic_drop_location_mrkr)))
//                            }
//                        } else {
                        presenter.getRoadPoints(track)
//                        }
//                        if (track?.order_status == OrderStatus.CONFIRMED) {
//                            tvDriverStatus.text = getString(R.string.driver_accepted_request)
//                        } else if (track?.order_status == OrderStatus.ONGOING) {
//                            if (track.my_turn == "1") {
//                                tvDriverStatus.text = getString(R.string.driver_is_on_the_way)
//                            } else {
//                                tvDriverStatus.text = getString(R.string.driver_completing_nearby_order)
//                            }
//                        }
                    }
                }

                OrderStatus.DRIVER_CANCELLED -> {
//                    openServiceFragment()
                    Toast.makeText(this, getString(R.string.ongoing_request_cancelled_by_driver), Toast.LENGTH_LONG).show()
                }
                else -> {
                    finish()
                }
            }
        }
    }

    private fun drawPolylineFromDriver(model: PolylineModel?) {
        line?.remove()
        val encodedString = model?.points ?: ""
        pathArray.add(encodedString)
        list = decodePoly(encodedString)
        val listSize = list.size
        sourceLatLong?.let { list.add(0, it) }
        destLong?.let { list.add(listSize + 1, it) }
        line = mMap?.addPolyline(PolylineOptions()
                .addAll(list)
                .width(8f)
                .color(if (mapType == GoogleMap.MAP_TYPE_NORMAL) ContextCompat.getColor(this, R.color.text_dark) else ContextCompat.getColor(this, R.color.white))
                .geodesic(true))

//            val builder = LatLngBounds.Builder()
//            val arr = ArrayList<Marker?>()
//            arr.add(carMarker)
//            arr.add(destMarker)
//            for (marker in arr) {
//                builder.include(marker?.position)
//            }
//            val bounds = builder.build()
//            val cu = CameraUpdateFactory.newLatLngBounds(bounds,
//                    activity?.let { Utils.getScreenWidth(it) } ?: 0,
//                    activity?.let { Utils.getScreenWidth(it) }?.minus(Utils.dpToPx(24).toInt())
//                            ?: 0,
//                    Utils.dpToPx(56).toInt())
//            googleMapHome?.animateCamera(cu)

//        val estimatedDistance = (((routes?.get("legs") as JSONArray).get(0) as JSONObject).get("distance") as JSONObject).get("text") as String
//        tvTime.text = model?.timeText

    }

    private fun restartCheckIdleTimer(checkIdleTimeInterval: Long) {
        checkIdleTimer.cancel()
        checkIdleTimer = Timer()
        checkIdleTimer.schedule(object : TimerTask() {
            override fun run() {
                if (CheckNetworkConnection.isOnline(this@TrackActivity)) {
                    checkOrderStatus()
                }
                restartCheckIdleTimer(20000L)
            }
        }, checkIdleTimeInterval)
    }

    private fun checkOrderStatus() {
        val request = JSONObject()
        request.put("type", OrderEventType.CUSTOMER_SINGLE_ORDER)
        request.put("access_token", ACCESS_TOKEN)
        request.put("order_token", order?.order_token)
        AppSocket.get().emit(Events.COMMON_EVENT, request, Ack {
            val response = Gson().fromJson<ApiResponse<Order>>(it[0].toString(),
                    object : TypeToken<ApiResponse<Order>>() {}.type)
            if (response?.statusCode == SUCCESS_CODE) {
                val orderModel = response.result
                val orderJson = JSONObject(it[0].toString()).getString("result")
                runOnUiThread {
                    when (orderModel?.order_status) {

                        OrderStatus.SERVICE_COMPLETE -> {
//                            openInvoiceFragment(orderJson)
                        }

                        OrderStatus.DRIVER_CANCELLED -> {
//                            openServiceFragment()
                            finish()
                            Toast.makeText(this, getString(R.string.ongoing_request_cancelled_by_driver), Toast.LENGTH_LONG).show()
                        }

                        else -> {
                            Logger.e("CUSTOMER_SINGLE_ORDER", "This status not handeled :" + orderModel?.order_status)

                        }
                    }
                }
            } else if (response.statusCode == StatusCode.UNAUTHORIZED) {
                AppUtils.logout(this)
            }
        })
    }

    private fun showOrderConfirmationAlert() {
        if (confirmationDialog == null) {
            confirmationDialog = AlertDialogUtil.getInstance().createOkCancelDialog(this,
                    R.string.order_confirmation,
                    R.string.order_confirmation_pending,
                    R.string.okay, 0, false,
                    object : AlertDialogUtil.OnOkCancelDialogListener {
                        override fun onOkButtonClicked() {
                            finish()
                        }

                        override fun onCancelButtonClicked() {
                            // Do nothing
                        }

                    })
        } else {
            confirmationDialog?.dismiss()
        }
        confirmationDialog?.show()
    }
}
