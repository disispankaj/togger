package com.buraq24.customer.ui.home.deliverystarts

import com.buraq24.customer.webservices.API
import com.buraq24.customer.webservices.ApiResponse
import com.buraq24.customer.webservices.RestClient
import com.buraq24.customer.webservices.models.TrackingModel
import com.buraq24.customer.webservices.models.nearestroad.RoadPoints
import com.buraq24.customer.webservices.models.order.Order
import com.buraq24.utilities.basearc.BasePresenterImpl
import com.buraq24.getApiError
import okhttp3.ResponseBody
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException

class DeliveryStartsPresenter : BasePresenterImpl<DeliveryStartsContract.View>(), DeliveryStartsContract.Presenter {

    override fun onGoingOrderApi() {
        getView()?.showLoader(true)
        RestClient.get().onGoingOrder().enqueue(object : Callback<ApiResponse<List<Order>>> {

            override fun onResponse(call: Call<ApiResponse<List<Order>>>?, response: Response<ApiResponse<List<Order>>>?) {
                getView()?.showLoader(false)
                if (response?.isSuccessful == true) {
                    if (response.body()?.statusCode == 200) {
                        getView()?.onApiSuccess(response.body()?.result)
                    } else {
                        getView()?.handleApiError(response.body()?.statusCode, response.body()?.msg)
                    }
                } else {
                    val errorModel = getApiError(response?.errorBody()?.string())
                    getView()?.handleApiError(errorModel.statusCode, errorModel.msg)
                }
            }

            override fun onFailure(call: Call<ApiResponse<List<Order>>>?, t: Throwable?) {
                getView()?.showLoader(false)
                getView()?.apiFailure()
            }
        })
    }

    override fun drawPolyLine(sourceLat: Double, sourceLong: Double, destLat: Double, destLong: Double, language: String?) {
        getView()?.showLoader(true)
        val BASE_URL_for_map = "https://maps.googleapis.com/maps/api/"
        val retrofit = Retrofit.Builder().baseUrl(BASE_URL_for_map).addConverterFactory(GsonConverterFactory.create()).build()
        val api = retrofit.create(API::class.java)
        val service = api.getPolYLine(sourceLat.toString() + "," + sourceLong, destLat.toString() + "," + destLong, language)
        service.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val responsePolyline = response.body()?.string()
                        val jsonRootObject = JSONObject(responsePolyline)
                        getView()?.polyLine(jsonRootObject)
                    } catch (e: IOException) {
                        e.printStackTrace()

                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                } else {
                    val errorModel = getApiError(response.errorBody()?.string())
                    getView()?.handleApiError(errorModel.statusCode, errorModel.msg)
                }
            }

            override fun onFailure(call: Call<ResponseBody>?, t: Throwable?) {
                getView()?.showLoader(false)
                getView()?.apiFailure()
            }
        })
    }

    override fun getRoadPoints(trackingModel: TrackingModel?) {
        //getView()?.showLoader(true)
        val latlng = trackingModel?.latitude.toString() + "," + trackingModel?.longitude.toString()
        RestClient.get().getRoadPoints(latlng).enqueue(object : Callback<RoadPoints> {
            override fun onResponse(call: Call<RoadPoints>?,
                                    response: Response<RoadPoints>?) {
                //getView()?.showLoader(false)
                getView()?.snappedPoints((response?.body()?.snappedPoints
                        ?: ArrayList()), trackingModel)
            }

            override fun onFailure(call: Call<RoadPoints>?, t: Throwable?) {
                //getView()?.showLoader(false)
                getView()?.snappedPoints((ArrayList()), trackingModel)
            }

        })
    }

    override fun requestCancelApiCall(map: HashMap<String, String>) {
        getView()?.showLoader(true)
        RestClient.get().cancelService(map).enqueue(object : Callback<ApiResponse<Any>> {

            override fun onResponse(call: Call<ApiResponse<Any>>?, response: Response<ApiResponse<Any>>?) {
                getView()?.showLoader(false)
                if (response?.isSuccessful == true) {
                    if (response.body()?.statusCode == 200) {
                        getView()?.onCancelApiSuccess()
                    } else {
                        getView()?.handleApiError(response.body()?.statusCode, response.body()?.msg)
                    }
                } else {
                    val errorModel = getApiError(response?.errorBody()?.string())
                    getView()?.handleApiError(errorModel.statusCode, errorModel.msg)
                }
            }

            override fun onFailure(call: Call<ApiResponse<Any>>?, t: Throwable?) {
                getView()?.showLoader(false)
                getView()?.apiFailure()
            }
        })
    }

}