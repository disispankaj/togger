package com.buraq24.customer.ui.home.services

import com.buraq24.customer.webservices.models.homeapi.HomeDriver
import com.buraq24.customer.webservices.models.homeapi.ServiceDetails
import com.buraq24.customer.webservices.models.nearestroad.RoadItem
import com.buraq24.utilities.basearc.BasePresenter
import com.buraq24.utilities.basearc.BaseView

class ServiceContract{

    interface View: BaseView{
        fun onApiSuccess(response: ServiceDetails?)
        fun snappedDrivers(snappedDrivers: List<HomeDriver>?)
    }

    interface Presenter: BasePresenter<View>{
        fun homeApi(map: Map<String, String>)
        fun getNearestRoadPoints(drivers: List<HomeDriver>?)
    }
}