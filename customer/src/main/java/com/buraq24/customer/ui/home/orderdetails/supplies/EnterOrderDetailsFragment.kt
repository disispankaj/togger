package com.buraq24.customer.ui.home.orderdetails.supplies


import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.transition.Explode
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.buraq24.customer.R
import com.buraq24.customer.ui.home.HomeActivity
import com.buraq24.customer.ui.home.ScheduleFragment
import com.buraq24.customer.ui.home.comfirmbooking.ConfirmBookingFragment
import com.buraq24.customer.ui.home.dropofflocation.DropOffLocationFragment
import com.buraq24.customer.ui.menu.etokens.ETokenActivity
import com.buraq24.customer.webservices.models.ServiceRequestModel
import com.buraq24.customer.webservices.models.homeapi.Category
import com.buraq24.utilities.CategoryId
import com.buraq24.utilities.SharedPrefs
import com.buraq24.utilities.constants.SERVICES
import com.buraq24.hideKeyboard
import com.buraq24.showSnack
import com.buraq24.utilities.webservices.models.Service
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.fragment_enter_order_details.*
import java.util.*

/**
 * Fragment to get the details of the order for the service: Gas, Mineral Water and Water Tanker
 * after getting the location and addresses from [DropOffLocationFragment]
 * */
class EnterOrderDetailsFragment : Fragment(), EnterOrderDetailsAdapter.ItemSelectedListener {

    private lateinit var capacityAdapter: ArrayAdapter<String>

    private var quantityAdapter: ArrayAdapter<String>? = null

    private val capacityNames = ArrayList<String>()

    private var categoriesList: ArrayList<Category>? = ArrayList()

    private lateinit var serviceRequest: ServiceRequestModel

    private var adapter: EnterOrderDetailsAdapter? = null

    private var selectedPosition = 0 // selected brand position

    private var subCategoryPosition = 0  // selected product position

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        serviceRequest = (activity as HomeActivity).serviceRequestModel
        serviceRequest.category_brand_id = -1
        serviceRequest.category_brand_product_id = -1
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            /* Enter and exit animations for this fragment */
            enterTransition = Explode()
            exitTransition = Explode()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_enter_order_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        categoriesList?.clear()
        categoriesList?.addAll((activity as HomeActivity).serviceDetails?.categories as ArrayList)
        if (categoriesList?.isEmpty() == false) {
            /*Sets the selected category details */
            val selectedPosition = getSelectedPosition(serviceRequest.category_brand_id ?: 0)
            setCategoriesData(selectedPosition)
            when (categoriesList?.get(0)?.category_id) {
                CategoryId.GAS, CategoryId.WATER_TANKER -> {
                    rvBrands.visibility = View.GONE
                    tvSelectBrand.visibility = View.GONE
                    groupEtokens.visibility = View.GONE
                } // hides the brands listing as there are not brands in gas and water tanker
                else -> {
                    if (categoriesList?.get(0)?.category_id == CategoryId.MINERAL_WATER) {
                        groupEtokens.visibility = View.VISIBLE
                    } else {
                        groupEtokens.visibility = View.GONE
                    }
                    rvBrands.visibility = View.VISIBLE
                    tvSelectBrand.visibility = View.VISIBLE
                    rvBrands.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
                    adapter = EnterOrderDetailsAdapter(activity, categoriesList, selectedPosition)
                    rvBrands.adapter = adapter
                    adapter?.setItemSelectedListener(this)
                    if (categoriesList?.isNotEmpty() == true) {
                        serviceRequest.category_brand_product_id = categoriesList?.get(0)?.products?.get(0)?.category_brand_product_id
                        serviceRequest.product_quantity = categoriesList?.get(0)?.products?.get(0)?.min_quantity
                    }
                }
            }

        } else {
            tvNext?.isEnabled = false // Disable next button if the categories are not coming from the backend
        }
        setListeners()
    }

    override fun onResume() {
        super.onResume()
        etQuantity.setText("")
    }

    override fun onDestroyView() {
        super.onDestroyView()
        if (categoriesList?.isNotEmpty() == true) {
            // de-selects the previous selected brand
            categoriesList?.get(selectedPosition)?.isSelected = false
        }
    }

    /* gets the selected position of the category brand*/
    private fun getSelectedPosition(categoryBrandId: Int): Int {
        for (i in categoriesList.orEmpty().indices) {
            if (categoriesList?.get(i)?.category_brand_id == categoryBrandId) {
                return i
            }
        }
        return 0
    }

    private fun setListeners() {

        tvNext.setOnClickListener {
            tvNext.hideKeyboard()
            if (checkValidations()) {
                if (serviceRequest.product_quantity == 0) {
                    serviceRequest.product_quantity = etQuantity.text.toString().toInt()
                }
                serviceRequest.final_charge = getFinalCharge()
                serviceRequest.future = ServiceRequestModel.BOOK_NOW
                fragmentManager?.beginTransaction()?.replace(R.id.container, ConfirmBookingFragment())?.addToBackStack("backstack")?.commit()
            }
        }
        tvSchedule.setOnClickListener {
            tvNext.hideKeyboard()
            if (checkValidations()) {
                if (serviceRequest.product_quantity == 0) {
                    serviceRequest.product_quantity = etQuantity.text.toString().toInt()
                }
                serviceRequest.final_charge = getFinalCharge()
                serviceRequest.future = ServiceRequestModel.SCHEDULE
                fragmentManager?.beginTransaction()?.replace(R.id.container, ScheduleFragment())?.addToBackStack("backstack")?.commit()
            }
        }
        spinnerCapacity.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                // Do Nothing
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                subCategoryPosition = position
                serviceRequest.product_quantity = 0
                serviceRequest.category_brand_product_id = categoriesList?.get(selectedPosition)?.products?.get(position)?.category_brand_product_id
                serviceRequest.productName = capacityNames[position]
                serviceRequest.price_per_item = categoriesList?.get(selectedPosition)?.products?.get(position)?.price_per_quantity
                        ?: 0f
                setQuantityAdapter()
            }
        }

        spinnerQuantity.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                // Do Nothing
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                if (position < spinnerQuantity?.adapter?.count?.minus(1) ?: 0) {
                    serviceRequest.product_quantity = parent?.getItemAtPosition(position)?.toString()?.toInt()
                } else {
                    serviceRequest.product_quantity = 0
                    etQuantity.visibility = View.VISIBLE
                    spinnerQuantity.visibility = View.GONE
                }
            }
        }

        viewBuyEtokens.setOnClickListener {
            startActivity(Intent(activity, ETokenActivity::class.java)
                    .addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP))
        }
    }

    /* Validates the entered data by user*/
    private fun checkValidations(): Boolean {
        val minQuantity = categoriesList?.get(selectedPosition)?.products?.get(subCategoryPosition)?.min_quantity
                ?: 0 // minimum quantity defined from the admin panel
        return if (serviceRequest.product_quantity == 0
                && (etQuantity.text.isEmpty() || etQuantity.text.toString().toInt() == 0)) {
            // user select others option form the list and doesn't fill the quantity manually
            etQuantity?.showSnack(R.string.please_enter_quantity)
            false
        } else if (serviceRequest.product_quantity ?: 0 == 0 &&
                etQuantity.text.toString().toInt() < minQuantity) {
            /* user filled the quantity less than the minimum quantity */
            etQuantity?.showSnack("${getString(R.string.min_quantity_validation_msg)} $minQuantity")
            false
        } else {
            true
        }

    }

    /* Callback for select brand */
    override fun onItemSelected(position: Int) {
        setCategoriesData(position)
    }

    /* Sets the selected category's data*/
    private fun setCategoriesData(position: Int) {
        selectedPosition = position
        serviceRequest.brandName = categoriesList?.get(position)?.name
        serviceRequest.category_brand_id = categoriesList?.get(position)?.category_brand_id
        capacityNames.clear()
        if (categoriesList?.isNotEmpty() == true) {
            for (i in categoriesList?.get(position)?.products.orEmpty()) {
                capacityNames.add(i.name ?: "")
            }
        }

        capacityAdapter = ArrayAdapter(activity, R.layout.layout_spinner_capacity, capacityNames)
        capacityAdapter.setDropDownViewResource(R.layout.item_capacity)
        spinnerCapacity.adapter = capacityAdapter
        subCategoryPosition = 0
        setQuantityAdapter()
    }

    /* Sets the quantity numerals on the quantity spinner*/
    private fun setQuantityAdapter() {
        val product = categoriesList?.get(selectedPosition)?.products?.get(subCategoryPosition)
        val quantitySize = product?.max_quantity?.minus(product.min_quantity ?: 0)?.plus(1)
        val arr = Array(quantitySize ?: 1)
        { i -> (product?.min_quantity ?: 0.plus(i)).toString() }.toMutableList()
        arr.add(getString(R.string.other)) // Add last option as others to add quantity from keyboard
        quantityAdapter = activity?.let { ArrayAdapter(it, R.layout.layout_spinner_capacity, arr) }
        quantityAdapter?.setDropDownViewResource(R.layout.item_capacity)
        spinnerQuantity.adapter = quantityAdapter
        spinnerQuantity.visibility = View.VISIBLE
        etQuantity.visibility = View.GONE
    }

    /* Calculates all the charges related to the selected service*/
    private fun getFinalCharge(): Double? {
        val service = Gson().fromJson<List<Service>>(SharedPrefs.with(activity).getString(SERVICES, ""), object : TypeToken<List<Service>>() {}.type)
        val product = categoriesList?.get(selectedPosition)?.products?.get(subCategoryPosition)
        val price = product?.price_per_quantity?.times(serviceRequest.product_quantity ?: 1)
                ?.plus(product.alpha_price?.toDouble() ?: 0.0)
                ?.plus((product.price_per_distance?.toDouble() ?: 0.0)
                        .times(serviceRequest.order_distance ?: 0f))
        var buraqPercentage = 0f
        service.forEach {
            if (it.category_id == serviceRequest.category_id) {
                buraqPercentage = it.buraq_percentage ?: 0f
            }
        }
        return 25.0//price?.div(100)?.times(buraqPercentage)?.plus(price)
    }


}
