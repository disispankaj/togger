package com.buraq24.customer.ui.signup.verifytop


import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import android.support.v4.app.Fragment
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import com.buraq24.*
import com.buraq24.customer.R
import com.buraq24.customer.ui.home.HomeActivity
import com.buraq24.customer.ui.signup.entername.EnterNameFragment
import com.buraq24.customer.webservices.models.SendOtp
import com.buraq24.utilities.*
import com.buraq24.utilities.constants.ACCESS_TOKEN
import com.buraq24.utilities.constants.ACCESS_TOKEN_KEY
import com.buraq24.utilities.constants.PROFILE
import com.buraq24.utilities.constants.SERVICES
import com.buraq24.utilities.webservices.models.LoginModel
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_otp.*
import java.util.*

/**
 * Fragment to let user enters the sent otp and redirects into the app if already registered
 * or redirects to the next sign up process if user is not already registered
 * after successful otp verification.
 * */

class OtpFragment : Fragment(), VerifyOtpContract.View {

    private val presenter = VerifyOtpPresenter()

    private lateinit var dialogIndeterminate: DialogIndeterminate

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        activity?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)
        return inflater.inflate(R.layout.fragment_otp, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.attachView(this)
        dialogIndeterminate = DialogIndeterminate(activity)
        pinView.setAnimationEnable(true) // enables animation while entering the otp in the pinview box
        tvPhone.text = String.format(Locale.US, "%s\n%s", getString(R.string.enter_otp_on), arguments?.getString("phone_number"))
        setListeners()
        startTimer()
    }

    override fun onDestroy() {
        countDownTimer.cancel() /* Cancels the otp expiry countdown timer*/
        super.onDestroy()
    }

    private fun setListeners() {
        tvResend.setOnClickListener { resendOtpApiCall() }
        tvEditNumber.setOnClickListener { fragmentManager?.popBackStackImmediate() }
        ivBack.setOnClickListener { fragmentManager?.popBackStackImmediate() }
        fabNext.setOnClickListener {
            if (pinView.text.toString().length < 4) {
                it.showSnack(getString(R.string.otp_validation_message)) // User enters invalid otp
            } else {
                val map = HashMap<String, String>()
                map["otp"] = pinView.text.toString()
                if (CheckNetworkConnection.isOnline(activity)) {
                    presenter.verifyOtp(map) //  Api call for otp verification
                } else {
                    CheckNetworkConnection.showNetworkError(rootView)
                }
            }
        }
        pinView.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                /* Do nothing*/
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                /* Do nothing*/
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s?.length == 4) {
                    /* hides the keyboard automatically after successfully entering 4 digits otp */
                    pinView.hideKeyboard()
                }
            }
        })
    }

    /* Starts otp expiry timer */
    private fun startTimer() {
        tvResend.text = ""
        tvResend.isEnabled = false
        countDownTimer.start()
    }

    /* Count down timer for the otp expiry */
    private val countDownTimer = object : CountDownTimer(120000, 1000) {

        override fun onTick(millisUntilFinished: Long) {
            tvResend?.text = getFormatFromDateUtc(Date(millisUntilFinished), "mm:ss")
        }

        override fun onFinish() {
            tvResend?.text = getString(R.string.resend_otp)
            tvResend?.isEnabled = true
        }
    }

    /* Api call to resend the opt*/
    private fun resendOtpApiCall() {
        if (CheckNetworkConnection.isOnline(activity)) {
            val map = arguments?.getSerializable("map") as HashMap<String, String>
            presenter.sendOtpApiCall(map)
        } else {
            CheckNetworkConnection.showNetworkError(rootView)
        }
    }

    /* Api success for OTP verification api */
    override fun onApiSuccess(response: LoginModel?) {
        if (response?.AppDetail?.user?.name?.isEmpty() == true) {
            /* User is not registered yet. Navigates to the next signup process to enter
               the sign up details */
            ACCESS_TOKEN = response.AppDetail?.access_token ?: ""
            fragmentManager?.let { it1 -> Utils.replaceFragment(it1, EnterNameFragment(), R.id.container, OtpFragment::class.java.simpleName) }
        } else {
            /* User is already registered. Navigation into the home screen*/
            ACCESS_TOKEN = response?.AppDetail?.access_token ?: ""
            SharedPrefs.with(activity).save(PROFILE, response?.AppDetail)
            SharedPrefs.with(activity).save(SERVICES, Gson().toJson(response?.services))
            activity?.finishAffinity()
            startActivity(Intent(activity, HomeActivity::class.java))
        }
        SharedPrefs.with(activity).save(ACCESS_TOKEN_KEY, response?.AppDetail?.access_token)
    }

    /* Api success for send OTP api*/
    override fun resendOtpSuccss(response: SendOtp?) {
        ACCESS_TOKEN = response?.access_token ?: ""
        SharedPrefs.get().save(ACCESS_TOKEN_KEY, ACCESS_TOKEN)
        rootView.showSnack(getString(R.string.otp_resent_successfully))
        pinView.setText("")
        startTimer()
    }

    override fun showLoader(isLoading: Boolean) {
        dialogIndeterminate.show(isLoading)
    }

    override fun apiFailure() {
        rootView.showSWWerror()
    }

    override fun handleApiError(code: Int?, error: String?) {
        rootView.showSnack(error.toString())
    }
}
