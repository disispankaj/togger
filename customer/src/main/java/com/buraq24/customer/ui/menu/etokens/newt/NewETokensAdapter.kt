package com.buraq24.customer.ui.menu.etokens.newt

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.buraq24.customer.R
import com.buraq24.customer.utils.AppUtils
import com.buraq24.customer.webservices.models.Company
import com.buraq24.Utils
import kotlinx.android.synthetic.main.item_new_etoken.view.*
import java.util.*

class NewETokensAdapter(private val companiesList: ArrayList<Company>?) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var listener: OnCompanySelectedListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_new_etoken, parent, false))
    }

    override fun getItemCount(): Int {
        return companiesList?.size ?: 0
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ViewHolder).onBind(companiesList?.get(position))
    }

    fun setOnItemClickedListener(listener: OnCompanySelectedListener) {
        this.listener = listener
    }

    inner class ViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {

        init {
            itemView?.tvViewOffers?.setOnClickListener {
                listener?.onCompanySelected(adapterPosition)
            }
        }

        fun onBind(company: Company?) = with(itemView) {
            tvBrandName?.text = company?.name
            var requestOptions = RequestOptions()
            requestOptions = requestOptions.transforms(RoundedCorners(context?.let { Utils.pxToDp(it, 6) }
                    ?: 6))
            context?.let { Glide.with(it).load(company?.image_url).apply(requestOptions).into(itemView.ivBrand) }
            tvDays.text = AppUtils.getServiceDaysString(company)
        }
    }

    interface OnCompanySelectedListener {
        fun onCompanySelected(position: Int)
    }
}