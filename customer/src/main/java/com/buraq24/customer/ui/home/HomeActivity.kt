package com.buraq24.customer.ui.home

import android.animation.Animator
import android.animation.ValueAnimator
import android.annotation.SuppressLint
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.location.Address
import android.location.Location
import android.os.Bundle
import android.os.Handler
import android.support.design.widget.NavigationView
import android.support.v4.app.FragmentManager
import android.support.v4.content.ContextCompat
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.OrientationHelper
import android.view.MenuItem
import android.view.View
import android.view.animation.LinearInterpolator
import android.widget.Toast
import com.buraq24.customer.R
import com.buraq24.Utils
import com.buraq24.getAccessToken
import com.buraq24.getLanguageId
import com.buraq24.setRoundProfileUrl
import com.buraq24.customer.ui.home.processingrequest.ProcessingRequestFragment
import com.buraq24.customer.ui.home.services.ServicesFragment
import com.buraq24.customer.ui.menu.ReferralActivity
import com.buraq24.customer.ui.menu.bookings.BookingsActivity
import com.buraq24.customer.ui.menu.contactUs.ContactUsActivity
import com.buraq24.customer.ui.menu.emergencyContacts.EContactsActivity
import com.buraq24.customer.ui.menu.etokens.ETokenActivity
import com.buraq24.customer.ui.menu.payments.PaymentsActivity
import com.buraq24.customer.ui.menu.settings.SettingsActivity
import com.buraq24.customer.ui.menu.settings.editprofile.EditProfileActivity
import com.buraq24.customer.ui.signup.SignupActivity
import com.buraq24.customer.utils.AppUtils
import com.buraq24.customer.utils.GridSpacingItemDecoration
import com.buraq24.customer.webservices.models.ServiceRequestModel
import com.buraq24.customer.webservices.models.homeapi.ServiceDetails
import com.buraq24.utilities.*
import com.buraq24.utilities.constants.*
import com.buraq24.utilities.location.LocationProvider
import com.buraq24.utilities.webservices.models.AppDetail
import com.buraq24.utilities.webservices.models.CurrentLocationModel
import com.buraq24.utilities.webservices.models.LoginModel
import com.buraq24.utilities.webservices.models.Service
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.google.android.gms.maps.model.JointType.ROUND
import com.google.android.gms.tasks.OnSuccessListener
import com.google.firebase.FirebaseApp
import com.google.firebase.iid.FirebaseInstanceId
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.nav_header_main2.*
import kotlinx.android.synthetic.main.nav_header_main2.view.*
import org.json.JSONArray
import org.json.JSONObject
import java.util.*

/**
 * This activity is the home screen for the user after successful login.
 * Shows the google maps.
 * Acts  as a container for whole booking process of all kinds of services using fragments.
 * Provides side navigation bar to navigate through other screens of the application.
 * Keeps server updated to the current location of the user.
 * */
class HomeActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener, OnMapReadyCallback, HomeContract.View {

    private var location: Location? = null

    private val presenter = HomePresenter()

    private var dialogIndeterminate: DialogIndeterminate? = null

    private val ratingDrawables = listOf(R.drawable.ic_1, R.drawable.ic_2, R.drawable.ic_3, R.drawable.ic_4, R.drawable.ic_5)

    var srcMarker: Marker? = null

    var destMarker: Marker? = null

    var mapFragment: SupportMapFragment? = null

    var servicesFragment: ServicesFragment? = ServicesFragment()

    var serviceDetails: ServiceDetails? = null

    var serviceRequestModel = ServiceRequestModel()

    var doubleBackToExitPressedOnce = false

    lateinit var googleMapHome: GoogleMap

    lateinit var locationProvider: LocationProvider

    private var blackPolyLine: Polyline? = null
    private var greyPolyLine: Polyline? = null

    private var list: ArrayList<LatLng>?=null



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        presenter.attachView(this)
        dialogIndeterminate = DialogIndeterminate(this)
        AppSocket.get().init(getAccessToken(this)) // Initialize socket
        FirebaseApp.initializeApp(this) // Initialise firebase app
        locationProvider = LocationProvider.CurrentLocationBuilder(this).build()
        locationProvider.getLastKnownLocation(OnSuccessListener {
            try {
                if (supportFragmentManager.backStackEntryCount > 0) {
                    supportFragmentManager.popBackStackImmediate("backstack", FragmentManager.POP_BACK_STACK_INCLUSIVE)
                }
            } catch (e: Exception) {
                e.printStackTrace()
                finishAffinity()
            }
            servicesFragment?.let { it1 -> supportFragmentManager.beginTransaction().add(R.id.container, it1).addToBackStack("backstack").commitAllowingStateLoss() }
            mapFragment = supportFragmentManager
                    .findFragmentById(R.id.map) as SupportMapFragment
            nav_view.setNavigationItemSelectedListener(this)
        })
        drawer_layout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
        setHeaderView()
        setSupportServices()
    }

    override fun onResume() {
        super.onResume()
        val nMgr = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        nMgr.cancelAll() // Clears all the notifications in the system notification tray if exists
        updateDataApiCall((location?.latitude ?: 0.0).toString(), (location?.longitude
                ?: 0.0).toString())
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.detachView()
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            when {
                supportFragmentManager.fragments.find { it is ProcessingRequestFragment } != null -> {
                    // Do nothing
                }
                supportFragmentManager.backStackEntryCount == 1 -> {
                    if (doubleBackToExitPressedOnce) {
                        finish()
                    } else {
                        Toast.makeText(this, getString(R.string.press_back_again_to_exit), Toast.LENGTH_SHORT).show()
                        setBackPressedOnce()
                    }
                }
                else -> super.onBackPressed()
            }
        }
    }


    /* handles the double back press time to exit the app*/
    private fun setBackPressedOnce() {
        doubleBackToExitPressedOnce = true
        Handler().postDelayed({
            doubleBackToExitPressedOnce = false
        }, 2000)    // changes the value back to false if user doesn't pressed again within given time
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        locationProvider.onActivityResult(requestCode, resultCode, data)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        locationProvider.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    /* Sets the profile header in the side navigation panel*/
    private fun setHeaderView() {
        val profile = SharedPrefs.with(this).getObject(PROFILE, AppDetail::class.java)
        val viewHeader = nav_view?.getHeaderView(0)
        viewHeader?.tvName?.text = profile?.user?.name
        val rating = profile?.ratings_avg
        viewHeader?.ivProfilePic?.setRoundProfileUrl(profile?.profile_pic_url)
        viewHeader?.tvNumber?.text = profile?.user?.phone_code + "-" + profile?.user?.phone_number
        if (rating ?: 0 > 0) {
            val checkedRating = if (rating ?: 0 >= 5) 5 else rating
            viewHeader?.ivRating?.setImageResource(ratingDrawables[checkedRating?.minus(1) ?: 0])
            viewHeader?.ivRating?.visibility = View.VISIBLE
            viewHeader?.tvRatingCount?.visibility = View.VISIBLE
            viewHeader?.tvRatingCount?.text = "· " + profile?.rating_count
        } else {
            viewHeader?.ivRating?.visibility = View.INVISIBLE
            viewHeader?.tvRatingCount?.visibility = View.INVISIBLE
        }

        viewHeader?.flProfilePic?.setOnClickListener {
            startActivity(Intent(this, EditProfileActivity::class.java)) }
    }

    /* Callback to handles the selection from the options in the side navigation panel */
    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {

            R.id.nav_sign_out -> {
                val builder = AlertDialog.Builder(this)
                builder.setMessage(getString(R.string.logout_confirmation))
                builder.setPositiveButton(getString(R.string.yes)) { dialog, _ ->
                    dialog?.dismiss()
                    if (CheckNetworkConnection.isOnline(this)) {
                        presenter.logout()
                    } else {
                        Toast.makeText(this, R.string.network_error, Toast.LENGTH_SHORT).show()
                    }
                }
                builder.setNegativeButton(R.string.no) { dialog, _ -> dialog?.dismiss() }
                builder.show()
            }

            R.id.nav_payments -> {
                startActivity(Intent(this, PaymentsActivity::class.java))
            }

            R.id.nav_promotions -> {
                Toast.makeText(this, R.string.coming_soon, Toast.LENGTH_SHORT).show()
            }

            R.id.nav_bookings -> {
                startActivity(Intent(this, BookingsActivity::class.java)
                        .addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP))
            }

            R.id.nav_etokens -> {
                startActivity(Intent(this, ETokenActivity::class.java)
                        .addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP))
            }

            /* R.id.nav_support -> {
                startActivity(Intent(this, SupportActivity::class.java)
                        .addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP))
            }*/

            R.id.nav_emergency_contact -> {
                startActivity(Intent(this, EContactsActivity::class.java)
                        .addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP))
            }

            R.id.nav_referral -> {
                startActivity(Intent(this, ReferralActivity::class.java)
                        .addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP))
            }

            R.id.nav_contact_us -> {
                startActivity(Intent(this, ContactUsActivity::class.java)
                        .addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP))
            }
            R.id.nav_settings -> {
                startActivity(Intent(this, SettingsActivity::class.java)
                        .addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP))
            }
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    /* Callback providing google maps object showing map is ready to use after getMapAsync*/
    @SuppressLint("MissingPermission")
    override fun onMapReady(googleMap: GoogleMap) {
        googleMapHome = googleMap
        googleMapHome.clear()
        googleMapHome.setOnMarkerClickListener { true }
        val mapType = SharedPrefs.with(this).getInt(PREF_MAP_VIEW, GoogleMap.MAP_TYPE_NORMAL)
        googleMapHome.mapType = mapType
        googleMapHome.uiSettings.isTiltGesturesEnabled = false // Disables the gesture to view 3D view of the google maps
        googleMapHome.uiSettings.isMyLocationButtonEnabled = false // Hides the default current locator button of google maps
        locationProvider = LocationProvider.LocationUpdatesBuilder(this).build()
        locationProvider.getLastKnownLocation(OnSuccessListener { location ->
            /* Gets the last known location of the user*/
            location?.let {
                this@HomeActivity.location = location
                googleMapHome.isMyLocationEnabled = true
                updateDataApiCall((location.latitude).toString(), (location.longitude).toString())
                googleMapHome.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(location.latitude, location.longitude), 14f))
                locationProvider.getAddressFromLatLng(location.latitude, location.longitude, object : LocationProvider.OnAddressListener {
                    override fun getAddress(address: String, result: List<Address>) {
                        /* gets the address from the current user's location*/
                        val currentLocation = CurrentLocationModel(location.latitude, location.longitude, address)
                        SharedPrefs.with(this@HomeActivity).save(PrefsConstant.CURRENT_LOCATION, currentLocation)
                        servicesFragment?.onMapReady(googleMap)
                    }
                })
            }
        })
    }

    /* Api call to get the support services listing */
    private fun setSupportServices() {
        if (CheckNetworkConnection.isOnline(this)) {
            presenter.getSupportList()
        }
    }

    /* Api call to update the user's latest data like location and fcm_id */
    private fun updateDataApiCall(lat: String, lng: String) {
        if (!CheckNetworkConnection.isOnline(this)) {
            return
        }
        FirebaseInstanceId.getInstance().instanceId.addOnCompleteListener {
            if (!it.isSuccessful) {
                Logger.e("getInstanceId failed", it.exception?.message ?: "")
            } else {
                // Get new Instance ID token
                val token = it.result.token
                SharedPrefs.with(this).save(FCM_TOKEN, token)
                val map = HashMap<String, String>()
                map["timezone"] = TimeZone.getDefault().id
                map["latitude"] = lat
                map["longitude"] = lng
                map["fcm_id"] = token
                map["language_id"] = getLanguageId(LocaleManager.getLanguage(this)).toString()
                presenter.updateDataApi(map)
                Logger.e("getFirebaseInstanceId", token)
            }
        }
    }

    /* Api success for logout user api.*/
    override fun logoutSuccess() {
        /* Logout successful. Clears*/
        SharedPrefs.with(this@HomeActivity).removeAllSharedPrefsChangeListeners()
        SharedPrefs.with(this@HomeActivity).removeAll()
        finishAffinity()
        startActivity(Intent(this@HomeActivity, SignupActivity::class.java))
        AppSocket.get().disconnect()
        AppSocket.get().off()
    }

    /* Api success for update data api.*/
    override fun onApiSuccess(login: LoginModel?) {
        SharedPrefs.with(this).save(PROFILE, login?.AppDetail)
        SharedPrefs.with(this).save(SERVICES, Gson().toJson(login?.services))
        setHeaderView()
    }

    /* Api success for get Support options listing*/
    override fun onSupportListApiSuccess(response: List<Service>?) {
        rvSupportServices.layoutManager = GridLayoutManager(this, 2,
                OrientationHelper.VERTICAL,
                false)
        rvSupportServices.adapter = SupportServicesAdapter(response)
        val spacingInPixels = resources.getDimensionPixelSize(R.dimen.spacing_home_support)
        rvSupportServices.addItemDecoration(GridSpacingItemDecoration(2, spacingInPixels, true))
    }

    override fun showLoader(isLoading: Boolean) {
        dialogIndeterminate?.show(isLoading)
    }

    override fun apiFailure() {
        /* Show error if needed */
    }

    override fun handleApiError(code: Int?, error: String?) {
        if (code == StatusCode.UNAUTHORIZED) {
            /* User's authorisation expired. Logout and ask user to login again.*/
            AppUtils.logout(this)
        }
    }

    /* Get google maps instance*/
    fun getMapAsync() {
        mapFragment?.getMapAsync(this)
    }

    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(LocaleManager.setLocale(newBase))
    }

    /**
     * Show markers for both source and destination LatLng and draw polyline path
     * between source and destination
     *
     * @param source source LatLng provided by the user.
     * @param destination destination LatLng provided by the user.
     * */
    fun showMarker(source: LatLng?, destination: LatLng?) {
        /*add marker for both source and destination*/

        srcMarker = googleMapHome.addMarker(source?.let { MarkerOptions().position(it) })
        srcMarker?.setIcon((BitmapDescriptorFactory.fromResource(R.drawable.ic_pickup_location_mrkr)))
        srcMarker?.setAnchor(0.5f, 0.5f)
        destMarker = googleMapHome.addMarker(destination?.let { MarkerOptions().position(it) })
        destMarker?.setIcon((BitmapDescriptorFactory.fromResource(R.drawable.ic_drop_location_mrkr)))
        destMarker?.setAnchor(0.5f, 0.5f)
        presenter.drawPolyLine(source, destination, language = LocaleManager.getLanguage(this))
    }

    /* Callback for the google direction api and to draw the polyline between provided
       source and destination location of the user */
    override fun polyLine(jsonRootObject: JSONObject, sourceLatLng: LatLng?, destLatLng: LatLng?) {

       // var line: Polyline? = null
        var lineOptions: PolylineOptions? = null

        blackPolyLine?.remove()
        val routeArray = jsonRootObject.getJSONArray("routes")

        val pathArray = ArrayList<String>()
        if (routeArray.length() == 0) {
            /* No route found */
            return
        }
        var routes: JSONObject? = null
        for (i in routeArray.length() - 1 downTo 0) {
            routes = routeArray.getJSONObject(i)
            val overviewPolylines = routes.getJSONObject("overview_polyline")
            val encodedString = overviewPolylines.getString("points")
            pathArray.add(encodedString)
            list = decodePoly(encodedString)
            val listSize = list?.size
            sourceLatLng?.let { list?.add(0, it) }
            destLatLng?.let { listSize?.plus(1)?.let { it1 -> list?.add(it1, it) } }
            if (i == 0) {

                lineOptions = PolylineOptions()
                        .width(8f)
                        .addAll(list)
                        .color(ContextCompat.getColor(this, R.color.text_dark))
                        .startCap(SquareCap())
                        .endCap(SquareCap())
                        .jointType(ROUND)
                        .geodesic(true)


                blackPolyLine = googleMapHome.addPolyline(lineOptions)

                val greyOptions = PolylineOptions()
                greyOptions.width(8f)
                greyOptions.color(Color.GRAY)
                greyOptions.startCap(SquareCap())
                greyOptions.endCap(SquareCap())
                greyOptions.jointType(ROUND)
                greyPolyLine = googleMapHome.addPolyline(greyOptions)

                animatePolyLine()
            }

            val builder = LatLngBounds.Builder()
            val arr = ArrayList<Marker?>()
            arr.add(srcMarker)
            arr.add(destMarker)
            for (marker in arr) {
                builder.include(marker?.position)
            }
            val bounds = builder.build()
            val cu = CameraUpdateFactory.newLatLngBounds(bounds,
                    Utils.getScreenWidth(this),
                    Utils.getScreenWidth(this).minus(Utils.dpToPx(24).toInt()),
                    Utils.dpToPx(56).toInt())
            googleMapHome.animateCamera(cu)
        }




        val estimatedDistance = (((routes?.get("legs") as JSONArray).get(0) as JSONObject).get("distance") as JSONObject).get("value") as Int
        try {
            serviceRequestModel.order_distance = estimatedDistance / 1000f
        } catch (e: Exception) {
            serviceRequestModel.order_distance = 0f
        }
    }

    /* decode the encoded path we got from the direction api to draw polyline*/
    private fun decodePoly(encoded: String): ArrayList<LatLng> {
        val poly = java.util.ArrayList<LatLng>()
        var index = 0
        val len = encoded.length
        var lat = 0
        var lng = 0
        while (index < len) {
            var b: Int
            var shift = 0
            var result = 0
            do {
                b = encoded[index++].toInt() - 63
                result = result or (b and 0x1f shl shift)
                shift += 5
            } while (b >= 0x20)
            val dlat = if (result and 1 != 0) (result shr 1).inv() else result shr 1
            lat += dlat
            shift = 0
            result = 0
            do {
                b = encoded[index++].toInt() - 63
                result = result or (b and 0x1f shl shift)
                shift += 5
            } while (b >= 0x20)
            val dlng = if (result and 1 != 0) (result shr 1).inv() else result shr 1
            lng += dlng
            val p = LatLng(lat.toDouble() / 1E5,
                    lng.toDouble() / 1E5)
            poly.add(p)
        }
        return poly
    }


    private fun animatePolyLine() {

        val animator = ValueAnimator.ofInt(0, 100)
        animator.duration = 1500
        animator.interpolator = LinearInterpolator()
        animator.addUpdateListener { anim ->
            val latLngList = blackPolyLine!!.points
            val initialPointSize = latLngList.size
            val animatedValue = anim.animatedValue as Int
            val newPoints = animatedValue * list?.size!! / 100

            if (initialPointSize < newPoints) {
                list?.subList(initialPointSize, newPoints)?.let { latLngList.addAll(it) }
                blackPolyLine!!.points = latLngList
            }
        }

        animator.addListener(polyLineAnimationListener)
        animator.start()

    }

    internal var polyLineAnimationListener: Animator.AnimatorListener = object : Animator.AnimatorListener {
        override fun onAnimationStart(animator: Animator) {


        }

        override fun onAnimationEnd(animator: Animator) {

            val blackLatLng = blackPolyLine!!.points
            val greyLatLng = greyPolyLine!!.points

            greyLatLng.clear()
            greyLatLng.addAll(blackLatLng)
            blackLatLng.clear()

            blackPolyLine!!.points = blackLatLng
            greyPolyLine!!.points = greyLatLng

            blackPolyLine!!.zIndex = 2f

            animator.start()
        }

        override fun onAnimationCancel(animator: Animator) {

        }

        override fun onAnimationRepeat(animator: Animator) {


        }
    }




}
