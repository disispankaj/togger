package com.buraq24.customer.ui.signup.verifytop

import com.buraq24.customer.webservices.models.SendOtp
import com.buraq24.utilities.basearc.BasePresenter
import com.buraq24.utilities.basearc.BaseView
import com.buraq24.utilities.webservices.models.LoginModel

class VerifyOtpContract{

    interface View: BaseView{
        fun onApiSuccess(response: LoginModel?)
        fun resendOtpSuccss(response: SendOtp?)
    }

    interface Presenter: BasePresenter<View>{
        fun verifyOtp(map: Map<String, String>)
        fun sendOtpApiCall(map: Map<String, String>)
    }
}