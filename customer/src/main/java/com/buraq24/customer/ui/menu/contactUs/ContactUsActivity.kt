package com.buraq24.customer.ui.menu.contactUs

import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity

import android.widget.Toast
import android.content.Intent
import android.net.Uri
import com.buraq24.customer.R
import com.buraq24.showSWWerror
import com.buraq24.showSnack
import com.buraq24.customer.utils.AppUtils
import com.buraq24.driver.ui.home.contactUs.ContactUsContract
import com.buraq24.utilities.*
import kotlinx.android.synthetic.main.activity_contact_us.*

class ContactUsActivity : AppCompatActivity(), ContactUsContract.View {

    private var presenter = ContactUsPresenter()

    private var dialogIndeterminate: DialogIndeterminate? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contact_us)
        presenter.attachView(this)
        dialogIndeterminate = DialogIndeterminate(this)
        tvBack.setOnClickListener {
            onBackPressed()
        }

        tvSubmit.setOnClickListener {
            val msg = etMsg.text.toString().trim()
            if (!msg.isEmpty()) {
                presenter.sendMsg(msg)
            } else {
                tvSubmit?.showSnack(getString(R.string.please_enter_message))
            }
        }

        tvReachViaMail.setOnClickListener {
            val intent = Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", "support@buraq24.com", null))
            try {
                startActivity(Intent.createChooser(intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP), getString(R.string.send_mail_to_buraq)))
            } catch (ex: android.content.ActivityNotFoundException) {
                Toast.makeText(this@ContactUsActivity, getString(R.string.no_email_app_installed), Toast.LENGTH_SHORT).show()
            }
        }

        tvReachViaPhone.setOnClickListener {
            val phone = "+968-24453336"
            val intent = Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null))
            startActivity(intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP))
        }
    }

    override fun onApiSuccess() {
        Toast.makeText(this, R.string.we_will_reach_soon, Toast.LENGTH_LONG).show()
        finish()
    }

    override fun showLoader(isLoading: Boolean) {
        dialogIndeterminate?.show(isLoading)
    }

    override fun apiFailure() {
        tvSubmit?.showSWWerror()
    }

    override fun handleApiError(code: Int?, error: String?) {
        if (code == StatusCode.UNAUTHORIZED) {
            AppUtils.logout(this)
        }else {
            tvSubmit?.showSnack(error.toString())
        }
    }


    override fun onDestroy() {
        super.onDestroy()
        presenter.detachView()
    }

    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(LocaleManager.setLocale(newBase))
    }

}