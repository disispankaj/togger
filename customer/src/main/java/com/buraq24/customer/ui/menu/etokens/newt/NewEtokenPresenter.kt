package com.buraq24.customer.ui.menu.etokens.newt

import com.buraq24.customer.webservices.ApiResponse
import com.buraq24.customer.webservices.RestClient
import com.buraq24.customer.webservices.models.Company
import com.buraq24.customer.webservices.models.homeapi.Category
import com.buraq24.utilities.basearc.BasePresenterImpl
import com.buraq24.getApiError
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class NewEtokenPresenter : BasePresenterImpl<NewETokenContract.View>(), NewETokenContract.Presenter {

    override fun getBrandsApiCall(categoryId: String?) {
        getView()?.showLoader(true)
        RestClient.get().serviceBrands(categoryId).enqueue(object : Callback<ApiResponse<ArrayList<Category>>> {

            override fun onResponse(call: Call<ApiResponse<ArrayList<Category>>>?, response: Response<ApiResponse<ArrayList<Category>>>?) {
                getView()?.showLoader(false)
                if (response?.isSuccessful == true) {
                    if (response.body()?.statusCode == 200) {
                        getView()?.onBrandsApiSuccess(response.body()?.result)
                    } else {
                        getView()?.handleApiError(response.body()?.statusCode, response.body()?.msg)
                    }
                } else {
                    val errorModel = getApiError(response?.errorBody()?.string())
                    getView()?.handleApiError(errorModel.statusCode, errorModel.msg)
                }
            }

            override fun onFailure(call: Call<ApiResponse<ArrayList<Category>>>?, t: Throwable?) {
                getView()?.showLoader(false)
                getView()?.apiFailure()
            }
        })
    }

    override fun getCompaniesApiCall(map: HashMap<String, String>?) {
        getView()?.showLoader(true)
        RestClient.get().companiesList(map).enqueue(object : Callback<ApiResponse<ArrayList<Company>>> {

            override fun onResponse(call: Call<ApiResponse<ArrayList<Company>>>?, response: Response<ApiResponse<ArrayList<Company>>>?) {
                getView()?.showLoader(false)
                if (response?.isSuccessful == true) {
                    if (response.body()?.statusCode == 200) {
                        getView()?.onCompaniesListApiSuccess(response.body()?.result)
                    } else {
                        getView()?.handleApiError(response.body()?.statusCode, response.body()?.msg)
                    }
                } else {
                    val errorModel = getApiError(response?.errorBody()?.string())
                    getView()?.handleApiError(errorModel.statusCode, errorModel.msg)
                }
            }

            override fun onFailure(call: Call<ApiResponse<ArrayList<Company>>>?, t: Throwable?) {
                getView()?.showLoader(false)
                getView()?.apiFailure()
            }
        })
    }

}