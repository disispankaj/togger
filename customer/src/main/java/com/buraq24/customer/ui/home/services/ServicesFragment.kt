package com.buraq24.customer.ui.home.services


import android.animation.ValueAnimator
import android.app.Activity
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Rect
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.net.ConnectivityManager
import android.os.*
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.content.ContextCompat
import android.transition.Explode
import android.transition.Transition
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.LinearInterpolator
import android.widget.Toast
import com.buraq24.customer.R
import com.buraq24.customer.ui.chatModule.ChatUserListActivity
import com.buraq24.Utils
import com.buraq24.getVehicleResId
import com.buraq24.showSWWerror
import com.buraq24.customer.ui.home.HomeActivity
import com.buraq24.customer.ui.home.deliverystarts.DeliveryStartsFragment
import com.buraq24.customer.ui.home.dropofflocation.DropOffLocationFragment
import com.buraq24.customer.ui.home.invoice.InvoiceFragment
import com.buraq24.customer.ui.home.processingrequest.ProcessingRequestFragment
import com.buraq24.customer.ui.menu.bookings.BookingsActivity
import com.buraq24.customer.utils.AppUtils
import com.buraq24.customer.webservices.ApiResponse
import com.buraq24.customer.webservices.models.HomeMarkers
import com.buraq24.customer.webservices.models.homeapi.HomeDriver
import com.buraq24.customer.webservices.models.homeapi.ServiceDetails
import com.buraq24.customer.webservices.models.order.Order
import com.buraq24.utilities.*
import com.buraq24.utilities.constants.*
import com.buraq24.utilities.location.LocationProvider
import com.buraq24.utilities.webservices.models.Service
import com.google.android.gms.location.LocationAvailability
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationResult
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.*
import com.google.android.gms.tasks.OnSuccessListener
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.yarolegovich.discretescrollview.DiscreteScrollView
import com.yarolegovich.discretescrollview.transform.Pivot
import com.yarolegovich.discretescrollview.transform.ScaleTransformer
import io.socket.client.Ack
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.content_home.*
import kotlinx.android.synthetic.main.fragment_services.*
import org.json.JSONObject
import java.util.*
import kotlin.collections.ArrayList


/**
 * A simple [Fragment] subclass.
 *
 */
class ServicesFragment : Fragment(), View.OnClickListener, ServiceContract.View {

    private val services = ArrayList<String>()

    private lateinit var servicesNames: Array<String?>

    private var map: GoogleMap? = null

    private var lat = 0.0

    private var lng = 0.0

    private var address = ""

    private var selectedServicePosition = 0

    private val presenter = ServicePresenter()

    private var apiInProgress = false

    private var homeActivity: HomeActivity? = null

    private var markersList = ArrayList<HomeMarkers?>()

    private var timerDrivers = Timer()

    private var currentLocation: Location? = null

    private var isCurrentLocation = false

    private var currentZoomLevel = 14f

    private val ICON_WIDTH: Int = 60

    private val ICON_HEIGHT: Int = 90

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_services, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.attachView(this)
        (activity as HomeActivity).ivMarker.visibility = View.VISIBLE
        homeActivity = activity as HomeActivity

        if (SharedPrefs.with(activity).getString(SERVICES, "").isNotEmpty()) {
            val service = Gson().fromJson<List<Service>>(SharedPrefs.with(activity).getString(SERVICES, ""), object : TypeToken<List<Service>>() {}.type)?.filter {
               it.category_id == 7
            }

            servicesNames = service?.associateWith {
                it.name
            }?.values?.toTypedArray()!!
        } else
            servicesNames = resources.getStringArray(R.array.services_list)


        ivSupport?.isEnabled = false
        services.clear()
        services.add(Services.GAS)
        services.add(Services.DRINKING_WATER)
        services.add(Services.WATER_TANKER)
        services.add(Services.FREIGHT)
        services.add(Services.TOW)
        //services.add(Services.FIRE_BRIGADE)
        //services.add(Services.AMBULANCE)
        //services.add(Services.E_WORKSHOP)
        rvCompanies.adapter = context?.let { ServiceAdapter(it, rvCompanies) }
        rvCompanies.setItemTransformer(ScaleTransformer.Builder().setMaxScale(1f)
                .setMinScale(0.9f)
                .setPivotX(Pivot.X.CENTER) // CENTER is a default one
                .setPivotY(Pivot.Y.CENTER) // CENTER is a default one
                .build())
        rvCompanies.setSlideOnFling(true)
        rvCompanies.setSlideOnFlingThreshold(1000)
        rvCompanies?.scrollToPosition((Int.MAX_VALUE / 2) + 2)
        homeActivity?.getMapAsync()
        setListeners()
//        if ((activity as HomeActivity).map != null) {
//            onMapReady(map)
//        }
//        activity?.registerReceiver(networkBroadcastReceiver, IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION))
    }

    override fun onResume() {
        super.onResume()
        startDriverTimer(0)
    }

    override fun onPause() {
        super.onPause()
        timerDrivers.cancel()
        timerDrivers.purge()
        removeAllDriverMarkers()
    }

    private fun setListeners() {
        ivProfile.setOnClickListener(this)
        ivSupport.setOnClickListener(this)
        tvDropOffLocation.setOnClickListener(this)
        tv_whereTo.setOnClickListener(this)
        ivTooger.setOnClickListener(this)
        ivNext.setOnClickListener(this)
        rvCompanies.addOnItemChangedListener(itemChangeListener)
        rvCompanies.addScrollStateChangeListener(scrollListener)
        fabMyLocation.setOnClickListener(this)
        fabSatellite.setOnClickListener(this)
        tvMapNormal.setOnClickListener(this)
        tvSatellite.setOnClickListener(this)
        ivChat.setOnClickListener(this)
        ivGallery.setOnClickListener(this)
    }

    fun onMapReady(map: GoogleMap?) {
        if (isVisible) {
            isCurrentLocation = true
            fabMyLocation?.setImageResource(R.drawable.ic_my_location_blue)
            this.map = map
            this.map?.setOnCameraMoveStartedListener(onCameraMoveStarted)
            this.map?.setOnCameraMoveListener(onCameraMoveListener)
            this.map?.setOnCameraIdleListener(onCameraMoveIdle)
            val mapType = SharedPrefs.with(activity).getInt(PREF_MAP_VIEW, GoogleMap.MAP_TYPE_NORMAL)
            fabSatellite.setImageResource(if (mapType == GoogleMap.MAP_TYPE_HYBRID)
                R.drawable.ic_satellite_blue else R.drawable.ic_satellite)

            tvMapNormal.isSelected = mapType == GoogleMap.MAP_TYPE_NORMAL
            tvSatellite.isSelected = mapType == GoogleMap.MAP_TYPE_HYBRID

            updateDropOffAddress()
        }

        Log.e("", "")
    }

    override fun onClick(v: View?) {
        when (v?.id) {

            R.id.ivGallery -> {
                startActivity(Intent(activity, BookingsActivity::class.java)
                        .addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP))
            }

            R.id.ivChat -> {
                var intent = Intent(activity, ChatUserListActivity::class.java)
                startActivity(intent)
            }

            R.id.fabMyLocation -> {
                LocationProvider.CurrentLocationBuilder(activity).build().getLastKnownLocation(OnSuccessListener {
                    currentLocation = it
                    if (it != null) {
                        focusOnCurrentLocation(it.latitude, it.longitude)
                    } else {
                        startLocationUpdates()
                    }
                })
            }

            R.id.fabSatellite -> {
                val mapType = SharedPrefs.with(activity).getInt(PREF_MAP_VIEW, GoogleMap.MAP_TYPE_NORMAL)
                if (mapType == GoogleMap.MAP_TYPE_NORMAL) {
                    map?.mapType = GoogleMap.MAP_TYPE_HYBRID
                    SharedPrefs.with(activity).save(PREF_MAP_VIEW, GoogleMap.MAP_TYPE_HYBRID)
                    fabSatellite.setImageResource(R.drawable.ic_satellite_blue)
                } else {
                    map?.mapType = GoogleMap.MAP_TYPE_NORMAL
                    SharedPrefs.with(activity).save(PREF_MAP_VIEW, GoogleMap.MAP_TYPE_NORMAL)
                    fabSatellite.setImageResource(R.drawable.ic_satellite)
                }
            }

            R.id.tvMapNormal -> {
                tvMapNormal.isSelected = true
                tvSatellite.isSelected = false
                map?.mapType = GoogleMap.MAP_TYPE_NORMAL
                SharedPrefs.with(activity).save(PREF_MAP_VIEW, GoogleMap.MAP_TYPE_NORMAL)
//                fabSatellite.setImageResource(R.drawable.ic_satellite_blue)
            }

            R.id.tvSatellite -> {
                tvMapNormal.isSelected = false
                tvSatellite.isSelected = true
                map?.mapType = GoogleMap.MAP_TYPE_HYBRID
                SharedPrefs.with(activity).save(PREF_MAP_VIEW, GoogleMap.MAP_TYPE_HYBRID)
//                fabSatellite.setImageResource(R.drawable.ic_satellite)
            }

//            R.id.ivMyLocation -> {
//                LocationProvider.CurrentLocationBuilder(activity).build().getLastKnownLocation(OnSuccessListener {
//                    if (it != null) {
//                        focusOnCurrentLocation(it.latitude, it.longitude)
//                    } else {
//                        startLocationUpdates()
//                    }
//                })
//            }

            R.id.ivProfile -> activity?.drawer_layout?.openDrawer(Gravity.START)

            R.id.ivSupport -> activity?.drawer_layout?.openDrawer(Gravity.END)

            R.id.tv_whereTo,R.id.ivTooger, R.id.tvDropOffLocation, R.id.ivNext -> {
                if (CheckNetworkConnection.isOnline(activity))
                    if (!apiInProgress) {
                        var selectedCategoryId = selectedServicePosition + 1

//                        if (selectedCategoryId == 6) {   // for fright 2 bcz position is 6 but value is 7
//                            selectedCategoryId = 7
//                        }

                       // if (selectedCategoryId == 1) {
                            selectedCategoryId = 7
                       // } else if (selectedCategoryId == 2) {
                       //     selectedCategoryId = 7
                       // }

                        if (selectedCategoryId == CategoryId.GAS
                                || selectedCategoryId == CategoryId.MINERAL_WATER
                                || selectedCategoryId == CategoryId.WATER_TANKER
                                || selectedCategoryId == CategoryId.FREIGHT
                                || selectedCategoryId == CategoryId.FREIGHT_2) {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                val explode = Explode()
                                explode.excludeChildren(rvCompanies, true)
                                val viewRect = Rect()
                                viewRect.set(0, 0, activity?.let { it1 -> Utils.getScreenWidth(it1) }
                                        ?: 0, Utils.dpToPx(90).toInt())
                                explode.epicenterCallback = object : Transition.EpicenterCallback() {
                                    override fun onGetEpicenter(transition: Transition?): Rect {
                                        return viewRect
                                    }
                                }
                                exitTransition = explode
                            }
                            val fragment = DropOffLocationFragment()
                            val bundle = Bundle()
                            bundle.putDouble(Constants.LATITUDE, lat)
                            bundle.putDouble(Constants.LONGITUDE, lng)
                            bundle.putString(Constants.ADDRESS, address)
                            bundle.putInt(Constants.CATEGORY,selectedCategoryId)
                            fragment.arguments = bundle
                            val requestModel = (activity as? HomeActivity)?.serviceRequestModel
                            when (selectedCategoryId) {
                                CategoryId.GAS, CategoryId.MINERAL_WATER, CategoryId.WATER_TANKER -> {
                                    homeActivity?.ivMarker?.setImageResource(R.drawable.ic_drop_location_mrkr)
                                    tvDropOffLocation?.setCompoundDrawablesRelativeWithIntrinsicBounds(activity?.let { ContextCompat.getDrawable(it, R.drawable.ic_drop_off_dot) }, null, null, null)
                                    tvDropOffLocation.setHint(R.string.enter_drop_off_location)
                                    requestModel?.pickup_latitude = 0.0
                                    requestModel?.pickup_longitude = 0.0
                                    requestModel?.pickup_address = ""
                                    requestModel?.dropoff_latitude = lat
                                    requestModel?.dropoff_longitude = lng
                                    requestModel?.dropoff_address = address
                                }
                                CategoryId.FREIGHT, CategoryId.TOW, CategoryId.FREIGHT_2 -> {
                                    homeActivity?.ivMarker?.setImageResource(R.drawable.ic_pickup_location_mrkr)
                                    tvDropOffLocation?.setCompoundDrawablesRelativeWithIntrinsicBounds(activity?.let { ContextCompat.getDrawable(it, R.drawable.ic_pick_up_dot) }, null, null, null)
                                    tvDropOffLocation.setHint(R.string.enter_pickup_location)
                                    requestModel?.pickup_latitude = lat
                                    requestModel?.pickup_longitude = lng
                                    requestModel?.pickup_address = address
                                    requestModel?.dropoff_latitude = 0.0
                                    requestModel?.dropoff_longitude = 0.0
                                    requestModel?.dropoff_address = ""
                                }
                            }
                            requestModel?.isCurrentLocation = isCurrentLocation
                            fragmentManager?.beginTransaction()?.replace(R.id.container, fragment)?.addToBackStack("backstack")?.commit()
                        } else {
                            Toast.makeText(activity, R.string.coming_soon, Toast.LENGTH_SHORT).show()
                        }
                    }
            }
        }
    }

    private fun startLocationUpdates() {
        val locationProvider = LocationProvider.LocationUpdatesBuilder(activity).apply {
            interval = 1000
            fastestInterval = 1000
        }.build()
        locationProvider.startLocationUpdates(object : LocationCallback() {
            override fun onLocationResult(p0: LocationResult?) {
                super.onLocationResult(p0)
                focusOnCurrentLocation(p0?.lastLocation?.latitude, p0?.lastLocation?.longitude)
                locationProvider.stopLocationUpdates(this)
                homeActivity?.getMapAsync()
            }

        })
    }

    private fun focusOnCurrentLocation(latitude: Double?, longitude: Double?) {
        val target = LatLng(latitude ?: 0.0, longitude ?: 0.0)
        val builder = CameraPosition.Builder()
        builder.zoom(14f)
        builder.target(target)
        ((activity as? HomeActivity)?.googleMapHome)?.animateCamera(CameraUpdateFactory.newCameraPosition(builder.build()))
    }

    private val itemChangeListener = DiscreteScrollView.OnItemChangedListener<ServiceAdapter.ViewHolder>
    { viewHolder, adapterPosition ->
        viewHolder?.setSelected(adapterPosition)
        showSelectedText(adapterPosition)
        changeLocationIcons()
        homeApiCall()
    }

    private fun changeLocationIcons() {
        when (selectedServicePosition + 1) {
            /*        CategoryId.GAS, CategoryId.MINERAL_WATER, CategoryId.WATER_TANKER -> {
                        homeActivity?.ivMarker?.setImageResource(R.drawable.ic_drop_location_mrkr)
                        tvDropOffLocation?.setCompoundDrawablesRelativeWithIntrinsicBounds(activity?.let { ContextCompat.getDrawable(it, R.drawable.ic_drop_off_dot) }, null, null, null)
                        tvDropOffLocation?.setHint(R.string.enter_drop_off_location)

                    }
                    CategoryId.FREIGHT, CategoryId.TOW, CategoryId.FREIGHT_2 -> {
                        homeActivity?.ivMarker?.setImageResource(R.drawable.ic_pickup_location_mrkr)
                        tvDropOffLocation?.setCompoundDrawablesRelativeWithIntrinsicBounds(activity?.let { ContextCompat.getDrawable(it, R.drawable.ic_pick_up_dot) }, null, null, null)
                        tvDropOffLocation?.setHint(R.string.enter_pickup_location)
                    }*/
            1, 2 -> {  // for CategoryId.FREIGHT_2  bcz it is 7 but position is 6
                homeActivity?.ivMarker?.setImageResource(R.drawable.ic_pickup_location_mrkr)
                tvDropOffLocation?.setCompoundDrawablesRelativeWithIntrinsicBounds(activity?.let { ContextCompat.getDrawable(it, R.drawable.ic_pick_up_dot) }, null, null, null)
                tvDropOffLocation?.setHint(R.string.enter_pickup_location)
            }
        }
    }

    private fun homeApiCall() {
        removeAllDriverMarkers()
        timerDrivers.cancel()
        val map = HashMap<String, String>()

/*        if ((((selectedServicePosition) % servicesNames.size) + 1) == 6) {  // earlier
            map["category_id"] = (((selectedServicePosition) % servicesNames.size) + 2).toString()
        } else {
            map["category_id"] = (((selectedServicePosition) % servicesNames.size) + 1).toString()
        }*/


       // map["category_id"] = getCategoryId().toString()
        map["category_id"] = getCategoryId().toString()

        //homeActivity?.serviceRequestModel?.category_id=categoryId


        map["latitude"] = lat.toString()
        map["longitude"] = lng.toString()
        map["distance"] = "50000"
        if (CheckNetworkConnection.isOnline(activity)) {
            apiInProgress = true
            presenter.homeApi(map)
            timerDrivers.cancel()
        } else {
            showErrorDialog(true)
        }
    }

    fun getCategoryId(): Int {
        // for cab booking
      /*  return if ((((selectedServicePosition) % servicesNames.size) + 1) == 1) {  // earlier

            (((selectedServicePosition) % servicesNames.size) + 4)
        } else if ((((selectedServicePosition) % servicesNames.size) + 1) == 2) {  // earlier
            (((selectedServicePosition) % servicesNames.size) + 6)
        } else 0*/

       return 7

    }

    private val scrollListener = object : DiscreteScrollView.ScrollStateChangeListener<ServiceAdapter.ViewHolder> {
        override fun onScroll(scrollPosition: Float, currentPosition: Int, newPosition: Int, currentHolder: ServiceAdapter.ViewHolder?, newCurrent: ServiceAdapter.ViewHolder?) {

        }

        override fun onScrollEnd(currentItemHolder: ServiceAdapter.ViewHolder, adapterPosition: Int) {
            showSelectedText(adapterPosition)
        }

        override fun onScrollStart(currentItemHolder: ServiceAdapter.ViewHolder, adapterPosition: Int) {
            currentItemHolder.setUnSelected(adapterPosition)
            hideSelectedText()
        }
    }

    private val onCameraMoveListener = GoogleMap.OnCameraMoveListener {
        //        markersList.forEach {
//            val iconResID = getVehicleResId((selectedServicePosition % servicesNames.size))
//            val icon = BitmapFactory.decodeResource(resources, iconResID)
//            val sizeFactor =
//            val scaledBitmap = Bitmap.createScaledBitmap(icon, icon.width, icon.height, false)
//            it?.setIcon(BitmapDescriptorFactory.fromBitmap(scaledBitmap))
//        }
        markersList.forEach {
            if (currentZoomLevel != map?.cameraPosition?.zoom) {
                currentZoomLevel = map?.cameraPosition?.zoom ?: 14f
                scaleDownMarker(it?.marker, (currentZoomLevel / 14f))
            }
        }
    }

    private val onCameraMoveStarted = GoogleMap.OnCameraMoveStartedListener {
        tvDropOffLocation?.text = ""
        fabMyLocation?.setImageResource(R.drawable.ic_my_location)
        isCurrentLocation = false
    }

    private val onCameraMoveIdle = GoogleMap.OnCameraIdleListener {
        updateDropOffAddress()
        if (MapUtils.getDistanceBetweenTwoPoints(LatLng(map?.cameraPosition?.target?.latitude
                        ?: 0.0,
                        map?.cameraPosition?.target?.longitude
                                ?: 0.0), LatLng(currentLocation?.latitude
                        ?: 0.0, currentLocation?.longitude ?: 0.0)) < 1) {
            fabMyLocation?.setImageResource(R.drawable.ic_my_location_blue)
            isCurrentLocation = true
        } else {
            fabMyLocation?.setImageResource(R.drawable.ic_my_location)
            isCurrentLocation = false
        }

    }

    private fun scaleDownMarker(marker: Marker?, scale: Float) {
        if ((ICON_WIDTH * scale).toInt() > 0 && (ICON_HEIGHT * scale).toInt() > 0 && isAdded) {
            Thread {
                val mapIcon: Bitmap? = getIconBitmap()
                val newBitMap: Bitmap? = Bitmap.createScaledBitmap(
                        mapIcon,
                        (ICON_WIDTH * scale).toInt(),
                        (ICON_HEIGHT * scale).toInt(),
                        false)
                Handler(Looper.getMainLooper()).post {
                    try {
                        marker?.setIcon(BitmapDescriptorFactory.fromBitmap(newBitMap))
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }.start()
        }
    }

    private fun getIconBitmap(): Bitmap {
        requireContext().resources
        return homeActivity?.resources.let { BitmapFactory.decodeResource(it, getVehicleResId(getCategoryId())) }
    }

    private fun updateDropOffAddress() {
        lat = map?.cameraPosition?.target?.latitude ?: 0.0
        lng = map?.cameraPosition?.target?.longitude ?: 0.0
        address = ""
//        LocationProvider.CurrentLocationBuilder(activity).build().getAddressFromLatLng(lat, lng,addressListener)
        activity?.runOnUiThread {
            getAddressFromLatLng(lat, lng)
        }
    }

    private val addressListener = object : LocationProvider.OnAddressListener {
        override fun getAddress(address: String, result: List<Address>) {
            this@ServicesFragment.address = address
            tvDropOffLocation?.text = address
        }
    }

    private fun getAddressFromLatLng(latitude: Double, longitude: Double) {
        val geocoder: Geocoder = Geocoder(activity, LocaleManager.getLocale(activity?.resources))
        Thread(Runnable {

            try {
                val addresses: List<Address> = geocoder.getFromLocation(latitude, longitude, 1)

                // Here 1 represent max location result to returned, by documents it recommended 1 to 5

//            val address = addresses[0].getAddressLine(0) // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
//            val city = addresses[0].locality
//            val state = addresses[0].adminArea
//            val country = addresses[0].countryName
//            val postalCode = addresses[0].postalCode
//            val knownName = addresses[0].featureName
                val addressString = StringBuilder()
                if (addresses.isNotEmpty()) {
                    for (i in 0 until addresses[0].maxAddressLineIndex + 1) {
                        addressString.append(addresses[0].getAddressLine(i))
                        if (i != addresses[0].maxAddressLineIndex) {
                            addressString.append(",")
                        }
                    }
                }

                Handler(Looper.getMainLooper()).post {
                    this@ServicesFragment.address = addressString.toString()
                    tvDropOffLocation?.text = address
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }).start()

    }

    private fun showSelectedText(position: Int) {
        selectedServicePosition = position % servicesNames.size
        tvSelectedService?.text = servicesNames[position % servicesNames.size]
        tvSelectedService?.animate()?.alpha(1f)?.setDuration(200)?.start()
    }

    private fun hideSelectedText() {
        selectedServicePosition = -1
        tvSelectedService?.animate()?.alpha(0f)?.setDuration(600)?.start()
    }

    override fun onDestroyView() {
        presenter.detachView()
//        activity?.unregisterReceiver(networkBroadcastReceiver)
        rvCompanies?.removeItemChangedListener(itemChangeListener)
        rvCompanies?.removeScrollStateChangeListener(scrollListener)
        map?.setOnCameraIdleListener(null)
        map?.setOnCameraMoveStartedListener(null)
        super.onDestroyView()
    }

    override fun onApiSuccess(response: ServiceDetails?) {

//        val isForceUpdate = activity?.let { showForcedUpdateDialog(it, response?.Versioning?.ANDROID?.user?.force.toString(), response?.Versioning?.ANDROID?.user?.normal.toString(), BuildConfig.VERSION_NAME) }
        val isForceUpdate = false

        if (isForceUpdate == false) {
            if (response?.currentOrders?.isNotEmpty() == true) {
                handleCurrentOrderStatus(response.currentOrders[0])

            } else if (response?.lastCompletedOrders?.isNotEmpty() == true) {
                openInvoiceFragment(Gson().toJson(response.lastCompletedOrders[0]))
            } else {
                homeActivity?.serviceDetails = response
                if (response?.categories?.isNotEmpty() == true) {


                    homeActivity?.serviceRequestModel?.category_id = response.categories[0].category_id
                    homeActivity?.serviceRequestModel?.category_brand_id = response.categories[0].category_brand_id
                    startDriverTimer(0)
                    /* for (driver in response.drivers as ArrayList) {
                         createMarker(driver.latitude, driver.longitude, R.drawable.ic)
                     }*/
                }
            }
            apiInProgress = false

        }
    }

    override fun snappedDrivers(snappedDrivers: List<HomeDriver>?) {
        setDriversOnMap(snappedDrivers)
    }

    private fun handleCurrentOrderStatus(order: Order?) {
        when (order?.order_status) {
            OrderStatus.SEARCHING -> {
                openProcessingFragment(order)
            }

            OrderStatus.CONFIRMED, OrderStatus.REACHED, OrderStatus.ONGOING -> {
                openDeliveryStartsFragment(order)
            }
        }
    }

    private fun openInvoiceFragment(orderJson: String?) {
        val fragment = InvoiceFragment()
        val bundle = Bundle()
        bundle.putString("order", orderJson)
        fragment.arguments = bundle
        fragmentManager?.popBackStack("backstack", FragmentManager.POP_BACK_STACK_INCLUSIVE)
        fragmentManager?.beginTransaction()?.add(R.id.container, fragment)?.addToBackStack("backstack")?.commit()
    }

    private fun openProcessingFragment(order: Order?) {
        val fragment = ProcessingRequestFragment()
        val bundle = Bundle()
        bundle.putString(ORDER, Gson().toJson(order))
        bundle.putBoolean("changeTimeOut", true)
        fragment.arguments = bundle
        fragmentManager?.beginTransaction()?.replace(R.id.container, fragment)?.addToBackStack("backstack")?.commit()
    }

    private fun openDeliveryStartsFragment(order: Order?) {
        val fragment = DeliveryStartsFragment()
        val bundle = Bundle()
        bundle.putString("order", Gson().toJson(order))
        fragment.arguments = bundle
        fragmentManager?.popBackStack("backstack", FragmentManager.POP_BACK_STACK_INCLUSIVE)
        activity?.supportFragmentManager?.beginTransaction()?.add(R.id.container, fragment)?.addToBackStack("backstack")?.commit()
    }

    override fun showLoader(isLoading: Boolean) {
        /* if (isLoading) {
             progressbar.visibility = View.VISIBLE
         } else {
             progressbar.visibility = View.GONE
         }*/
    }

    override fun apiFailure() {
        apiInProgress = false
        rootView.showSWWerror()
        showErrorDialog(false)
    }

    override fun handleApiError(code: Int?, error: String?) {
        apiInProgress = false
        if (code == StatusCode.UNAUTHORIZED) {
            AppUtils.logout(activity as Activity)
        } else {
            showErrorDialog(false)
        }
    }

    private fun showErrorDialog(networkError: Boolean) {
        val msgId: Int = if (networkError) {
            R.string.network_error
        } else {
            R.string.api_error_msg
        }
        AlertDialogUtil.getInstance().createOkCancelDialog(activity, R.string.error, msgId,
                R.string.retry, 0, false, object : AlertDialogUtil.OnOkCancelDialogListener {
            override fun onOkButtonClicked() {
                homeApiCall()
            }

            override fun onCancelButtonClicked() {

            }
        }).show()

    }

    private fun startDriverTimer(delay: Long) {
        timerDrivers.cancel()
        timerDrivers = Timer()
        timerDrivers.schedule(object : TimerTask() {
            override fun run() {
                getDrivers()
            }
        }, delay)
    }

    private fun getDrivers() {
        val jsonObject = JSONObject()
        jsonObject.put("type", CommonEventTypes.HOME_MAP_DRIVERS)
        jsonObject.put("access_token", SharedPrefs.get().getString(ACCESS_TOKEN_KEY, ""))
        jsonObject.put("latitude", lat)
        jsonObject.put("longitude", lng)
        jsonObject.put("category_id", homeActivity?.serviceRequestModel?.category_id)
        jsonObject.put("distance", 500000)
        AppSocket.get().emit(Events.COMMON_EVENT, jsonObject, Ack {

            Logger.e("Drivers", it[0].toString())
            Handler(Looper.getMainLooper()).post {
                if (isVisible) {
                    val typeToken = object : TypeToken<ApiResponse<ServiceDetails>>() {}.type
                    val response = Gson().fromJson(it[0].toString(), typeToken) as ApiResponse<ServiceDetails>
                    if (response.statusCode == SUCCESS_CODE) {
                        val drivers = response.result?.drivers
                        if (drivers?.isEmpty() == false) {
                            presenter.getNearestRoadPoints(drivers)
                        } else {
                            setDriversOnMap(drivers)
                        }

                    }
                }
            }
        })
        startDriverTimer(10000)
    }

    private fun setDriversOnMap(drivers: List<HomeDriver>?) {
        val updatedMarkers = ArrayList<HomeMarkers?>()
        val tempMarkers = ArrayList<HomeMarkers?>()
        tempMarkers.addAll(markersList)
        drivers?.forEach { driver ->
            var isDriverFound = false
            tempMarkers.forEach { marker ->
                if (driver.user_detail_id == marker?.marker?.tag as? Int?) {
                    isDriverFound = true
                    markersList.remove(marker)
                    updatedMarkers.add(marker)
                    val distance = FloatArray(3)
                    Location.distanceBetween(marker?.marker?.position?.latitude
                            ?: 0.0, marker?.marker?.position?.longitude ?: 0.0,
                            driver.latitude ?: 0.0, driver.longitude
                            ?: 0.0, distance)
                    val bearing = MapUtils.bearingBetweenLocations(LatLng(marker?.marker?.position?.latitude
                            ?: 0.0, marker?.marker?.position?.longitude ?: 0.0),
                            LatLng(driver.latitude ?: 0.0, driver.longitude
                                    ?: 0.0))
                    if (distance[0] > 5.0) {
                        marker?.marker.let {
                            //                            animateMarker(it, LatLng(driver.latitude ?: 0.0, driver.longitude
//                                    ?: 0.0), false)
                            marker?.moveValueAnimate?.cancel()
                            marker?.moveValueAnimate = animateMarker(it, LatLng(driver.latitude
                                    ?: 0.0, driver.longitude
                                    ?: 0.0))
                            marker?.rotateValueAnimator?.cancel()
                            marker?.rotateValueAnimator = rotateMarker(it, LatLng(driver.latitude
                                    ?: 0.0, driver.longitude
                                    ?: 0.0), bearing)
                        }
//                        marker?.let { rotateMarker(it, driver.bearing ?: 0f) }
                    }
//                    marker.position = LatLng(driver.latitude?:0.0, driver.longitude?:0.0)
                }
            }
            if (!isDriverFound) {
                val driverMarker = createDriverMarker(driver, getVehicleResId(driver.category_id))
                driverMarker?.isFlat = true
                val homeMarkers = HomeMarkers()
                homeMarkers.marker = driverMarker
                updatedMarkers.add(homeMarkers)
            }
        }

        markersList.forEach {
            it?.marker?.remove()
        }
        markersList.clear()
        markersList.addAll(updatedMarkers)
    }

    private fun createDriverMarker(driver: HomeDriver?, iconResID: Int): Marker? {
        val marker = map?.addMarker(MarkerOptions()
                .position(LatLng(driver?.latitude ?: 0.0, driver?.longitude ?: 0.0))
                .anchor(0.5f, 0.5f)
                .rotation(driver?.bearing ?: 0f))
        scaleDownMarker(marker, (currentZoomLevel / 14f))
        marker?.tag = driver?.user_detail_id
        return marker
    }

    private fun removeDriverMarker(marker: Marker?) {
        marker?.remove()
    }

    private fun removeAllDriverMarkers() {
        map?.clear()
        markersList.forEach {
            it?.moveValueAnimate?.cancel()
            it?.rotateValueAnimator?.cancel()
        }
        markersList.clear()
    }


    fun animateMarker(marker: Marker?, latLng: LatLng?): ValueAnimator? {
        var valueAnimatorMove: ValueAnimator? = null
        if (marker != null) {
            val startPosition = marker.position
            val endPosition = LatLng(latLng?.latitude ?: 0.0, latLng?.longitude ?: 0.0)
//            val startRotation = marker.rotation
            val latLngInterpolator = LatLngInterpolator.LinearFixed()
//            valueAnimatorMove?.cancel()
            valueAnimatorMove = ValueAnimator.ofFloat(0f, 1f)
            valueAnimatorMove?.duration = 10000 // duration 1 second
            valueAnimatorMove?.interpolator = LinearInterpolator()
            valueAnimatorMove?.addUpdateListener(object : ValueAnimator.AnimatorUpdateListener {
                override fun onAnimationUpdate(animation: ValueAnimator) {
                    try {
                        val v = animation.animatedFraction
                        val newPosition = latLngInterpolator.interpolate(v, startPosition
                                ?: LatLng(0.0, 0.0), endPosition)
                        marker?.position = newPosition
//                        carMarker?.setRotation(computeRotation(v, startRotation?:0f, currentBearing))
                    } catch (ex: Exception) {
                        ex.printStackTrace()
                    }
                }
            })
            valueAnimatorMove?.start()
        }
        return valueAnimatorMove
    }

    private fun rotateMarker(marker: Marker?, latLng: LatLng?, bearing: Float?): ValueAnimator? {
        var valueAnimatorRotate: ValueAnimator? = null
        if (marker != null) {
            val startPosition = marker.position
            val endPosition = LatLng(latLng?.latitude ?: 0.0, latLng?.longitude ?: 0.0)

            val latLngInterpolator = LatLngInterpolator.LinearFixed()
//            valueAnimatorRotate?.cancel()
            val startRotation = marker.rotation
            valueAnimatorRotate = ValueAnimator.ofFloat(0f, 1f)
//            valueAnimatorRotate?.startDelay = 200
            valueAnimatorRotate?.duration = 5000 // duration 1 second
            valueAnimatorRotate?.interpolator = LinearInterpolator()
            valueAnimatorRotate?.addUpdateListener(object : ValueAnimator.AnimatorUpdateListener {
                override fun onAnimationUpdate(animation: ValueAnimator) {
                    try {
                        val v = animation.animatedFraction
//                        val newPosition = latLngInterpolator.interpolate(v, startPosition
//                                ?: LatLng(0.0, 0.0), endPosition)
//                        carMarker?.setPosition(newPosition)
                        marker.rotation = computeRotation(v, startRotation, bearing
                                ?: 0f)
                    } catch (ex: Exception) {
                        ex.printStackTrace()
                    }
                }
            })
            valueAnimatorRotate?.start()

        }
        return valueAnimatorRotate
    }

    private fun computeRotation(fraction: Float, start: Float, end: Float): Float {
        val normalizeEnd = end - start // rotate start to 0
        val normalizedEndAbs = (normalizeEnd + 360) % 360

        val direction = (if (normalizedEndAbs > 180) -1 else 1).toFloat() // -1 = anticlockwise, 1 = clockwise
        val rotation: Float
        if (direction > 0) {
            rotation = normalizedEndAbs
        } else {
            rotation = normalizedEndAbs - 360
        }

        val result = fraction * rotation + start
        return (result + 360) % 360
    }

    private interface LatLngInterpolator {
        fun interpolate(fraction: Float, a: LatLng, b: LatLng): LatLng
        class LinearFixed : LatLngInterpolator {
            override fun interpolate(fraction: Float, a: LatLng, b: LatLng): LatLng {
                val lat = (b.latitude - a.latitude) * fraction + a.latitude
                var lngDelta = b.longitude - a.longitude
                // Take the shortest path across the 180th meridian.
                if (Math.abs(lngDelta) > 180) {
                    lngDelta -= Math.signum(lngDelta) * 360
                }
                val lng = lngDelta * fraction + a.longitude
                return LatLng(lat, lng)
            }
        }
    }

    private fun animateMarker(marker: Marker, toPosition: LatLng, hideMarker: Boolean): Marker {

        val handler = Handler()
        val start = SystemClock.uptimeMillis()
        val proj = map?.projection
        val startPoint = proj?.toScreenLocation(marker.position)
        val startLatLng = proj?.fromScreenLocation(startPoint)
        val duration: Long = 4000
        val interpolator = LinearInterpolator()

        handler.post(object : Runnable {
            override fun run() {
                val elapsed = SystemClock.uptimeMillis() - start
                val t = interpolator.getInterpolation(elapsed.toFloat() / duration)
                val lng = t * toPosition.longitude + (1 - t) * (startLatLng?.longitude ?: 0.0)
                val lat = t * toPosition.latitude + (1 - t) * (startLatLng?.latitude ?: 0.0)
                marker.position = LatLng(lat, lng)

                if (t < 1.0) {
                    // Post again 16ms later.
                    handler.postDelayed(this, 16)
                } else {
                    marker.isVisible = !hideMarker
                }
            }
        })
        return marker
    }

    private fun rotateMarker(marker: Marker, toRotation: Float) {
        val handler = Handler()
        val start = SystemClock.uptimeMillis()
        val duration = 4000L
        val startRotation = marker.rotation
        val interpolator = LinearInterpolator()
        handler.post(object : Runnable {
            override fun run() {
                val elapsed = SystemClock.uptimeMillis() - start
                val t = interpolator.getInterpolation(elapsed.toFloat() / duration)
                val rot = t * toRotation + (1 - t) * startRotation
                marker.rotation = if (-rot > 180) rot / 2 else rot
//                startRotation = if (-rot > 180) rot / 2 else rot
                if (t < 1.0) {
                    // Post again 16ms later.
                    handler.postDelayed(this, 16)
                }
            }
        })
    }

    private val networkBroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            if (intent.action == ConnectivityManager.CONNECTIVITY_ACTION) {
                homeApiCall()

            }
        }
    }

}
