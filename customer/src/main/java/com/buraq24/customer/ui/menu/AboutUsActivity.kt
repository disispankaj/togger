package com.buraq24.customer.ui.menu

import android.content.Context
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.buraq24.customer.R
import com.buraq24.utilities.LocaleManager
import kotlinx.android.synthetic.main.activity_about_us.*


class AboutUsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_about_us)
        setVersionName()
        setListeners()
    }

    private fun setVersionName() {
        try {
            val pInfo = this.packageManager.getPackageInfo(packageName, 0)
//            tvVersionName.text = """${getString(R.string.version)}  ${pInfo.versionName}"""
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }
    }

    private fun setListeners() {
        tvBack.setOnClickListener { onBackPressed() }
    }

    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(LocaleManager.setLocale(newBase))
    }
}
