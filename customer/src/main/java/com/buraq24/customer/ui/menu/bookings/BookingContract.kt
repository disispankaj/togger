package com.buraq24.customer.ui.menu.bookings

import com.buraq24.customer.webservices.models.order.Order
import com.buraq24.utilities.basearc.BasePresenter
import com.buraq24.utilities.basearc.BaseView

class BookingContract {

    interface View : BaseView {
        fun onApiSuccess(listBookings: ArrayList<Order?>)
        fun onOrderDetailsSuccess(response: Order?)
        fun onCancelApiSuccess()
    }

    interface Presenter : BasePresenter<View> {
        fun getHistoryList(take: Int, skip: Int, type: Int)
        fun getOrderDetails(orderId: Long)
        fun requestCancelApiCall(map: HashMap<String, String>)
    }

}