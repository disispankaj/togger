package com.buraq24.customer.ui.menu.bookings

import android.app.Activity
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.buraq24.customer.R
import com.buraq24.customer.utils.AppUtils
import com.buraq24.customer.webservices.models.order.Order
import kotlinx.android.synthetic.main.fragment_booking.*
import android.support.v7.widget.RecyclerView
import com.buraq24.showSWWerror
import com.buraq24.showSnack
import com.buraq24.utilities.*


class BookingsListFragment : Fragment(), BookingContract.View {

    companion object {
        const val TYPE_PAST = "PAST"
        const val TYPE_UPCOMING = "UPCOMING"
    }

    private val presenter = BookingsPresenter()

    private val listBookings = ArrayList<Order?>()

    private var skip = 0

    private var isLoading = false

    private var isLastPage = false

    private var adapter: BookingAdapter? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_booking, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.attachView(this)
        setAdapter()
        viewFlipperBooking.displayedChild = 0
        resetPagination()
        setListeners()
    }

    private fun setListeners() {
        swipeRefreshLayout.setOnRefreshListener {
            resetPagination()
        }
    }

    private fun resetPagination() {
        skip = 0
        isLastPage = false
        adapter?.setAllItemsLoaded(false)
        bookingHistoryApiCall()
    }

    private fun setAdapter() {
        rvBookings.layoutManager = LinearLayoutManager(rvBookings.context)
        adapter = BookingAdapter(listBookings)
        rvBookings.adapter = adapter
        rvBookings.addOnScrollListener(object : RecyclerView.OnScrollListener() {

            override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val layoutManager = rvBookings.layoutManager as LinearLayoutManager
                val visibleItemCount = layoutManager.childCount
                val totalItemCount = layoutManager.itemCount
                val firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition()

                if (!isLoading && !isLastPage) {
//                    if (layoutManager.findLastVisibleItemPosition() == adapter?.itemCount){
//                        bookingHistoryApiCall()
//                    }
                    if (visibleItemCount + firstVisibleItemPosition >= totalItemCount
                            && firstVisibleItemPosition >= 0
                            && totalItemCount >= Constants.PAGE_LIMIT) {
                        bookingHistoryApiCall()
                    }
                }
            }
        })
    }

    private fun bookingHistoryApiCall() {
        if (!CheckNetworkConnection.isOnline(activity)) {
            CheckNetworkConnection.showNetworkError(swipeRefreshLayout)
            return
        }
        when (arguments?.getString("type")) {
            TYPE_PAST -> {
                isLoading = true
                presenter.getHistoryList(Constants.PAGE_LIMIT, skip, 1)//type=1
            }

            TYPE_UPCOMING -> {
                isLoading = true
                presenter.getHistoryList(Constants.PAGE_LIMIT, skip, 2)//type=2
            }
        }
    }


    override fun onApiSuccess(listBookings: ArrayList<Order?>) {
        isLoading = false
//        this.listBookings.clear()
        swipeRefreshLayout.isRefreshing = false
        if (skip == 0 && listBookings.size == 0) {
            viewFlipperBooking.displayedChild = 2
        } else {
            if (skip == 0) {
                this.listBookings.clear()
            }
            if (listBookings.size < Constants.PAGE_LIMIT) {
                isLastPage = true
                adapter?.setAllItemsLoaded(true)
            }
            skip += Constants.PAGE_LIMIT
            viewFlipperBooking.displayedChild = 1
            this.listBookings.addAll(listBookings)
            adapter?.notifyDataSetChanged()
        }
    }

    override fun onCancelApiSuccess() {
        // Do nothing
    }

    override fun onOrderDetailsSuccess(response: Order?) {
        // Do nothing
    }

    override fun showLoader(isLoading: Boolean) {
//        if (isLoading && !swipeRefreshLayout.isRefreshing)

    }

    override fun apiFailure() {
        isLoading = false
        swipeRefreshLayout.isRefreshing = false
        viewFlipperBooking.displayedChild = 2
        view?.showSWWerror()
    }

    override fun handleApiError(code: Int?, error: String?) {
        isLoading = false
        swipeRefreshLayout.isRefreshing = false
        if (code == StatusCode.UNAUTHORIZED) {
            AppUtils.logout(activity as Activity)
        } else {
            view?.showSnack(error.toString())
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        presenter.detachView()
    }
}
