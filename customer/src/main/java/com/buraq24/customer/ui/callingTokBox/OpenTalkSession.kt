package com.buraq24.customer.ui.callingTokBox


data class OpenTalkSession(var data: Data)

data class Data(var session_id: String,
                var token: String)