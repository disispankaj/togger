package com.buraq24.customer.ui.menu.bookings

import android.app.Activity
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.request.RequestOptions
import com.buraq24.customer.R
import com.buraq24.customer.ui.menu.bookings.upcoming.ETokenBookingDetailActivity
import com.buraq24.customer.utils.AppUtils
import com.buraq24.customer.webservices.models.order.Order
import com.buraq24.utilities.*
import com.google.gson.Gson
import jp.wasabeef.glide.transformations.RoundedCornersTransformation
import kotlinx.android.synthetic.main.item_booking.view.*
import java.util.*


class BookingAdapter(private val listBookings: ArrayList<Order?>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val ITEM = 0

    private val LOADING = 1

    private var allItemsLoaded = false

    override fun getItemViewType(position: Int) =
            if (position >= listBookings.size) LOADING else ITEM


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == ITEM) {
            ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_booking, parent, false))
        } else {
            LoadViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_loading_footer, parent, false))
        }
    }


    override fun getItemCount(): Int = if (allItemsLoaded) listBookings.size else listBookings.size+1

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (getItemViewType(position)) {
            ITEM -> (holder as ViewHolder).bind(listBookings[position])
        }
    }

    inner class ViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {
        private var orderData: Order? = null

        init {
//            val height = Utils.getScreenWidth(itemView?.context as Activity) * 9 / 16
//            itemView.ivMap?.layoutParams?.height = height
            itemView?.setOnClickListener {
                val intent = if (orderData?.payment?.payment_type == PaymentType.E_TOKEN) {
                    Intent(itemView.context, ETokenBookingDetailActivity::class.java)
                } else {
                    Intent(itemView.context, BookingDetailsActivity::class.java)
                }
                it.context?.startActivity(intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                        .putExtra("data", Gson().toJson(orderData)))

            }
        }

        fun bind(orderData: Order?) {
            this.orderData = orderData
            with(itemView) {
                var requestOptions = RequestOptions()
                requestOptions = requestOptions.transforms(CenterCrop(), RoundedCornersTransformation(8,
                        0, RoundedCornersTransformation.CornerType.TOP))
                var lat = orderData?.pickup_latitude ?: 0.0
                var lng = orderData?.pickup_longitude ?: 0.0
                if (orderData?.dropoff_latitude?.toInt() != 0 && orderData?.dropoff_latitude?.toInt() != 0) {
                    lat = orderData?.dropoff_latitude ?: 0.0
                    lng = orderData?.dropoff_longitude ?: 0.0
                }
                Glide.with(context as Activity).applyDefaultRequestOptions(requestOptions).load(MapUtils.getStaticMapWithMarker(context, lat, lng)).into(ivMap)
                tvDateTime.text = DateUtils.getFormattedTimeForBooking(orderData?.order_timings
                        ?: "")
                val paymentTypeString = context?.getString(AppUtils.getPaymentStringId(orderData?.payment?.payment_type
                        ?: ""))
                if (orderData?.payment?.payment_type == PaymentType.E_TOKEN) {
                    tvPaymentTypeAmount.text = "$paymentTypeString · ${orderData.payment?.product_quantity}"
                } else {
                    tvPaymentTypeAmount.text = "$paymentTypeString · ${String.format(Locale.US, "%.2f %s", orderData?.payment?.final_charge?.toFloat(), context.getString(R.string.currency))}"
                }
                tvBookingId.text = "Id:${orderData?.order_token}"
                tvBookingStatus.text = when (orderData?.order_status) {
                    OrderStatus.SERVICE_COMPLETE, OrderStatus.CUSTOMER_CONFIRM_ETOKEN -> {
                        context.getString(R.string.completed)
                    }
                    OrderStatus.SCHEDULED, OrderStatus.DRIVER_PENDING, OrderStatus.DRIVER_APPROVED -> {
                        context.getString(R.string.Scheduled)
                    }
                    OrderStatus.CUSTOMER_CONFIRMATION_PENDING_ETOKEN, OrderStatus.ONGOING -> {
                        context.getString(R.string.ongoing)
                    }
                    OrderStatus.CONFIRMED -> {
                        context.getString(R.string.confirmed)
                    }
                    OrderStatus.REACHED -> {
                        context.getString(R.string.reached)
                    }
                    OrderStatus.C_TIMEOUT_ETOKEN -> {
                        context.getString(R.string.timeout)
                    }
                    else -> {
                        context.getString(R.string.cancelled)
                    }
                }

            }
        }
    }

    inner class LoadViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView)

    fun addBookings(listBookings: ArrayList<Order?>) {
        this.listBookings.addAll(listBookings)
        notifyDataSetChanged()
    }

    fun setAllItemsLoaded(isAllItemsLoaded: Boolean) {
        allItemsLoaded = isAllItemsLoaded
    }
}