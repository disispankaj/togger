package com.buraq24.customer.ui.menu.settings

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import com.buraq24.customer.MainActivity
import com.buraq24.customer.R
import com.buraq24.getLanguage
import com.buraq24.getLanguageId
import com.buraq24.showSWWerror
import com.buraq24.showSnack
import com.buraq24.customer.ui.home.WebViewActivity
import com.buraq24.customer.ui.menu.AboutUsActivity
import com.buraq24.customer.ui.menu.settings.editprofile.EditProfileActivity
import com.buraq24.customer.ui.signup.SignupActivity
import com.buraq24.customer.utils.AppUtils
import com.buraq24.utilities.*
import com.buraq24.utilities.constants.*
import com.buraq24.utilities.webservices.BASE_URL
import com.buraq24.utilities.webservices.models.AppDetail
import kotlinx.android.synthetic.main.activity_settings.*

class SettingsActivity : AppCompatActivity(), View.OnClickListener, SettingsContract.View {

    private var languagesAdapter: ArrayAdapter<String>? = null

    private var languages: Array<String>? = null

    private var presenter = SettingsPresenter()

    private var dialogIndeterminate: DialogIndeterminate? = null

    private var selectedLanguagePosition = 0

    private var tempLanguagePosition = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)

        presenter.attachView(this)
        setListener()
        setLanguageAdapter()
        dialogIndeterminate = DialogIndeterminate(this)
        val notification = SharedPrefs.with(this).getObject(PROFILE, AppDetail::class.java)
                .notifications
        switchPush.isChecked = notification == "1"
    }

    private fun setListener() {
        tvBack.setOnClickListener { onBackPressed() }
        toolbar.setNavigationOnClickListener {
            onBackPressed()
        }
        tvEditProfile.setOnClickListener(this)
        tvTermsConditions.setOnClickListener(this)
        tvPrivacy.setOnClickListener(this)
        tvAboutUs.setOnClickListener(this)
        tvSignout.setOnClickListener(this)
        switchPush.setOnCheckedChangeListener { buttonView, isChecked ->
            presenter.updateSettingsApiCall("", if (isChecked) "1" else "0")
        }
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.tvEditProfile -> {
                startActivity(Intent(this, EditProfileActivity::class.java))
            }
            R.id.tvTermsConditions -> {
                val intent = Intent(this, WebViewActivity::class.java)
                intent.putExtra(TITLE, getString(R.string.terms_and_conditions_caps))
                intent.putExtra(URL_TO_LOAD, "${BASE_URL}/pages/Customer/" +
                        getLanguageId(LocaleManager.getLanguage(this)))
                startActivity(intent)
            }
            R.id.tvPrivacy -> {
                val intent = Intent(this, WebViewActivity::class.java)
                intent.putExtra(TITLE, getString(R.string.privacy))
                intent.putExtra(URL_TO_LOAD, "${BASE_URL}/PPolicy")
                startActivity(intent)
            }
            R.id.tvAboutUs -> {
                startActivity(Intent(this, AboutUsActivity::class.java))
            }
            R.id.tvSignout -> {
                val builder = AlertDialog.Builder(this)
                builder.setMessage(getString(R.string.logout_confirmation))
                builder.setPositiveButton(getString(R.string.yes)) { dialog, _ ->
                    dialog?.dismiss()
                    if (CheckNetworkConnection.isOnline(this)) {
                        presenter.logout()
                    } else {
                        Toast.makeText(this, R.string.network_error, Toast.LENGTH_SHORT).show()
                    }
                }
                builder.setNegativeButton(R.string.no) { dialog, _ -> dialog?.dismiss() }
                builder.show()
            }

//            R.id.switchPush -> {
//                val notification = SharedPrefs.with(this).getObject(PROFILE, AppDetail::class.java).notification
//                presenter.updateSettingsApiCall("", notification)
//            }

        }
    }

    private fun setLanguageAdapter() {
        languages = resources.getStringArray(R.array.languages)
        languagesAdapter = ArrayAdapter(this, R.layout.layout_spinner_languages, languages)
        languagesAdapter?.setDropDownViewResource(R.layout.item_languages)
        spinnerLanguages.adapter = languagesAdapter
        selectedLanguagePosition = getLanguageId(LocaleManager.getLanguage(this)) - 1
        spinnerLanguages.setSelection(getLanguageId(LocaleManager.getLanguage(this)) - 1)
        spinnerLanguages.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                tempLanguagePosition = position
                if (LocaleManager.getLanguage(this@SettingsActivity) != getLanguage(position + 1)) {
                    SharedPrefs.get().save(PREFS_LANGUAGE_ID, position + 1)
                    SharedPrefs.get().save(LANGUAGE_CHANGED, true)
                    presenter.updateSettingsApiCall((position + 1).toString(), if (switchPush.isChecked) "1" else "0")
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {

            }
        }
    }


    private fun setNewLocale(language: String, restartProcess: Boolean): Boolean {
        LocaleManager.setNewLocale(this, language)

        val intent = Intent(this, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
        intent.putExtra("hold", false)
        startActivity(intent)

        if (restartProcess) {
            System.exit(0)
        }
        return true
    }

    override fun onSettingsApiSuccess() {
        if (LocaleManager.getLanguage(this@SettingsActivity) != getLanguage(tempLanguagePosition + 1)) {
            selectedLanguagePosition = tempLanguagePosition
            setNewLocale(getLanguage(selectedLanguagePosition + 1), true)
        }
    }

    override fun showLoader(isLoading: Boolean) {
        dialogIndeterminate?.show(isLoading)
    }

    override fun apiFailure() {
        rootView.showSWWerror()
        SharedPrefs.get().save(PREFS_LANGUAGE_ID, selectedLanguagePosition)
        setLanguageAdapter()
    }

    override fun handleApiError(code: Int?, error: String?) {
        if (code == StatusCode.UNAUTHORIZED) {
            AppUtils.logout(this)
        } else {
            rootView.showSnack(error ?: "")
            SharedPrefs.get().save(PREFS_LANGUAGE_ID, selectedLanguagePosition)
            setLanguageAdapter()
        }
    }

    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(LocaleManager.setLocale(newBase))
    }

    override fun logoutSuccess() {
        /* Logout successful. Clears*/
        SharedPrefs.with(this@SettingsActivity).removeAllSharedPrefsChangeListeners()
        SharedPrefs.with(this@SettingsActivity).removeAll()
        finishAffinity()
        startActivity(Intent(this@SettingsActivity, SignupActivity::class.java))
        AppSocket.get().disconnect()
        AppSocket.get().off()
    }

}