package com.buraq24.customer.ui.menu.settings.editprofile

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.buraq24.customer.R
import com.buraq24.setRoundProfilePic
import com.buraq24.setRoundProfileUrl
import com.buraq24.showSWWerror
import com.buraq24.showSnack
import com.buraq24.customer.utils.AppUtils
import com.buraq24.customer.utils.ImageUtils
import com.buraq24.customer.utils.PermissionUtils
import com.buraq24.utilities.*
import com.buraq24.utilities.constants.PROFILE
import com.buraq24.utilities.webservices.models.AppDetail
import kotlinx.android.synthetic.main.activity_edit_profile.*
import permissions.dispatcher.*
import java.io.File

@RuntimePermissions
class EditProfileActivity : AppCompatActivity(), EditProfileContract.View {

    private val presenter = EditProfilePresenter()

    private var dialogIndeterminate: DialogIndeterminate? = null
    lateinit var userData : AppDetail

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_profile)
        presenter.attachView(this)
        dialogIndeterminate = DialogIndeterminate(this)
        setData()
        setListeners()
    }

    private fun setListeners() {
        tvBack.setOnClickListener{onBackPressed()}
        ivProfilePic.setOnClickListener { getStorageWithPermissionCheck() }
        tvSave.setOnClickListener {
            val name = etName.text.toString().trim()
            if (name.isEmpty()) {
                rootView.showSnack(R.string.name_empty_validation_message)
            }else{
                editProfileApiCall(name)
            }
        }
    }

    private fun setData() {
        userData = SharedPrefs.with(this).getObject(PROFILE, AppDetail::class.java)
        etName.setText(userData.user?.name)
        if(userData?.profile_pic_url?.isEmpty() == false){
            ivProfilePic.setRoundProfileUrl(userData.profile_pic_url)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.detachView()
    }

    private fun editProfileApiCall(name: String){
        if (CheckNetworkConnection.isOnline(this)){
            presenter.updateProfileApiCall(name, File(imagePath),userData)
        }else{
            CheckNetworkConnection.showNetworkError(rootView)
        }
    }

    override fun onApiSuccess(response: AppDetail?) {
        Toast.makeText(this, getString(R.string.profile_updated_successfully), Toast.LENGTH_LONG).show()
        SharedPrefs.with(this).save(PROFILE, response)
        finish()
    }

    override fun showLoader(isLoading: Boolean) {
        dialogIndeterminate?.show(isLoading)
    }

    override fun apiFailure() {
        rootView.showSWWerror()
    }

    override fun handleApiError(code: Int?, error: String?) {
        if (code == StatusCode.UNAUTHORIZED) {
            AppUtils.logout(this)
        }else {
            rootView.showSnack(error ?: "")
        }
    }

    @NeedsPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.CAMERA)
    fun getStorage() {
        ImageUtils.displayImagePicker(this)
    }

    @OnShowRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.CAMERA)
    fun showLocationRationale(request: PermissionRequest) {
        PermissionUtils.showRationalDialog(ivProfilePic.context, R.string.permission_required_to_select_image, request)
    }

    @OnNeverAskAgain(Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.CAMERA)
    fun onNeverAskAgainRationale() {
        PermissionUtils.showAppSettingsDialog(ivProfilePic.context,
                R.string.permission_required_to_select_image)
    }

    @SuppressLint("NeedOnRequestPermissionsResult")
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        onRequestPermissionsResult(requestCode, grantResults)
    }

    private var imagePath: String? = ""

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                ImageUtils.REQ_CODE_CAMERA_PICTURE -> {
                    val file = ImageUtils.getFile(this)
                    ivProfilePic.setRoundProfilePic(file)
                    imagePath = file.absolutePath
                }

                ImageUtils.REQ_CODE_GALLERY_PICTURE -> {
                    data?.let {
                        val file = ImageUtils.getImagePathFromGallery(this, data.data)
                        ivProfilePic.setRoundProfilePic(file)
                        imagePath = file.absolutePath
                    }
                }
            }
        }
    }

    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(LocaleManager.setLocale(newBase))
    }
}
