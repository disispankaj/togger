package com.buraq24.customer.ui.menu.etokens.buy

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.buraq24.customer.R
import com.buraq24.customer.webservices.models.etoken.Etoken
import kotlinx.android.synthetic.main.item_buy_etoken.view.*
import java.util.*

class BuyETokensAdapter(private val eTokensList: ArrayList<Etoken>?) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_buy_etoken, parent, false))
    }

    override fun getItemCount(): Int {
        return eTokensList?.size ?: 0
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ViewHolder).onBind(eTokensList?.get(position))
    }

    inner class ViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {

        init {
            itemView?.btnBuy?.setOnClickListener {
                (itemView.context as? BuyETokenActivity?)?.showConfirmationDialog(eTokensList?.get(adapterPosition))
            }
        }

        fun onBind(eToken: Etoken?) = with(itemView) {
            if (eToken?.quantity == 1) {
                tvETokensCount?.text = "${eToken.quantity} ${context.getString(R.string.e_token)}"
            } else {
                tvETokensCount?.text = "${eToken?.quantity} ${context.getString(R.string.e_tokens)}"
            }
            tvProduct?.text = "${eToken?.categoryBrandProduct?.name}/${context?.getString(R.string.e_token)}"
            tvPrice?.text = "${getFormattedDecimal(eToken?.price
                    ?: 0f)} ${context?.getString(R.string.currency)}"
        }

        private fun getFormattedDecimal(num: Float): String? {
            return String.format(Locale.US, "%.2f", num)
        }
    }
}