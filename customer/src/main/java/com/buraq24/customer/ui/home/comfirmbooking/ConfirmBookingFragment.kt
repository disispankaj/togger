package com.buraq24.customer.ui.home.comfirmbooking


import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Bundle
import android.support.v4.app.Fragment
import android.transition.Explode
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.buraq24.customer.R
import com.buraq24.getCurentDateStringUtc
import com.buraq24.showSWWerror
import com.buraq24.showSnack
import com.buraq24.customer.ui.home.HomeActivity
import com.buraq24.customer.ui.home.processingrequest.ProcessingRequestFragment
import com.buraq24.customer.ui.menu.payments.PaymentsActivity
import com.buraq24.customer.ui.menu.payments.PromoActivity
import com.buraq24.customer.utils.AppUtils
import com.buraq24.customer.utils.CustomDialogClass
import com.buraq24.customer.webservices.models.ServiceRequestModel
import com.buraq24.customer.webservices.models.order.Order
import com.buraq24.utilities.*
import com.buraq24.utilities.constants.*
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_confirm_booking.*
import java.util.*

class ConfirmBookingFragment : Fragment(), ConfirmBookingContract.View, View.OnClickListener {

    private val presenter = ConfirmBookingPresenter()

    private val RQ_PAYMENT_TYPE = 400

    private var dialogIndeterminate: DialogIndeterminate? = null

    private var serviceRequest: ServiceRequestModel? = null

    private var selectedPaymentMethod = PaymentType.CARD

    private var tokenId: Int? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            enterTransition = Explode()
            exitTransition = Explode()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_confirm_booking, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.attachView(this)

        serviceRequest = (activity as HomeActivity).serviceRequestModel
        dialogIndeterminate = DialogIndeterminate(activity)
        setData()
        setListeners()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        presenter.detachView()
    }

    private fun setData() {
        val serviceRequest = (activity as HomeActivity).serviceRequestModel
        tvProductName.text = when (serviceRequest.category_id) {
            CategoryId.GAS -> {
                tvQuantity.text = String.format(Locale.getDefault(), "%s × %s", serviceRequest.productName, serviceRequest.product_quantity)
                tvBook.text = getString(R.string.book_now)
                tvTotalLabel.text = getString(R.string.total)
                getString(R.string.gas)
            }
            CategoryId.MINERAL_WATER -> {
                tvQuantity.text = String.format(Locale.getDefault(), "%s × %s", serviceRequest.productName, serviceRequest.product_quantity)
                tvTotalLabel.text = getString(R.string.total)
                tvBook.text = getString(R.string.order_now)
                serviceRequest.brandName
            }
            CategoryId.WATER_TANKER -> {
                tvQuantity.text = String.format(Locale.getDefault(), "%s × %s", serviceRequest.productName, serviceRequest.product_quantity)
                tvTotalLabel.text = getString(R.string.base_price)
                tvBook.text = getString(R.string.book_now)
                getString(R.string.water_tanker)
            }
            CategoryId.FREIGHT, CategoryId.FREIGHT_2 -> {
                tvQuantity.text = """${serviceRequest.productName}"""
                tvTotalLabel.text = getString(R.string.total)
                tvBook.text = getString(R.string.book_now)
                serviceRequest.brandName
            }

            else -> {
                tvTotalLabel.text = getString(R.string.total)
                tvBook.text = getString(R.string.book_now)
                getString(R.string.default_category_name)
            }
        }

        selectedPaymentMethod = if (serviceRequest.payment_type?.isEmpty() == true) {
            SharedPrefs.with(activity).getString(PAYMENT_TYPE, PaymentType.CARD)
        } else {
            serviceRequest.payment_type ?: PaymentType.CARD
        }
        if (serviceRequest.future == ServiceRequestModel.BOOK_NOW) {
            tvBook.setText(R.string.book_now)
        } else {
            tvBook.text = getString(R.string.schedule) + "\n" + serviceRequest.order_timings_text
        }
        showSelectedPaymentMethod()
    }


    private fun setListeners() {
        tvBook.setOnClickListener(this)
        tvPaymentMethod.setOnClickListener(this)
        tvPromo.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.tvBook -> bookingApiCall()

            R.id.tvPaymentMethod -> selectPaymentMethod()

            R.id.tvPromo -> {
                startActivity(Intent(activity,PromoActivity::class.java))
            }
        }
    }

    private fun bookingApiCall() {
        if (CheckNetworkConnection.isOnline(activity)) {
            val requestModel = (activity as HomeActivity).serviceRequestModel
            if (requestModel.category_id != CategoryId.GAS && requestModel.category_id != CategoryId.FREIGHT
                    && requestModel.category_id != CategoryId.WATER_TANKER
                    && requestModel.category_id != CategoryId.MINERAL_WATER
                    && requestModel.category_id != CategoryId.FREIGHT_2) {
                val dialog = CustomDialogClass(activity, R.style.Custom_Dialog)
                val window = dialog.window
                window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
                dialog.show()
            } else {
                if (requestModel.future == ServiceRequestModel.BOOK_NOW) {
                    requestModel.order_timings = getCurentDateStringUtc()
                }
//            requestModel.order_timings = getCurentDateStringUtc()
                requestModel.payment_type = selectedPaymentMethod
                requestModel.distance = 50000
                if (selectedPaymentMethod == PaymentType.E_TOKEN) {
                    requestModel.organisation_coupon_user_id = tokenId
                } else {
                    requestModel.organisation_coupon_user_id = null
                }
                presenter.requestServiceApiCall(requestModel)
            }
        } else {
            CheckNetworkConnection.showNetworkError(rootView)
        }
    }

    private fun selectPaymentMethod() {
        val intent = Intent(activity, PaymentsActivity::class.java)
        intent.putExtra(CATEGORY_ID, serviceRequest?.category_id)
        intent.putExtra(CATEGORY_BRAND_ID, serviceRequest?.category_brand_id)
        intent.putExtra(CATEGORY_PRODUCT_ID, serviceRequest?.category_brand_product_id)
        intent.putExtra(PAYMENT_TYPE, selectedPaymentMethod)
        startActivityForResult(intent, RQ_PAYMENT_TYPE)
    }

    override fun onApiSuccess(response: Order?) {
        val fragment = ProcessingRequestFragment()
        val bundle = Bundle()
        bundle.putString(ORDER, Gson().toJson(response))
        fragment.arguments = bundle
        fragmentManager?.beginTransaction()?.replace(R.id.container, fragment)?.addToBackStack("backstack")?.commitAllowingStateLoss()
    }

    override fun showLoader(isLoading: Boolean) {
        dialogIndeterminate?.show(isLoading)
    }

    override fun apiFailure() {
        rootView.showSWWerror()
    }

    override fun handleApiError(code: Int?, error: String?) {
        if (code == StatusCode.UNAUTHORIZED) {
            AppUtils.logout(activity as Activity)
        } else {
            rootView.showSnack(error.toString())
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RQ_PAYMENT_TYPE && resultCode == Activity.RESULT_OK && data != null) {
            selectedPaymentMethod = data.getStringExtra(PAYMENT_TYPE)
            if (selectedPaymentMethod == PaymentType.E_TOKEN) {
                tokenId = data.getIntExtra(TOKEN_ID, 0)
            }
            showSelectedPaymentMethod()
        }
    }

    private fun showSelectedPaymentMethod() {
        if (selectedPaymentMethod == PaymentType.CASH) {
        } else {
            //  Need to implement in future
            //
        }
        when (selectedPaymentMethod) {
            PaymentType.CASH -> {
                tvPaymentMethod.text = getString(R.string.cash)
                tvPaymentMethod.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_cash_black, 0, 0)
                tvTotal.text = String.format(Locale.US, "%.2f %s", serviceRequest?.final_charge, getString(R.string.currency))
            }
            PaymentType.CARD -> {
                tvPaymentMethod.text = getString(R.string.card)
                tvPaymentMethod.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_card_book, R.drawable.ic_arrow_down, 0)
                tvTotal.text = String.format(Locale.US, "%.2f %s", serviceRequest?.final_charge, getString(R.string.currency))
            }
//            PaymentType.E_TOKEN -> {
//                tvPaymentMethod.text = getString(R.string.e_tokens)
//                tvPaymentMethod.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_cash_book, R.drawable.ic_arrow_down, 0)
//                tvTotal.text = "0.0 " + getString(R.string.currency)
//            }
        }
    }
}
