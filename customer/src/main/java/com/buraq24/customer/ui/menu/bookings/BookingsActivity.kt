package com.buraq24.customer.ui.menu.bookings

import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.buraq24.GeneralFragmentPagerAdapter
import com.buraq24.customer.R
import com.buraq24.customer.ui.menu.bookings.BookingsListFragment.Companion.TYPE_PAST
import com.buraq24.customer.ui.menu.bookings.BookingsListFragment.Companion.TYPE_UPCOMING
import com.buraq24.utilities.LocaleManager
import kotlinx.android.synthetic.main.activity_bookings.*

class BookingsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bookings)
        setAdapter()
        ivBack.setOnClickListener {
            onBackPressed()
        }
    }


    private fun setAdapter() {
        val adapter = GeneralFragmentPagerAdapter(supportFragmentManager)
        val bundle = Bundle()
        val fragment1 = BookingsListFragment()
        bundle.putString("type", TYPE_PAST)
        fragment1.arguments = bundle
        adapter.addFragment(fragment1, getString(R.string.past))

        val bundle1 = Bundle()
        val fragment2 = BookingsListFragment()
        bundle1.putString("type", TYPE_UPCOMING)
        fragment2.arguments = bundle1
        adapter.addFragment(fragment2, getString(R.string.uncoming))

        viewPagerBookings.adapter = adapter
        tabLayout.setupWithViewPager(viewPagerBookings)

    }

    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(LocaleManager.setLocale(newBase))
    }

}