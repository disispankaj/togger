package com.buraq24.driver.ui.home.emergencyContacts

import com.buraq24.customer.webservices.ApiResponse
import com.buraq24.customer.webservices.RestClient
import com.buraq24.customer.webservices.models.EContact
import com.buraq24.utilities.basearc.BasePresenterImpl
import com.buraq24.getApiError
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class EContactPresenter: BasePresenterImpl<EContactsContract.View>(), EContactsContract.Presenter {

    override fun getContactsList() {
        getView()?.showLoader(true)
        RestClient.get().eContactsList().enqueue(object : Callback<ApiResponse<ArrayList<EContact>>> {
            override fun onResponse(call: Call<ApiResponse<ArrayList<EContact>>>?,
                                    response: Response<ApiResponse<ArrayList<EContact>>>?) {
                getView()?.showLoader(false)
                if (response?.isSuccessful == true) {
                    if (response.body()?.statusCode == 200) {
                        getView()?.onApiSuccess(response.body()?.result)
                    } else {
                        getView()?.handleApiError(response.body()?.statusCode, response.body()?.msg)
                    }
                } else {
                    val errorModel = getApiError(response?.errorBody()?.string())
                    getView()?.handleApiError(errorModel.statusCode, errorModel.msg)
                }
            }

            override fun onFailure(call: Call<ApiResponse<ArrayList<EContact>>>?, t: Throwable?) {
                getView()?.showLoader(false)
                getView()?.apiFailure()
            }

        })
    }
}