package com.buraq24.customer.ui.menu.settings

import com.buraq24.customer.webservices.models.paymentdetails.PaymentDetail
import com.buraq24.utilities.basearc.BasePresenter
import com.buraq24.utilities.basearc.BaseView

class SettingsContract{

    interface View: BaseView{
        fun onSettingsApiSuccess()
        fun logoutSuccess()
    }

    interface Presenter: BasePresenter<View>{
        fun updateSettingsApiCall(languageId: String?, notificationFlag: String?)
        fun logout()
    }
}