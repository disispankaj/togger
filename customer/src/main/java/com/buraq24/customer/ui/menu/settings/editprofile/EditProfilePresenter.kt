package com.buraq24.customer.ui.menu.settings.editprofile

import com.buraq24.customer.webservices.ApiResponse
import com.buraq24.customer.webservices.RestClient
import com.buraq24.utilities.basearc.BasePresenterImpl
import com.buraq24.utilities.constants.SUCCESS_CODE
import com.buraq24.getApiError
import com.buraq24.utilities.webservices.models.AppDetail
import okhttp3.MediaType
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File

class EditProfilePresenter : BasePresenterImpl<EditProfileContract.View>(), EditProfileContract.Presenter {

    override fun updateProfileApiCall(name: String?, profilePicFile: File?, userData: AppDetail?) {
        val map = HashMap<String, RequestBody>()
        map["name"] = RequestBody.create(MediaType.parse("text/plain"), name ?: "")
        map["email"] = RequestBody.create(MediaType.parse("text/plain"), userData?.user?.email
                ?: "")
        map["date_of_birth"] = RequestBody.create(MediaType.parse("text/plain"), userData?.user?.date_of_birth
                ?: "")
        if (profilePicFile != null && profilePicFile.exists()) {
            val body = RequestBody.create(MediaType.parse("image/jpeg"), profilePicFile)
            map["profile_pic\"; filename=\"profilePic.png\" "] = body
        }

        getView()?.showLoader(true)
        RestClient.recreate().get().updateProfile(map).enqueue(object : Callback<ApiResponse<AppDetail>> {
            override fun onResponse(call: Call<ApiResponse<AppDetail>>?, response: Response<ApiResponse<AppDetail>>?) {

                getView()?.showLoader(false)
                if (response?.isSuccessful == true) {
                    if (response.body()?.statusCode == SUCCESS_CODE) {
                        getView()?.onApiSuccess(response.body()?.result)
                    } else {
                        getView()?.handleApiError(response.body()?.statusCode, response.body()?.msg)
                    }
                } else {
                    val errorModel = getApiError(response?.errorBody()?.string())
                    getView()?.handleApiError(errorModel.statusCode, errorModel.msg)
                }
            }

            override fun onFailure(call: Call<ApiResponse<AppDetail>>?, t: Throwable?) {
                getView()?.showLoader(false)
                getView()?.apiFailure()
            }
        })
    }

}