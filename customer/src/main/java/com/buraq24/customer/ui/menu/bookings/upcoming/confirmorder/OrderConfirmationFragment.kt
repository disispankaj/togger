package com.buraq24.customer.ui.menu.bookings.upcoming.confirmorder


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast

import com.buraq24.customer.R
import com.buraq24.showSWWerror
import com.buraq24.showSnack
import com.buraq24.customer.ui.home.invoice.InvoiceFragment
import com.buraq24.customer.utils.AppUtils
import com.buraq24.customer.webservices.models.order.Order
import com.buraq24.utilities.*
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_order_confirmation.*

class OrderConfirmationFragment : Fragment(), OrderConfirmationContract.View {

    private var order: Order? = null

    private var dialogIndeterminate: DialogIndeterminate? = null

    private val presenter = OrderConfirmationPresenter()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_order_confirmation, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.attachView(this)
        dialogIndeterminate = DialogIndeterminate(activity)
        order = Gson().fromJson(arguments?.getString("order"), Order::class.java)
        setData()
        setListeners()
    }

    override fun onDestroy() {
        presenter.detachView()
        super.onDestroy()
    }

    private fun setListeners() {
        tvAccept.setOnClickListener { orderConfirmApiCall(true) }
        tvReject.setOnClickListener { orderConfirmApiCall(false) }
    }

    private fun setData() {
        tvAcceptInvoice.text = order?.brand?.brand_name
        etQuantity.text = "${order?.brand?.name} x ${order?.payment?.product_quantity}"
    }

    private fun orderConfirmApiCall(accept: Boolean) {
        if (CheckNetworkConnection.isOnline(activity)) {
            presenter.confirmOrder(order?.order_id?.toLong() ?: 0L, if (accept) "1" else "0")
        } else {
            CheckNetworkConnection.showNetworkError(rootView)
        }
    }

    override fun onApiSuccess(status: String) {
        if (status == "0") {
            activity?.finish()
            Toast.makeText(activity, getString(R.string.order_reject), Toast.LENGTH_LONG).show()
        } else {
            openInvoiceFragment()
        }
    }

    override fun showLoader(isLoading: Boolean) {
        dialogIndeterminate?.show(isLoading)
    }

    override fun apiFailure() {
        rootView.showSWWerror()
    }

    override fun handleApiError(code: Int?, error: String?) {
        if (code == StatusCode.UNAUTHORIZED) {
            AppUtils.logout(activity)
        } else {
            rootView.showSnack(error ?: "")
        }
    }

    private fun openInvoiceFragment() {
        val fragment = InvoiceFragment()
        val bundle = Bundle()
        bundle.putString("order", Gson().toJson(order))
        fragment.arguments = bundle
        fragmentManager?.beginTransaction()?.replace(R.id.container, fragment)?.commit()
    }

}
