package com.buraq24.customer.ui.menu.etokens.newt

import com.buraq24.customer.webservices.models.Company
import com.buraq24.customer.webservices.models.homeapi.Category
import com.buraq24.utilities.basearc.BasePresenter
import com.buraq24.utilities.basearc.BaseView

class NewETokenContract {

    interface View : BaseView {
        fun onBrandsApiSuccess(brandsList: ArrayList<Category>?)
        fun onCompaniesListApiSuccess(companiesList: ArrayList<Company>?)
    }

    interface Presenter : BasePresenter<View> {
        fun getBrandsApiCall(categoryId: String?)
        fun getCompaniesApiCall(map: HashMap<String, String>?)
    }
}