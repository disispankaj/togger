package com.buraq24.customer.ui.menu.bookings.upcoming

import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.view.animation.AccelerateDecelerateInterpolator
import android.view.animation.Animation
import android.view.animation.RotateAnimation
import android.widget.*
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import com.bumptech.glide.request.RequestOptions
import com.buraq24.customer.R
import com.buraq24.showSWWerror
import com.buraq24.showSnack
import com.buraq24.customer.ui.menu.bookings.BookingContract
import com.buraq24.customer.ui.menu.bookings.BookingsPresenter
import com.buraq24.customer.ui.menu.bookings.upcoming.confirmorder.OrderConfirmationFragment
import com.buraq24.customer.ui.menu.bookings.upcoming.track.TrackActivity
import com.buraq24.customer.utils.AppUtils
import com.buraq24.customer.webservices.models.order.Order
import com.buraq24.utilities.*
import com.google.gson.Gson
import io.socket.emitter.Emitter
import jp.wasabeef.glide.transformations.RoundedCornersTransformation
import kotlinx.android.synthetic.main.activity_etoken_booking_detail.*
import org.json.JSONObject
import java.util.HashMap

class ETokenBookingDetailActivity : AppCompatActivity(), BookingContract.View {

    private val presenter = BookingsPresenter()

    private var dialogIndeterminate: DialogIndeterminate? = null

    private var order: Order? = null

    var rotateAnimation: RotateAnimation? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_etoken_booking_detail)
        presenter.attachView(this)
        dialogIndeterminate = DialogIndeterminate(this)
        order = Gson().fromJson(intent.getStringExtra("data"), Order::class.java)
        setBookingData()
        setListeners()
        llFooter.visibility = View.GONE
    }

    override fun onResume() {
        super.onResume()
        orderDetailsApiCall()
        AppSocket.get().on(Events.ORDER_EVENT, orderEventListener)
    }

    override fun onPause() {
        super.onPause()
        AppSocket.get().off(Events.ORDER_EVENT, orderEventListener)
    }

    override fun onDestroy() {
        presenter.detachView()
        super.onDestroy()
    }

    private fun setBookingData() {
        val requestOptions = RequestOptions()
                .transforms(CenterCrop(), RoundedCornersTransformation(8, 0))
        var lat = order?.pickup_latitude ?: 0.0
        var lng = order?.pickup_longitude ?: 0.0
        if (order?.dropoff_latitude?.toInt() != 0 && order?.dropoff_latitude?.toInt() != 0) {
            lat = order?.dropoff_latitude ?: 0.0
            lng = order?.dropoff_longitude ?: 0.0
        }

        Glide.with(this)
                .load(MapUtils.getStaticMapWithMarker(this, lat, lng))
                .apply(requestOptions)
                .into(ivMap)
        tvDateTime.text = DateUtils.getFormattedTimeForBooking(order?.order_timings ?: "")
        val paymentType = getString(AppUtils.getPaymentStringId(order?.payment?.payment_type
                ?: "0"))
        tvPaymentTypeAmount.text = "$paymentType · ${order?.payment?.product_quantity}"
        tvBookingId.text = "Id:${order?.order_token}"
        tvBookingStatus.text = when (order?.order_status) {
            OrderStatus.SERVICE_COMPLETE, OrderStatus.CUSTOMER_CONFIRM_ETOKEN -> {
                getString(R.string.completed)
            }
            OrderStatus.SCHEDULED, OrderStatus.DRIVER_PENDING, OrderStatus.DRIVER_APPROVED -> {
                getString(R.string.Scheduled)
            }
            OrderStatus.CUSTOMER_CONFIRMATION_PENDING_ETOKEN, OrderStatus.ONGOING -> {
                getString(R.string.ongoing)
            }
            OrderStatus.CONFIRMED -> {
                getString(R.string.confirmed)
            }
            OrderStatus.REACHED -> {
              getString(R.string.reached)
            }
            OrderStatus.C_TIMEOUT_ETOKEN -> {
                getString(R.string.timeout)
            }
            else -> {
                getString(R.string.cancelled)
            }
        }
        tvDropOffLocation.text = if (order?.dropoff_address.isNullOrEmpty()) order?.pickup_address else order?.dropoff_address
        tvBrand.text = order?.brand?.brand_name
        tvProduct.text = "${order?.brand?.name} x ${order?.payment?.product_quantity}"
    }

    private fun setDriverData() {
        if (order?.driver == null) {
            tvDriverName.visibility = View.GONE
            ivProfilePic.visibility = View.GONE
            ivCall.visibility = View.GONE
        } else {
            tvDriverName.visibility = View.VISIBLE
            ivProfilePic.visibility = View.VISIBLE
            ivCall.visibility = View.VISIBLE
            val requestOptions = RequestOptions()
                    .transforms(CircleCrop())
                    .placeholder(R.drawable.ic_bg_menu)
            Glide.with(this)
                    .load(order?.driver?.profile_pic_url)
                    .apply(requestOptions)
                    .into(ivProfilePic)
            tvDriverName.text = order?.driver?.name
        }
    }


    private fun setListeners() {
        tvBack.setOnClickListener { onBackPressed() }
        ivCall.setOnClickListener { callDriver() }
        ivReload.setOnClickListener { orderDetailsApiCall() }
        tvCancel.setOnClickListener { showCancellationDialog() }
        tvTrackOrder.setOnClickListener {
            val intent = Intent(this, TrackActivity::class.java)
            intent.putExtra("order", Gson().toJson(order))
            startActivity(intent)
        }
    }

    private fun callDriver() {
        val phone = order?.driver?.phone_number.toString()
        val intent = Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null))
        startActivity(intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP))
    }

    private val orderEventListener = Emitter.Listener { args ->
        runOnUiThread {
            when (JSONObject(args[0].toString()).getString("type")) {
                OrderEventType.CUSTOMER_CONFIRMATION_PENDING_ETOKEN -> {
                    val orderJson = JSONObject(args[0].toString()).getJSONArray("order").get(0).toString()
                    val orderModel = Gson().fromJson(orderJson, Order::class.java)
                    if (order?.order_id == orderModel?.order_id) {
                        llFooter.visibility = View.GONE
                        order = orderModel
                        setBookingData()
                        openOrderConfirmationFragment()
                    }
                }
                OrderStatus.SERVICE_COMPLETE, OrderStatus.CUSTOMER_CONFIRM_ETOKEN -> {
                    val orderJson = JSONObject(args[0].toString()).getJSONArray("order").get(0).toString()
                    val orderModel = Gson().fromJson(orderJson, Order::class.java)
                    if (order?.order_id == orderModel?.order_id) {
                        order = orderModel
                        setBookingData()
                    }
                    clearAllFragments()
                }

                OrderEventType.ONGOING -> {
                    val orderJson = JSONObject(args[0].toString()).getJSONArray("order").get(0).toString()
                    val orderModel = Gson().fromJson(orderJson, Order::class.java)
                    if (order?.order_id == orderModel?.order_id) {
                        order = orderModel
                        setBookingData()
                    }
                }
                OrderEventType.DRIVER_RATED_SERVICE -> {
                    //Do nothing
                }
                else -> {
                    clearAllFragments()
                }

//                OrderEventType.CUSTOMER_CONFIRM_ETOKEN,
//                OrderEventType.SERVICE_COMPLETE -> {
//
//                }
            }
        }
    }

    private fun orderDetailsApiCall() {
        if (CheckNetworkConnection.isOnline(this)) {
            animateReloadButton(true)
            presenter.getOrderDetails(order?.order_id?.toLong() ?: 0L)
        } else {
            CheckNetworkConnection.showNetworkError(rootView)
        }
    }


    override fun onApiSuccess(listBookings: ArrayList<Order?>) {

    }

    override fun onOrderDetailsSuccess(response: Order?) {
        animateReloadButton(false)
        order = response
        setBookingData()
        setDriverData()
        tvBrand.visibility = View.VISIBLE
        tvProduct.visibility = View.VISIBLE
        when (response?.order_status) {
            OrderStatus.ONGOING,
            OrderStatus.CONFIRMED -> {
                llFooter.visibility = View.VISIBLE
                clearAllFragments()
            }
            OrderStatus.CUSTOMER_CONFIRMATION_PENDING_ETOKEN -> {
                llFooter.visibility = View.GONE
                openOrderConfirmationFragment()
            }
            else -> {
                llFooter.visibility = View.GONE
                clearAllFragments()
            }
        }
    }

    private fun clearAllFragments() {
        for (fragment in supportFragmentManager.fragments) {
            supportFragmentManager.beginTransaction().remove(fragment).commitAllowingStateLoss()
        }
    }

    override fun onCancelApiSuccess() {
        Toast.makeText(this, R.string.order_cancelled, Toast.LENGTH_LONG).show()
        finish()
    }

    override fun showLoader(isLoading: Boolean) {
//        dialogIndeterminate?.show(isLoading)
    }

    override fun apiFailure() {
        animateReloadButton(false)
        rootView.showSWWerror()
    }

    override fun handleApiError(code: Int?, error: String?) {
        animateReloadButton(false)
        if (code == StatusCode.UNAUTHORIZED) {
            AppUtils.logout(this)
        } else {
            rootView.showSnack(error ?: "")
        }
    }

    private fun showCancellationDialog() {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(true)
        dialog.setCanceledOnTouchOutside(true)
        dialog.setContentView(R.layout.dialog_cancel_reason)
        dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

     //   val etMessage = dialog.findViewById(R.id.etMessage) as EditText
//        val cancelSpinner = dialog?.findViewById<Spinner>(R.id.cancelSpinner)
//
//        val cancelReason = arrayOf("driver told to cancel", "ETA was too long",
//                "driver's call not reachable", "Booked by mistake")
//        if (cancelSpinner != null) {
//            val arrayAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, cancelReason)
//            cancelSpinner.adapter = arrayAdapter
//
//            cancelSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
//                override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
//                    cancelReason[position]
//                    // Toast.makeText(this@MainActivity, getString(R.string.selected_item) + " " + personNames[position], Toast.LENGTH_SHORT).show()
//                }
//
//                override fun onNothingSelected(parent: AdapterView<*>) {
//                    // Code to perform some action when nothing is selected
//                }
//            }
//        }

        val cancelGroup = dialog.findViewById<RadioGroup>(R.id.cancelGroup)
        val btn1 = dialog.findViewById<RadioButton>(R.id.btn1)
        val btn2 = dialog.findViewById<RadioButton>(R.id.btn2)
        val btn3 = dialog.findViewById<RadioButton>(R.id.btn3)

        val tvSubmit = dialog.findViewById(R.id.tvSubmit) as TextView
        tvSubmit.setOnClickListener {
            if (cancelGroup?.checkedRadioButtonId.toString().trim().isNotEmpty()) {
                val map = HashMap<String, String>()
                map["order_id"] = order?.order_id.toString()
                map["cancel_reason"] = cancelGroup?.checkedRadioButtonId.toString().trim()
                if (CheckNetworkConnection.isOnline(this)) {
                    presenter.requestCancelApiCall(map)
                    dialog.dismiss()
                } else {
                    CheckNetworkConnection.showNetworkError(rootView)
                }
            } else {
                Toast.makeText(this, getString(R.string.cancellation_reason_validation_text), Toast.LENGTH_SHORT).show()
            }
        }

        dialog.show()
        dialog.window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
    }

    private fun animateReloadButton(animate: Boolean) {
        if (animate) {
            rotateAnimation = RotateAnimation(0f, 360f, Animation.RELATIVE_TO_SELF,
                    0.5f, Animation.RELATIVE_TO_SELF, 0.5f)
            rotateAnimation?.repeatCount = Animation.INFINITE
            rotateAnimation?.duration = 1000
            rotateAnimation?.startOffset = 100L
            rotateAnimation?.interpolator = AccelerateDecelerateInterpolator()
            ivReload.startAnimation(rotateAnimation)
            ivReload.isEnabled = false
        } else {
            ivReload.isEnabled = true
            rotateAnimation?.cancel()
            ivReload.clearAnimation()
        }
    }

    private fun openOrderConfirmationFragment() {
        val fragment = OrderConfirmationFragment()
        val bundle = Bundle()
        bundle.putString("order", Gson().toJson(order))
        fragment.arguments = bundle
        supportFragmentManager?.beginTransaction()?.replace(R.id.container, fragment)?.commit()
    }

}
