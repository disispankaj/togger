package com.buraq24.customer.ui.menu.etokens.purchased

import com.buraq24.customer.webservices.models.etoken.PurchasedEToken
import com.buraq24.customer.webservices.models.paymentdetails.PaymentDetail
import com.buraq24.utilities.basearc.BasePresenter
import com.buraq24.utilities.basearc.BaseView

class PurchasedETokenContract {

    interface View : BaseView {
        fun onPurchasedETokensApiSuccess(purchasedETokensList: ArrayList<PurchasedEToken>?)
    }

    interface Presenter : BasePresenter<View> {
        fun purchasedETokenListApi(map: HashMap<String, String>)
    }
}