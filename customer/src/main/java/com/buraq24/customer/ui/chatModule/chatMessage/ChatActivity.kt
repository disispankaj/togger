package com.buraq24.customer.ui.chatModule.chatMessage

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.buraq24.*
import com.buraq24.customer.R
import com.buraq24.customer.ui.callingTokBox.AudioChatActivity
import com.buraq24.customer.ui.chatModule.Utility
import com.buraq24.customer.ui.chatModule.*
import com.buraq24.customer.webservices.ApiResponse
import com.buraq24.customer.webservices.RestClient
import com.buraq24.utilities.*
import com.buraq24.utilities.chatModel.ChatMessageListing
import com.buraq24.utilities.chatModel.MediaUpload
import com.buraq24.utilities.constants.PROFILE
import com.buraq24.utilities.webservices.BASE_URL_LOAD_IMAGE
import com.buraq24.utilities.webservices.models.AppDetail
import com.google.gson.Gson
import io.socket.client.Socket
import kotlinx.android.synthetic.main.activity_chat.*
import okhttp3.MediaType
import okhttp3.RequestBody
import pub.devrel.easypermissions.EasyPermissions
import pub.devrel.easypermissions.PermissionRequest
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class ChatActivity : AppCompatActivity(), ChatMessageContract.View, EasyPermissions.PermissionCallbacks, AppSocket.OnMessageReceiver, Utility.PassValues, AppSocket.ConnectionListener {


    private val chatMessages = ArrayList<ChatMessageListing>()

    private val presenter = ChatMessagesPresenter()

    private lateinit var adapter: ChatAdapter

    private lateinit var userId: String

    private lateinit var otherUserId: String
    private lateinit var otherUserName: String
    private lateinit var otherUserPicture: String

    private val permissionCode = 209

    private val permission = arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA)

    private var mediaType: String = ""

    private lateinit var utility: Utility

    private var callingApi = false

    private var socketReconnecting = false

    private lateinit var layoutManager: WrapContentLinearLayoutManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat)
        presenter.attachView(this)
        layoutManager = WrapContentLinearLayoutManager(this)
        layoutManager.stackFromEnd = true
        recyclerView.layoutManager = layoutManager
        adapter = ChatAdapter(this, chatMessages)
        recyclerView.adapter = adapter
        userId = SharedPrefs.with(this).getObject(PROFILE, AppDetail::class.java).user_id.toString()

        otherUserId = intent.getStringExtra(RECEIVER_ID)
        otherUserName = intent.getStringExtra(USER_NAME)
        otherUserPicture = intent.getStringExtra(PROFILE_PIC_URL)


        if (CheckNetworkConnection.isOnline(this)) {
            getChatApiCall()
        } else {
            CheckNetworkConnection.showNetworkError(rootView)
        }
        setListeners()
        setToolbar()
        AppSocket.get().connect()
        registerReceiver(broadcastReceiver, IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION))


        ivChatPhone.setOnClickListener {
            var intent = Intent(this, AudioChatActivity::class.java)
            intent.putExtra(RECEIVER_ID, otherUserId)
            intent.putExtra(USER_NAME, otherUserName)
            intent.putExtra(PROFILE_PIC_URL, otherUserPicture)
            startActivity(intent)
        }


    }

    override fun onBackPressed() {
        if (chatMessages.isNotEmpty()) {
            var x = chatMessages.size - 1
            while (x > 0) {
                if ((chatMessages[x].send_by == userId && chatMessages[x].isSent == true) || chatMessages[x].send_by == otherUserId) {
                    val intent = Intent()
                    intent.putExtra(LAST_MESSAGE, Gson().toJson(chatMessages[x]))
                    intent.putExtra(USER_ID, otherUserId)
                    setResult(Activity.RESULT_OK, intent)
                    break
                }
                x--
            }
        }
        super.onBackPressed()
    }

    private fun setToolbar() {
        toolbar.setNavigationIcon(R.drawable.ic_back)
        tvTitle.text = otherUserName
        Glide.with(this).load(otherUserPicture)
                .apply(RequestOptions().circleCrop()).into(ivProfilePic)
    }

    private fun setListeners() {
        ivDown.setOnClickListener { recyclerView.scrollToPosition(chatMessages.size - 1) }
        AppSocket.get().addConnectionListener(this)
        AppSocket.get().addOnMessageReceiver(this)
        toolbar.setNavigationOnClickListener { onBackPressed() }
        ivSend.setOnClickListener {
            val msg = etMessage.text.toString().trim()
            etMessage.setText("")
            val message = ChatMessageListing("", getCurrentDateTime(), otherUserId, userId, msg, "", getCurrentDateTime(), ChatType.TEXT, false, "", otherUserName, otherUserPicture, otherUserId)
            message.let { chatMessages.add(it) }
            adapter.notifyItemInserted(chatMessages.size - 1)
            recyclerView.scrollToPosition(chatMessages.size - 1)
            sendMessage(message)
        }
        etMessage.addTextChangedListener(textChangeListener)
        ivAdd.setOnClickListener { showMediaOption() }
        recyclerView.addOnScrollListener(onScrollListener)
    }

    private var onScrollListener = object : RecyclerView.OnScrollListener() {
        override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)
            if (chatMessages.isNotEmpty() && layoutManager.findFirstVisibleItemPosition() == 0 && !callingApi) {
                getChatApiCall(chatMessages.first().c_id ?: "")
            }
            if ((recyclerView?.layoutManager as LinearLayoutManager)
                            .findLastCompletelyVisibleItemPosition() > chatMessages.size - 4) {
                ivDown.visibility = View.GONE
                ivDown.setBackgroundResource(R.drawable.shape_scroll_bottom_chat)
            } else {
                ivDown.visibility = View.VISIBLE
            }
        }
    }

    private val textChangeListener = object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {

        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            if (s?.trim()?.isEmpty() == true) {
                ivSend.setImageResource(R.drawable.ic_send)
                ivSend.isEnabled = false
            } else {
                ivSend.setImageResource(R.drawable.ic_send_b)
                ivSend.isEnabled = true
            }
        }

    }

    private fun getChatApiCall(messageId: String = "", skip: Int = 0, messageOrder: String = MessageOrder.BEFORE) {
        callingApi = true
        presenter.getChatMessagesApiCall(getAuthAccessToken(this), messageId, otherUserId, Constants.PAGE_LIMIT, skip, messageOrder)
    }

    fun sendMessage(message: ChatMessageListing?) {
        if (AppSocket.get().isConnected) {
            AppSocket.get().sendMessage(message) { message ->
                val index = chatMessages.indexOf(chatMessages.find {
                    it.message_id == message?.message_id
                })
                chatMessages[index].isSent = true
                adapter.notifyItemChanged(index, adapter.CHANGE_SENT_STATUS)
                Log.e("got_ack", message.toString())
                recyclerView.scrollToPosition(chatMessages.size - 1)
            }
        } else {
            CheckNetworkConnection.showNetworkError(rootView)
            setFailedMessages()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.detachView()
        AppSocket.get().removeOnMessageReceiver(this)
        unregisterReceiver(broadcastReceiver)
    }

    override fun showLoader(isLoading: Boolean) {
        progressbar.visibility = if (isLoading) View.VISIBLE else View.INVISIBLE
    }

    override fun chatMessagesApiSuccess(chatList: ArrayList<ChatMessageListing>?, messageOrder: String) {

        Thread(Runnable {
            callingApi = false
            if (messageOrder == MessageOrder.BEFORE) {
                if (chatMessages.isEmpty()) {
                    chatList?.let { chatMessages.addAll(it) }
                    runOnUiThread { adapter.notifyDataSetChanged() }
                } /*else {
                    chatList?.let { chatMessages.addAll(0, it) }
                    val tempChat = ArrayList<ChatMessageListing>()
                    tempChat.addAll(chatMessages.distinctBy { it.message_id })
                    tempChat.sortBy { convertDateTimeInMillis(it.sent_at) }
                    chatMessages.clear()
                    chatMessages.addAll(tempChat)
                    runOnUiThread {
                        chatList?.size?.let { adapter.notifyItemRangeInserted(0, it) }
                        adapter.notifyItemChanged(chatList?.size ?: 0)
                    }
                }*/
            } else {
                chatList?.let { chatMessages.addAll(it) }
                val tempChat = ArrayList<ChatMessageListing>()
                tempChat.addAll(chatMessages.distinctBy { it.message_id })
                tempChat.sortBy { convertDateTimeInMillis(it.sent_at) }
                runOnUiThread {
                    chatMessages.clear()
                    chatMessages.addAll(tempChat)
                    adapter.notifyDataSetChanged()
                    if (messageOrder == MessageOrder.AFTER) {
                        scrollToBottom()
                    }
                }
            }


        }).start()
    }

    override fun apiFailure() {
        callingApi = false
        rootView.showSWWerror()
    }

    override fun handleApiError(code: Int?, errorBody: String?) {
        callingApi = false
        errorBody?.let { rootView.showSnack(it) }
    }


    override fun onConnectionStatusChanged(status: String?) {
        when (status) {
            Socket.EVENT_CONNECT -> {
                if (socketReconnecting) {
                    socketReconnecting = false
                    var x = chatMessages.size - 1
                    while (x > 0) {
                        if (chatMessages[x].isSent == true) {
                            getChatApiCall(messageId = chatMessages[x].c_id.toString(), messageOrder = MessageOrder.AFTER)
                            break
                        }
                        x--
                    }
                }
            }
            Socket.EVENT_DISCONNECT -> {
                socketReconnecting = true
                setFailedMessages()
            }
            Socket.EVENT_ERROR -> {

            }
            else -> {

            }
        }
    }

    private fun setFailedMessages() {
        for (i in 0 until chatMessages.size) {
            if (chatMessages[i].isSent == false) {
                chatMessages[i].isFailed = true
                adapter.notifyItemChanged(i, adapter.CHANGE_SENT_STATUS)
            }
        }
    }

    override fun onMessageReceive(message: ChatMessageListing?) {
        ivDown.setBackgroundResource(R.drawable.shape_scroll_bottom_chat_blue)
        val index = chatMessages.indexOfFirst { message?.message_id == it.message_id }
        if (index == -1) {
            message?.let { chatMessages.add(it) }
            var msg = chatMessages[chatMessages.size - 1]
            msg.message_id = chatMessages[0].message_id
            chatMessages.set(chatMessages.size - 1, msg)
            adapter.notifyItemChanged(chatMessages.size - 1)
            scrollToBottom()
        }
    }

    private fun scrollToBottom() {
        if ((recyclerView.layoutManager as LinearLayoutManager)
                        .findLastCompletelyVisibleItemPosition() > chatMessages.size - 4) {
            recyclerView.scrollToPosition(chatMessages.size - 1)
        }
    }

    private fun showMediaOption() {
        if (checkPermissionStatus()) {
            val bottomSheetMedia = BottomSheetMedia()
            bottomSheetMedia.setOnCameraListener(View.OnClickListener {
                openCamera()
                bottomSheetMedia.dismiss()
            })

            bottomSheetMedia.setOnGalleryListener(View.OnClickListener {
                openGallery()
                bottomSheetMedia.dismiss()

            })

            bottomSheetMedia.setOnCancelListener(View.OnClickListener {
                bottomSheetMedia.dismiss()
            })
            bottomSheetMedia.show((this).supportFragmentManager, "camera")
        }
    }

    private fun openGallery() {
        mediaType = UtilityConstants.GALLERY
        utility = Utility(this, mediaType)
        utility.selectImage()
    }

    private fun openCamera() {
        mediaType = UtilityConstants.CAMERA
        utility = Utility(this, mediaType)
        utility.selectImage()
    }

    override fun passImageURI(file: File?, photoURI: Uri?) {
        getUploadImageApiCall(getAuthAccessToken(this), file)
    }


    fun getUploadImageApiCall(authorization: String, file: File?) {

        val map = HashMap<String, RequestBody>()
        map["access_token"] = RequestBody.create(MediaType.parse("text/plain"), authorization)
        if (file != null && file.exists()) {
            val body = RequestBody.create(MediaType.parse("image/jpeg"), file)
            map["file_upload\"; filename=\"picture.png\" "] = body
        }

        showLoader(true)
        RestClient.get().uploadImageChat(map)
                .enqueue(object : Callback<ApiResponse<ChatMessageListing>> {
                    override fun onFailure(call: Call<ApiResponse<ChatMessageListing>>?, t: Throwable?) {
                        showLoader(false)
                        apiFailure()
                    }

                    override fun onResponse(call: Call<ApiResponse<ChatMessageListing>>?, response: Response<ApiResponse<ChatMessageListing>>?) {
                        showLoader(false)
                        if (response?.isSuccessful == true) {
                            if (response.body()?.statusCode == 200) {
                                var originalImage = BASE_URL_LOAD_IMAGE + response.body()?.result?.original
                                var thumbnail = BASE_URL_LOAD_IMAGE + response.body()?.result?.thumbnail

                                val message = ChatMessageListing("", getUniqueId(), otherUserId, userId, "", thumbnail, getCurrentDateTime(), ChatType.IMAGE, false, originalImage, otherUserName, otherUserPicture, otherUserId, false)
                                sendMessage(message)
                                //chatMessages.add(message)
                                //adapter.notifyItemInserted(chatMessages.size - 1)
                                //  recyclerView.scrollToPosition(chatMessages.size - 1)
                            } else {
                                handleApiError(response.body()?.statusCode, response.body()?.msg)
                            }
                        } else {
                            val errorModel = getApiError(response?.errorBody()?.string())
                            handleApiError(errorModel.statusCode, errorModel.msg)
                        }
                    }
                })
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
    }

    override fun onPermissionsDenied(requestCode: Int, perms: MutableList<String>) {
        Log.d("TAG", "onPermissionsDenied:" + requestCode + ":" + perms.size)
        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            openSettingDialog(this)
        }
    }

    @SuppressLint("MissingPermission")
    override fun onPermissionsGranted(requestCode: Int, perms: MutableList<String>) {
        showMediaOption()
    }


    @SuppressLint("MissingPermission")
    private fun checkPermissionStatus(): Boolean {
        return if (EasyPermissions.hasPermissions(this, *permission)) {
            true
        } else {
            EasyPermissions.requestPermissions(
                    PermissionRequest.Builder(this as AppCompatActivity, permissionCode, *permission)
                            .setRationale(getString(R.string.rationale_permission))
                            .setPositiveButtonText(R.string.ok)
                            .setNegativeButtonText(R.string.cancel)
                            .setTheme(R.style.AlertDialogWhiteBGTheme)
                            .build())
            false
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && data != null)
            utility.onActivityResult(requestCode, resultCode, data)
    }

    private val broadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            if (ConnectivityManager.CONNECTIVITY_ACTION == intent.action) {
                if (CheckNetworkConnection.isOnline(context as Activity)) {

                } else {
                    CheckNetworkConnection.showNetworkError(rootView)
                }
            }
        }
    }

    fun convertDateTimeInMillis(givenDateString: String): Long {
        var timeInMilliseconds: Long = 0
        var sdf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        try {
            var mDate = sdf.parse(givenDateString)
            timeInMilliseconds = mDate.time
            System.out.println("Date in milli :: " + timeInMilliseconds)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return timeInMilliseconds
    }

    fun getCurrentDateTime(): String {
        val sdf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        return sdf.format(Date())
    }

}
