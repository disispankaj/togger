package com.buraq24.customer.ui.home.orderdetails.heavyloads

import android.content.Context
import android.net.Uri
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.buraq24.customer.R
import kotlinx.android.synthetic.main.item_add_images.view.*

class AddImageAdapter(private val imageList: ArrayList<String>?) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val TYPE_ITEM = 1
    private val TYPE_HEADER = 2

    lateinit var context: Context

    private var mCallback: AddImageCallback? = null

    fun settingCallback(callback: AddImageCallback) {
        mCallback = callback
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        context = parent.context
        return if (viewType == TYPE_HEADER) {
            HeaderHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_add_image_header, parent, false))
        } else {
            ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_add_images, parent, false))
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (position == 0) {
            TYPE_HEADER
        } else {
            TYPE_ITEM
        }

    }


    override fun getItemCount(): Int {
        return imageList?.size!!+1
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        if (holder is ViewHolder) {

            holder.onBind(imageList?.get(position-1))

        }
    }


    interface AddImageCallback {
        fun addImageHeader()
    }

    inner class ViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {

        init {
            itemView?.ivCancel?.setOnClickListener {
                itemView.ivCancel?.isEnabled = false
                imageList?.removeAt(adapterPosition-1)
                notifyItemRemoved(adapterPosition-1)
                notifyItemRangeChanged(adapterPosition-1, (imageList?.size ?: 0) - 1)
                itemView.ivCancel?.isEnabled = false
            }
        }

        fun onBind(imagePath: String?) = with(itemView) {
            ivImage.setImageURI(Uri.parse(imagePath))
        }
    }


    inner class HeaderHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {

        init {
            itemView?.ivImage?.setOnClickListener {
                mCallback?.addImageHeader()
            }
        }

    }

}