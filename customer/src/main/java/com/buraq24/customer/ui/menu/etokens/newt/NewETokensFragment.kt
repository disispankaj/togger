package com.buraq24.customer.ui.menu.etokens.newt


import android.app.Activity
import android.content.Intent
import android.location.Address
import android.location.Location
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.buraq24.customer.R
import com.buraq24.showSWWerror
import com.buraq24.showSnack
import com.buraq24.customer.ui.home.orderdetails.supplies.EnterOrderDetailsAdapter
import com.buraq24.customer.ui.menu.etokens.GetAddressActivity
import com.buraq24.customer.ui.menu.etokens.buy.BuyETokenActivity
import com.buraq24.customer.utils.AppUtils
import com.buraq24.customer.webservices.models.Company
import com.buraq24.customer.webservices.models.homeapi.Category
import com.buraq24.utilities.*
import com.buraq24.utilities.constants.*
import com.buraq24.utilities.location.LocationProvider
import com.google.android.gms.tasks.OnSuccessListener
import kotlinx.android.synthetic.main.fragment_new_etokens.*

/**
 * Shows the E-Tokens offers available near the selected location to buy mineral water.
 */
class NewETokensFragment : Fragment(), NewETokenContract.View, EnterOrderDetailsAdapter.ItemSelectedListener, LocationProvider.OnAddressListener, NewETokensAdapter.OnCompanySelectedListener {

    private var REQ_CODE_ADDRESS = 400

    private var selectedPosition = 0

    private var categoriesList: ArrayList<Category>? = ArrayList()

    private var companiesList: ArrayList<Company>? = ArrayList()

    private var presenter = NewEtokenPresenter()

    private var locationProvider: LocationProvider? = null

    private var location: Location? = null

    private var dialogIndeterminate: DialogIndeterminate? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_new_etokens, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.attachView(this)
        dialogIndeterminate = DialogIndeterminate(activity)
        rvCompanies?.layoutManager = LinearLayoutManager(activity)
        val adapterNewETokens = NewETokensAdapter(companiesList)
        rvCompanies.adapter = adapterNewETokens
        adapterNewETokens.setOnItemClickedListener(this)
        rvBrands?.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL,
                false)
        val adapterOrderDetails = EnterOrderDetailsAdapter(activity, categoriesList, selectedPosition)
        rvBrands.adapter = adapterOrderDetails
        adapterOrderDetails.setItemSelectedListener(this)
        locationProvider = LocationProvider.CurrentLocationBuilder(activity, this).build()
        locationProvider?.getLastKnownLocation(OnSuccessListener {
            location = it
            locationProvider?.getAddressFromLatLng(location?.latitude ?: 0.0, location?.longitude
                    ?: 0.0, this)
            if (CheckNetworkConnection.isOnline(activity)) {
                presenter.getBrandsApiCall(CategoryId.MINERAL_WATER.toString())
            } else {
                CheckNetworkConnection.showNetworkError(rootView)
            }
        })
        setListeners()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQ_CODE_ADDRESS && resultCode == Activity.RESULT_OK && data != null) {
            location?.latitude = data.getDoubleExtra(Constants.LATITUDE, 0.0)
            location?.longitude = data.getDoubleExtra(Constants.LONGITUDE, 0.0)
            tvDropOffLocation.text = data.getStringExtra(Constants.ADDRESS)
            getCompaniesListApiCall()
        }
    }

    override fun getAddress(address: String, result: List<Address>) {
        tvDropOffLocation.text = address
    }

    private fun setListeners() {
        tvDropOffLocation.setOnClickListener {
            val address = tvDropOffLocation.text.toString().trim()
            val intent = Intent(activity, GetAddressActivity::class.java)
            if (address.isNotEmpty()) {
                intent.putExtra(Constants.LATITUDE, location?.latitude)
                intent.putExtra(Constants.LONGITUDE, location?.longitude)
                intent.putExtra(Constants.ADDRESS, address)
            }
            startActivityForResult(intent, REQ_CODE_ADDRESS)
        }
    }

    /* Callback on selecting the brand of the listed mineral water brands */
    override fun onCompanySelected(position: Int) {
        val intent = Intent(activity, BuyETokenActivity::class.java)
        intent.putExtra(ORGANISATION_ID, companiesList?.get(position)?.organisation_id)
        intent.putExtra(CATEGORY_BRAND_ID, categoriesList?.get(selectedPosition)?.category_brand_id)
        intent.putExtra(CATEGORY_IMAGE, categoriesList?.get(selectedPosition)?.image_url)
        intent.putExtra(DAYS_TO_DELIVER, AppUtils.getServiceDaysString(companiesList?.get(position)))
        intent.putExtra(BRAND_NAME, categoriesList?.get(selectedPosition)?.name)
        intent.putExtra(Constants.LATITUDE, location?.latitude)
        intent.putExtra(Constants.LONGITUDE, location?.longitude)
        intent.putExtra(Constants.ADDRESS, tvDropOffLocation.text.toString().trim())
        activity?.startActivityForResult(intent, BuyETokenActivity.RQ_BUY_ETOKEN)
    }

    override fun onItemSelected(position: Int) {
        selectedPosition = position
        getCompaniesListApiCall()
    }

    private fun getCompaniesListApiCall() {
        if (CheckNetworkConnection.isOnline(activity)) {
            val map = HashMap<String, String>()
            map["type"] = "New"
            map["skip"] = "0"
            map["take"] = "1000"
            map["category_brand_id"] = categoriesList?.get(selectedPosition)?.category_brand_id.toString()
            map["latitude"] = location?.latitude.toString()
            map["longitude"] = location?.longitude.toString()
            presenter.getCompaniesApiCall(map)
        } else {
            CheckNetworkConnection.showNetworkError(rootView)
        }
    }

    /* Api success callback for get brand list api for mineral water brands*/
    override fun onBrandsApiSuccess(brandsList: ArrayList<Category>?) {
        categoriesList?.clear()
        brandsList?.let { categoriesList?.addAll(it) }
        rvBrands?.adapter?.notifyDataSetChanged()
        getCompaniesListApiCall()
    }

    /* Api success callback for get companies listing api for mineral water companies*/
    override fun onCompaniesListApiSuccess(companiesList: ArrayList<Company>?) {
        this.companiesList?.clear()
        companiesList?.let { this.companiesList?.addAll(it) }
        if (companiesList?.isEmpty() == true) {
            tvNoData?.visibility = View.VISIBLE
        } else {
            tvNoData?.visibility = View.GONE
        }
        rvCompanies?.adapter?.notifyDataSetChanged()
    }

    override fun showLoader(isLoading: Boolean) {
        dialogIndeterminate?.show(isLoading)
    }

    override fun apiFailure() {
        rootView?.showSWWerror()
    }

    override fun handleApiError(code: Int?, error: String?) {
        rootView?.showSnack(error.toString())
        if (code == StatusCode.UNAUTHORIZED) {
            /* User's token expired. Redirects user to login screen */
            AppUtils.logout(activity as Activity)
        }
    }

}
