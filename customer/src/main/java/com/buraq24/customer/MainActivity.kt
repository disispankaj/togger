package com.buraq24.customer

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.ContentResolver
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.media.AudioAttributes
import android.net.Uri
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.buraq24.customer.ui.home.HomeActivity

import com.buraq24.customer.ui.signup.SignupActivity
import com.buraq24.utilities.LocaleManager
import com.buraq24.utilities.SharedPrefs
import com.buraq24.utilities.constants.ACCESS_TOKEN
import com.buraq24.utilities.constants.PROFILE
import com.buraq24.utilities.location.LocationProvider
import com.buraq24.utilities.webservices.models.AppDetail
import com.google.android.gms.location.LocationAvailability
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationResult
import java.util.*

class MainActivity : AppCompatActivity() {

    private var locationProvider: LocationProvider? = null

    private var handler = Handler()

    private var currentTime = Calendar.getInstance().timeInMillis

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        createNotificationChannel()
        var holdTime = 2000L
        if (!intent.getBooleanExtra("hold", true)) {
            handler.postDelayed(runnable, 0)
        } else {
            locationProvider = LocationProvider.LocationUpdatesBuilder(this).apply {
                interval = 1000
                fastestInterval = 1000
            }.build()
            locationProvider?.startLocationUpdates(locationUpdatesListener)
            handler.postDelayed(runnable, 6000)
        }
    }

    private fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val mNotificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            val mChannel = NotificationChannel("1", getString(R.string.requests), NotificationManager.IMPORTANCE_HIGH)
            mChannel.setShowBadge(true)
            mChannel.lightColor = Color.GRAY
            mChannel.enableLights(true)
            mChannel.enableVibration(true)
            mChannel.description = getString(R.string.request_channel_description)
            val attributes = AudioAttributes.Builder()
                    .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                    .setFlags(AudioAttributes.FLAG_AUDIBILITY_ENFORCED)
                    .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                    .build()
            val sound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + applicationContext.packageName + "/" + R.raw.custom_tone)
            mChannel.setSound(sound, attributes)
            mNotificationManager.createNotificationChannel(mChannel)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        handler.removeCallbacks(runnable)
    }

    private val runnable = Runnable {
        val proifle = SharedPrefs.with(this@MainActivity).getObject(PROFILE, AppDetail::class.java)
        if (proifle != null) {
            ACCESS_TOKEN = proifle.access_token ?: ""
            startActivity(Intent(this@MainActivity, HomeActivity::class.java))
        } else {
            startActivity(Intent(this@MainActivity, SignupActivity::class.java))
        }
        finishAffinity()
    }

    private val locationUpdatesListener = object : LocationCallback() {
        override fun onLocationResult(p0: LocationResult?) {
            super.onLocationResult(p0)
            val timeDifference = Calendar.getInstance().timeInMillis + 2000 - currentTime
            handler.postDelayed(runnable, if (timeDifference > 0) timeDifference else 0)
            locationProvider?.stopLocationUpdates(this)
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        locationProvider?.onActivityResult(requestCode, resultCode, data)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        locationProvider?.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(LocaleManager.setLocale(newBase))
    }

}
