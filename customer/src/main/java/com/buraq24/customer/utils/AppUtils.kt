package com.buraq24.customer.utils

import android.app.Activity
import android.content.Intent
import android.widget.Toast
import com.buraq24.customer.R
import com.buraq24.customer.ui.signup.SignupActivity
import com.buraq24.customer.webservices.models.Company
import com.buraq24.customer.webservices.models.order.Order
import com.buraq24.utilities.AppSocket
import com.buraq24.utilities.PaymentType
import com.buraq24.utilities.SharedPrefs
import java.lang.StringBuilder
import java.text.DateFormatSymbols
import java.util.*

class AppUtils {
    companion object {
        fun logout(activity: Activity?) {
            AppSocket.get().off()
            AppSocket.get().disconnect()
            Toast.makeText(activity, activity?.getString(R.string.session_expired_please_login_again), Toast.LENGTH_LONG).show()
            SharedPrefs.with(activity).removeAllSharedPrefsChangeListeners()
            SharedPrefs.with(activity).removeAll()
            activity?.finishAffinity()
            activity?.startActivity(Intent(activity, SignupActivity::class.java))
        }

        fun getCategoryIcon(categoryId: Int?) {

        }

        fun getFinalCharge(orderDetail: Order?): String {

            val payment = orderDetail?.payment
            val price = (payment?.product_quantity ?: 1)
                    .times(payment?.product_per_quantity_charge?.toDouble() ?: 1.00)
                    .plus((payment?.product_per_distance_charge?.toDouble()
                            ?: 0.00).times(payment?.order_distance?.toDouble() ?: 0.00))
                    .plus((payment?.product_per_weight_charge?.toDouble()
                            ?: 0.00).times(payment?.product_weight?.toDouble() ?: 0.0))
                    .plus(payment?.product_alpha_charge?.toDouble() ?: 0.00)
            when (orderDetail?.category_id) {
                1 -> {

                }

                2 -> {

                }

                3 -> {

                }
            }
            return String.format("%.2f", price)
        }

        fun getPaymentStringId(paymentType: String): Int {
            return when (paymentType) {
                PaymentType.CARD -> R.string.card
                PaymentType.CASH -> R.string.cash
                PaymentType.E_TOKEN -> R.string.e_token
                else -> R.string.card
            }
        }

        fun getServiceDaysString(company: Company?): String {
            val shortDays = DateFormatSymbols.getInstance(Locale.getDefault()).shortWeekdays
            val servingDays = StringBuilder("")
            if (company?.sunday_service == "1") servingDays.append(shortDays[1] + ", ")
            if (company?.monday_service == "1") servingDays.append(shortDays[2] + ", ")
            if (company?.tuesday_service == "1") servingDays.append(shortDays[3] + ", ")
            if (company?.wednesday_service == "1") servingDays.append(shortDays[4] + ", ")
            if (company?.thursday_service == "1") servingDays.append(shortDays[5] + ", ")
            if (company?.friday_service == "1") servingDays.append(shortDays[6] + ", ")
            if (company?.saturday_service == "1") servingDays.append(shortDays[7] + ", ")

            return servingDays.removeSuffix(", ").toString()
        }
    }

}