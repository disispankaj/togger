package com.buraq24.customer.utils

import java.text.SimpleDateFormat
import java.util.*

val utcFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", getLocale())

fun getLocale(): Locale {
        return Locale.ENGLISH }

fun dateFormatFromUTC(formatUTC: String, createdDate: String): String {

    utcFormat.timeZone = TimeZone.getTimeZone("Etc/UTC")

    val fmt = SimpleDateFormat(formatUTC, getLocale())
    return fmt.format(utcFormat.parse(createdDate))
}

fun getUtcTimeInMilliSec(timeInMillis: Long): String {
    var sdf = SimpleDateFormat("yyyy-MM-dd ")
    utcFormat.timeZone = TimeZone.getTimeZone("Etc/UTC")
    return sdf.format(Date(timeInMillis))
}