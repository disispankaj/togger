package com.buraq24.utilities.chatModel

data class ProfilePicUrl (val original: String?, val thumbnail: String?)