package com.buraq24.utilities.chatModel



data class ChatMessageList(
        var chatListing: ArrayList<ChatMessageListing>?,
        var chatCount: Int?
)