package com.buraq24.utilities.chatModel

import com.buraq24.utilities.MediaUploadStatus
import java.io.File

data class MediaUpload(
        var mediaUploadStatus: String = MediaUploadStatus.UPLOADED,
        var transferId: Int? = -1,
        var file: File?
)