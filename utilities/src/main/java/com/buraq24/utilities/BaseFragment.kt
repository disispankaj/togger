package com.buraq24.utilities

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModel
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.View
import android.content.Intent
import android.content.BroadcastReceiver
import android.content.Context
import android.net.ConnectivityManager
import android.content.IntentFilter


abstract class BaseFragment : Fragment() {

    abstract fun onNetworkConnected()

    abstract fun onNetworkDisconnected()

    private val networkChangeReceiver = NetworkChangeReceiver()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val intentFilter = IntentFilter()
        intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION)
        activity?.registerReceiver(networkChangeReceiver, intentFilter)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        activity?.unregisterReceiver(networkChangeReceiver)
    }

    inner class NetworkChangeReceiver : BroadcastReceiver() {

        override fun onReceive(context: Context, intent: Intent) {
            if (CheckNetworkConnection.isOnline(context)) {
                onNetworkConnected()
            } else {
                onNetworkDisconnected()
            }

        }
    }

}