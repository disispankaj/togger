package com.buraq24.utilities

import android.content.Context
import com.buraq24.utilities.constants.ACCESS_TOKEN_KEY
import java.util.*

const val AUTHORISATION = "authorization"
const val MESSAGE_ID = "messageId"
const val RECEIVER_ID = "receiver_id"
const val USER_ID = "user_id"
const val LIMIT = "limit"
const val SKIP = "skip"
const val MESSAGE_ORDER = "messageOrder"
const val LAST_MESSAGE = "last_message"
const val PROFILE_PIC_URL = "profilePicURL"
const val USER_NAME = "userName"
const val USER_TYPE = "userType"
const val CHAT_ID = "chat_id"

object Constants {
    val CATEGORY = "category"
    val LATITUDE = "latitude"
    val LONGITUDE = "longitude"
    val ADDRESS = "address"
    val DEFAULT_REASON_CUSTOMER_CANCELLED = "CANCELLING_WHILE_REQUESTING"
    val PAGE_LIMIT = 12000
    val FOR_ADD_CARD = 200

}

object Stripe{
    val TEST_CLIENT_ID = "ca_FlalFyxg6EXdIrjAJzuggpi8WGOlMhH1"    // ca_FlalFyxg6EXdIrjAJzuggpi8WGOlMhH1
    val TEST_SECRET_KEY = "sk_test_5P5fBvUwN6jRhrSg47ynhA6r001zGXrHWy"
    val TEST_PUBLISH_KEY ="pk_test_cvtwk93fv9cSHXXls2URwXFw00s0RTTBCX"

    val LIVE_CLIENT_ID = "ca_FlalJwLJsjAM8jCzkprKCE2RLP37Q7IO"
    val LIVE_SECRET_KEY = "sk_live_nv5MH0GthI2nDtj72cVwqH3q00uNkeUobo"
    val LIVE_PUBLISH_KEY ="pk_live_Xt4jGycFut96UDBDhSwWc6Yk00UIfWTQAb"

//    val STRIPE_CONNECT_URL ="https://connect.stripe.com/oauth/authorize?response_type=code&client_id=${TEST_CLIENT_ID}&scope=read_write"
    val STRIPE_CONNECT_URL ="https://connect.stripe.com/express/oauth/authorize?client_id=${TEST_CLIENT_ID}&state=%7BSTATE_VALUE%7D#/"
}

object StatusCode {
    val SUCCESS = 200
    val BAD_REQUEST = 400
    val UNAUTHORIZED = 401
    val SERVER_ERROR = 500
    val GET_NEW_CARD = 2
}

object CategoryId {
    val GAS = 1
    val MINERAL_WATER = 2
    val WATER_TANKER = 3
    val FREIGHT = 4
    val TOW = 5
    val FREIGHT_2 = 7
}

object LanguageIds {
    val ENGLISH = 1
    val HINDI = 2
    val URDU = 3
    val CHINESE = 4
    val ARABIC = 5
}

object PaymentType {
    val CASH = "Cash"
    val CARD = "Card"
    val E_TOKEN = "eToken"
}

object OrderStatus {
    val SEARCHING = "Searching"
    val ONGOING = "Ongoing"
    val CONFIRMED = "Confirmed"
    val REACHED = "Onreached"
    val CUSTOMER_CANCEL = "CustCancel"
    val SERVICE_COMPLETE = "SerComplete"
    val SERVICE_INVOICED = "SerInvoiced"
    val SERVICE_TIMEOUT = "SerTimeout"
    val SERVICE_REJECT = "SerReject"
    val DRIVER_CANCELLED = "DriverCancel"
    val SCHEDULED = "Scheduled"
    val DRIVER_PENDING = "DPending"
    val DRIVER_APPROVED = "DApproved"
    val DRIVER_SCHEDULED_CANCELLED = "DSchCancelled"
    val DRIVER_ARROVED = "DApproved"
    val DRIVER_SCHEDULE_SERVICE = "DPending"
    val DRIVER_START = "About2Start"


    val DRIVER_SCHEDULED_TIMEOUT = "DSchTimeout"
    val SYS_SCHEDULED_CANCELLED = "SysSchCancelled"

    val E_TOKEN_START = "eTokenSerStart"                         //Driver Starts a etoken Ride for customer(Customer) No status only event
    val CUSTOMER_CONFIRMATION_PENDING_ETOKEN = "SerCustPending" //Waiting for customer confirmation(Customer)
    val CUSTOMER_CANCELLED_ETOKEN = "SerCustCancel"            //Customer Cancels etoken order(Driver)
    val CUSTOMER_CONFIRM_ETOKEN = "SerCustConfirm"            //Customer Confirm etoken order(Driver)
    val C_TIMEOUT_ETOKEN = "CTimeout"                        //Customer Do not confirm or rejects in 2 minutes(Driver)


}

object CommonEventTypes {
    val UPDATE_DATA = "UpdateData"
    val HOME_MAP_DRIVERS = "CustHomeMap"
}

object OrderEventType {
    //    User
    val DRIVER_RATED_SERVICE = "DriverRatedService"
    val SERVICE_ACCEPT = "SerAccept"
    val SERVICE_REACHED = "OnReached"
    val SERVICE_REJECT = "SerReject"
    val SERVICE_COMPLETE = "SerComplete"
    val SERVICE_TIMEOUT = "SerTimeout"
    val CURRENT_ORDERS = "CurrentOrders"
    val CUSTOMER_SINGLE_ORDER = "CustSingleOrder"
    val DRIVER_CANCELLED = "DriverCancel"

    //   Driver
    val SERVICE_REQUEST = "SerRequest"
    val SERVICE_CANCEL = "SerCancel"
    val SERVICE_OTHER_ACCEPT = "SerOAccept"
    val CUSTOMER_RATED_SERVICE = "CustRatedService"
    val DRIVER_SCHEDULE_SERVICE = "DPending"
    val DRIVER_TIME_OUT = "DSchTimeout"
    val DRIVER_ARROVED = "DApproved"
    val DRIVER_START = "About2Start"
    val SYSTEM_CANCELLED = "SysSchCancelled"
    val ONGOING = "Ongoing"
    //types
    val CUSTOMER_CANCEL = "CustCancel"
    val SCHEDULED = "Scheduled"
    val DRIVER_SCHEDULED_CANCELLED = "DSchCancelled"

    // e-tokens
    val E_TOKEN_START = "eTokenSerStart"                         //Driver Starts a etoken Ride for customer(Customer) No status only event
    val CUSTOMER_CONFIRMATION_PENDING_ETOKEN = "SerCustPending" //Waiting for customer confirmation(Customer)
    val CUSTOMER_CANCELLED_ETOKEN = "SerCustCancel"            //Customer Cancels etoken order(Driver)
    val CUSTOMER_CONFIRM_ETOKEN = "SerCustConfirm"            //Customer Confirm etoken order(Driver)
    val C_TIMEOUT_ETOKEN = "CTimeout"                        //Customer Do not confirm or rejects in 2 minutes(Driver)

}

object Events {
    val ORDER_EVENT = "OrderEvent"
    val COMMON_EVENT = "CommonEvent"
}

object Broadcast {
    val NOTIFICATION = "notification"
}

class ChatType {
    companion object {
        const val TEXT = "text"
        const val IMAGE = "image"
    }
}

class MediaUploadStatus {
    companion object {
        const val NOT_UPLOADED = "not_uploaded"
        const val UPLOADING = "uploading"
        const val CANCELED = "canceled"
        const val UPLOADED = "unloaded"
    }
}

class MessageOrder {
    companion object {
        const val BEFORE = "BEFORE"
        const val AFTER = "AFTER"
    }
}

class UtilityConstants {
    companion object {
        const val GALLERY = "GALLERY"
        const val VIDEO = "VIDEO"
        const val CAMERA = "CAMERA"
        const val ACTION_TAKE_VIDEO = 1
        const val PATH = "/capcrop"
        const val IMAGENAME = "imageName"
        const val PHOTO_REQUEST_CAMERA = 2
        const val PHOTO_REQUEST_GALLERY = 3
        const val PHOTO_REQUEST_CROP = 4
        const val IMAGES_PREFIX = "IMG_"
        const val IMAGES_SUFFIX = ".jpg"

    }


}

object Day {
    val SUN = when (Locale.getDefault().displayLanguage) {
        Language.eng -> "Sun"
        Language.english -> "Sun"
        else -> "dom."

    }
    val MON = when (Locale.getDefault().displayLanguage) {
        Language.eng -> "Mon"
        Language.english -> "Mon"
        else -> "lun."

    }
    val TUE = when (Locale.getDefault().displayLanguage) {
        Language.eng -> "Tue"
        Language.english -> "Tue"
        else -> "mar."

    }
    val WED = when (Locale.getDefault().displayLanguage) {
        Language.eng -> "Wed"
        Language.english -> "Wed"
        else -> "mié."

    }
    val THU = when (Locale.getDefault().displayLanguage) {
        Language.eng -> "Thu"
        Language.english -> "Thu"
        else -> "jue."

    }
    val FRI = when (Locale.getDefault().displayLanguage) {
        Language.eng -> "Fri"
        Language.english -> "Fri"
        else -> "vie."

    }
    val SAT = when (Locale.getDefault().displayLanguage) {
        Language.eng -> "Sat"
        Language.english -> "Sat"
        else -> "sáb."

    }


    object Language {
        val english ="English"
        val eng = "en"
        val spa = "es"
    }
}