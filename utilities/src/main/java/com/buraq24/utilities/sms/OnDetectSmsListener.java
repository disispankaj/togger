package com.buraq24.utilities.sms;

public interface OnDetectSmsListener {
    void onSmsDetected(String message);
} 