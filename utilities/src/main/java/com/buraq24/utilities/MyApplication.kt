package com.buraq24.utilities

import android.app.Application
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.ContentResolver
import android.content.Context
import android.content.res.Configuration
import android.os.Build
import android.support.multidex.MultiDexApplication
import android.support.v7.app.AppCompatDelegate
import android.util.Log
import com.crashlytics.android.Crashlytics
import io.fabric.sdk.android.Fabric
import android.content.ContentResolver.SCHEME_ANDROID_RESOURCE
import android.net.Uri


class MyApplication : MultiDexApplication() {

    private val tag = "B_APP"

    override fun onCreate() {
        super.onCreate()
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
        SharedPrefs.with(applicationContext)
        Fabric.with(this, Crashlytics())

    }

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(LocaleManager.setLocale(base))
        Logger.e(tag, "attachBaseContext")
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        LocaleManager.setLocale(this)
        Logger.e(tag, "onConfigurationChanged: " + newConfig.locale.language)
    }
}