package com.buraq24.utilities.basearc

interface BasePresenter<in V : BaseView>
{
    fun attachView(view: V)
    fun detachView()
}