package com.buraq24.utilities.webservices.models

data class CurrentLocationModel(val latitude: Double?, val longitude: Double?, val address: String?)