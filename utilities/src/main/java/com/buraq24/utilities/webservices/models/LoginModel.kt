package com.buraq24.utilities.webservices.models

data class LoginModel(
        var AppDetail: AppDetail?,
        var services: List<Service>?,
        var supports: List<Support>?
)