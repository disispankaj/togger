package com.buraq24.utilities.webservices.models

data class ApiErrorModel(val statusCode: Int?, val msg: String?, val success: Int?)