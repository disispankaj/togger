package com.buraq24.utilities.webservices.models

data class Brand(
        var category_brand_id: Int?,
        var sort_order: Int?,
        var category_id: Int?,
        var name: String?,
        var image: String?,
        var image_url: String?
){
    override fun toString(): String {
        return name?:""
    }
}