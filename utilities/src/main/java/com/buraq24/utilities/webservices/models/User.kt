package com.buraq24.utilities.webservices.models

data class User(
        var user_id: Int?,
        var organisation_id: Int?,
        var stripe_customer_id: String?,
        var stripe_connect_id: String?,
        var name: String?,
        var email: String?,
        var phone_code: String?,
        var profile_pic_url: String?,
        var rating_avg: String?,
        var rating_count: Int?,
        var phone_number: Long?,
        var stripe_connect_token: String?,
        var date_of_birth : String?
)