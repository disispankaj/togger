package com.buraq24.utilities.webservices

import com.buraq24.getLanguageId
import com.buraq24.utilities.SharedPrefs
import com.buraq24.utilities.constants.ACCESS_TOKEN_KEY
import com.buraq24.utilities.LanguageIds
import com.buraq24.utilities.LocaleManager
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*
import java.util.concurrent.TimeUnit

//const val BASE_URL = "http://buraq24.com:9006"
//const val BASE_URL = "http://buraq24.com:9005"
//const val BASE_URL = "http://192.168.100.182:9005/"  // local
//const val BASE_URL = "http://demo.netsolutionindia.com:9007"  // live
const val BASE_URL = "http://demo.netsolutionindia.com:9008"  // live
const val BASE_URL_LOAD_IMAGE = "http://demo.netsolutionindia.com:9008/images/"
const val BASE_IMAGE_URL = "http://demo.netsolutionindia.com/Resize/"

open class BaseRestClient {



    fun createClient(): Retrofit {

        val httpLoggingInterceptor = HttpLoggingInterceptor()
        httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

        val okHttpClient = OkHttpClient
                .Builder()
                .writeTimeout(120, TimeUnit.SECONDS)
                .readTimeout(120, TimeUnit.SECONDS)
                .connectTimeout(120, TimeUnit.SECONDS)
                .addInterceptor(httpLoggingInterceptor)
                .addInterceptor { chain ->
                    val request = chain
                            .request()
                            .newBuilder()
                            .addHeader("language_id", getLanguageId(Locale.getDefault().language).toString())
                            .addHeader("access_token", SharedPrefs.get().getString(ACCESS_TOKEN_KEY, ""))
                            .build()
                    chain.proceed(request)
                }.build()

        return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build()
    }

}
