package com.buraq24.utilities.webservices.models

data class Service(
        var category_id: Int?,
        var category_type: String?,
        var default_brands: String?,
        var name: String?,
        var image_url: String?,
        var description: String?,
        var buraq_percentage: Float?,
        var brands: List<Brand>
){
    override fun toString(): String {
        return name?:""
    }
}