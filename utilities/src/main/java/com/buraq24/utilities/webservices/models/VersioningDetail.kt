package com.buraq24.utilities.webservices.models

data class VersioningDetail(
        var force: Int,
        var normal: Int
)