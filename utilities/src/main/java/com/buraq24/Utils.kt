package com.buraq24

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.res.Resources
import android.graphics.Color
import android.net.Uri
import android.provider.Settings
import android.support.annotation.IdRes
import android.support.annotation.StringRes
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v7.app.AlertDialog
import android.util.DisplayMetrics
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import com.bumptech.glide.request.RequestOptions
import com.buraq24.utilities.*
import com.buraq24.utilities.constants.ACCESS_TOKEN_KEY
import com.buraq24.utilities.webservices.models.ApiErrorModel
import com.google.gson.Gson
import java.io.File
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*


object Utils {
    fun pxToDp(context: Context, px: Int): Int {
        val displayMetrics = context.resources.displayMetrics
        return Math.round(px / (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT))
    }

    fun dpToPx(dp: Int): Float {
        return (dp * Resources.getSystem().displayMetrics.density)
    }

    fun replaceFragment(fragmentManager: FragmentManager, fragment: Fragment, @IdRes containerId: Int, tag: String) {
        fragmentManager.beginTransaction().setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left,
                R.anim.enter_from_left, R.anim.exit_to_right).replace(containerId, fragment, tag)
                .addToBackStack("backstack").commit()
    }

    fun replaceFragmentNoBackStack(fragmentManager: FragmentManager, fragment: Fragment, @IdRes containerId: Int, tag: String) {
        fragmentManager.beginTransaction().setCustomAnimations(R.anim.slide_in_top,
                R.anim.slide_in_bottom, R.anim.slide_out_bottom, R.anim.slide_out_top).replace(containerId, fragment, tag)
                .addToBackStack("backstack").commit()
    }

    fun addFragment(fragmentManager: FragmentManager, fragment: Fragment, @IdRes containerId: Int, tag: String) {
        fragmentManager.beginTransaction().setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left,
                R.anim.enter_from_left, R.anim.exit_to_right).add(containerId, fragment, tag)
                .addToBackStack("backstack").commit()
    }

    fun getScreenWidth(activity: Activity): Int {
        val displayMetrics = DisplayMetrics()
        activity.windowManager.defaultDisplay.getMetrics(displayMetrics)
        return displayMetrics.widthPixels
    }

    fun getScreenHeight(activity: Activity): Int {
        val displayMetrics = DisplayMetrics()
        activity.windowManager.defaultDisplay.getMetrics(displayMetrics)
        return displayMetrics.heightPixels
    }

    fun hideKeyboard(activity: Activity) {
        val imm = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        //Find the currently focused view, so we can grab the correct window token from it.
        var view = activity.currentFocus
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = View(activity)
        }
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

    fun View.showSnack(msg: String) {

        try {
            val snackBar = Snackbar.make(this, msg, Snackbar.LENGTH_LONG)
            val snackBarView = snackBar.view
            val textView = snackBarView.findViewById<View>(android.support.design.R.id.snackbar_text) as TextView
            textView.maxLines = 3
            snackBar.setAction(R.string.okay) { snackBar.dismiss() }
            snackBarView.setBackgroundColor(Color.parseColor("#27242b"))
            snackBar.setActionTextColor(Color.parseColor("#0078FF"))
            snackBar.show()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

}

fun ImageView.setRoundImageUrl(url: String?) {
    val requestOptions = RequestOptions()
            .fitCenter()
            .placeholder(R.drawable.ic_wedding)
            .error(R.drawable.ic_wedding)
            .transform(CircleCrop())
    this.context?.applicationContext?.let { Glide.with(it).load(url).apply(requestOptions).into(this) }
}

fun ImageView.setRoundProfileUrl(url: String?) {
    val requestOptions = RequestOptions()
            .fitCenter()
            .transform(CircleCrop())
            .placeholder(R.drawable.profile_pic_placeholder)
    this.context?.applicationContext?.let { Glide.with(it).load(url).apply(requestOptions).into(this) }
}

fun ImageView.setRoundProfilePic(file: File?) {
    val requestOptions = RequestOptions()
            .fitCenter()
            .transform(CircleCrop())
            .placeholder(R.drawable.profile_pic_placeholder)
    this.context?.applicationContext?.let { Glide.with(it).load(file).apply(requestOptions).into(this) }
}

fun ImageView.setImageUrl(url: String?) {
    val requestOptions = RequestOptions()
            .fitCenter()
    this.context?.applicationContext?.let { Glide.with(it).load(url).apply(requestOptions).into(this) }
}

fun getApiError(error: String?): ApiErrorModel {
    return Gson().fromJson(error, ApiErrorModel::class.java)
}


fun View.showSnack(msg: String) {

    try {
        val snackBar = Snackbar.make(this, msg, Snackbar.LENGTH_LONG)
        val snackBarView = snackBar.view
        val textView = snackBarView.findViewById<View>(android.support.design.R.id.snackbar_text) as TextView
        textView.maxLines = 3
        snackBar.setAction(R.string.okay) { snackBar.dismiss() }
        snackBarView.setBackgroundColor(Color.parseColor("#27242b"))
        snackBar.setActionTextColor(Color.parseColor("#0078FF"))
        snackBar.show()
    } catch (e: Exception) {
        e.printStackTrace()
    }
}

fun View.hideKeyboard() {
    val imm = this.context.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.hideSoftInputFromWindow(this.windowToken, 0)
}

fun View.showSnack(@StringRes id: Int) {

    try {
        val snackBar = Snackbar.make(this, id, Snackbar.LENGTH_LONG)
        val snackBarView = snackBar.view
        val textView = snackBarView.findViewById<View>(android.support.design.R.id.snackbar_text) as TextView
        textView.maxLines = 3
        snackBar.setAction(R.string.okay) { snackBar.dismiss() }
        snackBarView.setBackgroundColor(Color.parseColor("#27242b"))
        snackBar.setActionTextColor(Color.parseColor("#0078FF"))
        snackBar.show()
    } catch (e: Exception) {
        e.printStackTrace()
    }
}

fun View.showSWWerror() {
    try {
        val snackBar = Snackbar.make(this, R.string.sww_error, Snackbar.LENGTH_LONG)
        val snackBarView = snackBar.view
        val textView = snackBarView.findViewById<View>(android.support.design.R.id.snackbar_text) as TextView
        textView.maxLines = 3
        snackBar.setAction(R.string.okay) { snackBar.dismiss() }
        snackBarView.setBackgroundColor(Color.parseColor("#27242b"))
        snackBar.setActionTextColor(Color.parseColor("#0078FF"))
        snackBar.show()
    } catch (e: Exception) {
        e.printStackTrace()
    }
}

fun openSettingDialog(context: Activity?) {
    if (context != null) {
        val builder: android.app.AlertDialog.Builder = android.app.AlertDialog.Builder(context, R.style.AlertDialogWhiteBGTheme)
        builder.setMessage(R.string.deniedpermission)
                .setCancelable(false)
                .setPositiveButton(R.string.gotosettings) { _, _ ->
                    val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                    val uri = Uri.fromParts("package", context.packageName, null)
                    intent.data = uri
                    context.startActivityForResult(intent, 0)
                }
                .setNegativeButton("Cancel") { dialog, _ ->
                    dialog.dismiss()
                }
        val alert = builder.create()
        alert.show()
    }
}

fun getAuthAccessToken(context: Context?): String {
    return "bearer " + getAccessToken(context)
}

fun getAccessToken(context: Context?): String {
    return SharedPrefs.with(context).getString(ACCESS_TOKEN_KEY, "")
}

fun getUniqueId(): String {
    return (Calendar.getInstance().timeInMillis.toInt() + Random().nextInt(900) + 100).toString()
}

fun View.showSWWerrorWithError(@StringRes id: Int) {
    try {
        val snackBar = Snackbar.make(this, id, Snackbar.LENGTH_LONG)
        val snackBarView = snackBar.view
        val textView = snackBarView.findViewById<View>(android.support.design.R.id.snackbar_text) as TextView
        textView.maxLines = 3
        snackBar.setAction(R.string.okay) { snackBar.dismiss() }
        snackBarView.setBackgroundColor(Color.parseColor("#27242b"))
        snackBar.setActionTextColor(Color.parseColor("#ffffff"))
        snackBar.show()
    } catch (e: Exception) {
        e.printStackTrace()
    }
}

fun isYesterday(calendar: Calendar): Boolean {
    val tempCal = Calendar.getInstance()
    tempCal.add(Calendar.DAY_OF_MONTH, -1)
    return calendar.get(Calendar.DAY_OF_MONTH) == tempCal.get(Calendar.DAY_OF_MONTH)
}

fun convertDateTimeInMillis(givenDateString: String): Long {
    var timeInMilliseconds: Long = 0
    var sdf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
    try {
        var mDate = sdf.parse(givenDateString)
        timeInMilliseconds = mDate.time
        System.out.println("Date in milli :: " + timeInMilliseconds)
    } catch (e: ParseException) {
        e.printStackTrace()
    }
    return timeInMilliseconds
}

fun existsInWeek(calendar: Calendar): Boolean {
    val tempCal = Calendar.getInstance()
    tempCal.add(Calendar.DAY_OF_MONTH, -7)
    tempCal.set(Calendar.HOUR_OF_DAY, calendar.get(Calendar.HOUR_OF_DAY))
    tempCal.set(Calendar.MINUTE, calendar.get(Calendar.MINUTE))
    tempCal.set(Calendar.SECOND, calendar.get(Calendar.SECOND))
    return calendar.time.after(tempCal.time)
}

fun getFormatFromDate(date: Date, format: String): String? {
    val f = SimpleDateFormat(format, Locale.getDefault())
    return f.format(date)
}

fun getFormatFromDateUtc(date: Date, format: String): String? {
    val f = SimpleDateFormat(format, Locale.US)
    f.timeZone = TimeZone.getTimeZone("UTC")
    return f.format(date)
}

fun getCurentDateStringUtc(): String? {
    val f = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US)
    f.timeZone = TimeZone.getTimeZone("UTC")
    return f.format(Calendar.getInstance(TimeZone.getTimeZone("UTC")).time)
}

fun showForcedUpdateDialog(context: Context, versionForced: String, versionNormal: String, appVersion: String): Boolean {
    val builder = AlertDialog.Builder(context)

    return when {
        versionForced.toFloat() > appVersion.toFloat() -> {
            val dialog = builder.setPositiveButton(context.getString(R.string.update)) { _, _ ->

            }
                    .setCancelable(false)
                    .setTitle(context.getString(R.string.update))
                    .setMessage(context.getString(R.string.you_have_to_update_app))
                    .show()
            dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener {
                openAppUpdate(context)
                //            val wantToCloseDialog = false
//            //Do stuff, possibly set wantToCloseDialog to true then...
//            if (wantToCloseDialog)
//                dialog.dismiss()
//            //else dialog stays open. Make sure you have an obvious way to close the dialog especially if you set cancellable to false.
            }
            true
        }
        versionNormal > appVersion -> {
            builder.setPositiveButton("Update") { _, _ ->
                openAppUpdate(context)
            }
                    .setNegativeButton(android.R.string.cancel, object : DialogInterface.OnClickListener {
                        override fun onClick(dialog: DialogInterface?, which: Int) {
                            // Do nothing
                        }
                    })
                    .setCancelable(true)
                    .setTitle("Update Available")
                    .setMessage("You can update your app to the latest version to access the new features.")
                    .show()
            false
        }
        else -> false
    }
}

fun showForcedUpdateDialogDriver(context: Context, versionForced: String, versionNormal: String, appVersion: String): Dialog? {
    val builder = AlertDialog.Builder(context)
    return when {
        versionForced.toFloat() > appVersion.toFloat() -> builder.setPositiveButton("Update", { _, _ ->
            openAppUpdate(context)
        })
                .setCancelable(false)
                .setTitle("Update alert")
                .setMessage("You must have to update to latest version of the app .")
                .show()
        versionNormal > appVersion -> builder.setPositiveButton("Update", { _, _ ->
            openAppUpdate(context)
        })
                .setNegativeButton(android.R.string.cancel, object : DialogInterface.OnClickListener {
                    override fun onClick(dialog: DialogInterface?, which: Int) {
                        dialog?.dismiss()
                    }
                })
                .setCancelable(true)
                .setTitle("Update Available")
                .setMessage("You can update your app to the latest version to access the new features.")
                .show()
        else -> null
    }
}


fun openAppUpdate(context: Context) {
    val appPackageName = context.packageName // getPackageName() from Context or Activity object
    try {
        context.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)))
    } catch (exc: android.content.ActivityNotFoundException) {
        context.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)))
    }
}

fun getLanguageId(language: String): Int {
    return when (language) {
        LocaleManager.LANGUAGE_ENGLISH -> LanguageIds.ENGLISH
        LocaleManager.LANGUAGE_HINDI -> LanguageIds.HINDI
        LocaleManager.LANGUAGE_URDU -> LanguageIds.URDU
        LocaleManager.LANGUAGE_CHINESE -> LanguageIds.CHINESE
        LocaleManager.LANGUAGE_ARABIC -> LanguageIds.ARABIC
        else -> LanguageIds.ENGLISH
    }
}

fun getLanguage(id: Int): String {
    return when (id) {
        LanguageIds.ENGLISH -> LocaleManager.LANGUAGE_ENGLISH
        LanguageIds.HINDI -> LocaleManager.LANGUAGE_HINDI
        LanguageIds.URDU -> LocaleManager.LANGUAGE_URDU
        LanguageIds.CHINESE -> LocaleManager.LANGUAGE_CHINESE
        LanguageIds.ARABIC -> LocaleManager.LANGUAGE_ARABIC
        else -> LocaleManager.LANGUAGE_ENGLISH
    }
}


fun getVehicleResId(categoryId: Int?): Int {
    return when (categoryId) {
        CategoryId.GAS -> R.drawable.ic_mini_truck_m_gas
        CategoryId.MINERAL_WATER -> R.drawable.ic_mini_truck_water_m
        CategoryId.WATER_TANKER -> R.drawable.ic_truck_water_tank_m
        CategoryId.FREIGHT -> R.drawable.ic_truck_m_freights
        CategoryId.TOW -> R.drawable.ic_mini_truck_m_tow
        CategoryId.FREIGHT_2 -> R.drawable.ic_pin
        else -> R.drawable.ic_mini_truck_m_gas
    }
}




